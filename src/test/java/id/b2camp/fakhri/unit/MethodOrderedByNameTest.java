package id.b2camp.fakhri.unit;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class MethodOrderedByNameTest {

    @Test
    void testC() {

    }

    @Test
    void testB() {

    }

    @Test
    void testD() {

    }

    @Test
    void testA() {

    }
}
