package id.b2camp.fakhri.unit;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test Math operation in Calculator class")
class CalculatorTest {

    Calculator calculator;

    @BeforeAll
    static void setup() {
        System.out.println("Executing before all method");
    }

    @AfterAll
    static void cleanup() {
        System.out.println("Executing after all method");
    }

    @BeforeEach
    void beforeEachMethod() {
        calculator = new Calculator();
        System.out.println("Executing before each method");
    }

    @AfterEach
    void afterTestEachMethod() {
        System.out.println("Executing after each method");
    }

    // test<System Under Test>_<Condition or State Change>_<Expected Result>
    @Test
    @DisplayName("Test 4/2 = 2")
    void testIntegerDivision_WhenFourIsDividedByTwo_ShouldReturnTwo() {
        //arrange - given
        int dividend = 4;
        int divisor = 2;
        int expected = 2;

        //act - when
        int result = calculator.integerDivision(dividend, divisor);

        //assert - then
        assertEquals(expected, result);
    }

    @Test
//    @Disabled("TODO: Still need to figure out some cases")
    @DisplayName("Division by zero")
    void testIntegerDivision_WhenDividendByZero_ShouldThrowArithmeticException() {
        System.out.println("Running division by zero");
        //arrange
        int dividend = 4;
        int divisor = 0;
        String expectedExceptionMessage = "/ by zero";

        //act & assert
        ArithmeticException actualException = assertThrows(ArithmeticException.class, () -> {
            calculator.integerDivision(dividend, divisor);
        });

        //assert
        assertEquals(expectedExceptionMessage, actualException.getLocalizedMessage());
    }

    @ParameterizedTest
    @ValueSource(strings={"Fakhri", "Chaerul", "Insan", ""})
    void valueSourceDemo(String firstName) {
        System.out.println(firstName);
        assertNotNull(firstName);
    }

//    @MethodSource
    @ParameterizedTest
    @DisplayName("Test csv int minuend, subtrahend, expectedResult")
//    @CsvSource({
//            "33, 1, 32",
//            "54, 1, 53",
//            "24, 1, 23"
//    })
//    @CsvSource({
//            "apple, orange, banana",
//            "pineapple, ,",
//            "mango,"
//    })
    @CsvFileSource(resources = "/integer-division.csv")
    void integerSubtraction(int minuend, int subtrahend, int expected) {

        int result = calculator.integerSubtraction(minuend, subtrahend);

        assertEquals(expected, result, minuend + " - " + subtrahend + " did not produce " + expected);
    }

//    public static Stream<Arguments> integerSubtraction() {
//        return Stream.of(
//                Arguments.of(33, 1, 32),
//                Arguments.of(54, 1, 53),
//                Arguments.of(24, 1, 23)
//        );
//    }
}