package id.b2camp.fakhri.unit;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.Random.class)
public class MethodOrderedRandomlyTest {

    @Test
    void testA() {

    }

    @Test
    void testB() {

    }

    @Test
    void testC() {

    }

    @Test
    void testD() {

    }

}
