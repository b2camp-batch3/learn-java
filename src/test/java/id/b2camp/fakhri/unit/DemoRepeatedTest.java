package id.b2camp.fakhri.unit;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DemoRepeatedTest {

    Calculator calculator;

    @BeforeEach
    void beforeEachMethod() {
        calculator = new Calculator();
        System.out.println("Executing before each method");
    }

//    @RepeatedTest(value = 3, name = "{displayName}, Repetition {currentRepetition} of " + "{totalRepetition}")
    @RepeatedTest(3)
    @DisplayName("Division by zero")
    void testIntegerDivision_WhenDividendByZero_ShouldThrowArithmeticException(RepetitionInfo repetitionInfo, TestInfo testInfo) {
        System.out.println("Running division by zero");
        System.out.println("Running " + testInfo.getTestMethod().get().getName());
        System.out.println("Repetition #" + repetitionInfo.getCurrentRepetition() + " of " + repetitionInfo.getTotalRepetitions());
        //arrange
        int dividend = 4;
        int divisor = 0;
        String expectedExceptionMessage = "/ by zero";

        //act & assert
        ArithmeticException actualException = assertThrows(ArithmeticException.class, () -> {
            calculator.integerDivision(dividend, divisor);
        });

        //assert
        assertEquals(expectedExceptionMessage, actualException.getLocalizedMessage());
    }
}
