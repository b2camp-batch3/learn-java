package id.b2camp.fakhri.tdd.service;

import id.b2camp.fakhri.tdd.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserServiceTest {

    UserService userService;
    String firstname;
    String lastname;
    String email;
    String password;
    String repeatPassword;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl();
        firstname = "Fakhri";
        lastname = "Chaerul";
        email = "fakhrichaerul@mailinator.com";
        password = "12312412";
        repeatPassword = "12312412";
    }

    @Test
    @DisplayName("User object creation")
    void testCreateUser_whenUserDetailsProvided_returnUserObject() {
        User user = userService.createUser(firstname, lastname, email, password, repeatPassword);

        assertNotNull(user, "The createUser() method should not have returned null");
        assertEquals(firstname, user.getFirstName(), "User first name is incorrect");
        assertEquals(lastname, user.getLastName(), "user last name is incorrect");
        assertEquals(email, user.getEmail(), "User email is incorrect");
        assertNotNull(user.getId(), "User id is missing");

    }

    @DisplayName("Empty first name causes correct exception")
    @Test
    void testCreateUser_whenFirstNameIsEmpty_shouldThrowsIllegalArgumentException() {
        String firstname = "";
        String expectedExceptionMessage = "First name must not be empty";

        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
            userService.createUser(firstname, lastname, email, password, repeatPassword);
        });

        assertEquals(expectedExceptionMessage, thrown.getMessage());
    }
}
