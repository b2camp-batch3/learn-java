package id.b2camp.fakhri.integration.service;

import id.b2camp.fakhri.integration.io.CustomersDatabase;
import id.b2camp.fakhri.integration.io.CustomersDatabaseMapImpl;
import org.junit.jupiter.api.*;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerServiceImplTest {

    CustomersDatabase customersDatabase;
    CustomerService customerService;
    String createdUserId = "";

    @BeforeAll
    void setup() {
        // Create & initialize database
        customersDatabase = new CustomersDatabaseMapImpl();
        customersDatabase.init();
        customerService = new CustomerServiceImpl(customersDatabase);
    }

    @AfterAll
    void cleanup() {
        // Close connection
        // Delete database
        customersDatabase.close();
    }

    @Test
    @Order(1)
    @DisplayName("Create User works")
    void testCreateUser_whenProvidedWithValidDetails_returnsUserId() {
        HashMap<String, String> user = new HashMap<>();
        user.put("firstName", "Fakhri");
        user.put("lastName", "Insan");

        createdUserId = customerService.createUser(user);

        assertNotEquals(createdUserId, "User id should not be null");
    }


    @Test
    @Order(2)
    @DisplayName("Update user works")
    void testUpdateUser_whenProvidedWithValidDetails_returnsUpdatedUserDetails() {
        HashMap<String, String> newUserDetails = new HashMap<>();
        newUserDetails.put("firstName", "Bruce");
        newUserDetails.put("lastName", "Banner");

        Map updatedUserDetails = customerService.updateUser(createdUserId, newUserDetails);

        assertEquals(newUserDetails.get("firstName"), updatedUserDetails.get("firstName"), "Returned value of username is incorrect");
        assertEquals(newUserDetails.get("lastName"), updatedUserDetails.get("lastName"), "Returned value of username is incorrect");
    }

    @Test
    @Order(3)
    @DisplayName("Find user works")
    void testGetUserDetails_whenProvidedWithValidUserId_returnsUserDetails() {
        Map userDetails = customerService.getUserDetails(createdUserId);

        assertNotNull(userDetails, "user details should not be null");
        assertEquals(createdUserId, userDetails.get("userId"), "Returned user details contains incorrect user id");
    }

    @Test
    @Order(4)
    @DisplayName("Delete user works")
    void testDeleteUser_whenProvidedWithValidUserId_returnsUserDetails() {
        customerService.deleteUser(createdUserId);

        assertNull(customerService.getUserDetails(createdUserId), "user should not be found after delete");
    }
}