package id.b2camp.fakhri.day7.finalkeyword;

public class Advertising {

    private String publisherName;

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }
}


class Newspaper extends Advertising {

    final void trending(String title) {
        System.out.println(title);
    }
}

final class ColumnNews extends Newspaper {

//    method dari super yang sudah final tidak bisa diturunkan
//    final void trending(String title) {
//        System.out.println(title + "rubah");
//    }
}

// super class yang sudah final tidak bisa diturunkan
//class FakeNews extends ColumnNews {
//
//}

