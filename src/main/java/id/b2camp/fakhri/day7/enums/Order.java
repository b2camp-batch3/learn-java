package id.b2camp.fakhri.day7.enums;

public class Order {

    private double amount;
    private PaymentStatus status;

    public Order(double amount, PaymentStatus status) {
        this.amount = amount;
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }
}
