package id.b2camp.fakhri.day7.enums;

public class EnumFakhriApp {
    public static void main(String[] args) {

        PaymentStatus pending = PaymentStatus.PENDING;
        PaymentStatus unpaid = PaymentStatus.UNPAID;
        System.out.println(pending);
        System.out.println(unpaid);

        String pendingString = pending.name();
        String unpaidString = unpaid.name();
        System.out.println(pendingString);
        System.out.println(unpaidString);

        PaymentStatus paymentStatusPending = PaymentStatus.valueOf(pendingString);
        PaymentStatus paymentStatusUnpaid = PaymentStatus.valueOf(unpaidString);
        System.out.println(paymentStatusPending);
        System.out.println(paymentStatusUnpaid);

        System.out.println("---");
        Order order = new Order(2000_000, PaymentStatus.PAID);
        System.out.println("Order ini seharga  " + order.getAmount());
        System.out.println("Order ini statusnya " + order.getStatus());
        System.out.println("Deskripsi " + order.getStatus().getCode() + " " + order.getStatus().getDescription());

    }
}
