package id.b2camp.fakhri.day7.enums;

public enum PaymentStatus {
    PENDING(10, "Pembayaran sedang diproses"),
    UNPAID(99, "Pembayaran gagal dibayar"),
    PAID(20, "Pembayaran sudah dibayar"),
    ;

    final int code;
    final String description;

    PaymentStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
