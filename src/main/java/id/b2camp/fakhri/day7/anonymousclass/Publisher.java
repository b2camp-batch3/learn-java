package id.b2camp.fakhri.day7.anonymousclass;

public interface Publisher {

    void publishToNewspaper();

    void publishToMagazine();
}
