package id.b2camp.fakhri.day7.anonymousclass;

public class AnonymousFakhriApp {
    public static void main(String[] args) {

//        Publisher publisher = new Publisher();

        // instantiate interface dengan anonymous class
        Publisher publisher = new Publisher() {
            @Override
            public void publishToNewspaper() {
                System.out.println("publish to newspaper");
            }

            @Override
            public void publishToMagazine() {
                System.out.println("publish to magazine");
            }
        };
        publisher.publishToNewspaper();
        publisher.publishToMagazine();

        System.out.println("---");
        // instantiate abstract class dengan anonymous class
        Player player = new Player() {
            @Override
            public void dribling() {
                System.out.println("pemain mendribling bola");
            }
        };
        player.dribling();
    }
}
