package id.b2camp.fakhri.day7.statickeyword;


public class MathUtils {

    // static method
    static void multiply(int value) {
        System.out.println("hasil penggandaan adalah " + value * 2);
    }

    static void plus(int value, int secondValue) {
        int result = value + secondValue;
        System.out.println("hasil " + value + " ditambah " + secondValue + " = " + result);
    }
}
