package id.b2camp.fakhri.day7.statickeyword;

public class StaticFakhriApp {
    public static void main(String[] args) {

        String java = Programming.PROGRAMMING_LANGUAGE;
        boolean isDynamic = Programming.IS_DYNAMIC;
        int version = Programming.VERSION;
        System.out.println(java);
        System.out.println(isDynamic);
        System.out.println(version);

        System.out.println("---");
        MathUtils.multiply(4);
        MathUtils.plus(100, 2000);

        System.out.println("---");
        JVMProgram.Java javaProgram = new JVMProgram.Java();
        javaProgram.setVersion("11");
        System.out.println(javaProgram.getVersion());
    }
}
