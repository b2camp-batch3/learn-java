package id.b2camp.fakhri.day7.statickeyword;

public class JVMProgram {

    String language;

    public JVMProgram(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    // static inner class
    static class Java {
        String version;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }
}
