package id.b2camp.fakhri.day7.statickeyword;

public class Programming {

    // static properties
    static String PROGRAMMING_LANGUAGE = "Java";
    static boolean IS_DYNAMIC = false;
    static int VERSION = 11;
}
