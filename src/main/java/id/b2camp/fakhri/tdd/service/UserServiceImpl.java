package id.b2camp.fakhri.tdd.service;

import id.b2camp.fakhri.tdd.model.User;

import java.util.UUID;

public class UserServiceImpl implements UserService {

    @Override
    public User createUser(String firstname, String lastname, String email, String password, String repeatPassword) {
        if (firstname == null || firstname.isEmpty()) {
            throw new IllegalArgumentException("First name must not be empty");
        }

        return new User(UUID.randomUUID().toString(), firstname, lastname, email);
    }
}
