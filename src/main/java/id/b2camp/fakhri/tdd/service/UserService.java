package id.b2camp.fakhri.tdd.service;

import id.b2camp.fakhri.tdd.model.User;

public interface UserService {

    User createUser(String firstname, String lastname, String email, String password, String repeatPassword);
}
