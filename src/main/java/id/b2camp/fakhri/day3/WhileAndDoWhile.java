package id.b2camp.fakhri.day3;

import java.util.Arrays;

public class WhileAndDoWhile {
    public static void main(String[] args) {

        int[] numbers = new int[]{1, 2, 3, 4, 5};
//        while (numbers.length > 1) {
//            System.out.println(Arrays.toString(numbers));
//        }

        do {
            System.out.println(Arrays.toString(numbers));
        } while (numbers.length > 10);

//        int count = 0;
//        while (true) {
//            System.out.println(count++);
//        }

//        int countB = 1001;
//        do {
//            System.out.println(countB);
//            countB++;
//        } while (countB <= 100);
    }
}
