package id.b2camp.fakhri.day3;

import java.util.Scanner;

public class ScannerFakhri {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("What is your name?");
        String name = scanner.nextLine();
        System.out.println("Oh your name is " + name + ", nice to meet you");

        System.out.println("What is your age?");
        int age = scanner.nextInt();
        if (age <= 10) {
            System.out.println("Oh you are a teen");
        } else if (age > 10 && age <= 20) {
            System.out.println("Oh you are not an adult");
        } else {
            System.out.println("Oh you are an adult");
        }
    }
}
