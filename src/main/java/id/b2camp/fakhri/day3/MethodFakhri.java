package id.b2camp.fakhri.day3;

public class MethodFakhri {
    public static void main(String[] args) {
        char[] lettersA = {'a', 'b', 'c'};
        char[] lettersB = {'d', 'e', 'd'};
        System.out.println("alphabet ini punya angka " + countChar(lettersA));
        System.out.println("alphabet ini punya angka " + countChar(lettersB));
        System.out.println("---");
        System.out.println(messageChar(lettersA));
        System.out.println(messageChar(lettersB));
        System.out.println("---");
        System.out.println(messageChar(lettersA, "This alpahabet has numbers of "));
        System.out.println(messageChar(lettersB, "This alpahabet has numbers of "));
        System.out.println("---");
        System.out.println("jumlah character yang dicari adalah " + searchLetter(lettersA, 'b'));
        System.out.println("jumlah character yang dicari adalah " + searchLetter(lettersB, 'd'));
    }

    static int countChar(char[] lettersParam) {
        return lettersParam.length;
    }

    static String messageChar(char[] lettersParam) {
        return "alphabet ini punya angka " + lettersParam.length;
    }

    static String messageChar(char[] lettersParam, String desc) {
        return desc + lettersParam.length;
    }

    static int searchLetter(char[] lettersParam, char searchedChar) {
        int counter = 0;
        for (char letter : lettersParam) {
            if (letter == searchedChar) {
                counter++;
            }
        }
        return counter;
    }
}
