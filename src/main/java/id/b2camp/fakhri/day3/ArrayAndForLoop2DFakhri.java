package id.b2camp.fakhri.day3;

public class ArrayAndForLoop2DFakhri {
    public static void main(String[] args) {

        String[][] string2d = new String[][] {{"Play ", "with ", "array "},{"two dimensional ", "is "},{"fun"}} ;
        for (String[] string1d : string2d) {
            for (String s : string1d) {
                System.out.print(s);
            }
            System.out.println();
        }

        int[][] board = new int[4][3];
        for (int[] numbers : board) {
            for (int number : numbers) {
                System.out.print(number);
            }
            System.out.println();
        }
    }
}
