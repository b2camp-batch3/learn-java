package id.b2camp.fakhri.day3;

public class ClassAndObjectFakhri {
    public static void main(String[] args) {
        // Class itu adalah template atau blueprint
        // Object itu hasil bentuk dari yang kita definisikan dari class
        SmartPhone samsung = new SmartPhone("Samsung", "A12", true);
        SmartPhone iphone = new SmartPhone("IPhone", "6", false);

        System.out.println("This phone is from bran " + samsung.brand + " " + samsung.type);
        System.out.println("This phone is from bran " + iphone.brand + " " + iphone.type);
        System.out.println("---");
        System.out.println(samsung.phoneDescription());
        System.out.println(iphone.phoneDescription());
        System.out.println("---");
        SmartPhone smartPhone = new SmartPhone();
        System.out.println(smartPhone.isNew);
    }

    static class SmartPhone {
        String brand;
        String type;
        boolean isNew;

        SmartPhone() {
            this.brand = "Phone";
            this.type = "Phone Type";
            this.isNew = true;
        }

        SmartPhone(String brand, String type, boolean isNew) {
            this.brand = brand;
            this.type = type;
            this.isNew = isNew;
        }

        String phoneDescription() {
            return "This phone is from bran " + brand + " " + type;
        }
    }
}
