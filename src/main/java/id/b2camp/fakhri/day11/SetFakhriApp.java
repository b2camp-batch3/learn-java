package id.b2camp.fakhri.day11;

import java.util.*;

public class SetFakhriApp {
    public static void main(String[] args) {
        Set<String> friendNamesSet = new HashSet<>(List.of("Budi","Fakhri", "Chaerul", "Chaerul", "Insan", "Insan"));
        System.out.println(friendNamesSet);

        List<String> duplicatedList = new ArrayList<>(List.of("Lana", "Lana", "Hana", "Hana", "Hana"));
        System.out.println(duplicatedList);

        Set<String> unduplicatedSet = new HashSet<>(duplicatedList);
        System.out.println(unduplicatedSet);

        Collections.synchronizedSet(unduplicatedSet);
    }
}
