package id.b2camp.fakhri.day11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArrayListFakhriApp {
    public static void main(String[] args) {
        String[] namesArray = new String[4];
        namesArray[0] = "Fakhri";
        namesArray[1] = "Chaerul";
        System.out.println(Arrays.toString(namesArray));

        List<String> namesArrayList = new ArrayList<>();
        namesArrayList.add("Budi");
        namesArrayList.add("Cahyono");
        System.out.println(Arrays.toString(namesArrayList.toArray()));
        namesArrayList.add("Susi");
        namesArrayList.add("Lana");
        System.out.println(Arrays.toString(namesArrayList.toArray()));
        namesArrayList.remove("Lana");
        System.out.println(Arrays.toString(namesArrayList.toArray()));

        System.out.println("---");
        List<String> secondNamesArrayList = new ArrayList<>();
        secondNamesArrayList.addAll(namesArrayList);
        System.out.println(Arrays.toString(secondNamesArrayList.toArray()));
        secondNamesArrayList.removeAll(secondNamesArrayList);
        System.out.println(Arrays.toString(secondNamesArrayList.toArray()));

        List<String> strings = new ArrayList<>(List.of("Carl", "Mitch"));
        strings.add("Sana");
        strings.add("Hana");
        System.out.println(Arrays.toString(strings.toArray()));

        System.out.println(namesArray[1]);
        System.out.println(namesArrayList.get(1));

        System.out.println(namesArray.length);
        System.out.println(namesArrayList.size());

        Collections.shuffle(namesArrayList);


        List<List<String>> nestedList = new ArrayList<>();
        nestedList.add(strings);
        nestedList.add(namesArrayList);
        System.out.println(nestedList);

        System.out.println("---");
        for (List<String> stringList : nestedList) {
            for (String s : stringList) {
                System.out.println(s);
            }
        }
    }
}
