package id.b2camp.fakhri.day11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class LinkedListFakhriApp {
    public static void main(String[] args) {
        List<Integer> numbersLinkedList = new LinkedList<>();
        int counter = 0;
        for (int i = 0; i < 10000; i++) {
            numbersLinkedList.add(counter++);
        }
        System.out.println(numbersLinkedList);
        numbersLinkedList.get(70);
        numbersLinkedList.add(70, 13);


        List<Integer> numbersArrayList = new ArrayList<>();
        int counterB = 0;
        for (int i = 0; i < 10000; i++) {
            numbersArrayList.add(counterB++);
        }
        System.out.println(numbersArrayList);
        numbersArrayList.get(70);
        numbersArrayList.add(70, 14);

        // Array List lebih cepat untuk operasi GET data, look up DATA
        // Linked List lebih cepat untuk operasi ADD, REMOVE and UPDATE

        proceedList(numbersLinkedList);

        Collections.reverse(numbersArrayList);
    }

    private static void proceedList(List list) {

    }
}
