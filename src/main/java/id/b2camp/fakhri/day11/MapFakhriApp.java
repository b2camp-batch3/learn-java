package id.b2camp.fakhri.day11;

import java.util.*;

public class MapFakhriApp {
    public static void main(String[] args) {
        Map<Integer, String> employee = new HashMap<>();
        employee.put(1, "Mitch");
        employee.put(2, "Bradley");
        employee.put(3, "Shiina");

        System.out.println(employee);
        System.out.println(employee.get(2));
        System.out.println(employee.get(3));

        HashMap<String, List<String>> employeeDivision = new HashMap<>();
        ArrayList<String> itEmployees = new ArrayList<>(List.of("Sunny", "Bobby", "Hilman"));
        ArrayList<String> hrEmployees = new ArrayList<>(List.of("Hanna", "Yanni", "John"));
        employeeDivision.put("IT", itEmployees);
        employeeDivision.put("HR", hrEmployees);
        System.out.println(employeeDivision);
        System.out.println("---");
        System.out.println(employeeDivision.get("IT"));
        System.out.println(employeeDivision.get("HR"));

        employeeDivision.put("SALES", List.of("Santy", "Santy"));
        System.out.println(employeeDivision);
        employeeDivision.replace("SALES", List.of("Santy"));
        System.out.println(employeeDivision);

        System.out.println(employeeDivision.size());
        System.out.println(employeeDivision.entrySet());
        Collection<List<String>> values = employeeDivision.values();
        System.out.println(values);

        Collections.synchronizedMap(employeeDivision);

        SortedMap<String, String> sortedMap = new TreeMap<>();
        sortedMap.put("B", "BCD");
        sortedMap.put("A", "ABC");
        sortedMap.put("D", "DCE");
        sortedMap.put("Z", "ZET");
        System.out.println(sortedMap);

    }
}
