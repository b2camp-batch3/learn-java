package id.b2camp.fakhri.day6.interfaces;

public interface GasolineFuel {

    String getOctane();
}
