package id.b2camp.fakhri.day6.interfaces;

public interface ElectricFuel {

    String getVolt();
}
