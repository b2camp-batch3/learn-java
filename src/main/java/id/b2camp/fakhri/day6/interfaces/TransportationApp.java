package id.b2camp.fakhri.day6.interfaces;

public class TransportationApp {
    public static void main(String[] args) {
        Taxi taxi = new Taxi();
        taxi.drive();
        System.out.println(taxi.tier());
        System.out.println(taxi.seat());
        System.out.println(taxi.getOctane());
        System.out.println(taxi.getVolt());
    }
}
