package id.b2camp.fakhri.day6.interfaces;

public class Taxi implements Car, GasolineFuel, ElectricFuel {

    @Override
    public void drive() {
        System.out.println("Driving Taxi");
    }

    @Override
    public int seat() {
        return 6;
    }

    @Override
    public int tier() {
        return 4;
    }

    @Override
    public String getDriverName() {
        return "Driver Name";
    }

    @Override
    public String getOctane() {
        return "Ron 99";
    }

    @Override
    public String getVolt() {
        return "100 volt";
    }
}
