package id.b2camp.fakhri.day6.interfaces;

public interface Car {

    void drive();

    int seat();

    int tier();

    String getDriverName();
}
