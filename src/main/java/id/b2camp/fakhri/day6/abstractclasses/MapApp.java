package id.b2camp.fakhri.day6.abstractclasses;

public class MapApp {
    public static void main(String[] args) {

        City city = new City();
        city.setLatitude(1231231);
        city.setLongitude(-12312312);
        System.out.println("City = " + city.getLatitude() + " " + city.getLongitude());
        city.trackingLocation();
    }
}
