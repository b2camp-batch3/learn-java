package id.b2camp.fakhri.day6.abstractclasses;

public abstract class Location {

    private double latitude;
    private double longitude;

    public abstract void trackingLocation();

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
