package id.b2camp.fakhri.day6.abstractclasses;

public class City extends Location {

    @Override
    public void trackingLocation() {
        System.out.println("track location city");
    }
}
