package id.b2camp.fakhri.day6.accessmodifiers;

import id.b2camp.fakhri.day6.accessmodifiers.layer1.SmartPhone;
import id.b2camp.fakhri.day6.accessmodifiers.layer1.layer2.Laptop;

public class GadgetApp {
    public static void main(String[] args) {

        SmartPhone samsung = new SmartPhone("Samsung", 2_000_000);
        samsung.setBrand("Samsung Galaxy");
        samsung.setPrice(4000_000);
        System.out.println(samsung.getBrand());
        System.out.println(samsung.getPrice());
    }
}

