package id.b2camp.fakhri.day6.accessmodifiers.layer1.layer2;

public class Laptop {

    final protected String brand;
    protected double price;

    protected Laptop(String brand, double price) {
        this.brand = brand;
        this.price = price;
    }
}
