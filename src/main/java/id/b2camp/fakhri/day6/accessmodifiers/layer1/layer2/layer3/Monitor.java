package id.b2camp.fakhri.day6.accessmodifiers.layer1.layer2.layer3;

public class Monitor {

    public static void main(String[] args) {
        Monitor monitor = new Monitor("Benq", 3000_000);
        System.out.println(monitor.brand);
        System.out.println(monitor.price);
    }

    private String brand;
    private double price;

    private Monitor(String brand, double price) {
        this.brand = brand;
        this.price = price;
    }
}
