package id.b2camp.fakhri.day6.accessmodifiers.layer1.layer2;

public class LaptopApp {
    public static void main(String[] args) {

        Laptop asus = new Laptop("Asus", 5000_000);
        System.out.println(asus.brand);
        System.out.println(asus.price);
    }
}
