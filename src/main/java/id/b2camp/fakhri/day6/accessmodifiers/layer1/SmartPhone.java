package id.b2camp.fakhri.day6.accessmodifiers.layer1;

public class SmartPhone {

    private String brand;
    private double price;

    public SmartPhone(String brand, double price) {
        this.brand = brand;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
