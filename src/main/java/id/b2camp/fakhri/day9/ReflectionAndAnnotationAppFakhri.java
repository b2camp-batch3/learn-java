package id.b2camp.fakhri.day9;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

public class ReflectionAndAnnotationAppFakhri {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
        Prioritization lari = new Prioritization("Lari", LocalDateTime.now());
        Field[] fields = lari.getClass().getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }

        Method[] methods = lari.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        System.out.println("---");
        if (lari.getClass().isAnnotationPresent(VeryImportant.class)) {
            System.out.println("This is very important");
        }

        for (Field field : lari.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(ImportantActivity.class)) {
                field.setAccessible(true);
                Object objectValue = field.get(lari);
                System.out.println(objectValue + " this is an important activity");
            }
        }

        for (Method method : lari.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(MustDoImmediately.class)) {
                MustDoImmediately annotation = method.getAnnotation(MustDoImmediately.class);
                method.invoke(lari);
                for (int i = 0; i < annotation.times() - 1; i++) {
                    method.invoke(lari);
                }
            }
        }

        System.out.println("---");
        PrioritizationWithLombok koding = new PrioritizationWithLombok("Koding", LocalDateTime.now());
        System.out.println("activity = " + koding.getActivity());
        System.out.println("time = " + koding.getTime());
    }
}
