package id.b2camp.fakhri.day9;

import java.time.LocalDateTime;

@VeryImportant
public class Prioritization {

    @ImportantActivity
    private String activity;

    private LocalDateTime time;

    public Prioritization(String activity, LocalDateTime time) {
        this.activity = activity;
        this.time = time;
    }

    @MustDoImmediately(times = 3)
    public void shouldDoNow() {
        System.out.println("Should do it now");
    }

    public void canDoItLater() {
        System.out.println("Can do it later");
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
