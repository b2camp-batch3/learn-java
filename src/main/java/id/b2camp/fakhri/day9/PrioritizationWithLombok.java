package id.b2camp.fakhri.day9;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PrioritizationWithLombok {

    private String activity;
    private LocalDateTime time;

}
