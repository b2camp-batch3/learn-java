package id.b2camp.fakhri.day8;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class IdCardApp {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws InvalidNameException, InvalidDateException {

        String name = input("Name :");
        String birthDateStr = input("Tanggal Lahir(yyyy-MM-dd)");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthDate = LocalDate.parse(birthDateStr, formatter);

        try {
            ValidationUtils.validateIdCardName(name);
            ValidationUtils.validateIdCardBirthDate(birthDate);
        } catch (InvalidNameException e) {
            throw new InvalidNameException("Name gagal diinput" + e.getMessage());
        } catch (InvalidDateException e) {
            throw new InvalidDateException("Tanggal lahir gagal diinput" + e.getMessage());
        } finally {
            System.out.println("Always proceesed");
        }

//        ValidationUtils.validateRuntimeIdCardName(name);
//        ValidationUtils.validateRuntimeIdCardBirthDate(birthDate);

        IdCardForm fakhriIdCard = new IdCardForm(name, birthDate);
        System.out.println("Data " + fakhriIdCard.getName() + " berhasil dicetak");
    }

    static String input(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }
}
