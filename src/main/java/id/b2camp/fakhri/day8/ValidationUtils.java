package id.b2camp.fakhri.day8;

import java.time.LocalDate;
import java.util.Objects;

public class ValidationUtils {

    public static void validateIdCardName(String name) throws InvalidNameException {
        if (name.isEmpty() || name.isBlank()) {
            throw new InvalidNameException("Name cannot be blank");
        } else if (name.equalsIgnoreCase("null")) {
            throw new InvalidNameException("Name cannot be null");
        }
    }

    public static void validateIdCardBirthDate(LocalDate birthDate) throws InvalidDateException {
        if (Objects.isNull(birthDate)) {
            throw new InvalidDateException("Birth date cannot be null");
        } else if (birthDate.isAfter(LocalDate.now())) {
            throw new InvalidDateException("Birth date cannot be after current date");
        }
    }

    public static void validateRuntimeIdCardName(String name) throws RuntimeException {
        if (name.isEmpty() || name.isBlank()) {
            throw new RuntimeException("Name cannot be blank");
        } else if (name.equalsIgnoreCase("null")) {
            throw new RuntimeException("Name cannot be null");
        }
    }

    public static void validateRuntimeIdCardBirthDate(LocalDate birthDate) throws RuntimeException {
        if (Objects.isNull(birthDate)) {
            throw new RuntimeException("Birth date cannot be null");
        } else if (birthDate.isAfter(LocalDate.now())) {
            throw new RuntimeException("Birth date cannot be after current date");
        }
    }
}
