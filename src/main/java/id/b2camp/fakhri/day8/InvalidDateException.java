package id.b2camp.fakhri.day8;

public class InvalidDateException extends Throwable {

    public InvalidDateException(String message) {
        super(message);
    }
}
