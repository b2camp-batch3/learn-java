package id.b2camp.fakhri.day8;

import java.time.LocalDate;

public class IdCardForm {

    private String name;
    private LocalDate birthDate;

    public IdCardForm(String name, LocalDate birthDate) {
        this.name = name;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
