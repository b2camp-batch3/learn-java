package id.b2camp.fakhri.day8;

public class InvalidNameException extends Throwable {

    public InvalidNameException(String message) {
        super(message);
    }
}
