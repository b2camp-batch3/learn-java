package id.b2camp.fakhri.day2;

public class SwitchFakhri {
    public static void main(String[] args) {
        String gender = "FEMALE";
//        if (gender.equals("MALE")) {
//
//        } else if (gender.equals("FEMALE")){
//
//        } else {
//
//        }

        switch (gender) {
            case "MALE":
                System.out.println("I'm male");
                break;
            case "FEMALE":
                System.out.println("I'm female");
                break;
            default:
                System.out.println("I'm a bird");
        }
    }
}
