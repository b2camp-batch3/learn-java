package id.b2camp.fakhri.day2;

public class ComparisonFakhri {

    public static void main(String[] args) {
        int fakhriAge = 29;
        int budiAge = 25;
        System.out.println(fakhriAge < budiAge);
        System.out.println(fakhriAge <= budiAge);
        System.out.println(fakhriAge >= budiAge);
        System.out.println(fakhriAge > budiAge);
        System.out.println(fakhriAge == budiAge);
        System.out.println(fakhriAge != budiAge || fakhriAge == budiAge);

        System.out.println("---");
        System.out.println("Fakhri".equalsIgnoreCase("fakhri"));

        boolean isStudent = true;
        boolean isAdult = false;
        boolean isPassed = true;

        System.out.println("---");
        System.out.println(isAdult && isPassed);
        System.out.println(isAdult || isPassed);

        System.out.println("---");
        boolean isOkay = true;
        boolean isMature = false;
        System.out.println(isStudent || isStudent && isPassed && isOkay && isMature);
        System.out.println((false && false) || (false || true));


        System.out.println(true && true);
        System.out.println(true && false);
        System.out.println(true || true);
        System.out.println(true || false);

        System.out.println(false && true);
        System.out.println(!(false || false));

        System.out.println("---");
        System.out.println(0 >= -1 && 3 == 1 && 5 >= 6);
    }
}
