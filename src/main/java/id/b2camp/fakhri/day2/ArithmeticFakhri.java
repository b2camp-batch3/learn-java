package id.b2camp.fakhri.day2;

public class ArithmeticFakhri {
    public static void main(String[] args) {
        int one = 1;
        int two = 2;
        System.out.println(one + two);
        System.out.println(23 + 13);
        System.out.println(12 - 1);
        System.out.println(123 * 2);
        System.out.println(24 / 2);

        System.out.println(23 * 12 - (1 + 5));

        System.out.println(Math.max(1, 5));
        System.out.println(Math.pow(1.2, 2.5));
        System.out.println(Math.floor(2.8));
        System.out.println(Math.log(123));

        System.out.println("Selesai Koding");
    }
}
