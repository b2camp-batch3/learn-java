package id.b2camp.fakhri.day2;

public class IfElseFakhri {
    public static void main(String[] args) {
        int currentAge = 10;
        int adultAge = 18;
        if (currentAge >= adultAge) {
            System.out.println("Hooray, I'm adult");
        } else if (currentAge > 9 && currentAge < adultAge) {
            System.out.println("I'm a teen");
        } else {
            System.out.println("I'm not and adult");
        }

        System.out.println("Executed no matter what");
        System.out.println("---");

        String message = currentAge >= adultAge ? "Hooray, I'm adult" : "I'm not and adult";
        System.out.println(message);
    }
}
