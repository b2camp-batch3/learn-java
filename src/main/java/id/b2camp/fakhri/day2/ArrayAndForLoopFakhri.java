package id.b2camp.fakhri.day2;

import java.util.Arrays;

public class ArrayAndForLoopFakhri {
    public static void main(String[] args) {

        int[] numbers = new int[5];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = 3;

        String[] strings = new String[5];
        boolean[] booleans = new boolean[3];
        System.out.println(Arrays.toString(numbers));
        System.out.println(Arrays.toString(strings));
        System.out.println(Arrays.toString(booleans));

        System.out.println("---");
        System.out.println(Arrays.toString(numbers));

        String[] names = {"Fakhri", "Chaerul", "Insan"};
        System.out.println(Arrays.toString(names));
        System.out.println(names.length);
        System.out.println(names[2]);

        System.out.println("---");
        for (String name : names) {
            if (name.equalsIgnoreCase("Fakhri")) {
                System.out.println(name);
            }
        }

        System.out.println("---");
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

        for (int i = names.length - 1; i >= 0; i--) {
            System.out.println(names[i]);
        }


        System.out.println("---");
        int[] numbersB = {1, 2, 3, 4, 5};
        for (int i : numbersB) {
            if (i == 4) {
                continue;
            }
            System.out.println(i);
        }
    }
}
