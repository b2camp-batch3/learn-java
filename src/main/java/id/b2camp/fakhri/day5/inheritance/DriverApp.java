package id.b2camp.fakhri.day5.inheritance;

import java.util.Scanner;

// Contoh Inheritance
public class DriverApp {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Driver driver = new Driver();
        String input = scanner.nextLine();
        driver.name = input;
        System.out.println("Hello saya adalah mitra dari DriverApp, nama saya " + driver.name);

        CarDriver carDriver = new CarDriver("Budi", "Bandung");
        System.out.println("Hello saya adalah mitra dari DriverApp, nama saya " + carDriver.name);
        System.out.println("Our location is in " + carDriver.location);

        System.out.println("---");
        MotorcycleRider rider = new MotorcycleRider("Santo");
        rider.greeting("Yanti");

        System.out.println("---");
        System.out.println(rider.motorcycleMaxSeat());

    }
}

class Driver {
    String name;
    String location;

    void greeting(String customer) {
        System.out.println("Hello " + customer + " saya adalah mitra dari DriverApp, nama saya " + name);
    }

    int maxSeat() {
        return 4;
    }

    Driver() {
    }

    Driver(String name) {
        this.name = name;
    }

    Driver(String name, String location) {
        this.name = name;
        this.location = location;
    }
}

class CarDriver extends Driver {

    CarDriver(String name, String location) {
        super(name, location);
    }
}

class MotorcycleRider extends Driver {

    // ini adalah method overriding
    void greeting(String customer) {
        System.out.println("Hello " + customer + " saya adalah rider dari DriverApp, nama saya " + name);
    }

    int motorcycleMaxSeat() {
        return super.maxSeat();
    }

    MotorcycleRider(String name) {
        super(name);
    }
}
