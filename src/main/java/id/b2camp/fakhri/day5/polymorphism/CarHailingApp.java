package id.b2camp.fakhri.day5.polymorphism;

// Contoh Polymorphism
public class CarHailingApp {
    public static void main(String[] args) {
        Car car = new Car();
        car.brand = "toyota";
        System.out.println(car.brand);

        BaseCar camperVan = new CamperVan("VW");
        System.out.println(camperVan.brand);

        System.out.println("---");
        sayHello(new Car("Toyota"));
        sayHello(new BaseCar("Mitsubishi"));
        sayHello(new LuxuryCar("VW"));

        System.out.println("---");
        CamperVan zohoVan = new CamperVan("Zoho");
        checkMaxSeat(zohoVan);

        System.out.println("-beware of variable hiding-");
        Taxi taxi = new Taxi("Converto");
        System.out.println(taxi.brand);

        Car carTaxi = (Car) taxi;
        System.out.println(carTaxi.brand);
    }

    static void sayHello(Car car) {
        System.out.println("Hello welcome to our services, you are right now in " + car.brand);
    }

    static void checkMaxSeat(BaseCar baseCar) {
        if (baseCar instanceof LuxuryCar) {
            LuxuryCar car = (LuxuryCar) baseCar;
            car.maxSeat = 4;
            System.out.println("Our Luxury Car max seat is " + car.maxSeat);
        } else if (baseCar instanceof CamperVan) {
            CamperVan camperVan = (CamperVan) baseCar;
            camperVan.maxSeat = 6;
            System.out.println("Our Camper Van max seat is " + camperVan.maxSeat);
        } else {
            System.out.println("Our max seat is full");
        }
    }
}

class Car {
    String brand;

    Car() {

    }

    Car(String brand) {
        this.brand = brand;
    }
}

class BaseCar extends Car {
    int maxSeat;

    BaseCar(String brand) {
        super(brand);
    }

    BaseCar(String brand, int maxSeat) {
        super(brand);
        this.maxSeat = maxSeat;
    }
}

class LuxuryCar extends BaseCar {

    LuxuryCar(String brand) {
        super(brand);
    }
}

class CamperVan extends BaseCar {

    CamperVan(String brand) {
        super(brand);
    }
}

class Taxi extends Car {
    String brand;

    Taxi(String brand) {
        super(brand);
    }
}