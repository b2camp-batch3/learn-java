package id.b2camp.fakhri.day1;

import java.math.BigDecimal;
import java.time.LocalDate;

public class HelloFakhri {

    public static void main(String[] args) {
        System.out.println("Hello Fakhri");

        int one = 1;
        int two = 2;
        float twoPointOne = 2.0f;
        double twoPointTwo = 2.2;
        boolean isOld = false;

        String name = "Fakhri";
        LocalDate now = LocalDate.now();
        Boolean isNew = true;
        BigDecimal price = BigDecimal.valueOf(10_000_000);

        System.out.println(one);
        System.out.println(two);
        System.out.println(name);
        System.out.println(isOld);

        System.out.println(name.toUpperCase());
        System.out.println(name.toLowerCase());
        System.out.println(name.charAt(0));
        System.out.println(name.charAt(1));
        System.out.println(now);
        System.out.println(isNew);
        System.out.println(price);

        Integer oneNonPrimitive = Integer.valueOf(one);
        Float oneFloat = Float.valueOf(one);
        System.out.println("-----");
        System.out.println(name);
        System.out.println(name.charAt(3));
        System.out.println(name.charAt(2));
        System.out.println(name.charAt(1));
        System.out.println(name.charAt(0));
    }
}
