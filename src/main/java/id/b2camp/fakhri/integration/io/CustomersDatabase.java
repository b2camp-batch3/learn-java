package id.b2camp.fakhri.integration.io;

import java.util.Map;

public interface CustomersDatabase {
    void init();
    void close();
    Map save(String userId, Map userDetails);
    Map update(String userId, Map user);
    Map find(String userId);
    void delete(String userId);
}
