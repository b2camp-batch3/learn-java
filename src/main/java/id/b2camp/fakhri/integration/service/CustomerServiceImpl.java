package id.b2camp.fakhri.integration.service;

import id.b2camp.fakhri.integration.io.CustomersDatabase;

import java.util.Map;
import java.util.UUID;

public class CustomerServiceImpl implements CustomerService {

    CustomersDatabase customersDatabase;

    public CustomerServiceImpl(CustomersDatabase customersDatabase) {
        this.customersDatabase = customersDatabase;
    }

    @Override
    public String createUser(Map userDetails) {
        String userId = UUID.randomUUID().toString();
        userDetails.put("userId", userId);
        customersDatabase.save(userId, userDetails);
        return userId;
    }

    @Override
    public Map updateUser(String userId, Map userDetails) {
        Map existingUser = customersDatabase.find(userId);
        if(existingUser == null) throw new IllegalArgumentException("User not found");

        existingUser.put("firstName", userDetails.get("firstName"));
        existingUser.put("lastName", userDetails.get("lastName"));

        return customersDatabase.update(userId, existingUser);
    }

    @Override
    public Map getUserDetails(String userId) {
        return customersDatabase.find(userId);
    }

    @Override
    public void deleteUser(String userId) {
        Map existingUser = customersDatabase.find(userId);
        if(existingUser == null) throw new IllegalArgumentException("User not found");

        customersDatabase.delete(userId);
    }
}
