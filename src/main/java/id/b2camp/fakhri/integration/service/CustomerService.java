package id.b2camp.fakhri.integration.service;

import java.util.Map;

public interface CustomerService {
    String createUser(Map userDetails);
    Map updateUser(String userId, Map userDetails);
    Map getUserDetails(String userId);
    void deleteUser(String userId);
}
