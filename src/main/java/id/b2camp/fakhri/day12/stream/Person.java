package id.b2camp.fakhri.day12.stream;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private String name;
    private int age;
    private Gender gender;
    private BigDecimal tax;
//    private LocalDate subscriptionDate;
//    private LocalDateTime createdAt;
//    private Double latitude;
//    private Double longitude;
}
