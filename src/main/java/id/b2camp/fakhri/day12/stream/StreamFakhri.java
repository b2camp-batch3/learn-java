package id.b2camp.fakhri.day12.stream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamFakhri {

    public static void main(String[] args) {
        List<Person> people = getPeople();

        List<Person> femalePeople = new ArrayList<>();
        for (Person person : people) {
            if (person.getGender().equals(Gender.FEMALE)) {
                femalePeople.add(person);
            }
        }
        femalePeople.forEach(System.out::println);

        System.out.println("---");
        List<Person> malePeople = people.stream()
                .filter(person -> person.getGender().equals(Gender.MALE))
                .collect(Collectors.toList());
        malePeople.forEach(System.out::println);

        System.out.println("---");
        List<Person> youngToAge = people.stream()
                .sorted(Comparator.comparing(Person::getAge).reversed().thenComparing(Person::getGender).reversed())
                .collect(Collectors.toList());
        youngToAge.forEach(System.out::println);

        System.out.println("---");
        boolean allHasMoreThan17 = people.stream()
                .allMatch(person -> person.getAge() < 10);
        System.out.println(allHasMoreThan17);

        List<Person> adults = people.stream()
                .filter(person -> person.getAge() > 17)
                .collect(Collectors.toList());
        adults.forEach(System.out::println);

        System.out.println("---");
        BigDecimal sumOfTaxes = people.stream()
                .filter(person -> person.getAge() > 18)
                .map(Person::getTax)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(sumOfTaxes);

        Map<Gender, List<Person>> groupByGender = people.stream()
                .collect(Collectors.groupingBy(Person::getGender));

        groupByGender.forEach((gender, person) -> {
            System.out.println(gender);
            person.forEach(System.out::println);
            System.out.println();
        });

        List<Map.Entry<Gender, List<Person>>> collect = groupByGender.entrySet()
                .stream()
                .filter(k -> k.getKey().equals(Gender.MALE))
                .collect(Collectors.toList());
        collect.forEach(x -> System.out.println(x));

        System.out.println("---");
        people.stream()
                .filter(person -> person.getGender().equals(Gender.FEMALE))
                .max(Comparator.comparing(Person::getAge))
                .ifPresent(System.out::println);
    }

    static List<Person> getPeople() {
        return List.of(
                new Person("Fakhri", 28, Gender.MALE, BigDecimal.valueOf(10_000_000)),
                new Person("Maya", 17, Gender.FEMALE, BigDecimal.valueOf(10_000_000)),
                new Person("Fauzi", 50, Gender.MALE, BigDecimal.valueOf(10_000_000)),
                new Person("Marlo", 32, Gender.MALE, BigDecimal.valueOf(10_000_000)),
                new Person("Nurul", 14, Gender.FEMALE, BigDecimal.valueOf(10_000_000)),
                new Person("Shita", 25, Gender.FEMALE, BigDecimal.valueOf(10_000_000)),
                new Person("Sari", 14, Gender.FEMALE, BigDecimal.valueOf(10_000_000)),
                new Person("Arya", 25, Gender.MALE, BigDecimal.valueOf(10_000_000))
        );
    }
}
