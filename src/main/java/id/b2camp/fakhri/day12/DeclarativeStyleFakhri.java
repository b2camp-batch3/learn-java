package id.b2camp.fakhri.day12;

import java.util.List;

public class DeclarativeStyleFakhri {
    public static void main(String[] args) {
        List<String> names = List.of("Fakhr", "Chaerul", "Insan", "Nemo", "Bruce", "Joker", "Jeny");

        //declarative style : tell what to do and but not how to do it
        if (names.contains("Nemo")) {
            System.out.println("Nemo found");
        } else {
            System.out.println("Nemo not found");
        }
    }
}
