package id.b2camp.fakhri.day12;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FunctionalStyleFakhri {
    public static void main(String[] args) {
        List<String> names = List.of("Fakhr", "Chaerul", "Insan", "Nemo", "Dory", "Bruce", "Jane", "Gill");

        //functional style == declarative style + higher-order functions
        //composing functions that can be work together

        //much like how we pass data, we can pass code or functions around.
        // these functions that can either receive or return functions are called higher-order functions
        if (names.contains("Nemo")) {
            System.out.println("Nemo found");
        } else {
            System.out.println("Nemo not found");
        }

        System.out.println(
                names.stream()
                        .filter(name -> name.length() == 4)
                        .map(String::toUpperCase)
                        .collect(Collectors.joining(", "))
        );

        //functional style flows like the problem statement has less complexity
        //simple problem need simple solution, complex problem need manageable solution

        // DO NOT DO THE CODE BELOW, it behave but ..
        List<String> results = new ArrayList<>();
        names.stream() // it will broke if change to parallelStream() cause of sharing mutability
                .filter(name -> name.length() == 4)
                .map(String::toUpperCase)
                .forEach(name -> results.add(name)); // mutation is ok, but sharing mutability is a devil works!!
        //make sure the lambda expressions and the functional pipeline are pure.
        //what is pure?
        //Pure function do not have side-effects.
        //they are idempotent

        //Rule for purity
        //1. the function does not mutate or change anything that is visible from outside
        //2. the function does not depend on anything from outside that may possibly change

        System.out.println(results);

        // DO NOT throw exception in FP, FP and exceptions are mutually exclusive
        /*try {
            names.stream()
                     .map(name -> ImperativeStyleApp.transform(name))
                    .forEach(System.out:: println);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }*/

    }
}

//hard to write, easy to read