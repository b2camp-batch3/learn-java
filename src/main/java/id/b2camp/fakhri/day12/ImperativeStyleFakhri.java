package id.b2camp.fakhri.day12;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImperativeStyleFakhri {
    public static void main(String[] args) {
        List<String> names = List.of("Fakhr", "Chaerul", "Insan", "Nemo", "Bruce", "Joker", "Jeny");

        //imperative style : tell what to do and also how to do it
        boolean found = false;

        // even though this is familiar but a complex loop
        // for (int i = 0; i < names.size(); i++) {}
        for (String name : names) {
            if (name.equals("Nemo")) {
                found = true;
                break;
            }
        }

        if (found) {
            System.out.println("Nemo found");
        } else {
            System.out.println("Nemo not found");
        }

        // print all names in uppercase, comma separated, of length 4
        for (String name : names) {
            if (name.length() == 4) {
                System.out.println(name.toUpperCase() + ", ");
            }
        }

        //imperative style has higher accidental complexity

        // we use to handling exception in imperative style code
        List<String> results = new ArrayList<>();
        for (String result : results) {
            try {
                results.add(transform(result));
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        //exception handling is an imperative style of programming idea
        //treat errors as a form of data and deal with them downstream

        System.out.println(results);
    }

    static String transform(String name) throws IOException {
        if (Math.random() > 0.5) {
            throw new IOException("reason");
        }

        return name.toUpperCase();
    }
}

// garbage variable
// has mutability
// verbose
// error prone
// really hard to parallelize

// it is easier to write due to familiarity but hard to read
// due to verbosity, moving parts....


//easy to write, hard to read
