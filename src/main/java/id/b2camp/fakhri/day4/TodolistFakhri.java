package id.b2camp.fakhri.day4;

import java.util.Scanner;

public class TodolistFakhri {

    static String[] model = new String[1];
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {

            viewTodos();
            System.out.println("Menu: ");
            System.out.println("1. Add");
            System.out.println("2. Remove");
            System.out.println("x. Exit");
            String input = input("Pilih");
            if (input.equals("1")) {
                formAdd();
            } else if (input.equals("2")) {
                formRemove();
            } else if (input.equals("x")) {
                break;
            } else {
                System.out.println("Invalid input");
            }
        }
    }

    static void viewTodos() {
        for (int i = 0; i < model.length; i++) {
            String todo = model[i];
            int number = i + 1;

            if (todo != null) {
                System.out.println(number + ". " + todo);
            }
        }
    }

    static String input(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    static void formAdd() {
        System.out.println("Add new todo");
        String input = input("input the following text or (x) for cancel");

        if (input.equals("x")) {
            // Cancel
        } else {
            addTodo(input);
        }
    }

    static void addTodo(String todo) {
        boolean isFull = true;

        for (int i = 0; i < model.length; i++) {
            if (model[i] == null) {
                isFull = false;
                break;
            }
        }

        if (isFull) {
            String[] tempModel = model;
            model = new String[model.length * 2];

            for (int i = 0; i < tempModel.length; i++) {
                model[i] = tempModel[i];
            }
        }

        for (int i = 0; i < model.length; i++) {
            if (model[i] == null) {
                model[i] = todo;
                break;
            }
        }
    }

    static void formRemove() {
        System.out.println("Remove todo");
        String input = input("Remove todo by number or (x) for cancel");

        if (input.equals("x")) {

        } else {
            boolean isSuccess = removeTodo(Integer.valueOf(input));
            if (!isSuccess) {
                System.out.println("Fail removed todo : " + input);
            }
        }
    }

    static boolean removeTodo(int number) {
        if ((number - 1) >= model.length) {
            return false;
        }

        if (model[number - 1] == null) {
            return false;
        }

        for (int i = (number - 1); i < model.length; i++) {
            if (i == (model.length - 1)) {
                model[i] = null;
            } else {
                model[i] = model[i + 1];
            }
        }

        return true;
    }
}
