package id.b2camp.fakhri.day10.optional;

import java.util.Optional;

public class OptionalAppFakhri {
    public static void main(String[] args) {

        Customer customer = new Customer("Fakhri", null);
        //proses logic
        String customerName = customer.getName().toLowerCase();
        System.out.println(customerName);

        if (customer.getEmail() != null) {
            System.out.println(customer.getEmail().toLowerCase());
        } else {
            System.out.println("Customer tidak punya email");
        }

        System.out.println("---");
        Optional<String> email = Optional.ofNullable(customer.getEmail());
        if (email.isPresent()) {
            System.out.println(email.get().toLowerCase());
        } else {
            System.out.println("Customer tidak punya email");
        }

        System.out.println("---");
        String emailB = Optional.ofNullable(customer.getEmail())
                .map(String::toLowerCase)
                .orElse("Customer tidak punya email");
        System.out.println(emailB);

        System.out.println("---");
        Customer customerB = null;
        String s = Optional.ofNullable(customerB)
                .map(c -> c.getAddress())
                .map(a -> a.getCity())
                .orElse("User not exist");

        System.out.println(s);
    }
}
