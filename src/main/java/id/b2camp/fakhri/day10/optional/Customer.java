package id.b2camp.fakhri.day10.optional;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Customer {

    private String name;
    private String email;
    private Address address;

    public Customer(String name, String email, Address address) {
        this.name = name;
        this.email = email;
        this.address = address;
    }

    public Customer(String name, String email) {
        this.name = name;
        this.email = email;
    }
}
