package id.b2camp.fakhri.day10.lambda;

public class FakhriLambdaApp {
    public static void main(String[] args) {

        printMessage(new AdditionalMessage() {
            @Override
            public void print(String prefix, String suffix) {
                System.out.println(prefix + " Test Print dengan anonymous class" + suffix);
            }
        });

        printMessage((p, s) -> {
            System.out.println(p + "Test print dengan lambda");
        });

        printMessage((p, s) -> System.out.println("Test print lambda"));

        printMessage2(FakhriLambdaApp::print);
    }

    private static void printMessage(AdditionalMessage additionalMessage) {
        additionalMessage.print("Message ", " !");
    }

    private static void printMessage2(AdditionalMessageWithoutParameter additionalMessage) {
        additionalMessage.print();
    }

    private static void print() {
        System.out.println("Test method reference");
    }
}
