package id.b2camp.fakhri.day10.lambda;

public interface AdditionalMessage {

    void print(String prefix, String suffix);
}
