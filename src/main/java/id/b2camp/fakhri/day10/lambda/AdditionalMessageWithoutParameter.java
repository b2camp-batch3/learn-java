package id.b2camp.fakhri.day10.lambda;

public interface AdditionalMessageWithoutParameter {
    void print();
}
