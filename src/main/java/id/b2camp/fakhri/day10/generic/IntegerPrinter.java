package id.b2camp.fakhri.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class IntegerPrinter {

    private Integer value;

    public void print() {
        System.out.println(value);
    }
}
