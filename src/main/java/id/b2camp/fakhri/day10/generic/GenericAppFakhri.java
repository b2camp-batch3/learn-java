package id.b2camp.fakhri.day10.generic;

public class GenericAppFakhri {
    public static void main(String[] args) {

        IntegerPrinter integerPrinter = new IntegerPrinter(1);
        integerPrinter.print();
        DoublePrinter doublePrinter = new DoublePrinter(2.0);
        doublePrinter.print();

        System.out.println("---");
        NumericPrinter<Integer> integerNumericPrinter = new NumericPrinter<>(1);
        NumericPrinter<Double> doubleNumericPrinter = new NumericPrinter<>(2.0);
        NumericPrinter<Long> longNumericPrinter = new NumericPrinter<>(4L);
        integerNumericPrinter.print();
        doubleNumericPrinter.print();
        longNumericPrinter.print();

        System.out.println("---");
        CashPaymentType cashPaymentType = new CashPaymentType(20000.0);
        Transaction<CashPaymentType> paymentTypeTransaction = new Transaction<>(cashPaymentType);
        paymentTypeTransaction.order();

        EWalletPaymentType eWalletPaymentType = new EWalletPaymentType(300000.0);
        Transaction<EWalletPaymentType> eWalletPaymentTypeTransaction = new Transaction<>(eWalletPaymentType);
        eWalletPaymentTypeTransaction.order();
    }
}
