package id.b2camp.fakhri.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DoublePrinter {

    private Double value;

    public void print() {
        System.out.println(value);
    }
}
