package id.b2camp.fakhri.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Transaction<T extends PaymentType> {

    private T paymentType;

    public void order() {
        System.out.println("Your order being processed with " + paymentType);
    }
}
