package id.b2camp.fakhri.day10.generic;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CashPaymentType extends PaymentType {

    public CashPaymentType(Double amount) {
        super(amount);
    }
}
