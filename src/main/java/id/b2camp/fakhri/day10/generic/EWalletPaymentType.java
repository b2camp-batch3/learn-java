package id.b2camp.fakhri.day10.generic;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EWalletPaymentType extends PaymentType {
    public EWalletPaymentType(Double amount) {
        super(amount);
    }
}
