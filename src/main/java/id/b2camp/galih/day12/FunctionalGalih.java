package id.b2camp.galih.day12;

import java.util.List;
import java.util.SortedSet;
import java.util.stream.Collectors;

public class FunctionalGalih {
    public static void main(String[] args) {

        List<String> heroes = List.of("Alice", "Nana", "Valir", "Eudora", "Vale", "Kadita", "Cecilion");

        if (heroes.contains("Vale")) {
            System.out.println("di temukan");
        } else {
            System.out.println("tidak di temukan");
        }

        System.out.println(
                heroes.stream()
                        .filter(hero -> hero.length() == 6)
                        .map(String::toUpperCase)
                        .collect(Collectors.joining(", "))
        );
    }
}
