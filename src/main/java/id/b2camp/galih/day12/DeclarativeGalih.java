package id.b2camp.galih.day12;

import java.awt.*;
import java.util.List;

public class DeclarativeGalih {
    public static void main(String[] args) {

        List<String> hero = List.of("Balmond", "Miya", "Layla", "Nana", "Yin", "Hanabi", "Fanny");

        if (hero.contains("Miya")) {
            System.out.println("Miya found");
        } else {
            System.out.println("Miya not found");
        }
    }
}
