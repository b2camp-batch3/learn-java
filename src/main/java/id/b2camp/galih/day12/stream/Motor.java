package id.b2camp.galih.day12.stream;

import lombok.*;

import java.lang.annotation.Target;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class Motor {

    private String merk;
    private String jenisMotor;
    private int ccMotor;
    private Transmission transmission;
    private BigDecimal price;
}
