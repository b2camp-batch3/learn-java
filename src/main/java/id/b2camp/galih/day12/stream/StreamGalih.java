package id.b2camp.galih.day12.stream;

import id.b2camp.fakhri.day12.stream.Gender;
import id.b2camp.fakhri.day12.stream.Person;

import java.awt.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class StreamGalih {
    public static void main(String[] args) {

        List<Motor> motors = getMotor();

        List<Motor> manualKendaraan = new ArrayList<>();
        for (Motor motor1 : motors) {
            if (motor1.getTransmission().equals(Transmission.MANUAL)) {
                manualKendaraan.add(motor1);
            }
        }
        manualKendaraan.forEach(System.out::println);

        System.out.println(" ==== ");
        List<Motor> maticKendaraan = motors.stream()
                .filter(motor -> motor.getTransmission().equals(Transmission.MATIC))
                .collect(Collectors.toList());
        maticKendaraan.forEach(System.out::println);

        System.out.println(" === ");
        List<Motor> smallestToBig = motors.stream()
                .sorted(Comparator.comparing(Motor::getCcMotor).reversed().thenComparing(Motor::getCcMotor).reversed())
                .collect(Collectors.toList());
        smallestToBig.forEach(System.out::println);

        System.out.println(" === ");
        boolean overThan135 = motors.stream()
                .allMatch(motor -> motor.getCcMotor() < 135);
        System.out.println(overThan135);

        System.out.println(" === ");
        List<Motor> tight = motors.stream()
                .filter(motor -> motor.getCcMotor() > 125)
                .collect(Collectors.toList());
        tight.forEach(System.out::println);

        System.out.println(" === ");
        BigDecimal jumlahCcMotor = motors.stream()
                .filter(motor -> motor.getCcMotor() > 100)
                .map(Motor::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(jumlahCcMotor);


        System.out.println(" === ");
        List<Motor> merkKendaraan = motors.stream()
                .filter(motor -> motor.getMerk().equals("honda"))
                .collect(Collectors.toList());
        merkKendaraan.forEach(System.out::println);

        System.out.println(" ============ ");
        Map<Transmission,List<Motor>> jenisInTransmission = motors.stream()
                .collect(Collectors.groupingBy(Motor::getTransmission));

        jenisInTransmission.forEach(((transmission, motors1) -> {
            System.out.println(transmission);
            motors1.forEach(System.out::println);
            System.out.println();
        }));

    }



   static List<Motor> getMotor(){
        return List.of(
                new Motor("honda", "revo", 110, Transmission.MANUAL,BigDecimal.valueOf(100000)),
                new Motor("yamaha", "nMax", 155, Transmission.MATIC, BigDecimal.valueOf(150000)),
                new Motor("yamaha", "Aerox", 165, Transmission.MATIC, BigDecimal.valueOf(2000000)),
                new Motor("honda", "Supra ", 135, Transmission.MANUAL, BigDecimal.valueOf(2500000)),
                new Motor("suzuki", "gsxR", 125, Transmission.MANUAL, BigDecimal.valueOf(3000000)),
                new Motor("suzuki", "Vstrom", 185, Transmission.MANUAL,BigDecimal.valueOf(3500000))
        );
    }
}
