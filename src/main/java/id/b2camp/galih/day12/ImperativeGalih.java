package id.b2camp.galih.day12;

import java.util.List;
import java.util.Locale;

public class ImperativeGalih {
    public static void main(String[] args) {

        List<String> equipments = List.of("Obeng", "Palu", "Paku Beton","Tang", "Gergaji", " Bor mesin");

        boolean isFound = false;

        for (String equipment : equipments){
            if (equipment.equals("Obeng")) {
                isFound = true;
                break;
            }
        }

        if (isFound) {
            System.out.println("Obeng di temukan");
        } else {
            System.out.println(" Obeng tidak di temukan");
        }

        for (String equipment : equipments) {
            if (equipment.length() == 5) {
                System.out.println(equipment.toUpperCase() + ", ");
            }
        }
        System.out.println(" === ");
    }
}
