package id.b2camp.galih.day11;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Set {
    public static void main(String[] args) {

        HashSet<String> motorParts = new HashSet<>(List.of(" Oli ", " Ban ", " Shock ", " Aki Motor ", " Kampas Rem "));
        System.out.println(motorParts);

        List<String> duplicatedSparePartMotor = new ArrayList<>(List.of(" Aki ", " Spion ", " Aki "," Lampu Rem ", " Spion ", " Lampu Rem "));
        System.out.println(duplicatedSparePartMotor);

        java.util.Set<String> unduplicatedSet = new HashSet<>(duplicatedSparePartMotor);
        System.out.println(unduplicatedSet);

    }
}
