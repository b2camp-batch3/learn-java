package id.b2camp.galih.day11;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LinkedListGalih {
    public static void main(String[] args) {

        /**
         *Array List lebih cepat untuk operasi GET data, look up DATA
         *Linked List lebih cepat untuk operasi ADD, REMOVE and UPDATE
         **/

        List<Integer> speedLinkedList = new LinkedList<>();
        int counter = 5;
        for (int i = 0; i < 50; i++) {
            speedLinkedList.add(counter++);
        }
        System.out.println(speedLinkedList);
        speedLinkedList.get(30);
        speedLinkedList.add(30, 35);

        List<Integer> numbersArrayList = new ArrayList<>();
        int counter2 = 1;
        for (int i = 0; i < 100; i++) {
            numbersArrayList.add(counter2++);
        }
        System.out.println(numbersArrayList);
        numbersArrayList.get(50);
        numbersArrayList.add(50, 55);

    }
}
