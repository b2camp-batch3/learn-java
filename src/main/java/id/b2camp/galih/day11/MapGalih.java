package id.b2camp.galih.day11;

import java.util.*;

public class MapGalih {
    public static void main(String[] args) {

        Map<Integer, String> fruit = new HashMap<>();
        fruit.put(1, " Anggur ");
        fruit.put(2, " Apel ");
        fruit.put(3, " Buah Naga ");
        fruit.put(4, " Cerry ");

        System.out.println(fruit);
        System.out.println(fruit.get(2));
        System.out.println(fruit.get(3));

        HashMap<String, List<String>> drupe = new HashMap<>();
        ArrayList<String> buahBerbiji = new ArrayList<>(List.of(" Semangka ", " Anggur ", " Pepaya "));
        ArrayList<String> buahBerbiji2= new ArrayList<>(List.of(" Jambu Biji ", " Markisa ", " Sawo "));
        drupe.put("kel 1", buahBerbiji);
        drupe.put("kel 2", buahBerbiji2);
        System.out.println(drupe);

        System.out.println("---");
        drupe.put("kel 3", List.of(" Pisang ", " jeruk "));
        System.out.println(drupe);

        Map<String, String> buah = new HashMap<>();
        buah.put("ab", "nanas");
        buah.put("cd", "salak");
        buah.put("ef", "rambutan");
        System.out.println(buah);
    }
}
