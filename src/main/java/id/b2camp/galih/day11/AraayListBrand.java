package id.b2camp.galih.day11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AraayListBrand {
    public static void main(String[] args) {


        String [] brand = new String[5] ;
        brand [0] = " Baju ";
        brand [1] = " Sepatu ";
        System.out.println(Arrays.toString(brand));

        System.out.println(" ==== ");
        List<String> brandList = new ArrayList<>();
        brandList.add(" Celana ");
        brandList.add(" Jam ");
        brandList.add(" Kaos Kaki ");
        brandList.add(" Jeans ");
        System.out.println(Arrays.toString(brandList.toArray()));
        brandList.add(" Tas ");
        System.out.println(Arrays.toString(brandList.toArray()));
        brandList.remove(" Jam ");
        System.out.println(Arrays.toString(brandList.toArray()));


        System.out.println(" === ");
        List<String> brandList2 = new ArrayList<>();
        brandList2.addAll(brandList);
        System.out.println(Arrays.toString(brandList2.toArray()));
        brandList2.removeAll(brandList2);
        System.out.println(Arrays.toString(brandList2.toArray()));


        List<String> kota = new ArrayList<>(List.of(" Depok ", " Jakarta"));
        kota.add(" Bogor ");
        kota.add(" Bandung ");
        System.out.println(Arrays.toString(kota.toArray()));
        System.out.println(kota.get(2));
        System.out.println(kota.size());

    }
}

