create SCHEMA galih_department CASCADE;
CREATE SCHEMA galih_depertement;
CREATE TABLE galih_depertement.locations (
    location_id SERIAL PRIMARY KEY,
    street_address VARCHAR(100),
    postal_code VARCHAR(10),
    city VARCHAR(100),
    state_province VARCHAR(100),
    country_id INTEGER
);

CREATE TABLE galih_depertement.regions (
    region_id SERIAL PRIMARY KEY,
    region_name VARCHAR(100)
);

CREATE TABLE galih_depertement.countries (
    country_id SERIAL PRIMARY KEY,
    country_name VARCHAR(100)
);
CREATE TABLE galih_depertement.departments (
    department_id SERIAL PRIMARY KEY,
    department_name VARCHAR(100),
    manager_id INTEGER,
    location_id INTEGER,
    FOREIGN KEY (location_id) REFERENCES galih_depertement.locations (location_id)
);
CREATE TABLE galih_depertement.jobs (
    job_id SERIAL PRIMARY KEY,
    job_title VARCHAR(100),
    min_salary NUMERIC(10, 2),
    max_salary NUMERIC(10, 2)
);
CREATE TABLE galih_depertement.employees (
    employee_id SERIAL PRIMARY KEY,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    email VARCHAR(100),
    phone_number VARCHAR(20),
    hire_date DATE,
    job_id INTEGER,
    salary NUMERIC(10, 2),
    manager_id INTEGER,
    department_id INTEGER,
    FOREIGN KEY (job_id) REFERENCES galih_depertement.jobs (job_id),
    FOREIGN KEY (manager_id) REFERENCES galih_depertement.employees (employee_id),
    FOREIGN KEY (department_id) REFERENCES galih_depertement.departments (department_id)
);
ALTER TABLE galih_depertement.locations
ADD CONSTRAINT fk_country
FOREIGN KEY (country_id)
REFERENCES galih_depertement.countries (country_id);

-- Add a foreign key constraint to the region_id column
ALTER TABLE galih_depertement.locations
ADD CONSTRAINT fk_region
REFERENCES galih_depertement;

DROP TABLE galih_depertement.regions;
drop table galih_depertement.locations cascade;

CREATE TABLE galih_depertement.regions (
    region_id SERIAL PRIMARY KEY,
    region_name VARCHAR(100)
);
CREATE TABLE galih_depertement.locations (
    location_id SERIAL PRIMARY KEY,
    street_address VARCHAR(100),
    postal_code VARCHAR(10),
    city VARCHAR(100),
    state_province VARCHAR(100),
    country_id INTEGER REFERENCES galih_depertement.countries (country_id),
    region_id INTEGER REFERENCES galih_depertement.regions (region_id)
);
CREATE TABLE galih_depertement.jobs_history (
    job_history_id SERIAL PRIMARY KEY,
    employee_id INTEGER,
    start_date DATE,
    end_date DATE,
    job_id INTEGER,
    department_id INTEGER,
    FOREIGN KEY (employee_id) REFERENCES galih_depertement.employees (employee_id),
    FOREIGN KEY (job_id) REFERENCES galih_depertement.jobs (job_id),
    FOREIGN KEY (department_id) REFERENCES galih_depertement.departments (department_id)
);
ALTER TABLE galih_depertement.departments
ADD CONSTRAINT fk_location
FOREIGN KEY (location_id)
REFERENCES galih_depertement.locations (location_id);

ALTER TABLE galih_depertement.employees
DROP COLUMN manager_id;

INSERT INTO galih_depertement.jobs (job_id, job_title, min_salary, max_salary)
VALUES (1, 'Manager', 50000.00, 80000.00),
       (2, 'Analyst', 40000.00, 60000.00),
       (3, 'Clerk', 25000.00, 40000.00);

      SELECT job_id, job_title, min_salary, max_salary
FROM galih_depertement.jobs;

UPDATE galih_depertement.jobs
SET job_title='Manager', min_salary=5000000, max_salary=15000000
WHERE job_id=nextval('galih_depertement.jobs_job_id_seq'::regclass);

SELECT job_id, job_title, min_salary, max_salary
FROM galih_depertement.jobs;

UPDATE galih_depertement.jobs
SET job_title='Manager', min_salary=5000000, max_salary=15000000
WHERE job_id = 1;

UPDATE galih_depertement.jobs
SET job_title='analyst', min_salary=6000000, max_salary=20000000
WHERE job_id = 2;

UPDATE galih_depertement.jobs
SET job_title='clerk', min_salary=7000000, max_salary=17000000
WHERE job_id = 3;

INSERT INTO galih_depertement.employees
(first_name, last_name, email, phone_number, hire_date, job_id, salary, department_id)
VALUES
('Ranti', 'Putri', 'ranti@gmail.com', '08102536100', '2023-09-09', 1, 5000000, 2);

INSERT INTO galih_depertement.regions
(region_id, region_name)
VALUES(nextval('galih_depertement.regions_region_id_seq'::regclass), 'Jakarta');

INSERT INTO galih_depertement.regions
(region_id, region_name)
VALUES(nextval('galih_depertement.regions_region_id_seq'::regclass), 'Depok');

INSERT INTO galih_depertement.regions
(region_id, region_name)
VALUES(nextval('galih_depertement.regions_region_id_seq'::regclass), 'Bandung');

INSERT INTO galih_depertement.regions
(region_id, region_name)
VALUES(nextval('galih_depertement.regions_region_id_seq'::regclass), 'Banten');

SELECT region_id, region_name
FROM galih_depertement.regions;

INSERT INTO galih_depertement.countries
(country_id, country_name)
VALUES(nextval('galih_depertement.countries_country_id_seq'::regclass), 'Pasar Rebo');

INSERT INTO galih_depertement.countries
(country_id, country_name)
VALUES(nextval('galih_depertement.countries_country_id_seq'::regclass), 'Cimanggis');

INSERT INTO galih_depertement.countries
(country_id, country_name)
VALUES(nextval('galih_depertement.countries_country_id_seq'::regclass), 'Cimahi');

INSERT INTO galih_depertement.countries
(country_id, country_name)
VALUES(nextval('galih_depertement.countries_country_id_seq'::regclass), 'Pangandaran');

-- Memasukkan lokasi baru ke dalam tabel locations
INSERT INTO galih_depertement.locations
(location_id, street_address, postal_code, city, state_province, country_id, region_id)
VALUES(
    nextval('galih_depertement.locations_location_id_seq'::regclass),
    'Cipayung', -- street_address
    '108', -- postal_code
    'Jakarta Timur', -- city
    'DKI Jakarta', -- state_province
    1,  -- country_id
    1,   -- region_id
);

SELECT location_id, street_address, postal_code, city, state_province, country_id, region_id
FROM galih_depertement.locations;

SELECT department_id, department_name, manager_id, location_id
FROM galih_depertement.departments;

SELECT job_history_id, employee_id, start_date, end_date, job_id, department_id
FROM galih_depertement.jobs_history;

SELECT employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, department_id
FROM galih_depertement.employees;

SELECT job_id, job_title, min_salary, max_salary
FROM galih_depertement.jobs;

SELECT employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, department_id
FROM galih_depertement.employees;

SELECT region_id, region_name
FROM galih_depertement.regions;

SELECT country_id, country_name
FROM galih_depertement.countries;

-- DROP SCHEMA galih_depertement;

CREATE SCHEMA galih_depertement AUTHORIZATION b2camp_user;

SELECT e.employee_id, e.first_name, e.last_name, d.department_name
FROM galih_depertement.employees e
INNER JOIN galih_depertement.departments d ON e.department_id = d.department_id;

-- LEFT JOIN
SELECT e.employee_id, e.first_name, e.last_name, d.department_name
FROM galih_depertement.employees e
LEFT JOIN galih_depertement.departments d ON e.department_id = d.department_id;

-- RIGHT JOIN
SELECT e.employee_id, e.first_name, e.last_name, d.department_name
FROM galih_depertement.employees e
RIGHT JOIN galih_depertement.departments d ON e.department_id = d.department_id;

SELECT department_id, COUNT(employee_id) AS employee_count
FROM galih_depertement.employees
GROUP BY department_id;