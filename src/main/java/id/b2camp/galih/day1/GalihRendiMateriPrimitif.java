package id.b2camp.galih.day1;

public class GalihRendiMateriPrimitif {

    public static void main(String[] args) {
        /*
        Tipe byte minimum nilainya  -128
         maxsimum 127
         */
        System.out.println("=====Contoh tipe Data Byte=====");
        byte contohByteGalih = -125;
        System.out.println(contohByteGalih);



        /*
        Tipe data short mininum nilainya -32768
        maxsimum 32767
         */
        System.out.println("====Contoh tipe data Short===");
        short cobaDataGalihNilaiMinimum = -30000;
        System.out.println(cobaDataGalihNilaiMinimum);



        /*
         Tipe data integeer Nilai minimumnya -2147483648
         nilai maksimumnya 2147483647.
         */
        System.out.println("coba tipe data integer");
        int cobaNilaiIntGalih = 2000000;
        System.out.println(cobaNilaiIntGalih);
        System.out.println("cobaDataGalihNilaiMinus");
        int cobaDataGalihNilaiMinus = -1000000;
        System.out.println(cobaDataGalihNilaiMinus);



        /* tipe data long  nilai minimum -9223372036854775808
         nilai minimum 9223372036854775807
         */
        System.out.println("===coba Tipe Data Long Galih===");
        long cobaTipeDataLongGalih = 100000;
        System.out.println(cobaTipeDataLongGalih);
        System.out.println("===tipe data long yang minus===");
        long cobaTipeDataLongYangMinus = -100000;
        System.out.println(cobaTipeDataLongYangMinus);


        /* tipe data char atau disebut karakter
        menampung karakter apa saja dan hanya 1 karakter contoh ; huruf 'S'
         */
        char lakilaki = 'L';
        char perempuan = 'P';
        System.out.println(lakilaki);
        System.out.println(perempuan);


        /**
         * tipe data no primitif
         * unutk mencari nilai maximal anatar 2 bilangan
         */
        System.out.println("hasil mencari bilangan max dengan non primitif" + Long.max(10,40));


        /**
         * percobaan penambanhan bilangan non primitif
          */
        Long cobaTipePrimitifMengoperasikanOperator = Long.sum(10,50);
        System.out.println("Hasil dari penjumlahan dengan tipe data non primitif nya adalah  = " +cobaTipePrimitifMengoperasikanOperator);


        /**
         * percobaan penambahan bilangan non primitif
         */
        Long a1 = 100L;
        Long b1 = 1000L;
        Long hasilpenambahan = a1 + b1;
        System.out.println("Hasil pakai non primitif " +hasilpenambahan);
    }
}
