package id.b2camp.galih.day2;

public class SwitchCase {



    public static void main(String[] args) {
        Integer hari = 5;
        switch (hari){
            case 1:
                System.out.println("Hari ini Hari senin");
                break;
            case 2 :
                System.out.println("Hari ini hari selasa");
                break;
            case 3 :
                System.out.println("Hari ini hari rabu");
                break;
            case 4 :
                System.out.println("Hari ini Hari kamis");
                break;
            case 5:
                System.out.println("Hari ini hari jumat");
                break;
            case 6:
                System.out.println("Hari ini hari sabtu");
                break;
            case 7:
                System.out.println("Hari ini hari minggu");
                break;
            default:
                System.out.println("Error");
        }
    }
}
