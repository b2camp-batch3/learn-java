package id.b2camp.galih.day2;

public class ArayForLoopGalih {
    public static void main(String[] args) {

        /**
         * araay 2 dimensi
         * tipe datanya [] [] nama tipe data
         */

        int[][] hurufAngka = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9},
        };
        System.out.println("Coba Print 1); " + hurufAngka[0][2]);
        System.out.println("Coba Print 2 ); " + hurufAngka[1][3]);

        /**
         * araay 1 dimensi
         * tipe data [] nama tipe datanya
         */

        String[] namaKota = {"Depok", "Bogor", "Jakarta", "Bandung",};
        System.out.println("coba print  " + namaKota[3]);


        /**
         * pengulangan dengan metode for i
         */

        int angka1;
        int angka2 = 15;
        for (angka1 = 20; angka1 >= angka2; angka1--) {
            System.out.print(angka1);
        }


        /**
         * array metode pengulangan dengan for each
         */
        int[] nilaiUjian = {70, 80, 90, 100};
        for (int nilaiUjianSiswaLakiLaki : nilaiUjian) {
            System.out.println(nilaiUjianSiswaLakiLaki);
        }
    }
}
