package id.b2camp.galih.day2;

import java.util.Scanner;

public class Arithmatics {
    public static void main(String[] args) {

        /**
         * pembuatan rumus hitung luas persegi
         */

        Scanner input = new Scanner(System.in);
        System.out.println("======rumus luas persegi======");
        int panjang = input.nextInt();
        int lebar = input.nextInt();
        int luasPersegi = panjang * lebar;
        System.out.println("Total luas persegi adalah : " + luasPersegi);
    }
}
