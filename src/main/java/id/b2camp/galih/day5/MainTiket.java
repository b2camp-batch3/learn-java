package id.b2camp.galih.day5;

public class MainTiket {
    public static void main(String[] args) {
        TiketBus tiketBus = new TiketBus();
        tiketBus.setKotaTujuan( " Jepang ");
        tiketBus.setNamaPemilik(" Parto ");
        tiketBus.setNoKursi  ("8");
        tiketBus.cetakTiket();


        TiketPesawat tiketPesawat = new TiketPesawat();
        tiketPesawat.setKotaTujuan(" Malaysia ");
        tiketPesawat.setNamaPemilik(" Aryo ");
        tiketPesawat.setNoKursi( "5");
        tiketPesawat.cetakTiket();

    }
}
