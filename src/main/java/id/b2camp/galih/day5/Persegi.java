package id.b2camp.galih.day5;

public class Persegi extends BangunDatar{

    int sisi;

    public Persegi(int sisi) {this.sisi = sisi; }

    @Override
    float Luas() { return sisi * sisi; }

    }
