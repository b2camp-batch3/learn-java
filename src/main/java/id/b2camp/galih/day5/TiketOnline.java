package id.b2camp.galih.day5;

public class TiketOnline {

public String kotaTujuan;
public String noKursi;
public String namaPemilik;

public void cetakTiket(){
    System.out.println(" ============== ");
    System.out.println(" menampilkan struck tiket anda ");
    System.out.println(" ============== ");
}

    public String getKotaTujuan() {
        return kotaTujuan;
    }

    public void setKotaTujuan(String kotaTujuan) {
        this.kotaTujuan = kotaTujuan;
    }

    public String getNoKursi() {
        return noKursi;
    }

    public void setNoKursi(String noKursi) {
        this.noKursi = noKursi;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }
}
