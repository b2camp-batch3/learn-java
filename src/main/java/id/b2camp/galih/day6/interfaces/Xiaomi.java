package id.b2camp.galih.day6.interfaces;

public class Xiaomi implements PowerOn, PowerOff, Menu {



    @Override
    public String getMasukKeMenuLayar() {
        return( " All Aplikasi ");
    }

    @Override
    public String getTombolMati() {
        return (" Layar Handphone Mati");
    }

    @Override
    public String getTombolOn() {
        return (" Layar Handphone Hidup");
    }
}
