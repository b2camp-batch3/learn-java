package id.b2camp.galih.day6.accessmodifiers.layer1.layer2;

public class WartegBerkah {

    public String menu;
    public double price;


    public WartegBerkah(String menu, double price) {
        this.menu = menu;
        this.price = price;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
