package id.b2camp.galih.day6.accessmodifiers;

import id.b2camp.galih.day6.accessmodifiers.layer1.WartegJaya;
import id.b2camp.galih.day6.accessmodifiers.layer1.layer2.WartegBerkah;


public class FoodApp {
    public static void main(String[] args) {

        WartegJaya wartegJaya = new WartegJaya(" Ikan mas ", 15000);
        wartegJaya.setMenu(" Ayam Bakar");
        wartegJaya.setPrice(10000);

        System.out.println(wartegJaya.getMenu());
        System.out.println(wartegJaya.getPrice());


        WartegBerkah wartegBerkah = new WartegBerkah("Tahu Goreng ", 3000);
        wartegBerkah.setMenu("Tempe Bacem ");
        wartegBerkah.setPrice(4000);

        System.out.println(wartegBerkah.getMenu());
        System.out.println(wartegBerkah.getPrice());


    }
}
