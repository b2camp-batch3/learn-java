package id.b2camp.galih.day6.abstrackclasses;

public class Human extends Running {

    @Override
    public void trackingRunning() {
        System.out.println(" Track Human Running" );
    }
}
