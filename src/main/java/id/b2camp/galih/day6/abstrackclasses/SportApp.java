package id.b2camp.galih.day6.abstrackclasses;

public class SportApp {
    public static void main(String[] args) {

        Human human = new Human();
        human.setDistance(3);
        human.setTime(15);
        System.out.println(" human = " + human.getDistance() + "      " + human.getTime());
        human.trackingRunning();


    }
}
