package id.b2camp.galih.day6.abstrackclasses;

public abstract class Running {

    private double Distance;
    private double Time;

    public abstract void trackingRunning ();


    public double getDistance() {return Distance;}

    public void setDistance(double distance) {this.Distance = distance;}

    public double getTime() {return Time;}

    public void setTime(double time) {this.Time= time;}
}
