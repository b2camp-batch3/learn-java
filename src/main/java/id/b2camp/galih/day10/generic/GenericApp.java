
package id.b2camp.galih.day10.generic;

public class GenericApp {

    public static void main(String[] args) {
        Player<String> player = new Player<>(" Rodri ") ;
        Player<Integer> noPunggung = new Player<Integer>(15);

        String pemainBola = player.getNamaPemainClubManCity();
        Integer noPunggungPemainBola = noPunggung.getNamaPemainClubManCity();
        System.out.println(" Pemain Bola " + pemainBola + " nomor punggung " +  noPunggungPemainBola );
    }
}

