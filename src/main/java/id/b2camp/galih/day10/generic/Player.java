
package id.b2camp.galih.day10.generic;

public class Player <T> {

    private T namaPemainClubManCity;

    public Player(T namaPemainClubManCity) {
        this.namaPemainClubManCity = namaPemainClubManCity;
    }

    public T getNamaPemainClubManCity () {
        return namaPemainClubManCity;
    }

    public void setNamaPemainClubManCity (T namaPemainClubManCity){
        this.namaPemainClubManCity = namaPemainClubManCity;
    }
}

