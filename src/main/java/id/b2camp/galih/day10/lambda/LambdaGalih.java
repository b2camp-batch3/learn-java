
package id.b2camp.galih.day10.lambda;

import id.b2camp.rajip.day10.lambda.Total;

public class LambdaGalih {
    public static void main(String[] args) {


        RunApp distance = new RunApp() {
            @Override
            public double hitungJarak(double jarak, int waktu) {
                return ( jarak + waktu);
            }
        };
        System.out.println(distance.hitungJarak(2000, 10));


        // D
        RunApp distance2 = ((a, b) -> (a + b));
        System.out.println(distance2.hitungJarak(2500, 12));


    }
}

