package id.b2camp.galih.day9;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionAndAnnatationGalih {
    public static void main(String[] args) {


        Person nama = new Person("Galih", " Rendi");
        Field[] fields = nama.getClass().getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }


        System.out.println(" === ");
        Method[] declaredMethods = nama.getClass().getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println(declaredMethod);
        }


        System.out.println(" === ");
       if (nama.getClass().isAnnotationPresent(SuperImportant.class)) {
           System.out.println(" Super Important Person ");
       }

        for (Field declaredField : nama.getClass().getDeclaredFields()) {
            if (declaredField.isAnnotationPresent(ImportantPerson.class)) {
                System.out.println(declaredField + " is Important Person ");
            }
        }

        System.out.println(" ==== ");
        for (Method declaredMethod : nama.getClass().getDeclaredMethods()) {
            if (declaredMethod.isAnnotationPresent(WantToDo.class)) {
                WantToDo annotation = declaredMethod.getAnnotation(WantToDo.class);
                for (int i = 0; i < annotation.height(); i++) {
                    System.out.println(" want to do ");
                }
                }
            }

        System.out.println(" === ");
        PersonWithLombok personWithLombok = new PersonWithLombok(" Rian", 25);
        System.out.println(" Name Saya " + personWithLombok.getName());
        System.out.println(" Age " + personWithLombok.getAge());

    }
    }


