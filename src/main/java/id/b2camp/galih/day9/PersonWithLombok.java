package id.b2camp.galih.day9;


import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class PersonWithLombok {
    private String name;
    private Integer age;
}
