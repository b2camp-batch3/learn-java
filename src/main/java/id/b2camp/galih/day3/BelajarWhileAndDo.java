package id.b2camp.galih.day3;

public class BelajarWhileAndDo {
    public static void main(String[] args) {

        int angkaGenap = 0;
        do {
            System.out.println(angkaGenap);
        angkaGenap++;
        } while ( angkaGenap <= 10);



        int kekuatanSaya = 0;
        int batasKekuatanSaya = 50;
        while (kekuatanSaya < batasKekuatanSaya) {
            System.out.println(kekuatanSaya);
            kekuatanSaya++;
        }


    }
}
