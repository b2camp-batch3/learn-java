package id.b2camp.galih.day3;

public class AraayForLoop {
    public static void main(String[] args) {

        String[][] buah = {
                {"Mangga", "Pisang", "Apel", "Semangka"},
                {"Apel", "Jeruk", "Pepaya", "Rambutan"}
        };

        for (int a = 0; a < buah.length; a++) {
            for (int b = 0; b < buah.length; b++) {
                System.out.println(buah[a][b]);
            }
        }

            String[][] huruf = new String[][]{ {"A", "B", "C", "D",},
                                               {"E", "F", "G", "H",},
                                               {"I", "J", "K", "L",}};
            for (String[] C : huruf) {
                for (String e : C) {
                    System.out.print(e + ", ");
                }
            }

        System.out.println(" ");
            String [][] cobastrings2d = new String[][] {{" saya ", " download ", " game "}, {"mobile legend", "efootball",}, {"di playstore"}};
        for (String[] string1d : cobastrings2d) {
            for (String s : string1d) {
                System.out.println(s);
            }
        }

    }
}
