package id.b2camp.galih.day3;

public class Karyawan {

    public void memanggilMethodDiClassKaryawanTanpaParameter () {
        System.out.println(" tidak ada parameter di method ini");
    }

    public void memanggilMethodDenganParameter ( String nama,String alamat, String daerah) {
    nama = "Retno";
    alamat = "Depok";
    daerah = "Pekapuran";

        System.out.println( "method ini memanggil dengan parameter : " +nama + " dan alamat saya di " +alamat + " daerah saya " +daerah );

    }
}
