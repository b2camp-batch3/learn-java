package id.b2camp.galih.day7.anonymousclass;

public class ManchesterCityApp {
    public ManchesterCityApp() {
    }

    public static void main(String[] args) {

        Player player = new Player(){

            @Override
            public void playerPassedToPartner() {System.out.println(" Diterima Dengan Baik passing nya");}
            @Override
            public void playerKicksAtGoal() {System.out.println(" dan Gaoaalllllll ");}


        };

        player.playerKicksAtGoal();
        player.playerPassedToPartner();


        System.out.println(" ---------- ");
        PlayerFootball erlingHaaland = new PlayerFootball() {
            @Override
            public void shootingHard() {
                System.out.println(" Dengan Kaki kanan nya yang Terkuat ");

            }
        };
        erlingHaaland.shootingHard();


    }
}
