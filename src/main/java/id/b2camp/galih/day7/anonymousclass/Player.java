package id.b2camp.galih.day7.anonymousclass;

public interface Player {

    void playerKicksAtGoal();

    void playerPassedToPartner();

}
