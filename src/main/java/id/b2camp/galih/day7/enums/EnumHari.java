package id.b2camp.galih.day7.enums;

public class EnumHari {
    public static void main(String[] args) {

        Hari senin = Hari.SENIN;
        Hari selasa = Hari.SELASA;
        Hari rabu = Hari.RABU;
        System.out.println(senin);
        System.out.println(selasa);
        System.out.println(rabu);

        System.out.println(" ------ ");


        ListTiapHari listTiapHari = new ListTiapHari(" Lari ", 3);
        System.out.println(Hari.SENIN + listTiapHari.getKegiatan()) ;
        System.out.println(listTiapHari.getRecess());

        System.out.println(" ------ ");

        ListTiapHari listTiapHari2 = new ListTiapHari(" Main Bola", 1);
        System.out.println(Hari.SELASA + listTiapHari2.getKegiatan()) ;
        System.out.println(listTiapHari2.getRecess());

        System.out.println(" ----- ");

        ListTiapHari listTiapHari3 = new ListTiapHari(" Kumpul Keluarga", 2);
        System.out.println(Hari.RABU + listTiapHari3.getKegiatan()) ;
        System.out.println(listTiapHari3.getRecess());

    }
}
