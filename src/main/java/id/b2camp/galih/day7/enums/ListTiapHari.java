package id.b2camp.galih.day7.enums;

public class ListTiapHari {

    private String kegiatan;
    private double recess;

    public ListTiapHari(String kegiatan, double recess) {
        this.kegiatan = kegiatan;
        this.recess = recess;
    }

    public String getKegiatan() {
        return kegiatan;
    }

    public void setKegiatan(String kegiatan) {
        this.kegiatan = kegiatan;
    }

    public double getRecess() {
        return recess;
    }

    public void setRecess(double recess) {
        this.recess = recess;
    }
}
