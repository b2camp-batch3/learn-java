package id.b2camp.galih.day7.finalkey;

public class MadrasahAliyah {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


class LobbyUtama extends MadrasahAliyah {

    final void  adminitrasi(String admin) {
        System.out.println(admin);
    }
}

final class RuangGuru extends LobbyUtama {

}


