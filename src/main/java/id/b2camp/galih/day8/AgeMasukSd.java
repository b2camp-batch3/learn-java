package id.b2camp.galih.day8;

public class AgeMasukSd extends Age{

    public AgeMasukSd(String message) {
        super(message);
    }
    static Integer minimalAge( Integer age) {

        try {
            if (age < 6 ) {
                throw new Age(" Usia Kamu Belum Bisa Masuk SD ");
            }else if (age > 18) {
                throw new Age(" Usia Kamu Melebihi Batas ");
            }else if (age > 6 && age < 18) {
                System.out.println(" Kamu Bisa Daftar Di Formulir Berikut ");
            }
        }catch (Age a) {
            System.out.println(a.getMessage());
        }return age;
    }
}
