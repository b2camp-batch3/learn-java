package id.b2camp.galih.day8;

public class MainAritmatic {
    public static void main(String[] args) {
        /*
        Exception jika nilai A melebihi MIN VALUE data integer
         */

        Aritmatic aritmatic = new Aritmatic();
        ValidateNumber validateNumber = new ValidateNumber();
        aritmatic.setA(-999999998);
        aritmatic.setB(10000000);
        validateNumber.pembagian(aritmatic.a, aritmatic.b);

        /*
        Exception apabila nilai A sama dengan 0
         */
        Aritmatic aritmatic1 = new Aritmatic();
        aritmatic1.setA(0);
        aritmatic1.setB(1000);
        validateNumber.pembagian(aritmatic1.a, aritmatic1.b);


    }
}
