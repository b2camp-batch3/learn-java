package id.b2camp.fanri.day11;


import java.util.*;

public class MapFanriApp {
    public static void main(String[] args) {
        //Analogi Hashmap itu seperti lemari yang ada key dan isi velue-nya
        //Key-nya sebelah kiri diamond/Generic, valuenya sebalah kanan atau tipe data
        Map<Integer, String> listPersibSquad = new HashMap<>();
        listPersibSquad.put(1, "Teja Paku Alam");
        listPersibSquad.put(2, "Nick Kuipers");
        listPersibSquad.put(7, "Beckham Putra");
        listPersibSquad.put(10, "Mark Klok");

        System.out.println(listPersibSquad);
        //Untuk mengambil data, diinput key-nya.
        System.out.println(listPersibSquad.get(7));


        //store LinkedList di dalam Map, ada diamond di dalam diamond
        System.out.println("\n*** LinkedList di dalam Map ***");
        System.out.println("================================");
        HashMap<String, List<String>> posisiPemain = new HashMap<>();
        ArrayList<String> strikerPersib = new ArrayList<>(List.of("Ciro Alves", "David Da Silva", "Ezra Walian"));
        ArrayList<String> playMakerPersib = new ArrayList<>(List.of("Mark Klok", "Beckham Putra", "Putu Gede"));
        posisiPemain.put("FW", strikerPersib);
        posisiPemain.put("MF", playMakerPersib);
        System.out.println(posisiPemain);
        System.out.println(posisiPemain.get("FW"));
        System.out.println(posisiPemain.get("MF"));

        //mengganti data dengan put dan replace
        /*put dan replace sama-sama mengganti data, tapi kalau put dimana data key-nya
         tidak benar maka akan menambah data  baru, sedangkan replace tidak menambah data baru  */
        posisiPemain.put("FW", List.of("Peri Sandria", "Asep Dayat"));
        System.out.println(posisiPemain);
        posisiPemain.replace("FW", List.of("Ronaldo", "Messi"));
        System.out.println(posisiPemain);

        System.out.println(posisiPemain.size());

        //ambil semua ker dengan entrySet
        System.out.println("-----");
        System.out.println(posisiPemain.entrySet());
        System.out.println(posisiPemain.values());

    }
}
