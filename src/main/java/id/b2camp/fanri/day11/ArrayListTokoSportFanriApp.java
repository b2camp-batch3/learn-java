package id.b2camp.fanri.day11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListTokoSportFanriApp {

    public static void main(String[] args) {
        //Array sederhana
        System.out.println("*** Array biasa ***");
        System.out.println("===================");
        String[] typeOfGoods = new String[5];
        typeOfGoods[0] = "Sepatu Bola";
        typeOfGoods[1] = "Raket Badminton";
        typeOfGoods[2] = "Baju Renang";
        System.out.println(Arrays.toString(typeOfGoods));


        //Arraylist
        //dengan ArrayList tidak perlu mencantuman size untuk penambahan data krn sudah dynamic
        List<String> footballGears = new ArrayList<>();

        //Operasi penambahan dengan .add
        System.out.println("\n *** ArrayList ***");
        System.out.println("===================");
        System.out.println("add");
        footballGears.add("Bola sepak");
        footballGears.add("Sepatu bola");
        footballGears.add("Dekker lutut");
        footballGears.add("Kaos kaki panjang");
        System.out.println(footballGears);
        footballGears.add("Kaos bola");
        footballGears.add("Calana bola");
        footballGears.add("Sarung tangan kiper");
        System.out.println(footballGears);

        //Operasi edit/merubah dengan .set
        System.out.println("\nset");
        footballGears.set(2, "Dekker pelindung kaki");
        System.out.println(footballGears);

        //Operasi mengakses data dengan .get
        System.out.println("\nget");
        String buyOne = new String(footballGears.get(3));
        System.out.println(buyOne);

        //Operasi hapus dengan .remove
        System.out.println("\nremove");
        footballGears.remove(5);
        System.out.println(footballGears);





    }
}
