package id.b2camp.fanri.day11;

import java.util.*;

public class SetFanriApp {
    //untuk set jika ada value atau data yg sama maka otomatis akan di override/ditimpa/di-duplicate
    //kalau datanya tidak mau di duplicate/ditimpa bisa menggunakan Arraylist
    //secara urutan data random
    public static void main(String[] args) {
        Set<String> guitarHeroes = new HashSet<>(List.of("Nuno Bettencourt", "Nuno Bettencourt", "Paul Gilbert", "Tim Henson", "Dewa Bujana"));
        System.out.println(guitarHeroes);

        //mengurutkan data agar berurutan
        Set<String> TreeOfguitarHeroes = new TreeSet<>(List.of("Nuno Bettencourt", "Nuno Bettencourt", "Paul Gilbert", "Tim Henson", "Dewa Bujana"));
        System.out.println(TreeOfguitarHeroes);

        //menggunakan ArrayList agar datanya tidak di duplicate/ditimpa
        List<String> guitarHeroes2 = new ArrayList<>(List.of("Nuno Bettencourt", "Nuno Bettencourt", "Paul Gilbert", "Tim Henson", "Dewa Bujana"));
        System.out.println(guitarHeroes2);


    }
}
