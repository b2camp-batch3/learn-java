package id.b2camp.fanri.day11;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LinkedListFanriApp {
    public static void main(String[] args) {
        //LinkedList perhitungannya berurutan indexnya, cth : index 1 punya ref index 2, 3 ref 4 dst...
        //LinkedList untuk get data sequential, cth mw ambil data ke 100, dicari mulai index 0 hingga 100 (lebih lama)
        //LinkedList biasanya dipakai untuk penambahan atau remove data dimana datanya sangat banyal
        List<Integer> stadiumSeats = new LinkedList<>();
        int counter = 0;
        for (int i = 0; i <= 25000; i++) {
            stadiumSeats.add(counter++);
        }
        System.out.println(stadiumSeats);
        System.out.println(stadiumSeats.get(12500));
        stadiumSeats.add(13000, 13);
        System.out.println(stadiumSeats.get(13000));


//        //Tes kecepatan get data dengan ArrayList (lebih cepat untuk operasi get)
          //Arraylist kalau penambahan/remove data baru, dia membuat object baru, makanya jadi lebih lama
//        ArrayList<Object> stadiumSeatsArrayList = new ArrayList<>();
//        int counterArrayList = 0;
//        for (int i = 0; i <= 25000; i++) {
//            stadiumSeatsArrayList.add(counterArrayList++);
//        }
//        System.out.println(stadiumSeatsArrayList);
//        System.out.println(stadiumSeatsArrayList.get(12500));



    }

}
