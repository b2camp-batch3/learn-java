package id.b2camp.fanri.day5;

/*
    POLYMORPHISYM
    : Memiliki banyak bentuk dimana sebuah object bisa berasal/berbentuk berubah-ubah

 */

class Soldier {
    String name;

    Soldier(String name) {
        this.name = name;
    }

    void display() {
        System.out.println("\nName \t: " + this.name);
    }

    // Penggunaan Polymorphism bisa dilihat pada method attack ini
    // Apapun yg memiliki tipe Super class Soldier -> maka GreenBeret, Sniper, Soldier, Spy
    // dapat dipakai meskipun berbeda tipe
    void attack(Soldier enemy) {
        System.out.println(this.name + " Attack! -> " + enemy.name);
    }

}

class GreenBeret extends Soldier{

    String type = "Fighter";

    GreenBeret(String name) {
        super(name);
    }
    @Override
    void display() {
        super.display();
        System.out.println("Type \t: " + this.type);
    }

}

class Spy extends Soldier{
    String type = "Scout and Spy the enemy territory ";

    Spy(String name) {
        super(name);
    }
    @Override
    void display() {
        super.display();
        System.out.println("Type \t: " + this.type);
    }

}

class Sniper extends Soldier {
    String type = "Sniper or Shooter";

    Sniper(String name) {
        super(name);
    }
    @Override
    void display() {
        super.display();
        System.out.println("Type \t: " + this.type);
    }

}


public class MiniGameTheCommandosFanri {

    public static void main(String[] args) {
        Soldier soldier1 = new Soldier("Commandos 1");
        GreenBeret soldier2 = new GreenBeret("Tiny");

        soldier1.display();
        soldier2.display();

        // Polymorphic
        // Tipe data : Soldier, Object : GreenBeret
        Soldier soldier3 = new GreenBeret("Fins");
        soldier3.display();

        Spy soldier4 = new Spy("Frenchy");
        soldier4.display();

//        // Contoh salah (1) di bawah yg tidak bisa dipakai :
//        // Spy adalah seorang Soldier, Tapi Soldier belum tentu seorang Spy
//        Spy soldier4 = new Soldier("Frenchy");
//        soldier4.display();

//        // Contoh salah (2) di bawah tidak bisa dipakai:
//        // Beda tipe dimana Sniper tidak meng-inherit dari Spy
//        Spy soldier4 = new Sniper("Frenchy");
//        soldier4.display();


        // Array List
        Soldier[] groupOfSoldier = new Soldier[3];
        groupOfSoldier[0] = soldier1;
        groupOfSoldier[1] = soldier2;
        groupOfSoldier[2] = soldier3;

        groupOfSoldier[0].display();
        groupOfSoldier[1].display();
        groupOfSoldier[2].display();

        System.out.println("==========================\n");

        // Attack Method in action
        // soldier1 bisa attack soldier 2, 3, 4 meskipun berbeda tipe (GreenBeret/Spy/Sniper)
        soldier1.attack(soldier2);
        soldier1.attack(soldier3);
        soldier2.attack(soldier4);
        soldier3.attack(soldier1);

    }
}
