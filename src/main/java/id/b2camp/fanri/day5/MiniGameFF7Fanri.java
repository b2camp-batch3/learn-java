package id.b2camp.fanri.day5;
/*
    INHERITANCE
    Di mini game ini diterapkan OOP inheritance dengan Super/Parent Class : Hero (Object 1) &
    Sub/Child Class : ArmoredHero (Object 2).
    Kedua object tsb. akan saling menyerang dan mengurangi life dari damage yang didapat.
 */

class Hero  {
    // atribute
    String name;
    double attack, life;

    //constructor
    Hero(String nameInput, double attackInput, double lifeInput) {
        this.name = nameInput;
        this.attack = attackInput;
        this.life = lifeInput;
    }

    // method attack
    void attack(Hero enemy) {
        System.out.println("\n******  FIGHT !!! ****** \n" + this.name + " attacking " + enemy.name);
        enemy.takeDamage(this.attack);
    }

    void takeDamage(double damage) {
        System.out.println(this.name + " received damage " + damage);
        this.life = this.life - damage;
    }

    void display() {
        System.out.println("\nHero Name\t = " + this.name);
        System.out.println("Life\t\t = " + this.life);
        System.out.println("Attack Power = " + this.attack);
    }
}

// inherit dari Hero
class ArmoredHero extends Hero {
    String type = "Defense type";
    // subclass constructor
    ArmoredHero(String nameInput, double attackInput, double lifeInput) {
        super(nameInput, attackInput, lifeInput);
    }

    /*Anotasi @Override digunakan diatas metode yg meng override method di Superclass,
      Jika method tidak cocok dgn sebuah method di superclass, maka akan error  */
    // Ovverride menambah Tipe atau karakter Armored Hero
    @Override
    void display() {
        super.display();
        System.out.println("Type \t\t = " + this.type);
    }

    /* Override armor : tambahan untuk defense (menahan serangan dengan memangkas serangan lawan
       menjadi separuh dari Attack Power lawan */
    @Override
    void takeDamage(double damage) {
        System.out.println("Armor " + this.name + " is active, " + this.name +  " only received half damage " +  damage + " -> " + 0.5 * damage);
        this.life = this.life - (0.5 * damage);
    }


}

public class MiniGameFF7Fanri {
    public static void main(String[] args) {
        // menggunakan super constructor
        Hero hero1 = new Hero("Cloud", 75, 100);
        ArmoredHero hero2 = new ArmoredHero("Barret", 45, 125);

        hero1.display();
        System.out.print("\n       VS \n");
        hero2.display();

        // menggunakan super method
        hero1.attack(hero2);
        hero2.attack(hero1);

        System.out.println("\n\nCurrent heroes status :");
        hero1.display();
        hero2.display();


    }
}
