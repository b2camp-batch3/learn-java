package id.b2camp.fanri.day9;

@Mandatory //Annotation dipaka di CLASS PRIORITY
public class Priority {

    //Kalau Anotationnya FIELD maka disini dipakainya
    @MandatoryProcedure
    private String safetyProcedure, safetyCheckTime;

    public Priority(String safetyProcedure, String safetyCheckTime) {
        this.safetyProcedure = safetyProcedure;
        this.safetyCheckTime = safetyCheckTime;
    }

    @MandatoryAirAndGroundCrew(times = 3)
    // tes tambahan method safetyInTheAir
    public void safetyInTheAir() {
        System.out.println("Remove before flight");
    }
    @MandatoryAirAndGroundCrew(times = 3)
    public void safetyOnGround() {
        System.out.println("Watch all signs in the Apron");
    }

    public String getSafetyProcedure() {
        return safetyProcedure;
    }

    public void setSafetyProcedure(String safetyProcedure) {
        this.safetyProcedure = safetyProcedure;
    }

    public String getSafetyCheckTime() {
        return safetyCheckTime;
    }

    public void setSafetyCheckTime(String safetyCheckTime) {
        this.safetyCheckTime = safetyCheckTime;
    }
}
