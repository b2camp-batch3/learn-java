package id.b2camp.fanri.day9;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MandatoryAirAndGroundCrew {

        int times() default 1;
//        String desc() default "string";
}
