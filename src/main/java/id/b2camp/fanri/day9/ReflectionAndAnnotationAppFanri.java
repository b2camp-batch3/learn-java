package id.b2camp.fanri.day9;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

public class ReflectionAndAnnotationAppFanri {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {

        System.out.println("\n==========================");
        System.out.println("*** THIS IS REFLECTION ***");
        System.out.println("==========================");
        // REFLECTION, contoh dibawah untuk melihat ada fields dan methods apa saja
        Priority safetyCheck = new Priority("Safety gear", "Every time before entering the Air-side of the airport");
        Field[] fields = safetyCheck.getClass().getDeclaredFields();   //mirror-ing fields (untuk tau ada field apa saja)
        for (Field field : fields) {
            System.out.println(field);
        }
        //mirror-ing method (Untuk mengetahui ada method apa saja : methodnya didapat dr file Priority)
        Method[] methods = safetyCheck.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        // ANNOTATION : Metadata atau informasi tambahan yg berjalan di dlm class/method/properties
        System.out.println("\n==========================");
        System.out.println("*** THIS IS ANNOTATION ***");
        System.out.println("==========================");
        if(safetyCheck.getClass().isAnnotationPresent(Mandatory.class)) {
            System.out.println("This is Mandatory Class shown by Annotation");
        }

        //Anotasi diterapkan di Field
        for (Field field : safetyCheck.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(MandatoryProcedure.class)) {
                field.setAccessible(true);
                Object objectValue = field.get(safetyCheck);
                System.out.println(objectValue + " is mandatory before entering the Air-side");
            }
        }

        //Anotasi diterapkan di Method
        for (Method method : safetyCheck.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(MandatoryAirAndGroundCrew.class)) {
                MandatoryAirAndGroundCrew annotation = method.getAnnotation(MandatoryAirAndGroundCrew.class);
                method.invoke(safetyCheck);
                for (int i = 0; i < annotation.times() - 1; i++) {
                    method.invoke(safetyCheck);
                }
            }
        }

        // LOMBOK
        System.out.println("\n==============================");
        System.out.println("*** THIS IS LOMBOK Library ***");
        System.out.println("==============================");
        PriorityLombok flying = new PriorityLombok("flying", LocalDateTime.now());
        System.out.println("activity = " + flying.getActivity());
        System.out.println("time = " + flying.getTime());

    }

}
