package id.b2camp.fanri.day9;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//Annotation
@Target(ElementType.TYPE)  //@Target bisa ke Class, Method, Property
@Retention(RetentionPolicy.RUNTIME) //@Retenetion dijalankan di : sblm compile (SOURCE), saat sudah di compile(CLASS), atau saat dijalankan (RUNTIME :  90% dipakai)
public @interface Mandatory {

}
