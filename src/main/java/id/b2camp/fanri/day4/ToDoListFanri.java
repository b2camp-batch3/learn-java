package id.b2camp.fanri.day4;

import java.util.Scanner;

public class ToDoListFanri {

    static String[] model = new String[5];
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            viewTodos();
            System.out.println("To Do List Menu: ");
            System.out.println("(A) Adding To Do List");
            System.out.println("(R) Removing To Do List");
            System.out.println("(E) Exit");
            String input = input("Please select (A), (R) or (E) : ");

            if(input.equalsIgnoreCase("A")) {
                formAdd();
            } else if (input.equalsIgnoreCase("R")) {
                formRemove();
            } else if(input.equalsIgnoreCase("E")){
                break;
            }else {
                System.out.println("Invalid input, please select choose menu 'A', 'R' or 'E' ");
            }
        }
    }

    private static void formRemove() {
        System.out.println("Remove todo");
        String input = input("Input the number(only) in the list to remove it, or choose 'E' for cancel ");

        if (input.equalsIgnoreCase("E")) {

        } else {
            boolean isSuccess = removeTodo(Integer.parseInt(input));
            if(isSuccess) {
                System.out.println("Failed to remove todo: " + input);
            }
        }

    }

    private static boolean removeTodo(int number) {
        if ((number - 1) >= model.length -1) {
            return false;
        }

        if (model[number - 1] == null) {
            return false;
        }

        for (int i = (number - 1); i < model.length; i++) {
            if (i == (model.length - 1)) {
                model[i] = null;
            } else {
                model[i] = model[i + 1];
            }
        }

        return true;
    }

    private static void viewTodos() {
        for (int i = 0; i < model.length; i++) {
            String todo = model[i];
            int number = i + 1;

            if (todo != null) {
                System.out.println(number + ". " + todo);
            }
        }
    }

    private static void formAdd() {
        System.out.println("Adding new to do list");
        String input = input("Write your text 'To Do List' below, or choose 'E' to cancel it");

        if (input.equalsIgnoreCase("E")) {

        }else {
            addTodo(input);
        }
    }

    private static void addTodo(String input) {
        boolean isFull = true;

        for (int i = 0; i < model.length; i++) {
            if (model[i] == null) {
                isFull = false;
                break;
            }
        }
        if(isFull) {
            String[] tempModel = model;
            model = new String[model.length * 2];

            for (int i = 0; i < tempModel.length; i++) {
                model[i] = tempModel[i];
            }
        }

        for (int i = 0; i < model.length; i++) {
            if (model[i] == null) {
                model[i] = input;
                break;
            }
        }

    }

    static String input(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

}
