package id.b2camp.fanri.day3;

public class WhileLoopAndDoWhileLoopFanri {
    public static void main(String[] args) {

        //While Loop
        int nilaiA = 1;
        boolean kondisi1 = true;

        System.out.println("Start 1 !!!");

        while (kondisi1) {
            System.out.println("While loop ke- " + nilaiA);
            nilaiA++;
            if (nilaiA == 10) {
                kondisi1 = false;
            }
        }
        System.out.println("Stop-1 !!!");
        System.out.println("============= \n");


        //Do While Loop
        System.out.println("Start-2 !!!");
        int nilaiB = 0;
        boolean kondisi2 = true;

        do{
            nilaiB++;
            System.out.println("Do While loop ke-" + nilaiB);

            if (nilaiB == 15) {
                kondisi2 = false;
            }
        }while (kondisi2);
        System.out.println("Stop-2 !!!");

    }
}
