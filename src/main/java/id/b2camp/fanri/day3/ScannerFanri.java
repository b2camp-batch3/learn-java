package id.b2camp.fanri.day3;

import java.util.Scanner;

public class ScannerFanri {

    public static void main(String[] args) {
        Scanner valueInput = new Scanner(System.in);
        double luas, alas, tinggi;

        System.out.println("Luas Segitiga");
        System.out.print("Masukan Alas = ");
        alas = valueInput.nextInt();
        System.out.println("Masukan Tinggi = ");
        tinggi = valueInput.nextInt();

        luas = 0.5 * alas * tinggi;
        System.out.println("Luas segitiga = " + luas);
    }
}
