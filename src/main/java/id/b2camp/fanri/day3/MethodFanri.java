package id.b2camp.fanri.day3;

public class MethodFanri {
    public static void main(String[] args) {
        String name = "FaNrI AhMadi";
        System.out.println(name.toLowerCase());
        System.out.println(name.toUpperCase());
        System.out.print("========================= \n");

        char[] huruf = {'X', 'y', 'Z'};
        char[] simbol = {'#', '$', '@', '?', '-', '^'};
        System.out.println(messageChar1(huruf));
        System.out.println(messageChar2(simbol));
        System.out.print("========================== \n");

    }
    static int countChar(char[] hurufSimbolParam) {
        return hurufSimbolParam.length;
    }

    static  String messageChar1(char[] hurufSimbolParam) {
        return "Huruf ini berjumlah = " + hurufSimbolParam.length;
    }

    static  String messageChar2(char[] hurufSimbolParam) {
        return "Simbol ini berjumlah = " + hurufSimbolParam.length;
    }

}
