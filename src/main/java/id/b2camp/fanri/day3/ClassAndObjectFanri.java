package id.b2camp.fanri.day3;

//Template Class
class Car{
    String brand, model, type, sourcePower;
    int price;
}

public class ClassAndObjectFanri {

    //Membuat Object dari class Car
    public static void main(String[] args){
        Car car1 = new Car();
        car1.brand = "Mazda";
        car1.model = "CX-9";
        car1.type = "Elite";
        car1.sourcePower = "Gasoline Engine";
        car1.price = 800000000;

        System.out.println("Brand mobil = " + car1.brand);
        System.out.println("Model = " + car1.model);
        System.out.println("Type = " + car1.type);
        System.out.println("Sumber Tenaga = " + car1.sourcePower);
        System.out.println("Harga = "+ car1.price);
        System.out.print("================================ \n");

        Car car2 = new Car();
        car2.brand = "Toyota";
        car2.model = "Cross";
        car2.type = "X";
        car2.sourcePower = "Hybrid Engine";
        car2.price = 700000000;

        System.out.println("Brand mobil = " + car2.brand);
        System.out.println("Model = " + car2.model);
        System.out.println("Type = " + car2.type);
        System.out.println("Sumber Tenaga = " + car2.sourcePower);
        System.out.println("Harga = "+ car2.price);
        System.out.print("================================ \n");

        Car car3 = new Car();
        car3.brand = "Hyundai";
        car3.model = "Ionoq 5";
        car3.type = "Ioniq";
        car3.sourcePower = "Electric Car";
        car3.price = 850000000;

        System.out.println("Brand mobil = " + car3.brand);
        System.out.println("Model = " + car3.model);
        System.out.println("Type = " + car3.type);
        System.out.println("Sumber Tenaga = " + car3.sourcePower);
        System.out.println("Harga = "+ car3.price);
        System.out.print("================================ \n");
    }

}
