package id.b2camp.fanri.day3;

import java.util.Arrays;

public class ArrayAndForLoop2DFanri {
    public static void main(String[] args) {
        //Lat. String 1
        String[][] sportLegend = new String[][] {{"Soccer ", "Badminton ", "Basket Ball"}, {"Maldini ", "Susi ", "Kobe",}, {"Italy ", "Indonesia ", "USA"}};
        for (String[] sports : sportLegend) {
            for (String sport : sports) {
                System.out.print(sport);
            }
            System.out.println();
        }
        System.out.println("================ \n");

        //Lat. String 2
        String[][] car = new String[3][3];
        car[0][0] = "BMW";
        car[0][1] = "VW";
        car[0][2] = "Volvo";
        car[1][0] = "Honda";
        car[1][1] = "Toyota";
        car[1][2] = "Mazda";
        car[2][0] = "Ford";
        car[2][1] = "Kia";
        car[2][2] = "Hyundai";
        for (String[] brand : car)
        System.out.println(Arrays.toString(brand));
        System.out.println("============== \n");


        //Lat. Penambahan Matrix
        int[][] matrix1 = {
                {2,1,2},
                {3,6,9},
                {1,2,2},
        };

        int[][] matrix2 = {
                {9,8,7},
                {6,5,4},
                {3,2,1},
        };

        printArray(matrix1);
        printArray(matrix2);

        int[][] result = tambah(matrix1, matrix2);

        printArray(result);
    }

    public static int[][] tambah(int[][] matrix_1, int[][] matrix_2){
        int row2 = matrix_2.length;
        int column2 = matrix_2[0].length;

        int[][] result = new int[row2][column2];

        for(int i = 0; i < row2; i++) {
            for(int j = 0; j <column2; j++ ) {
                result[i][j] = matrix_1[i][j] + matrix_2[i][j];
            }
        }
        return result;
    }

    public static void printArray(int[][] dataArray) {
        int row1 = dataArray.length;
        int column1 = dataArray.length;

        for(int i = 0; i< row1;i++){
            System.out.print("[");
            for(int j = 0; j < column1; j++) {
                System.out.print(dataArray[i][j]);

                if(j < (column1 - 1)) {
                    System.out.print(" ");
                } else {
                    System.out.print("]");
                }
            }
            System.out.print("\n");
        }
        System.out.print("\n");
    }
}
