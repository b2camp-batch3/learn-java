CREATE TABLE fanri_table (
    id int NOT NULL,
    full_name varchar(50) NOT NULL,
    address varchar(50) NOT NULL,
    phone_number int NOT NULL,
    email varchar(50) NOT NULL,
);

INSERT INTO fanri_table (id, full_name, address, phone_number, email)
VALUES (101, 'Fanri Ahmadi', 'Jl. Pakuan VI, 40249 Kab. Bandung', 081235678, 'fanreiz@gmail.com'),
       (102, 'Kuya Ngora', 'Jl. Sumbawa, 4239 Sumbawa', 0813444678, 'Kuya@kuya.com'),
       (103, 'Pace Hideung', 'Jl. Papua, 48970 PNG', 0813444888, 'Hideung@bodas.com'),
       (104, 'Si Ganteng', 'Jl. Amerika, 428939 Cilengkrang', 090444678, 'Pehul@Ronaldo.com');

SELECT * FROM fanri_table;

INSERT INTO fanri_table (id, full_name, address, phone_number, email)
VALUES (105, 'Ronaldo Setiawan', 'Jl. Protugla, 4349  Bandung', 081235888, 'ronaldo@gmail.com');

ALTER TABLE fanri_table add column place_of_birth varchar(20);
ALTER TABLE fanri_table drop column place_of_birth;

ALTER TABLE fanri_table
ADD COLUMN place_of_birth varchar(20),
ADD COLUMN birth_date date;

ALTER TABLE public.fanri_table ALTER COLUMN phone_number TYPE varchar USING phone_number::varchar;

UPDATE fanri_table SET phone_number  = '08125567678', place_of_birth = 'Jayapura', birth_date = '2000-10-10' where id = 101;
UPDATE fanri_table SET phone_number  = '+68178968998', place_of_birth = 'Jaya Wijaya', birth_date = '1945-10-10' where id = 103;

INSERT INTO fanri_table (id, full_name, address, phone_number, email, place_of_birth, birth_date)
VALUES (106, 'Jackson F Tiago', 'Jl. Pagarsih, 404243, Bandung', '+628987987', 'jeko@gmail.com', 'Garut', '1956-09-09'),
       (107, 'Robi Darwis', 'Jl. Cicaheum 3339 Bandung', '+62987986983', 'RD@gmail.com', 'Texas', '1977-12-12'),
       (108, 'Ciro Beckhamp', 'Jl. Ujung Berung 48440 BAndung', '0815676736', 'Ciro888@gmail.com', 'Gunung Anten', '1980-09-12');

SELECT  id, full_name, email FROM fanri_table ORDER BY full_name ASC;
SELECT id, full_name, address, phone_number FROM fanri_table ORDER BY id desc;
SELECT id, full_name, address, phone_number FROM fanri_table ORDER BY id asc limit 5;

DELETE FROM fanri_table WHERE id = 108;