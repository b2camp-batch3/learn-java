package id.b2camp.fanri.day12;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionalFanri {
    public static void main(String[] args) {
        //Strean itu termasuk Functional
        //Paradigma Functional
        List<String> mazdaCars = List.of("CX-2", "CX-3", "CX-30", "CX-5", "Biante", "CX-9", "CX-60", "RX-7", "Miyata");

        List<String> mazdaCarsHas4Char = mazdaCars.stream()
                .filter(mazdaCar -> mazdaCar.length() == 4)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        mazdaCarsHas4Char.forEach(System.out::println);


//        //cth di bwh salah karena ada sharing mutability antar collection dimana harus dihindari
//        //sharing mutability : di dlm functional object diluar stream mempengaruhi object lainnya
//        System.out.println("----------");
//        List<String> results = new ArrayList<>(); //Bikin object penampung
//        //Bikin functionality nya juga
//        mazdaCars.parallelStream()
//                .filter(mazdaCar -> mazdaCar.length() == 4)
//                .forEach(mazdaCar -> results.add(mazdaCar));
//        results.forEach(System.out::println);


        //Error Handling di Imperative sebaiknya tidak digunakan krn functionalnya menjadi tidak pure lg
        //
//        System.out.println("-----------");
//        mazdaCars.stream()
//                .map(mazdaCar-> {
//                   try {
//                       if (mazdaCar.length() > 4) {
//                           System.out.println(mazdaCar);
//                       }
//                   } catch (IndexOutOfBoundsException ex) {
//                       System.out.println(ex.getMessage());
//                   }
//                   return mazdaCar;
//                }).collect(Collectors.toList());


    }
}
