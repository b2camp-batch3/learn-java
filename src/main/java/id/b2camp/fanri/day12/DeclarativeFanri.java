package id.b2camp.fanri.day12;

import java.util.List;

public class DeclarativeFanri {
    public static void main(String[] args) {
        List<String> mazdaCars = List.of("CX-2", "CX-3", "CX-30", "CX-5", "Biante", "CX-9", "CX-60");

        //Paradigma Declarative : Tidak tau proses kerjanya, tapi tau cara menggunakannya
        //Lombok jg termasuk Declarative krn tidak tau cara kerjanya
        if (mazdaCars.contains("CX-30")) {
            System.out.println(true + ", our Mazda Collections is : " + mazdaCars);
        }

    }
}
