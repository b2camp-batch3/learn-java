package id.b2camp.fanri.day12.stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Car {

    private Brand brand;
    private String type;
    private int year;
    private BigDecimal price;
    private int stok;
}
