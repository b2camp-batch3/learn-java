package id.b2camp.fanri.day12.stream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MobilBekasApp {
    public static void main(String[] args) {
        List<Car> cars = getCar();

        //cth tidak pakai Stream
        System.out.println("============== Without Stream ==========");
        System.out.println("----- List Brand of car using for - if  -----");
        ArrayList<Car> toyotaCars = new ArrayList<>();
        for (Car car : cars) {
            if (car.getBrand().equals(Brand.TOYOTA)) {
                toyotaCars.add(car);
            }
        }
        System.out.println(toyotaCars);
        System.out.println("----------");
        toyotaCars.forEach(System.out::println);

        //Menggunakan Stream menjadi lebih explisit
        System.out.println("\n============== With Stream =============");
        System.out.println("----- List Brand of car using .filter -----");
        List<Car> mazdaCars = cars.stream()
                .filter(car -> car.getBrand().equals(Brand.MAZDA)) //.filter menggunakan Lambda
                .collect(Collectors.toList());
        mazdaCars.forEach(System.out::println);

        System.out.println("\n----- List Brand of car using .filter -----");
        List<Car> hondaCars = cars.stream()
                .filter(car -> car.getBrand().equals(Brand.HONDA)) //.filter menggunakan Lambda
                .collect(Collectors.toList());
        hondaCars.forEach(System.out::println);

        System.out.println("\n----- List Type of cars using .filter-----");
        List<Car> typeNissans = cars.stream()
                .filter(car -> car.getType().equals("KICK")) //.filter menggunakan Lambda
                .collect(Collectors.toList());
        typeNissans.forEach(System.out::println);


        System.out.println("\n----- .sorted year of cars -----");
        List<Car> yearCars = cars.stream()
                //.sorted : Mengurutkan dr nilai kecil ke besar.
                .sorted(Comparator.comparing(Car::getYear).thenComparing(Car::getBrand)) //.sorted menggunakan Comparator
                .collect(Collectors.toList());
        yearCars.forEach(System.out::println);

        System.out.println("\n----- .sorted price of cars (.reversed) -----");
        List<Car> priceCars = cars.stream()
                //.reversed : Mengurutkan dari nilai besar ke kecil
                .sorted(Comparator.comparing(Car::getPrice).reversed().thenComparing(Car::getBrand))
                .collect(Collectors.toList());
        priceCars.forEach(System.out::println);

        System.out.println("\n----- boolean dengan .allMatch -----");
        boolean allCarsAbove2015 = cars.stream()
                .allMatch(car -> car.getYear() > 2010);
        System.out.println(allCarsAbove2015);

        System.out.println("\n----- .filter : year above 2010 -----");
        List<Car> adults = cars.stream()
                .filter(car -> car.getYear() > 2015)
                .collect(Collectors.toList());
        adults.forEach(System.out::println);

        System.out.println("\n----- add car prices -----");
        BigDecimal sumOfPrices = cars.stream()
                .map(Car::getPrice)    //Lambda bisa diubah ke reference begitupun sebaliknya
                .reduce(BigDecimal.ZERO, BigDecimal::add);//BigDdecimal.Zero : Asal dr nilai valuenya, accumulator pakai BiGDecimal add, max, min, dll
        System.out.println("Total harga jual semua mobil di showroom ini  = " + sumOfPrices);

        //Proses Grouping
        System.out.println("\n----- Grouping by Map -----");
        Map<Brand, List<Car>> groupByBrand = cars.stream()
                .collect(Collectors.groupingBy(Car::getBrand));
        groupByBrand.forEach((brand, carList) -> {
            System.out.println(brand);
            carList.forEach(System.out::println);
            System.out.println();
        });

        // cth Map di Stream
        System.out.println("\n======= Contoh Map di Stream : .filter BRAND ======= ");
        //Mencari data BRAND dengan Map
        List<Map.Entry<Brand, List<Car>>> collect = groupByBrand.entrySet()
                .stream()
                .filter(k -> k.getKey().equals(Brand.MAZDA))
                .collect(Collectors.toList());
        collect.forEach(System.out::println);

        //mencari nilai tertinggi
        System.out.println("\n----- .min : Mencari mobil dengan tahun paling tua dari semua data mobil -----");
        cars.stream()
                .min(Comparator.comparing(Car::getYear))
                .ifPresent(System.out::println);

        System.out.println("\n----- .min : Mencari harga termurah dari semua data mobil -----");
        cars.stream()
                .min(Comparator.comparing(Car::getPrice))
                .ifPresent(System.out::println);

        System.out.println("\n----- .min : Mencari harga termahal dari semua data mobil -----");
        cars.stream()
                .max(Comparator.comparing(Car::getPrice))
                .ifPresent(System.out::println);

        //mencari nilai terendah
        System.out.println("\n----- .max and .filter : Mencari mobil dengan tahun paling muda untuk Brand tertentu -----");
        cars.stream()
                .filter(car -> car.getBrand().equals(Brand.TOYOTA))
                .max(Comparator.comparing(Car::getYear))
                .ifPresent(System.out::println);


    }

    static List<Car> getCar() {
        return  List.of(
                new Car(Brand.MAZDA, "CX-5", 2015, BigDecimal.valueOf(250_000_000), 1),
                new Car(Brand.TOYOTA, "CROSS", 2021, BigDecimal.valueOf(500_000_000), 1),
                new Car(Brand.HONDA, "BRIO", 2015, BigDecimal.valueOf(120_000_000), 2),
                new Car(Brand.HONDA, "JAZZ", 2009, BigDecimal.valueOf(75_000_000), 2),
                new Car(Brand.NISSAN, "MAGNITE", 2022, BigDecimal.valueOf(195_000_000), 1),
                new Car(Brand.DAIHATSU, "TERIOS", 2014, BigDecimal.valueOf(110_000_000), 3),
                new Car(Brand.SUZUKI, "KATANA", 1997, BigDecimal.valueOf(35_000_000), 1),
                new Car(Brand.TOYOTA, "AVANZA", 2012, BigDecimal.valueOf(70_000_000), 1),
                new Car(Brand.TOYOTA, "AVANZA", 2017, BigDecimal.valueOf(90_000_000), 1),
                new Car(Brand.MAZDA, "BIANTE", 2020, BigDecimal.valueOf(350_000_000), 1),
                new Car(Brand.NISSAN, "KICK", 2022, BigDecimal.valueOf(600_000_000), 1)


        );
    }
}

