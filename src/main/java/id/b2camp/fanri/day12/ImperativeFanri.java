package id.b2camp.fanri.day12;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImperativeFanri {
    public static void main(String[] args) {
        List<String> mazdaCars = List.of("CX-2", "CX-3", "CX-30", "CX-5", "Biante", "CX-9", "CX-60");

        //Paradigma Imperative : mudah ditulis, susah dibaca
        //Tahu cara kerja dan prosesnya
        boolean isFound = false;  //Imperative pasti ada object penampungnya (object temporary)
        for (String name : mazdaCars) {
            if(name.equals("CX-9")){
                isFound = true;
                break;
            }
        }

        System.out.println(isFound);

        //Impertive untuk checking IOException
        ArrayList<String> results = new ArrayList<>(List.of("CX-10", "CX-1", "CX-8"));
        for (String result : results) {
            try {
                results.add(transform(result));
            }catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    //Bikin function untuk tracing error
    static String transform(String mazdaCar) throws IOException {
        if (Math.random() > 0.5) {
            throw new IOException("reason");
        }
        return mazdaCar.toUpperCase();
    }

}
