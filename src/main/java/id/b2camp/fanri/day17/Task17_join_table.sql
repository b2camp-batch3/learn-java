-- initialize db
CREATE TABLE fanri_pet_shop.users(
user_id INTEGER primary key not null,
email VARCHAR(20) not null,
password VARCHAR not null,
created_by INTEGER,
created_at TIMESTAMP
);

CREATE TABLE fanri_pet_shop.products(
product_id INTEGER primary key not null,
name VARCHAR(50) not null,
price DECIMAL not null,
stock INTEGER not null,
category VARCHAR(50),
url VARCHAR(50),
created_by INTEGER,
created_at TIMESTAMP
);

CREATE TABLE fanri_pet_shop.transactions(
transaction_id INTEGER primary key not null,
quantity INTEGER not null,
price DECIMAL not null,
payment_type VARCHAR(20) not null,
total_pay DECIMAL not null,
transaction_date DATE not null,
created_by INTEGER,
created_at TIMESTAMP,
product_id INTEGER
);

SELECT * FROM fanri_pet_shop.users;
SELECT * FROM fanri_pet_shop.products;
SELECT * FROM fanri_pet_shop.transactions;

ALTER TABLE fanri_pet_shop.products RENAME COLUMN url TO picture;

ALTER TABLE fanri_pet_shop.transactions ADD FOREIGN KEY (product_id) REFERENCES fanri_pet_shop.products(product_id);
ALTER TABLE fanri_pet_shop.transactions ADD FOREIGN KEY (created_by) REFERENCES fanri_pet_shop.users(user_id);

-- insert dummy data
INSERT INTO fanri_pet_shop.users (user_id, email, password, created_by, created_at)
VALUES	(01, 'fanri@gmil.com', 'kuya99', 01, CURRENT_TIMESTAMP),
		(02, 'ahmadi@gmil.com', 'dapur', 01, CURRENT_TIMESTAMP),
		(03, 'asep@gmil.com', 'mahmud12',01, CURRENT_TIMESTAMP),
		(04, 'wawan@gmil.com', 'motor45', 01, CURRENT_TIMESTAMP),
		(05, 'mawar@gmil.com', 'cuyah', 01, CURRENT_TIMESTAMP),
		(06, 'bunga@gmil.com', 'haihai', 01, CURRENT_TIMESTAMP),
		(07, 'kurakura@gmil.com', 'ahai', 01, CURRENT_TIMESTAMP);

INSERT INTO fanri_pet_shop.products  (product_id, name, price , stock, category, picture, created_by, created_at)
VALUES	(01, 'Whiskas 1 kg', 75000, 100, 'Makanan kucing', 'https://www.petshop.net/', 01, CURRENT_TIMESTAMP),
		(02, 'Kitten 500 gr', 37500, 200, 'Makanan kucing', 'https://petfood.netlify.app/pet-gallery/#', 01, CURRENT_TIMESTAMP),
		(03, 'Whiskas 250 gr', 20000, 250, 'Makanan kucing', 'https://petvetpetfood.net/', 01, CURRENT_TIMESTAMP),
		(04, 'Juara 1kg', 25000, 500, 'Makanan burung', 'https://www6.birdfood.net/', 01, CURRENT_TIMESTAMP),
		(05, 'Pedigree 1kg', 100000, 100, 'Makanan anjing', 'https://lenda.net/international/dog/', 01, CURRENT_TIMESTAMP),
		(06, 'O-cat 1kg', 120000, 100, 'Makanan kucing premium', 'https://lenda.net/international/cat/', 01, CURRENT_TIMESTAMP),
		(07, 'Sakura 500 gr', 50000, 100, 'Makanan ikan koi', 'https://petvetpetfood.net/', 01, CURRENT_TIMESTAMP),
		(08, 'Takari 500 gr', 45000, 100, 'Makanan ikan hias', 'https://petfood.netlify.app/pet-gallery/#', 01, CURRENT_TIMESTAMP) ;

INSERT INTO fanri_pet_shop.products  (product_id, name, price , stock, category, picture, created_by, created_at)
VALUES (09, 'Kenari-25 500 gr', 10000, 200, 'Makanan burung kenari', NULL , 01, CURRENT_TIMESTAMP);

INSERT INTO fanri_pet_shop.transactions
(transaction_id, quantity, price, payment_type, total_pay, transaction_date, created_by, created_at, product_id)
VALUES	(01, 2, 20000, 'Cash', 40000, '2023-07-01', 01, CURRENT_TIMESTAMP, 03),
		(02, 1, 120000, 'Qris', 120000, '2023-07-07', 01, CURRENT_TIMESTAMP, 06),
		(03, 4, 25000, 'Transfer', 100000, '2023-07-09', 01, CURRENT_TIMESTAMP, 04),
		(04, 1, 37500, 'Cash', 37500, '2023-07-09', 01, CURRENT_TIMESTAMP, 02),
		(05, 1, 100000, 'Cash', 100000, '2023-07-10', 02, CURRENT_TIMESTAMP, 05),
		(06, 1, 50000, 'Qris', 50000, '2023-07-11', 02, CURRENT_TIMESTAMP, 07);

INSERT INTO fanri_pet_shop.transactions
(transaction_id, quantity, price, payment_type, total_pay, transaction_date, created_by, created_at, product_id)
VALUES	(07, 3, 10000, 'Cash', 30000, '2023-07-12', 01, CURRENT_TIMESTAMP, 09);


SELECT * FROM fanri_pet_shop.users;
SELECT * FROM fanri_pet_shop.products;
SELECT * FROM fanri_pet_shop.transactions;

-- joining table
select t.transaction_id, t.quantity, t.total_pay, p.name, p.price, p.picture
from fanri_pet_shop.transactions t
inner join fanri_pet_shop.products p on t.product_id = p.product_id;

select t.transaction_id, t.quantity, t.total_pay, p.name, p.price, p.picture
from fanri_pet_shop.transactions t
left join fanri_pet_shop.products p on t.product_id = p.product_id;

select t.transaction_id, t.quantity, t.total_pay, p.name, p.price, p.picture
from fanri_pet_shop.transactions t
right join fanri_pet_shop.products p on t.product_id = p.product_id;

select t.transaction_id, t.quantity, t.total_pay, p.name, p.price, p.picture
from fanri_pet_shop.transactions t
join fanri_pet_shop.products p on t.product_id = p.product_id;

select transaction_id, product_id, price, total_pay
from fanri_pet_shop.transactions t
group by product_id, price, transaction_id, total_pay;

select transaction_id, product_id, price, total_pay
from fanri_pet_shop.transactions t
group by product_id, price, transaction_id, total_pay
having price >= 50000;

select transaction_id, product_id, price, total_pay
from fanri_pet_shop.transactions t
group by product_id, price, transaction_id, total_pay
having price <= 50000;

select avg(price)
from fanri_pet_shop.transactions;

select count(price)
from fanri_pet_shop.transactions;