package id.b2camp.fanri.day2;

public class ComparisonFanri {

    public static void main(String[] args) {

        //Operator Comparison
            System.out.print("\n\n");
            System.out.println("*** Comparison Operator ***");
            System.out.println("===========================");

            //Niai Variabel :
            int ft = 1;
            int cm = 30;

            boolean comp;

            // ">"
            comp = (ft > cm);
            System.out.println(ft + " > " + cm + " = " + comp);

            // ">="
            comp = (ft >= cm);
            System.out.println(ft + " >= " + cm + " = " + comp);

            // "<"
            comp = (ft < cm);
            System.out.println(ft + " < " + cm + " = " + comp);

            // "<="
            comp = (ft <= cm);
            System.out.println(ft + " <= " + cm + " = " + comp);

            // "=="
            comp = (ft == cm);
            System.out.printf("%d == %d : %s \n", ft, cm, comp);

            // "!="
            comp = (ft != cm);
            System.out.printf("%d != %d : %s \n\n\n", ft, cm, comp);

        //Logic Comparison
        System.out.println("*** Logic Comparison  ***");
        System.out.println("=========================");

        boolean benar, salah, tambahan, logicComp;

        benar  = true;
        salah = false;
        tambahan = true;
            //OR "||"
            logicComp = (benar || benar);
            System.out.println(benar + "  || " + benar + "  = " + logicComp);

            logicComp = (benar || salah);
            System.out.println(benar + "  || " + salah + " = " + logicComp);

            logicComp = (salah || benar);
            System.out.println(salah + " || " + benar + "  = " + logicComp);

            logicComp = (salah || salah);
            System.out.println(salah + " || " + salah + " = " + logicComp + "\n");

            //AND "&&"
            logicComp = (benar &&  benar);
            System.out.println(benar + "  && " + benar + "  = " + logicComp);

            logicComp = (benar &&  salah);
            System.out.println(benar + "  && " + salah + " = " + logicComp);

            logicComp = (salah &&  benar);
            System.out.println(salah + " && " + benar + "  = " + logicComp);

            logicComp = (salah && salah);
            System.out.println(salah + " && " + salah + " = " + logicComp + "\n");


            //Coba2 Logic Tambahan
            System.out.println(benar || salah && benar && salah );
            System.out.println((benar && salah) || (benar || salah));
            System.out.println((benar && salah) && (benar || salah));
            System.out.print("\n");

            //Case sensitive
            System.out.println("FanRi ahMadi".equalsIgnoreCase("Fanri Ahmadi"));
            System.out.println("FanRi ahMadi".equalsIgnoreCase("FanRi AhMadi"));
            System.out.println("Fanri Ahmadi".equals("Fanri Ahmadi"));
            System.out.println("Fanri Ahmadi".equals("FaNrI ahMadi"));
    }

}
