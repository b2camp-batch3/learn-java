package id.b2camp.fanri.day2;

public class SwitchCaseFanri {
    public static void main(String[] args) {

        //Masukan Password Nama anda :
        String passwordNama = "kuya";

        switch(passwordNama) {
            case "Fanri":
                System.out.println("Password anda benar, silahkan masuk!");
                break;
            case "Ahmadi":
                System.out.println("Password anda juga benar, silahkan masuk!");
                break;
            default:
                System.out.println("Password anda salah, silahkan pulang !!!");
        }

        System.out.println("Selesai");




    }
}
