package id.b2camp.fanri.day2;

public class ArithmeticFanri {
    public static void main(String[] args) {

        //Nilai variabel :
        int angkaA = 19;
        int angkaB = 7;

        int result;

        //Penjumlahan
        System.out.println(angkaA + angkaB);

        //Pengurangan
        result = angkaA - angkaB;
        System.out.printf("%d - %d = %d \n", angkaA, angkaB, result);;

        //Perkalian
        result = angkaA * angkaB;
        System.out.printf("%d * %d = %d \n", angkaA, angkaB, result);;

        //Pembagian
        int angkaC = 19;
        float angkaD = 7;
        float result2;
        result2 = angkaC / angkaD;
        System.out.printf("%d / %.2f = %.4f \n", angkaC, angkaD, result2);

        //Modulus (Sisa pembagian)
        result = angkaA % angkaB;
        System.out.println(angkaA + " % " + angkaB + " = " + result);
        System.out.printf("%d %% %d = %d \n", angkaA, angkaB, result);;

    }

}
