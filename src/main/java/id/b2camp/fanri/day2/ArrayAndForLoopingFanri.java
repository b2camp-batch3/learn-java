package id.b2camp.fanri.day2;

import java.util.Arrays;

public class ArrayAndForLoopingFanri {
    public static void main(String[] args) {
        String[] cities = {"Jayapura", "Malang", "Manado", "Palangkaraya", "Medan"};
        int[] nilai = {1, 2, 3, 4, 5};
        int i;
        for (i = 0; i < cities.length; i++ ){
            System.out.println(cities[i] + "\n");
            System.out.println("--------");
            System.out.println(cities[2]);
            System.out.println(cities[1]);
            System.out.println(cities[4]);
            System.out.println(cities[3]);
            System.out.println(cities[0]);
            System.out.println("--------");
            System.out.println(nilai[4]);
            System.out.println(nilai[3]);
            System.out.println(nilai[2]);
            System.out.println(nilai[1]);
            System.out.println(nilai[0]);
            System.out.println("--------");

        }
        System.out.println(Arrays.toString(cities));
        System.out.println(Arrays.toString(nilai));
    }
}
