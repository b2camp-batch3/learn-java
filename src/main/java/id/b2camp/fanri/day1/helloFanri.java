package id.b2camp.fanri.day1;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

public class helloFanri {

    public static void main (String[] args){
        System.out.println("Hello Fanri");

        //Primitive data types
        int noAbsen = 9;
        float suhu = 25.7F;
        double pi = 3.14159265358979;
        char simbol  = '?';
        boolean ganteng = false;

        //Non-primitive data types
        String kondisi = "===RUNGKAD===";
        Boolean isGanteng = false;
        LocalTime wib = LocalTime.now();
        LocalDate now = LocalDate.now();
        BigDecimal length = BigDecimal.valueOf(33_789_383);

        //Output
        System.out.println("No. absen saya adalah : " + noAbsen);
        System.out.println("Suhu siang ini : " + suhu + " °C");
        System.out.println("Nilai tetap pi adalah: " + pi);
        System.out.print("Apakah saya guanteng " + simbol + simbol + simbol + "...");
        System.out.println("Tentu saja " + ganteng);
        System.out.println("...Hadeehhh " + kondisi + " hatiku");
        System.out.println("Seriusan kegantengan saya " + isGanteng + " " +simbol);
        System.out.println("Situ ganteng dilihat kalau dari jarak " + length + " KM");
        System.out.print("recorded at : " + wib + " wib "+ " on " + now);
    }
}
