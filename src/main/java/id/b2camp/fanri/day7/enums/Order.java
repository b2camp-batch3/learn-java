package id.b2camp.fanri.day7.enums;

public class Order {

    private int amount;
    private Menu menu;

    public Order(int amount, Menu menu) {
        this.amount = amount;
        this.menu = menu;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}