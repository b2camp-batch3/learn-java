package id.b2camp.fanri.day7.enums;

public enum Menu {
    BIRIYANI_MUTTON (101, "Nasi khas Arabian dengan daging domba", 70000),
    BIRIYANI_CHICKEN (102,"Nasi khas Arabian dengan daging ayam", 60000),
    SHAWARMA (201, "Kebab khas Saudi Arabia", 25000),
    OM_ALI (301,"Bubur gandum khas Mesir", 20000);


    final int code;
    final String description;
    final int price;

    Menu(int code, String description, int price) {
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }
}
