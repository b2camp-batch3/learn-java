package id.b2camp.fanri.day7.enums;

public class ArabianRestoAppFanri {
    public static void main(String[] args) {

        Menu biriyani1 = Menu.BIRIYANI_MUTTON;
        Menu biriyani2 = Menu.BIRIYANI_CHICKEN;
        Menu shawarma = Menu.SHAWARMA;
        Menu omAli = Menu.OM_ALI;
        System.out.println(biriyani1);
        System.out.println(biriyani2);
        System.out.println(shawarma);
        System.out.println(omAli);

        Order order1 = new Order(2, Menu.BIRIYANI_MUTTON);
        System.out.println("Customer memesan " + order1.getAmount() + " porsi " + order1.getMenu() + ", Total : Rp. " + order1.getMenu().price*order1.getAmount() + "... Sukran Ya Habibie !!!");

        Order order2 = new Order(4, Menu.BIRIYANI_CHICKEN);
        System.out.println("Customer memesan " + order2.getAmount() + " porsi " + order2.getMenu() + ", Total : Rp. " + order2.getMenu().price*order2.getAmount() + "... Sukran Ya Habibie !!!");

        Order order3 = new Order(6, Menu.SHAWARMA);
        System.out.println("Customer memesan " + order3.getAmount() + " porsi " + order3.getMenu() + ", Total : Rp. " + order3.getMenu().price*order3.getAmount() + "... Sukran Ya Habibie !!!");

        Order order4 = new Order(3, Menu.OM_ALI);
        System.out.println("Customer memesan " + order4.getAmount() + " porsi " + order4.getMenu() + ", Total : Rp. " + order4.getMenu().price*order4.getAmount()  + "... Sukran Ya Habibie !!!");

    }
}
