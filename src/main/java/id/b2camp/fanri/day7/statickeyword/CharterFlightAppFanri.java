package id.b2camp.fanri.day7.statickeyword;

public class CharterFlightAppFanri {
    public static void main(String[] args) {

        String aircraft = AircraftFleet.NARROW_BODY;
        boolean isLongRange = AircraftFleet.LONG_RANGE;
        int seatConfig = AircraftFleet.SEAT_CONFIG;
        System.out.println(aircraft);
        System.out.println(isLongRange);
        System.out.println(seatConfig);

        System.out.println("=========");
        Operations.burnFuel(27000, 8000);

        System.out.println("=========");
        PilotRating.Boeing javaProgram = new PilotRating.Boeing();
        javaProgram.setVersion("737");
        System.out.println(javaProgram.getVersion());

    }

}
