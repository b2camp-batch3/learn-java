package id.b2camp.fanri.day7.statickeyword;


public class PilotRating {

    public String getTypeRating() {
        return typeRating;
    }

    public void setTypeRating(String typeRating) {
        this.typeRating = typeRating;
    }

    public PilotRating(String typeRating) {
        this.typeRating = typeRating;
    }


    String typeRating;

    static class Boeing {

        String type;

        public String getVersion() {
            return type;
        }

        public void setVersion(String type) {
            this.type = type;
        }
    }
}