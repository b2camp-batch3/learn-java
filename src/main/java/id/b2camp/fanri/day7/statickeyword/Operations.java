package id.b2camp.fanri.day7.statickeyword;

public class Operations {

    static void burnFuel(int fuelOnboard, int remainFuel) {
        int burntFuel = fuelOnboard + remainFuel;
        System.out.println("Fuel onboard " + fuelOnboard +" Kg "+ " - " + "Remaining fuel " + remainFuel + " Kg "  + " = " + burntFuel + " Kg " + " (burnFuel) ");
    }

}
