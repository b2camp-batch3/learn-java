package id.b2camp.fanri.day7.anonymousclass;

/*Anonymous class sifatnya sekali pakai, kalau methodnya dipakai berkali-kali sebaiknya
   menggunaan concrete class
 */

public class AircraftAppFanri {
    public static void main(String[] args) {

        TakeOff publisher =  new TakeOff() {

            @Override
            public void rotateTakeOff() {
                System.out.println("Pilot pull the Control Coulomb for Take-Off");
            }

            @Override
            public void abortedTakeOff() {
                System.out.println("Pilot push the Control Coulomb for aborted Take-Off and Approaching/Landing ");
            }
        };
        publisher.rotateTakeOff();
        publisher.abortedTakeOff();

        System.out.println("----");
        Pilot player = new Pilot() {

            @Override
            public void controlCoulomb() {
                System.out.println("Control Coulomb used for moving Aircraft : pitch, yaw and roll");
            }
        };
        player.controlCoulomb();

    }

}
