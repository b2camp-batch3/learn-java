package id.b2camp.fanri.day7.finalkeyword;

public class Travel {

    private String travelName;

    public String getTravelName() {
        return travelName;
    }

    public void setTravelName(String travelName) {
        this.travelName = travelName;
    }
}


class Armada extends Travel {
    final void miniBus(int seatConfig) {
        System.out.println(seatConfig);
    }

}

final class Brand extends Armada {

    // method di superclassnya tidak dapat diturunkan karena sudah diberi final
//    void trending(String title) {
//        System.out.println(title + "rubah");
//    }
}

//Super class yg sudah di "final" maka tidak dapat di extends lg
//class FakeNews extends ColumnNews {
//
//}