package id.b2camp.fanri.day8;

public class InvalidBookingName extends Throwable {

    public InvalidBookingName(String message) {
        super(message);
    }
}
