package id.b2camp.fanri.day8;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class Validation {

    public static void validateBookingName(String name) throws InvalidBookingName {
        if (name.isBlank()) {
            throw new InvalidBookingName("Nama tidak boleh blank");
        } else if (name.isEmpty()) {
            throw new InvalidBookingName("Nama tidak boleh kosong");
        }
    }

    public static void validateDepartureDateAndTime(LocalDateTime departureDateAndTime) throws InvalidBookingDateAndTime {
        if (Objects.isNull(departureDateAndTime)) {
            throw new InvalidBookingDateAndTime("Tanggal dan Jam booking tidak boleh kosong");
        } else if (departureDateAndTime.isBefore(LocalDateTime.now())) {
            throw new InvalidBookingDateAndTime("booking tidak boleh diisi tanggal dan jam yang telah lewat !!!");
        }
    }

    public static void validateRoute(String route) throws InvalidBookingRoute {
        if (route.isBlank()) {
            throw new InvalidBookingRoute("Rute tidak boleh blank");
        } else if (route.isEmpty()) {
            throw new InvalidBookingRoute("Rute tidak boleh kosong");
        }
    }



    public static void validateRunTimeBookingName(String name) throws RuntimeException {
        if (name.isBlank()) {
            throw new RuntimeException("Nama tidak boleh blank");
        } else if (name.isEmpty()) {
            throw new RuntimeException("Nama tidak boleh kosong");
        }
    }

    public static void validateRunTimeDepartureDateAndTime(LocalDateTime departureDateAndTime) throws RuntimeException {
        if (Objects.isNull(departureDateAndTime)) {
            throw new RuntimeException("Tanggal dan Jam booking tidak boleh kosong");
        } else if (departureDateAndTime.isBefore(LocalDateTime.now())) {
            throw new RuntimeException("booking tidak boleh diisi tanggal dan jam yang telah lewat !!!");
        }
    }

    public static void validateRunTimeRoute(String route) throws RuntimeException {
        if (route.isBlank()) {
            throw new RuntimeException("Rute tidak boleh blank");
        } else if (route.isEmpty()) {
            throw new RuntimeException("Rute tidak boleh kosong");
        }
    }
}