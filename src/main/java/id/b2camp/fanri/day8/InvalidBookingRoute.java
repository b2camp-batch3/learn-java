package id.b2camp.fanri.day8;

public class InvalidBookingRoute extends Throwable{
    public InvalidBookingRoute(String message) {
        super(message);
    }
}
