package id.b2camp.fanri.day8;

import java.time.LocalDateTime;

public class BookingInput {

    private String name;
    private LocalDateTime departureDateAndTime;

    private String route;

    public BookingInput(String name, LocalDateTime departureDateAndTime, String route) {
        this.name = name;
        this.departureDateAndTime = departureDateAndTime;
        this.route = route;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDepartureDateAndTime() {
        return departureDateAndTime;
    }

    public void setDepartureDateAndTime(LocalDateTime departureDateAndTime) {
        this.departureDateAndTime = departureDateAndTime;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }
}
