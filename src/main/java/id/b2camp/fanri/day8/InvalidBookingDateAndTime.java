package id.b2camp.fanri.day8;

public class InvalidBookingDateAndTime extends Throwable{
    public InvalidBookingDateAndTime(String message) {
        super(message);
    }
}
