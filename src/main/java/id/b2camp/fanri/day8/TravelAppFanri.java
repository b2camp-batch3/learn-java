package id.b2camp.fanri.day8;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class TravelAppFanri {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) /*throws InvalidBookingName, InvalidBookingDateAndTime, InvalidBookingRoute */{

        System.out.println("Booking Fanri's Travel Bandung <-> Jakarta");

        String nama = input("Nama: ");
        String keberangkatan = input("Tanggal & Waktu keberangkatan : ");
        String rute = input("Rute : ");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime departureDateAndTime  = LocalDateTime.parse(keberangkatan, formatter);

        // proses validasi
//        try {
//            Validation.validateBookingName(nama);
//            Validation.validateDepartureDateAndTime(departureDateAndTime);
//            Validation.validateRoute(rute);
//        } catch (InvalidBookingName e) {
//            throw new InvalidBookingName(e.getMessage());
//        } catch (InvalidBookingDateAndTime e) {
//            throw new InvalidBookingDateAndTime(e.getMessage());
//        } catch (InvalidBookingRoute e) {
//            throw new InvalidBookingRoute(e.getMessage());
//        } finally {
//            System.out.println("Proses booking tiket selesai");
//        }

        Validation.validateRunTimeBookingName(nama);
        Validation.validateRunTimeDepartureDateAndTime(departureDateAndTime);
        Validation.validateRunTimeRoute(rute);

        BookingInput booking1 = new BookingInput(nama, departureDateAndTime, rute);
        System.out.println("Tiket berhasil dipesan oleh tuan : " + booking1.getName() + "\n Waktu : " + booking1.getDepartureDateAndTime() + "\n Rute : " + booking1.getRoute()) ;

    }

    static String input(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }


}
