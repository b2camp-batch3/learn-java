package id.b2camp.fanri.day10.lambda;

public class LambdaFanriApp {
    public static void main(String[] args) {

        //contoh menggunakan anonymous class
        sendMessage(new CockpitMessage() {
            @Override
            public void print(String airtoAir, String airToGround) {
                System.out.println("This is cockpit " + airtoAir + " and " + airToGround + " using Anonymous class");
            }
        });

        //menggunakan  Lambda menjadi lebih simple
        sendMessage((aa, ag) -> {
            System.out.println("this is cockpit " + aa + " and " + ag + " using Lambda");
        });

        //tes2 Lambda
        Knot aircraftSpeed = (gs, c) -> gs * c;
        System.out.println(aircraftSpeed.knot(1000,0.540 ) + " Knot");

    }

    private static void sendMessage(CockpitMessage cockpitMessage){
            cockpitMessage.print("Message airToAir", "Message airtoGround");
    }

}


