package id.b2camp.fanri.day10.lambda;

public interface Knot {
    double knot(double groundSpeed, double constant);
}
