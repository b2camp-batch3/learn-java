package id.b2camp.fanri.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
//Generic dari Transaction
public class KokoTransaction<T extends KokoPaymentType>{

    private T paymentType;

    public void displayOrder() {
        System.out.println("Belanja busana koko dengan pembayaran " + paymentType);
    }

}
