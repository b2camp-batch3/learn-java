package id.b2camp.fanri.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class DataKoko <K extends TokoKoko> {
    private K value;

    public String getData(String jenis) {
        return jenis;
    }
    public Integer getData(Integer stok) {
        return stok;
    }
    public Double getData(Double harga) {
        return harga;
    }



}
