package id.b2camp.fanri.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class KokoPaymentType {

    private Double totalAmount;
}
