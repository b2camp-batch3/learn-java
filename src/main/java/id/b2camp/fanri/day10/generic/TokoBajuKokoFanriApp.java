package id.b2camp.fanri.day10.generic;


//Generic : Untuk memastikan data yg dimasukan Tipe data/object Sama
//Generic : Mengkotakan suatu tipe data bhw 1 calss bisa diisi secara dynamic
//Generic : Dengan generic tidak perlu mengulang codingan yg sama
public class TokoBajuKokoFanriApp {
    public static void main(String[] args) {

        //Pemakaian Generic : Tipe data yg dynamic
        //Tipe data primitive tidak dapat dipakai
        System.out.println("==============");
        NumericTokoKoko<String> kokoKurta = new NumericTokoKoko<>("Kurta");
        NumericTokoKoko<String> bulanIni = new NumericTokoKoko("Agustus");
        NumericTokoKoko<Integer> stokKurta = new NumericTokoKoko<>(250);
        NumericTokoKoko<Double> hargaKurta = new NumericTokoKoko<>(125000.00);

        kokoKurta.display();
        bulanIni.display();
        stokKurta.display();
        hargaKurta.display();

        System.out.println("\n=============");
        TokoKoko gamis = new TokoKoko("Gamis Pria", 78, 250000.00);
        DataKoko<TokoKoko> data = new DataKoko<>(gamis);

        System.out.println(data.getData(gamis.getJenis()));
        System.out.println(data.getData(gamis.getStok()));
        System.out.println(data.getData(gamis.getHarga()));


        // membatasi batasan di dlm generic dengan boundary
        // boundary tidak bisa multiple
        // contoh dibawah File KokoTransaction di boundary dengan tipenya CashKokoPaymentType
        System.out.println("\n*** Boundary ***");
        System.out.println("================");
        CashKokoPaymentType cash = new CashKokoPaymentType(350000.0);
        KokoTransaction<CashKokoPaymentType> paymentTypeKokoTransaction = new KokoTransaction<>(cash);
        paymentTypeKokoTransaction.displayOrder();

        // KokoTransaction di boundary lg  dengan tipenya EdcMachinePaymentType
        EdcMachinePaymentType debit = new EdcMachinePaymentType(750000.0);
        KokoTransaction<EdcMachinePaymentType> eWalletPaymentTypeKokoTransaction = new KokoTransaction<>(debit);
        eWalletPaymentTypeKokoTransaction.displayOrder();

    }
}
