package id.b2camp.fanri.day10.generic;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
//Class ini memliki tipe data numeric
public class NumericTokoKoko<T> {

    private T value;

    public void display () {
        System.out.println(value);
    }
}
