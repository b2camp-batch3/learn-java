package id.b2camp.fanri.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TokoKoko {
    private String jenis;
    private Integer stok;
    private Double harga;
}
