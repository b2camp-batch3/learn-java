package id.b2camp.fanri.day10.generic;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class EdcMachinePaymentType extends KokoPaymentType {
    public EdcMachinePaymentType(Double totalAmount) {
        super(totalAmount);
    }
}
