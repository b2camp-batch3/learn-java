package id.b2camp.fanri.day10.generic;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CashKokoPaymentType extends KokoPaymentType {

    public CashKokoPaymentType(Double totalAmount) {
        super(totalAmount);
    }
}
