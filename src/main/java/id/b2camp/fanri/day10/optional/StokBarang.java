package id.b2camp.fanri.day10.optional;

import id.b2camp.fakhri.day10.optional.Address;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StokBarang {
    private String namaBarang;
    private Integer stok;
    private Harga harga;

    public StokBarang(String namaBarang, Integer stok, Harga harga) {
        this.namaBarang = namaBarang;
        this.stok = stok;
        this.harga = harga;
    }

    public StokBarang(String namaBarang, Integer stok) {
        this.namaBarang = namaBarang;
        this.stok = stok;
    }
}
