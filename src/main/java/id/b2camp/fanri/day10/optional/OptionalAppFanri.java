package id.b2camp.fanri.day10.optional;

import java.util.Optional;

//Optional : Untuk menggantikan data yg sebenrnya dengan data baru

public class OptionalAppFanri {
    public static void main(String[] args) {
        StokBarang stokBarang = new StokBarang("Sorban", null);
        //proses logic
        String namaStokBarang = stokBarang.getNamaBarang().toLowerCase();
        System.out.println(namaStokBarang);


        //Sebelum menggunakan optional (Kurang efisien dan kurang clarity):
        if(stokBarang.getStok() !=null) {
            System.out.println(stokBarang.getStok());
        } else {
            System.out.println("Stok " + namaStokBarang + " sedang kosong");
        }

        //Setelah menggunakan optional :
        System.out.println("\n*** Dengan Optional ***");
        System.out.println("=======================");
        Optional<Integer> stok = Optional.ofNullable(stokBarang.getStok());
        if(stok.isPresent()) {
            System.out.println(stok.get());
        } else {
            System.out.println("Stok " + namaStokBarang + " saat ini : " + stokBarang.getStok() + " atau " + stok);
        }


        //method untuk mempersingkat Optional diatas
        System.out.println("\n=========================");
        Integer stok2 = Optional.ofNullable(stokBarang.getStok())//Ketika datanya ada
                .orElse(stokBarang.getStok()); //ketika datanya tidak ada
        System.out.println(stok2);


        //Pengecekan optional ketika layernya sudah makin dalam
        System.out.println("==========");
        StokBarang kokoGamis = null;  //datanya sudah Null duluan bisa langsung dieri Optional
        Double jenisBarangKoko = Optional.ofNullable(kokoGamis)
                .map(h -> h.getHarga())
                .map(a -> a.getAnak())
                .orElse(null);

        System.out.println(jenisBarangKoko);

    }
}
