package id.b2camp.fanri.day10.optional;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Harga {
    private Double dewasa;
    private Double anak;
}
