package id.b2camp.fanri.day6.interfaces;

/* interface : menurunkan kontrak khusus dengan menggunakan implement (Tidak apat di extend)
   karena bersifat khusus */
public class AircraftHandlingApp {
    public static void main(String[] args) {
        Airport cgk = new Airport();
        cgk.status();
        System.out.println(cgk.customsAvailable());
        System.out.println(cgk.immigrationsAvailable());
        System.out.println(cgk.quarantineAvailable());
    }
}
