package id.b2camp.fanri.day6.interfaces;


// cara menurunkan interface dengen menggunakan implements
public class Airport implements Customs, Quarantine, Immigrations {

    @Override
    public void status() {
        System.out.println("International Airport");

    }

    @Override
    public String customsAvailable() {
        return "24 hours";
    }

    @Override
    public String immigrationsAvailable() {
        return "24 hours";
    }

    @Override
    public String quarantineAvailable() {
        return "12 hours";
    }
}
