package id.b2camp.fanri.day6.accessmodifiers;

public class FixedWing {
        // public : bisa diakses oleh class manapun
        public static void methodPublic(){
            System.out.println("Boeing 787 - Dream Liner");
            methodPrivate();
        }

        // private : hanya dapat diakses  oleh class yg bersangkutan
        private static void methodPrivate(){

            System.out.println("Airbus 321 - Neo ");
        }

        // default : hanya dapat diakses oleh class di dalam package yg sama
        static void methodDefault(){

            System.out.println("Desault Falcon 7X");
        }

        // protected : hanya dapat diakses oleh class di dalam package yg sama & subclassnya
        protected static void methodProtected() {

            System.out.println("Gulfstream G700");
        }
}
