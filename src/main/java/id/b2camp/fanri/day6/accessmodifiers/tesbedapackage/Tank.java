package id.b2camp.fanri.day6.accessmodifiers.tesbedapackage;

public class Tank {
    public static void landTankPublic() {
        System.out.println( "Tank Leopard Public");
    }
    private static void landTankPrivate() {
        System.out.println( "Tank Leopard Private");
    }
    static void landTankDefault() {
        System.out.println( "Tank Leopard Private");
    }

    protected static void landTankProtected() {
        System.out.println( "Tank Leopard Private");
    }
}
