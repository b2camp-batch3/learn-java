package id.b2camp.fanri.day6.accessmodifiers;

public class RotaryWing {

    public static void methodPublic(){
        System.out.println("Apache AH-64");
        methodPrivate();
    }

    private static void methodPrivate(){

        System.out.println("Sikorsky UH-60 Black Hawk");
    }

    static void methodDefault(){

        System.out.println("Bell Huey");
    }

    protected static void methodProtected() {

        System.out.println("Mill Mi-26");
    }
}
