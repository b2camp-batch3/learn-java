package id.b2camp.fanri.day6.accessmodifiers;

import id.b2camp.fanri.day6.accessmodifiers.tesbedapackage.Tank;

public class Aircraft {
    public static void main(String[] args) {
        //access modifier : public, private, default, protected

        FixedWing.methodPublic();

        // method private tidak dapat dipanggil disini
        //FixedWing.methodPrivate();

        // default : Karena ada dalam package yg sama maka dapat diakses
        FixedWing.methodDefault();
        FixedWing.methodProtected();

        RotaryWing.methodPublic();
        // method private tidak dapat dipanggil disini
        //FixedWing.methodPrivate();

        RotaryWing.methodDefault();
        RotaryWing.methodProtected();

        //TES MEMANGGIL METHOD BEDA PACKAGE
        Tank.landTankPublic();
//        Tank.landTankPrivate(); "Tidak bisa karena Private"
//        Tank.landTankDefault(); "Tidak bisa karena default-nya beda package"
//        Tank.landTankProtected; "Tidak bisa karena beda package"
    }

}
