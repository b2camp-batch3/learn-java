package id.b2camp.fanri.day6.abstractclasses.minigamethecommandos;

//import id.b2camp.fanri.day6.abstractclasses.soldier.Soldier;
import id.b2camp.fanri.day6.abstractclasses.soldier.SoldierSpy;
import id.b2camp.fanri.day6.abstractclasses.soldier.SoldierSniper;

public class MiniGameTheComandosFanri {
    public static void main(String[] args) {

        // membuat object dari non-abstract class
        SoldierSpy soldierSpy = new SoldierSpy("Frank");
        soldierSpy.display();

        SoldierSniper soldierSniper = new SoldierSniper("John");
        soldierSniper.display();

        soldierSniper.levelUp();
        soldierSpy.levelUp();
        soldierSniper.display();
        soldierSpy.display();

        // membuat object dari abstract class
        // tidak dapat dibuat object dari Soldier (abstract class - tidak punya object)
//        Soldier soldier = new Soldier("Joe");
//        soldier.display();
    }

}
