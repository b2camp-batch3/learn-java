package id.b2camp.fanri.day6.abstractclasses.soldier;

//inherit dr abstract Soldier
public class SoldierSpy extends Soldier{
    public SoldierSpy(String name) {
        super(name);
    }

    @Override
    public void levelUp() {
        this.setLevel(2);
    }
}
