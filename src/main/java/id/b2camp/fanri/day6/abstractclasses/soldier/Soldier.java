package id.b2camp.fanri.day6.abstractclasses.soldier;

/* abstract class Soldier : abstract class ini akan "memaksa" sub-classnya (SoldierSniper &
   SoldierSpy untuk menginherit apapun yg ada di abstract Soldier  */
public abstract class Soldier {
    private String name;
    private int level;

    public Soldier(String name) {
        this.name = name;
        this.level = 1;
    }
    public void display() {
        System.out.println("I am " + this.name);
        System.out.println(" Level " + this.level);
    }

    /* abstract method : method ini "memaksa SoldierSniper & SoldierSpy untuk menggunakan
       method ini, kalau tidak maka akan error */
    public abstract void levelUp();

    // setter
    public void setLevel(int deltaLevel) {
        this.level += deltaLevel;
    }

}
