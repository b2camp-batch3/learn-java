package id.b2camp.fanri.day6.abstractclasses.soldier;

public class SoldierSniper extends Soldier {

    public SoldierSniper(String name) {
        super(name);
    }

    @Override
    public void levelUp() {
        this.setLevel(1);
    }
}
