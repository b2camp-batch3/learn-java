package id.b2camp.syahrul.day4;

public class TodoListSyahrul {
    public static String[] tipe = new String[10];

    public static java.util.Scanner scanner = new java.util.Scanner(System.in);

    public static String input(String info) {
        System.out.print(info + " : ");
        String input = scanner.nextLine();
        return input;
    }

    public static void main(String[] args) {

        while (true) {
            menunjukanTodoList();

            System.out.println("Tipe Menu: ");
            System.out.println("(+) Tipe Tambah");
            System.out.println("(-) Tipe Hapus");
            System.out.println("(x) Tipe out Keluar");

            var input = input("Pilih Tipe");

            if (input.equals("+")) {
                tambahListTodo();
            } else if (input.equals("-")) {
                hapusListTodo();
            } else if (input.equals("x")) {
                break;
            } else {
                System.out.println("Pilihan Tidak Di Temukan");
            }
        }
    }

    public static void menunjukanTodoList() {
        System.out.println("TIPETODOLIST");
        for (var i = 0; i < tipe.length; i++) {
            var todo = tipe[i];
            var no = i + 1;

            if (todo != null) {
                System.out.println(no + ". " + todo);
            }
        }
    }

    public static void menambahTodoList(String todo) {
        var isFull = true;
        for (int i = 0; i < tipe.length; i++) {
            if (tipe[i] == null) {
                isFull = false;
                break;
            }
        }

        if (isFull) {
            var temp = tipe;
            tipe = new String[tipe.length * 2];

            for (int i = 0; i < temp.length; i++) {
                tipe[i] = temp[i];
            }
        }
        for (var i = 0; i < tipe.length; i++) {
            if (tipe[i] == null) {
                tipe[i] = todo;
                break;
            }
        }
    }



    public static boolean menghapusTodoList(Integer number) {
        if ((number - 1) >= tipe.length) {
            return false;
        } else if (tipe[number - 1] == null) {
            return false;
        } else {
            for (int i = (number - 1); i < tipe.length; i++) {
                if (i == (tipe.length - 1)) {
                    tipe[i] = null;
                } else {
                    tipe[i] = tipe[i + 1];
                }
            }
            return true;
        }
    }

    public static void tambahListTodo() {
        System.out.println("MENAMBAHKAN TIPETODOLIST");
        var todo = input("Todo (x Jika Batal (Tidak Jadi)");
        if (todo.equals("x")) {
        } else {
            menambahTodoList(todo);}
    }
    public static void hapusListTodo() {
        System.out.println("MENGHAPUS TODOLIST");
        var angka = input("Simbol yang Dihapus (x Jika Batal)");
        if (angka.equals("x")) {
        } else {
            boolean berhasil = menghapusTodoList(Integer.valueOf(angka));
            if (berhasil) {
            }
            System.out.println("Gagal menghapus simbol todolist : " + angka);
        }
    }
}
