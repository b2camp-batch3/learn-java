package id.b2camp.syahrul.day5;

public class CinemaStudioApp {
}
    class CinemaxStudio {
        String movietitle;
        int showtimes;
        int studio;

        CinemaxStudio(String movietitle, int showtimes, int studio) {
            this.movietitle = movietitle;
            this.showtimes = showtimes;
            this.studio = studio;
        }
        void seat() {
            System.out.println("Film recommend adalah film " + movietitle + " di CINEMAXXSTUDIO");
            System.out.println("=====================");
        }
    }

    class Studio1 extends CinemaxStudio {
        Studio1(String movietitle, int showtimes, int studio) {
            super(movietitle, showtimes, studio);
        }
        void seat() {
            super.seat();
            System.out.println("Film " + movietitle + "jam " + showtimes + " siang" + " di studio " + studio);
            System.out.println("=====================");
        }
    }

    class Studio2 extends CinemaxStudio {
        Studio2(String movietitle, int showtimes, int studio) {
            super(movietitle, showtimes, studio);
        }
        void seat() {
            System.out.println("Film " + movietitle + "jam " + showtimes + " siang" + " di Studio " + studio);
            System.out.println("====================");
        }
    }

    class Studio3 extends CinemaxStudio {
        Studio3(String movietitle, int showtimes, int studio) {
            super(movietitle, showtimes, studio);
        }
        void seat() {
            System.out.println("Film " + movietitle + " jam " + showtimes + " sore" + " di studio " + studio);}


        public static void main(String[] args) {


            Studio1 tayang1 = new Studio1("ALADDIN ", 13, 1);
            tayang1.seat();
            System.out.println(" Film " + tayang1.movietitle + "studio 1 Akan segera di mulai");
            System.out.println("===================");

            Studio2 tayang2 = new Studio2("MINIONS ", 12, 2);
            tayang2.seat();
            System.out.println("Film studio " + tayang2.studio + " MINIONS akan segera selesai");
            System.out.println("===================");

            Studio3 tayang3 = new Studio3("INSIDIOUS", 15, 3);
            tayang3.seat();
            System.out.println("===================");
            System.out.println("INSIDIOUS akan mulai jam " + tayang3.showtimes + " sore mohon untuk menunggu");


        }

    }
