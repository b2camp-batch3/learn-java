package id.b2camp.syahrul.day5;

public class Restaurant {
    public static void main(String[] args) {
       Employee employee = new Chef("Kevin", "chef", "de cuisine");
        sayHello(employee);

        Waiter Waiters = new Waiter("Yani", "waiters");
        serve(Waiters);

        Employee employee1 = new Chef("Rendi", "chef", "pastry");
        sayHello(employee1);

        System.out.println("---");

        cook(new Chef("Dito", "Chef", "eksekutif"));
        sayHello(new Waiter("Rara","waiters"));

        System.out.println("---");

        serve(new Waiter("Riri", "waiters"));
        sayHello(new Employee("Budi", "manager"));
    }

    static void sayHello(Employee employee) {
        System.out.println("Hallo, selamat datang di restaurant, nama saya " + employee.name + ", saya sebagai " + employee.role + " senang melayani anda.");
    }

    static void cook(Chef chef) {
        System.out.println("Chef " + chef.name + " sedang memasak di dapur bagian " + chef.section + ".");
    }

    static void serve(Waiter waiter) {
        System.out.println("Pelayan " + waiter.name + " sedang melayani pelanggan");
    }
}

class Employee {
    String name;
    String role;

    Employee(String name, String role) {
        this.name = name;
        this.role = role;
    }
}

class Chef extends Employee {
    String section;

    Chef(String name, String role, String section) {
        super(name, role);
        this.section = section;
    }
}

class Waiter extends Employee {


    Waiter(String name, String role) {
        super(name, role);
    }
}
