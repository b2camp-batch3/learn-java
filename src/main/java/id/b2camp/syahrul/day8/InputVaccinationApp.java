package id.b2camp.syahrul.day8;

import java.util.Scanner;

public class InputVaccinationApp {

    static Scanner scanner = new Scanner(System.in);

    static String inputdata(String shortmessage) {
        System.out.println(shortmessage);
        return scanner.nextLine();
    }

    public static void main(String[] args)
            throws InvalidNameException, InvalidAddresException, InvalidTypeOfVaccinationException {
        System.out.println("Info pendaftaran vaksinasi mohon data untuk di isi");
        System.out.println("---");

        String name = inputdata("Nama : ");
        String addres = inputdata("Alamat : ");
        String typeofvacinnation = inputdata("Jenis Vaksinasi : ");

        try {
            ValidationRegistrationVaccinations.validInName(name);
            ValidationRegistrationVaccinations.validInAddres(addres);
            ValidationRegistrationVaccinations.validInTypeOfVaccination(typeofvacinnation);

        } catch (InvalidNameException nameException) {
            throw new InvalidNameException("Gagal menginput nama " + nameException.getMessage());
        } catch (InvalidAddresException addresException) {
            throw new InvalidAddresException("Gagal menginput alamat " + addresException.getMessage());
        } catch (InvalidTypeOfVaccinationException typeOfVaccinationException) {
            throw new InvalidTypeOfVaccinationException("Gagal menginput jenis vaksinasi " + typeOfVaccinationException.getMessage());
        } finally {
            System.out.println("Inputan selalu diproses");

            ValidationRegistrationVaccinations.validRuntimeinName(name);
//            ValidationRegistrationVaccinations.validRuntimeinAddres(addres);
//            ValidationRegistrationVaccinations.validRuntimeinTypeOfVaccination(typeofvacinnation);

            PersonalDataVaccination vaccinationnama = new PersonalDataVaccination(name, addres, typeofvacinnation);
            System.out.println("Hasil input vaksinasi atas nama " + vaccinationnama.getName() + " berhasil ditemukan");
            System.out.println("---");

            PersonalDataVaccination vaccinationalamat = new PersonalDataVaccination(name, addres, typeofvacinnation);
            System.out.println("Hasil input alamat " + vaccinationalamat.getAddres() + " berhasil ditemukan");
            System.out.println("---");

            PersonalDataVaccination vaccinationtype = new PersonalDataVaccination(name, addres, typeofvacinnation);
            System.out.println("Hasil input jenis vaksin " + vaccinationtype.getTypeofvaccination() + " berhasildi temukan");

        }
    }
}





