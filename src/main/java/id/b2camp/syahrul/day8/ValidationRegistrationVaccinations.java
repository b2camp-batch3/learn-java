package id.b2camp.syahrul.day8;

public class ValidationRegistrationVaccinations {

    public static void validInName(String name) throws InvalidNameException {
        if (name.isBlank()) {
            throw new InvalidNameException("Wajib mengisi nama");
        } else if (name.isEmpty()) {
            throw new InvalidNameException("Nama tidak wajib kosong");
        }

    }

    public static void validInAddres(String addres) throws InvalidAddresException {
        if (addres.isBlank()) {
            throw new InvalidAddresException("Wajib mengisi alamat anda");
        } else if (addres.isEmpty()) {
            throw new InvalidAddresException("alamat tidak wajib kosong");
        }
    }

    public static void validInTypeOfVaccination(String typeofvaccination) throws InvalidTypeOfVaccinationException {
        if (typeofvaccination.isBlank()) {
            throw new InvalidTypeOfVaccinationException("Wajib mengisi jenis vaksin");
        } else if (typeofvaccination.isEmpty()) {
            throw new InvalidTypeOfVaccinationException("tidak wajib kosong");
        }

    }



    public static void validRuntimeinName(String name) throws RuntimeException {
        if (name.isBlank()) {
            throw new RuntimeException("Wajib mengisi nama");
        } else if (name.isEmpty()) {
            throw new RuntimeException("Nama tidak wajib kosong");
        }

    }

    public static void validRuntimeinAddres(String addres) throws RuntimeException {
        if (addres.isBlank()) {
            throw new RuntimeException("Wajib mengisi alamat anda");
        } else if (addres.isEmpty()) {
            throw new RuntimeException("alamat tidak wajib kosong");
        }
    }

    public static void validRuntimeinTypeOfVaccination(String typeofvaccination) throws RuntimeException {
        if (typeofvaccination.isBlank()) {
            throw new RuntimeException("Wajib mengisi jenis vaksin");
        } else if (typeofvaccination.isEmpty()) {
            throw new RuntimeException("tidak wajib kosong");
        }

    }
}