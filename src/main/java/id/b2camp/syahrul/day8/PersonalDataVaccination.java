package id.b2camp.syahrul.day8;

public class PersonalDataVaccination {

    private String name;
    private String addres;
    private String typeofvaccination;


    public PersonalDataVaccination(String name, String addres, String typeofvaccination) {
        this.name = name;
        this.addres = addres;
        this.typeofvaccination = typeofvaccination;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public String getTypeofvaccination() {
        return typeofvaccination;
    }

    public void setTypeofvaccination(String typeofvaccination) {
        this.typeofvaccination = typeofvaccination;
    }


}
