package id.b2camp.syahrul.day11;

import java.util.*;

public class LinkedListSyahrul {
    public static void main(String[] args) {
        List<String> birdspecies = new LinkedList<>();
        birdspecies.add("Burung merpati");
        birdspecies.add("Burung cendrawasih");
        birdspecies.add("Burung elang");
        birdspecies.add("Burung beo");
        birdspecies.add("Burung unta");
        birdspecies.add("Burung perkutut");
        System.out.println(birdspecies.size());
        birdspecies.addAll(birdspecies);
        System.out.println(birdspecies);

        System.out.println("---");
        System.out.println(birdspecies.get(0));
        System.out.println(birdspecies.get(2));
        System.out.println(birdspecies.get(4));

        System.out.println("---");
        List<Integer> sequencenumbers = new LinkedList<>();
        int sequence = 0;
        for (int i = 0; i < 1000; i++){
            sequencenumbers.add(sequence++);}
        System.out.println(sequencenumbers);
        System.out.println(sequencenumbers.get(7));
        System.out.println(sequencenumbers.get(27));
        sequencenumbers.add(11);
    }
}