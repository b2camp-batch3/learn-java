package id.b2camp.syahrul.day11;

import java.util.*;

public class SetSyahrul {
    public static void main(String[] args){
        System.out.println("Novel");
        System.out.println("---");

        Set<String> novel = new HashSet<>(List.of("Novel Kambing jantan", "Novel Kambing jantan", "Novel Ngenest", "Novel Bumi", "Novel Amba", "Novel Amba"));
        System.out.println(novel);
        System.out.println("---");

        Set<String> creator = new HashSet<>(List.of("Pencipta : " , "Laksmi pamuntjak", "Laksmi pamuntjak", "Ernest prakasa", "Ernest prakasa", "Tere liye", "Raditya dika"));
        System.out.println(novel.size());
        System.out.println(creator);
        System.out.println("---");

        Set<String> jadwal = new HashSet<>(List.of("Senin", "Senin", "Selasa", "Selasa","Selasa", "Rabu", "Kamis", "Kamis"));
        System.out.println(jadwal.size());
        System.out.println(jadwal);
    }
}
