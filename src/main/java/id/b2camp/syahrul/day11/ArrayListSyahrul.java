package id.b2camp.syahrul.day11;

import java.util.*;

public class ArrayListSyahrul {
    public static void main(String[] args) {

        List<String> treenames = new ArrayList<>();
        treenames.add("Pohon mangga");
        treenames.add("Pohon jambu");
        treenames.add("Pohon srikaya");
        System.out.println(treenames);
        System.out.println(treenames.size());
        System.out.println("---");

        treenames.remove("Pohon jambu");
        treenames.add("Pohon nangka");
        treenames.add("Pohon durian");
        System.out.println(treenames);
        System.out.println("---");

        treenames.remove("Pohon nangka");
        treenames.add("Pohon rambutan");
        System.out.println(treenames);
        System.out.println(Arrays.toString(treenames.toArray()));
        System.out.println("---");
        System.out.println(treenames.get(0));
        System.out.println(treenames.get(2));
    }
}