package id.b2camp.syahrul.day11;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class MapSyahrul {
    public static void main(String[] args) {
        Map<Integer, String> typehandphone = new HashMap<>();
        System.out.println("Toko Handphone");
        System.out.println("---");
        typehandphone.put(1, "Handphone samsung");
        typehandphone.put(2, "Handphone advan");
        typehandphone.put(3, "Handphone asus");
        typehandphone.put(4, "Handphone xiomi");
        typehandphone.put(5, "Handphone oppo");
        typehandphone.put(6, "Handphone infinix");
        typehandphone.put(7, "Handphone vivo");

        System.out.println(typehandphone.size());
        System.out.println(typehandphone);
        System.out.println("---");
        System.out.println(typehandphone.get(3));
        System.out.println(typehandphone.get(5));

        System.out.println("---");
        HashMap<String, List<String>> store = new HashMap<>();
        ArrayList<String> shopkeeper = new ArrayList<>(List.of("Lala", "Digo", "Bayu", "Dewi"));
        store.put("Penjaga toko", shopkeeper);
        System.out.println(store);
        System.out.println(store);
        System.out.println(store.get("Penjaga toko"));

        System.out.println("---");
        ArrayList<String> customer = new ArrayList<>(List.of("Jojo membeli hp samsung", "Dito membeli hp infinix", "Rita membeli hp vivo"));
        store.put("Customer", customer);
        System.out.println(store);
    }
}
