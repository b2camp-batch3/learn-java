package id.b2camp.syahrul.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
    public class ParfumeDescription<T extends Parfume> {
        private T parfume;
        public void brand() {
            System.out.println("Penjelasan terkait parfum " + parfume);
        }
    }
