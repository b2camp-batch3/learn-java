package id.b2camp.syahrul.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
    public class ParfumeComposition <T> {
        private T value;

        public void print() {
            System.out.println(value);

        }
    }
