package id.b2camp.syahrul.day10.generic;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
    public class Morris extends Parfume{
        public Morris(String typeparfum) {
            super(typeparfum);
        }
    }
