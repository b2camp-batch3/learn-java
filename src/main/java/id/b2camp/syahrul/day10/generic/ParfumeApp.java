package id.b2camp.syahrul.day10.generic;

public class ParfumeApp {
        public static void main(String[] args) {
            Morris keterangan = new Morris("Morris");
            ParfumeDescription<Morris> parfumeDescription = new ParfumeDescription<>(keterangan);
            parfumeDescription.brand();

            ParfumeComposition<String> type = new ParfumeComposition<>("Parfum : Morris");


            ParfumeComposition<String> usefor = new ParfumeComposition<>("Bisa di pakai untuk : Perempuan dan Laki-laki");


            ParfumeComposition<String> composition = new ParfumeComposition<>("Komposisi : Mengandung alcohol denat, parfum dan, aqua");


            ParfumeComposition<String> description = new ParfumeComposition<>("Peringatan : jauhkan dari jangkauan anak-anak, parfum mengandung alkohol hindari juga dengan kontak mata");
            type.print();
            System.out.println("---");
            usefor.print();
            System.out.println("---");
            composition.print();
            System.out.println("---");
            description.print();
        }
    }



