package id.b2camp.syahrul.day10.lambda;

public class Cupboard {
    private static void descriptionType(WardrobeType clothingType) {
        clothingType.description("Lemari pakaian dua pintu", "Selma Alexa");
        clothingType.description("Lemari pakaian satu pintu", "Saga li");
    }

    private static void descriptionFunction(WardrobeFunction clothingTpe) {
        clothingTpe.description();
    }

    private static void description() {
        System.out.println("Fungsi lemari sendiri adalah untuk menyimpan aneka barang atau semacam seperti pakaian");
    }

    public static void main(String[] args) {
        descriptionType(new WardrobeType() {
            @Override
            public void description(String type, String brand) {
                System.out.println(brand + " Adalah lemari yang bahannya semi keras");
            }

        });
        System.out.println("---");
        descriptionType((type, brand) -> {
            System.out.println("Lemari " + brand + " adalah termasuk " + type);

        });
        System.out.println("---");
        descriptionType((type, brand) -> {
            System.out.println("Lemari " + brand + " dijual sekarang jangan sampai kehabisan");
        });
        System.out.println("---");
        descriptionFunction(Cupboard::description);
    }
}