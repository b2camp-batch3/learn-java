package id.b2camp.syahrul.day10.opsional;

import java.util.Optional;

public class WifiApp {
    public static void main(String[] args) {
        Wifi wifi = new Wifi("Indihome", null, "Optik fiber", null);

        String nameWifi = Optional.ofNullable(wifi.getNamewifi())
                .map(String::toLowerCase)
                .orElse("Nama wifi tidak tersambung");
        System.out.println("Wifi : " + nameWifi);

        System.out.println("---");

        String paswordwifi = Optional.ofNullable(wifi.getPasword())
                .map(p -> p.toLowerCase())
                .orElse("Pasword wifi tidak tersambung");
        System.out.println("Pasword : " + paswordwifi);

        System.out.println("---");

        String teknologi = Optional.ofNullable(wifi.getTeknologi())
                .map(t -> t.toLowerCase())
                .orElse("Teknologi wifi tidak tersambung");
        System.out.println("Teknologi : " + teknologi);

        System.out.println("---");

        String mbps = Optional.ofNullable(wifi.getMbps())
                .map(m -> m.toLowerCase())
                .orElse("Mbps tidak tersambung");
        System.out.println("Mbps : " + mbps);

    }
}

