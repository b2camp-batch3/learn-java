package id.b2camp.syahrul.day10.opsional;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Wifi {

    private String namewifi;
    private String pasword;
    private String teknologi;
    private String mbps;
}

