package id.b2camp.syahrul.day7.statickeyword;

public class ApplicationApp {
    public static void main(String[] args) {
        String pembuat = MakerApplication.CREATOR ;
        double version =MakerApplication.VERSION;
        String aplication = MakerApplication.APPLICATION;

        System.out.println();
        System.out.println("Pembuat aplikasi " + aplication + " adalah " + pembuat);
        System.out.println("Dan facebook saat ini adalah versi" + " " + version);
        System.out.println("Dan juga aplikasi " + aplication + " sudah banyak sekali digunakan oleh sebagian masyarkat di dunia"+".");

        System.out.println("---");

        Application.Description description = new Application.Description();
        description.setDeskripsi("Facebook adalah media sosial dan layanan jejaring sosial daring amerika yang di miliki oleh meta platforms");
        System.out.println("DESKRIPSI aplication : " +description.getDeskripsi()+".");

    }
}
