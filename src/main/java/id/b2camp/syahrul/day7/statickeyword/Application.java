
package id.b2camp.syahrul.day7.statickeyword;

public class Application {
    String download;

    public String getDownload() {return download;}

    public void setDownload(String download) {this.download = download;}

    static class Description {
        String deskripsi;

        public String getDeskripsi() {return deskripsi;}

        public void setDeskripsi(String deskripsi) {this.deskripsi = deskripsi;}
    }
}