package id.b2camp.syahrul.day7.anonymousclasses;

public interface ElectricFanFunction {
    String fungsi();

    int inch();

    void peringatan();

}
