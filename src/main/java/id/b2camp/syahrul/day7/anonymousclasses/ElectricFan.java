package id.b2camp.syahrul.day7.anonymousclasses;

public class ElectricFan {
    public static void main(String[] args) {


        ElectricFanBrand electricFanBrand = new ElectricFanBrand() {
            @Override
            public void namebrand() {
                System.out.println("Cosmos");
            }

        };
        electricFanBrand.namebrand();


        ElectricFanFunction electricFanFunction = new ElectricFanFunction() {
            @Override
            public String fungsi() {
                return "Fungsi dari kipas ini adalah menghasilkan angin, guna mendinginkan udara, serta memberikan efek menyegarkan di saat udara terasa panas" + ".";

            }

            @Override
            public int inch() {
                return 12;
            }

            @Override
            public void peringatan() {
                System.out.println("PERINGATAN");
                System.out.println("---");
                System.out.println("Bahaya Pemakaian kipas angin berlebihan");
                System.out.println("---");
                System.out.println("1. Mudah kesemutan");
                System.out.println("2. Menyebabkan kaku pada otot leher");
            }
        };
        electricFanFunction.fungsi();
        electricFanFunction.inch();
        electricFanFunction.peringatan();

    }
}