package id.b2camp.syahrul.day7.enums;

public class Warehouse {
    public static void main(String[] args) {
        SellStatus stokpertama = SellStatus.AVAILABLE ;
        SellStatus stokkedua = SellStatus.SUCCESS;
        SellStatus stokketiga = SellStatus.SOLD_OUT;
        System.out.println(stokpertama);
        System.out.println(stokkedua);
        System.out.println(stokketiga);


        System.out.println("---");
        StockWareHouse stockwarehouse = new StockWareHouse("Minyak", 20);
        System.out.println("Stok barang " + stockwarehouse.getStokbarang() + " dengan jumblah " + stockwarehouse.getJumlah());
        StockWareHouse stockwarehouse1 = new StockWareHouse("Sabun", 30);
        System.out.println("Stok barang " + stockwarehouse1.getStokbarang() + " dengan jumblah " + stockwarehouse1.getJumlah());
        StockWareHouse stockwarehouse2 = new StockWareHouse("Susu",100);
        System.out.println("Stok barang " + stockwarehouse2.getStokbarang() + " dengan jumblah " + stockwarehouse2.getJumlah());
        System.out.println("---");
        System.out.println("Stok " + stockwarehouse.getStokbarang() + " : " + stokpertama.getLanguage() );
        System.out.println("Stok " + stockwarehouse1.getStokbarang() + " : " + stokkedua.getLanguage());
        System.out.println("Stok " + stockwarehouse2.getStokbarang() + " : " + stokketiga.getLanguage());

    }
}
