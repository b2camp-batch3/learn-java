package id.b2camp.syahrul.day7.enums;

public class StockWareHouse {
    private String stokbarang;
    private int jumlah;
    public StockWareHouse(String stokbarang, int jumlah) {
        this.stokbarang = stokbarang;
        this.jumlah = jumlah;
    }

    public String getStokbarang() {
        return stokbarang;
    }

    public void setStokbarang(String stokbarang) {
        this.stokbarang = stokbarang;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumblah(int jumlah) {
        this.jumlah = jumlah;
    }
}