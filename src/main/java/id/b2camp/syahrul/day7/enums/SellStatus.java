package id.b2camp.syahrul.day7.enums;

public enum SellStatus {

    AVAILABLE("Barang tersedia"),
    SUCCESS("Sukses terjual"),
    SOLD_OUT("Barang habis terjual");


    final String language;

    public String getLanguage() {
        return language;
    }

    SellStatus(String language) {
        this.language = language;


    }
}