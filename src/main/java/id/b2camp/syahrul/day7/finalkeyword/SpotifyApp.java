package id.b2camp.syahrul.day7.finalkeyword;

public class SpotifyApp {
    public static void main(String[] args) {


        SongsJourney songsJourney = new SongsJourney();
        songsJourney.setNameSongs("Journey");
        System.out.println("Penyanyi lagu " + songsJourney.getNameSongs() + " adalah " + songsJourney.singersongs("Woodz"));
        System.out.println("---");

        SongsGoldenhour songsGoldenhour = new SongsGoldenhour();
        songsGoldenhour.setNameSongs("Golden Hour");
        System.out.println("Penyanyi lagu " + songsGoldenhour.getNameSongs() + " adalah " + songsGoldenhour.singersongs("Jvke"));
        System.out.println("---");

        SongsAsItWas songsAsItWas = new SongsAsItWas();
        songsAsItWas.setNameSongs("As it was");
        System.out.println("Penyanyi lagu " + songsAsItWas.getNameSongs() + " adalah " + songsAsItWas.singersongs("Harry Styles"));


    }
}