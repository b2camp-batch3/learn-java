package id.b2camp.syahrul.day7.finalkeyword;

public class Spotify {
    private String nameSongs;

    public String getNameSongs() {
        return nameSongs;
    }

    public void setNameSongs(String nameSongs) {
        this.nameSongs = nameSongs;
    }
}

class SongsAsItWas extends Spotify {
    final String singersongs(String sing) {
        return sing;
    }
}

final class SongsJourney extends SongsAsItWas {

}

final class SongsGoldenhour extends SongsAsItWas {

}
