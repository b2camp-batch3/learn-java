package id.b2camp.syahrul.day2;

import java.util.Arrays;

public class ArrayandForloopSyahrul {
    public static void main(String[] args) {

        String[] nama = {"Herri", "Daus", "Karin", "Siti", "Adit", "Ririn"};
        System.out.println(nama[3]);
        System.out.println(nama[5]);
        System.out.println(Arrays.toString(nama));
        for (String name : nama)
            if (name.equalsIgnoreCase("Herri")) {
                System.out.println(name);

                System.out.println("---");

                int[] angka = {100, 200, 300, 400, 500};
                int total = 2;

                for (int i = 0; i < angka.length; i++);
                total += angka[2];

                System.out.println("total angka " + total);

                System.out.println("---");

                int[] nilai = {10, 20, 30, 40};
                System.out.println(nilai[3]);

                System.out.println("---");


                System.out.println("nilai maksimum " + nilai);

                for (int n = 0; n < nilai.length; n++) {
                    System.out.println(nilai[n]);
                }
                System.out.println("---");

                for (int g = nilai.length - 1; g >= 0; g--) {
                    System.out.println(nilai[g]);
                }



            }
    }
}
