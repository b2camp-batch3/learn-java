package id.b2camp.syahrul.day2;

public class ComparisonSyahrul {

    public static void main(String[] args) {
int ukuranBaju = 40;
int ukuranKemeja = 41;

boolean isStudent = true;
boolean isAdult = false;
boolean isPassed = true;


        System.out.println(ukuranBaju >= ukuranKemeja);
        System.out.println(ukuranBaju == ukuranKemeja);
        System.out.println(ukuranBaju <= ukuranKemeja);
        System.out.println(ukuranBaju > ukuranKemeja);
        System.out.println(ukuranBaju != ukuranKemeja);
        System.out.println(ukuranBaju < ukuranKemeja);


        System.out.println("---");

        System.out.println(isStudent || isAdult);
        System.out.println(isStudent && isAdult);
        System.out.println(isAdult || isPassed);
        System.out.println(isStudent && isAdult || isPassed);
        System.out.println(isAdult || isStudent && isPassed);
        System.out.println(isStudent && isPassed || isAdult);

    }

}
