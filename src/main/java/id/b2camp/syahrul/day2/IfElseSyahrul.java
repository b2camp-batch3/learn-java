package id.b2camp.syahrul.day2;


public class IfElseSyahrul {
    public static void main(String[] args) {


        System.out.println("menu minuman");
        System.out.println("1.es teh");
        System.out.println("2.es susu");
        System.out.println("3.es jeruk");
        System.out.println("4.air putih");

        int menu = 2;

        if (menu == 1) {
            System.out.println("es teh");
        } else if (menu == 2) {
            System.out.println("es susu");
        } else if (menu == 3) {
            System.out.println("es jeruk");
        } else if (menu == 4) {
            System.out.println("air putih");
        } else {
            System.out.println("Tidak tersedia");

        }
    }

}