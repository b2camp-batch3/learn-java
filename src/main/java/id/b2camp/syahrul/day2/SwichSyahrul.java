package id.b2camp.syahrul.day2;

public class SwichSyahrul {

    public static void main(String[] args) {
        String size = "L";

        switch (size){
            case "S" :
                System.out.println("ukuran kecil");
                break;
            case "M" :
                System.out.println("ukuran sedang");
                break;
            case "L" :
                System.out.println("ukuran besar");
                break;
            case "Xl" :
                System.out.println("ukuran extra besar");
                break;
            case "XLL" :
                System.out.println("ukuran double extra besar");
                break;
            default:
                System.out.println("ukuran tidak valid");
                    break;

        }
    }
}
