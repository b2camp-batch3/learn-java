package id.b2camp.syahrul.day2;

public class ArithmeticSyahrul {
    public static void main(String[] args) {


        int a = 8;
        int b = 4;

        System.out.println(a * b);
        System.out.println(a / b);
        System.out.println(a - b);
        System.out.println(a + b);


        System.out.println(101 * 20);
        System.out.println(80 / 10);
        System.out.println(110 - 12);
        System.out.println(200 + 30);
        System.out.println(21 * 12);

        System.out.println(Math.max(6, 5 ));
        System.out.println(Math.min(3, 5));
        System.out.println(Math.abs(5));
        System.out.println(Math.log(9));

    }
}
