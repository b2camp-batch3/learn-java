package id.b2camp.syahrul.day12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FunctionalSyahrul {
    public static void main(String[] args) {
        List<String> names = List.of("Hilda", "Maudi", "Agus", "Mimi", "Caca", "Bagas");
        if (names.contains("Hilda")) {
            System.out.println("Hilda found");
        } else {
            System.out.println("Hilda not found");
        }
        List<String> listname = List.of("Maya", "Adi", "Mala", "Uci", "Sasa", "Gea");
        List<String> names1 = listname.stream()
                .filter(name -> name.length() < 4)
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        names1.forEach(System.out::println);
        System.out.println("---");

        List<String> lastname = List.of("Gaga", "Lui", "Rusli", "Uci", "Salma", "Bia");
        List<String> names2 = lastname.stream()
                .filter(name -> name.length() != 5)
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        names2.forEach(System.out::println);


    }
}