package id.b2camp.syahrul.day12;

import java.io.IOException;
import java.util.List;

public class ImperativeStylSyahrul {
    public static void main(String[] args) {
        List<String> names = List.of("Deni", "Alia", "Cindy", "Crishtin", "Charlie", "Nina", "Ananda", "Alan");

        System.out.println("Nama dengan jumlah huruf genap: ");
        for (String name : names) {
            if (name.length() == 4) {
                System.out.println(name.toUpperCase() + ", ");

            }
        }
        System.out.println("\n");
        System.out.println("---");

        System.out.println("Nama yang diawali dengan huruf 'C': ");
        for (String name : names) {
            if (name.contains("c") || name.contains("C")) {
                System.out.print(name + ", ");
            }
        }

        System.out.println("\n");
        System.out.println("---");

        System.out.println("Nama yang diawali dengan huruf 'A': ");
        for (String name : names) {
            if (name.startsWith("A")) {
                System.out.print(name + ", ");

            }
        }
    }
}






