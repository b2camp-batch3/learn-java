package id.b2camp.syahrul.day12.stream;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Shoes {
    private String brandShoes;
    private String useFor;
    private String color;
    private double size;

}
