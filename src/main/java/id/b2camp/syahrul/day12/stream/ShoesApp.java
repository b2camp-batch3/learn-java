package id.b2camp.syahrul.day12.stream;
import id.b2camp.gita.day12.stream.Animals;

import java.util.List;
import java.util.stream.Collectors;

public class ShoesApp {
    public static void main(String[] args) {
        List<Shoes> sepatu = getshose();

        List<Shoes> typeshoes = sepatu.stream()
                .filter(shoes -> shoes.getBrandShoes().equals("Nike"))
                .collect(Collectors.toList());
        typeshoes.forEach(System.out::println);

        System.out.println("---");

        List<Shoes> typeShoes1 = sepatu.stream()
                .filter(shoes -> shoes.getBrandShoes().equalsIgnoreCase("Adidas"))
                .collect(Collectors.toList());
        typeShoes1.forEach(System.out::println);

        System.out.println("---");

        List<Shoes> sizeShose = sepatu.stream()
                .filter(shoes -> shoes.getSize() == 40)
                .collect(Collectors.toList());
        sizeShose.forEach(System.out::println);
        System.out.println("---");

        List<Shoes> colorShoes = sepatu.stream()
                .filter(shoes -> shoes.getColor().equalsIgnoreCase("Hitam"))
                .collect(Collectors.toList());
        colorShoes.forEach(System.out::println);

        System.out.println("---");

        List<Shoes> useShoes = sepatu.stream()
                .filter(shoes -> shoes.getUseFor().equalsIgnoreCase("Bermain basket"))
                .collect(Collectors.toList());
        useShoes.forEach(System.out::println);


    }

    static List<Shoes> getshose() {
        return List.of(
                new Shoes("Nike", "Berolahraga", "Hitam", 40),
                new Shoes("Adidas", "Bermain Basket", "Putih", 32),
                new Shoes("Air jordan", "Bermain basket", "Biru",40),
                new Shoes("Puma", "Berpergian", "Hitam", 41),
                new Shoes("Rebook", "Berolahraga", "Oren", 36),
                new Shoes("Saucony","Pelari dan atlet", "Putih", 40),
                new Shoes("Fila","Berjalan-jalan, dan berolahraga","Putih", 39),
                new Shoes("Macbeth", "Untuk gaya sehari-hari", "Abu-abu", 37),
                new Shoes("Skecher", "Berjalan-jalan, dan berolahraga", "Hitam",43),
                new Shoes("Vans", " Bermain skateboard", "Kuning", 40)
        );
    }
}