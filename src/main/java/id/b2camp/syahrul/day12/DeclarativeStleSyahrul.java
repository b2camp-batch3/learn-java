package id.b2camp.syahrul.day12;

import java.util.List;

public class DeclarativeStleSyahrul {
    public static void main(String[] args) {
        List<String> fruit = List.of("Mangga", "Apel", "Jeruk", "Nanas");
        List<String> fruit1 = List.of("Mangga", "Apel", "Melon", "Jeruk", "Nanas");

        if (fruit1.contains("Melon")) {
            System.out.println("Buah Melon ditemukan ");
        } else {
            System.out.println("Buah Melon tidak ditemukan");

        }
        System.out.println("Daftar buah : " + fruit);

        if (fruit.contains("Daftar buah")) {
            System.out.println("Buah Melon ditemukan ");
        } else {
            System.out.println("Buah Melon tidak ditemukan");


        }
    }
}





