package id.b2camp.syahrul.day1;

public class HelloSyahrul {
    public static void main(String[] args) {

        System.out.println("syahrul");

        //lima tipe data primitive
        byte empat = 4;
        int usia = 18;
        char insial, s = 'S';
        boolean benar = true;
        double price = 19.99;


        //lima tipe data non primitive
        String name = "syahrul";
        Integer[] angka = {1, 2, 3, 4, 5};
        Byte iniByte = null;
        Long iniLong = 1000L;
        Boolean married = true;


        System.out.println(empat);
        System.out.println(usia);
        System.out.println(s);
        System.out.println(benar);
        System.out.println(price);


        System.out.println(name);
        System.out.println(angka[0]);
        System.out.println(iniByte);
        System.out.println(iniLong);
        System.out.println(married);

    }
}
