package id.b2camp.syahrul.day9;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Time;
import java.time.LocalDateTime;

public class CleaningServiceApp {

    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
        CleaningService kebersihan = new CleaningService("Menyapu", "Agar terhindar dari debu dan kotoran-kotoran sampah");
        CleaningService kebersihan1 = new CleaningService("Mengepel", "Agar terlihat bersih dan menghilangkan kotoran");
        Field[] bidang = kebersihan.getClass().getDeclaredFields();
        for (Field field : bidang) {
            System.out.println(field);
        }
        Method[] methods = kebersihan.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        System.out.println("---");
        if (kebersihan.getClass().isAnnotationPresent(DescriptionCleaningActivity.class)) {
            System.out.println("Tugas cleaning service utamanya yaitu melakukan pembersihan serta pemeliharaan lantai");
            System.out.println("Di antara nya adalah : ");
            System.out.println("1. Menyapu");
            System.out.println("2. Mengepel");
        }


        System.out.println("---");
        for (Field field : kebersihan.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Aktivitas1.class)) {
                field.setAccessible(true);
                Object object = field.get(kebersihan);
                Object object1 = field.get(kebersihan1);
                System.out.println("Dini ditugaskan " + object + " dibagian lantai atas");
                System.out.println("Ririn ditugaskan " + object1 + " dibagian lantai bawah");
            }
        }

        System.out.println("---");
        for (Method method : kebersihan.getClass().getDeclaredMethods()){
            if(method.isAnnotationPresent(CleaningTool.class)){
                CleaningTool cleaningTool1 = method.getAnnotation(CleaningTool.class);
                for (int i = 0; i < cleaningTool1.number(); i++)
                    method.invoke(kebersihan);
            }
        }
        System.out.println("---");
        LombokWithCleaningSchedule schedule = new LombokWithCleaningSchedule("Senin-rabu", 08.00);
        LombokWithCleaningSchedule schedule1 = new LombokWithCleaningSchedule("Kamis-sabtu", 08.00);
        System.out.println("Dini mendapatkan jadwal kebersihan di hari " + schedule.getHari() + " dijam " + schedule.getJam() + " Pagi");
        System.out.println("Sedangkan Ririn mendapatkan jadwal kebersihan di hari " + schedule1.getHari() + " dijam " + schedule1.getJam() + " Pagi");

    }

}

