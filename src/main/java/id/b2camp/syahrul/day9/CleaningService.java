package id.b2camp.syahrul.day9;

@DescriptionCleaningActivity
public class CleaningService {

    @Aktivitas1
    private String aktivitas;
    private String fungsi;

    public CleaningService(String aktivitas, String fungsi) {
        this.aktivitas = aktivitas;
        this.fungsi = fungsi;
    }
    @CleaningTool(number = 3)
    public void broomtoolandmoptool() {
        System.out.println("Alat yang akan digunakan Dini dan Ririn adalah sapu dan pel-an");
    }
    public void utility(){System.out.println("Alat sapu dan pel-an adalah termasuk alat yang akan dipakai");}

    public String getAktivitas() {
        return aktivitas;
    }

    public void setAktivitas(String aktivitas) {
        this.aktivitas = aktivitas;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }
}





