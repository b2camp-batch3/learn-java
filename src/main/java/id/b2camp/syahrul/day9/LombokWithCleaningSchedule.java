package id.b2camp.syahrul.day9;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter

public class LombokWithCleaningSchedule {

    private String hari;
    private double jam;
}

