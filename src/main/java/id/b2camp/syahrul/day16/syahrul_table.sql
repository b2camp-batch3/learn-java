CREATE TABLE syahrul_table(
ID INT PRIMARY KEY NOT NULL,
Name varchar(20) NOT NULL,
Gender varchar (20) NOT NULL,
Age INT NOT NULL,
Address varchar(50) NOT NULL,
Status varchar (20) NOT NULL,
Postal_code varchar (10) NOT NULL
);

insert into syahrul_table (ID, Name ,Gender,  Age, Address, Status, Postal_code)
values (172, 'Joko Rahmat', 'Laki-laki', 35, 'Jl. Cempaka Indah II No. 24', 'Sudah menikah', '92567');

insert into syahrul_table (ID, Name ,Gender,  Age, Address, Status, Postal_code)
values (539, 'Jawir Yudodo', 'LAKI-lAKI',  21 , 'Jl. Selamet no.40', 'Belum menikah', '52672');

insert into syahrul_table (ID, Name ,Gender,  Age, Address, Status, Postal_code)
values (736, 'Miranti', 'Perempuan', 22, 'Jl. Pegangsaan Timur Nomor 14', 'Belum menikah', '25637');

insert into syahrul_table (ID, Name ,Gender,  Age, Address, Status, Postal_code)
values (374, 'Yanti Maria', 'Perempuan', 40, 'Jl. Sukaasih no.4', 'Sudah Menikah', '26357');

insert into syahrul_table (ID, Name ,Gender,  Age, Address, Status, Postal_code)
values (835, 'Yuyun Sukmawati', 'Perempuan', 52,  'Jl. Nirmala no.2', 'Sudah menikah', '37284');

insert into syahrul_table (ID, Name ,Gender,  Age, Address, Status, Postal_code)
values (484, 'Anwar Jaenal', 'Laki-laki', 20, 'Jl. Anggrek no.6', 'Belom menikah', '36384');

insert into syahrul_table (ID, Name ,Gender,  Age, Address, Status, Postal_code)
values(236, 'Siti Jaenab', 'Perempuan', 32, 'Jl. Parung no.10', 'Sudah menikah', '17483');


drop table syahrul_table;


select * from  syahrul_table where Name = 'Miranti';

select * from  syahrul_table orde by Age asc;

select * from  syahrul_table order by ID desc;

select * from  syahrul_table where Status = 'Sudah menikah' and Name like 'J%';

select * from  syahrul_table where not Age = 35;

delete from  syahrul_table where Postal_code = '36384';

select * from  syahrul_table where Address = 'Jl. Sukaasih no.4' and Postal_code like '26357';

select * from syahrul_table;



