package id.b2camp.syahrul.day3;

import java.util.Scanner;
public class ScannerSyahrul {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("nama saya adalah : ");
       String nama = scanner.nextLine();
        System.out.println("nama saya adalah " + nama + " senang bertemu denganmu");


        System.out.println("usia kamu adalah : ");
        String usia = scanner.nextLine();
        System.out.println("usia saya adalah " + usia + " saya baru saja lulus sekolah");


        System.out.println("masukkan jenis kelamin (pria/wanita) ");
        String jenisKelamin = scanner.nextLine();

        if (jenisKelamin.equalsIgnoreCase("pria")) {
            System.out.println("saya adalah seorang pria");
        }
        else if (jenisKelamin.equalsIgnoreCase("wanita")) {
            System.out.println("saya adalah seorang wanita");
        }
        else
            System.out.println("jenisKelamin tida valid");
    }
}