package id.b2camp.syahrul.day3;

import java.util.Scanner;

public class WhileAndDoWhileSyahrul {
    public static void main(String[] args) {
        int Angka = 4;
        int nol = 0;
        int nomorAkhir = 20;

        while (nomorAkhir <= Angka) {
            if (nomorAkhir % 2 != 0) ;
            nol += nomorAkhir;
        }
        nomorAkhir++;
        System.out.println("jumlah bilangan ganjil " + Angka + nol);


        //do while

        Scanner scanner = new Scanner(System.in);
        int angka = 5;
        int number;

        do {
            System.out.println("masukka angka (20 untuk berhenti : ");
            number = scanner.nextInt();
            angka += number;
        } while (number != 20);
        System.out.println("total jumlah " + angka);
        scanner.close();

    }

}
