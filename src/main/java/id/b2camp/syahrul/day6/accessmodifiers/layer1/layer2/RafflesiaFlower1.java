package id.b2camp.syahrul.day6.accessmodifiers.layer1.layer2;

public class RafflesiaFlower1 {
    protected String name;
    protected String origin;
    protected String color;


    public RafflesiaFlower1(String name, String origin, String color) {
        this.name = name;
        this.origin = origin;
        this.color = color;
    }

    public void info(){

    }
}
