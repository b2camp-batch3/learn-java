package id.b2camp.syahrul.day6.accessmodifiers.layer1.layer2;

import id.b2camp.syahrul.day6.accessmodifiers.layer1.layer2.RafflesiaFlower1;

public class RafflesiaFlower {
    public static void main(String[] args) {

        RafflesiaFlower1 rafflesiaFlower = new RafflesiaFlower1("Rafflesia", "Sumatra dan kalimantan", "Merah");
        System.out.println( "bunga yang terdapat di indonesia di daerah " + rafflesiaFlower.origin + " ini adalah bunga " + rafflesiaFlower.name + " yang memiliki warna " + rafflesiaFlower.color+".");

    }
}