package id.b2camp.syahrul.day6.accessmodifiers;

import id.b2camp.syahrul.day6.accessmodifiers.layer1.TulipsFlower;

public class Flower {
    public static void main(String[] args) {
        TulipsFlower tulipsFlower = new TulipsFlower("Tulip", "Asia tengah", "Merah,kuning dan pink");
        System.out.println("Bunga " + tulipsFlower.getName() + " adalah bunga yang berasal dari " + tulipsFlower.getOrigin()+".");
        System.out.println("---");
        System.out.println(tulipsFlower.getName() + " adalah bunga yang memiliki warna yang cantik dan memiliki tiga warna juga yaitu warna " + tulipsFlower.getColor()+"." );
        System.out.println("---");
        System.out.println("Bunga " + tulipsFlower.getName() + " juga kebanyakan tumbuh subur di daerah yang beriklim sub tropis"+".");

    }
}
