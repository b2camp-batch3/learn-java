package id.b2camp.syahrul.day6.accessmodifiers.layer1;

public class TulipsFlower {
    private String name;
    private String origin;
    private String color;




    public TulipsFlower(String name, String origin, String color) {
        this.name = name;
        this.origin = origin;
        this.color = color;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


}