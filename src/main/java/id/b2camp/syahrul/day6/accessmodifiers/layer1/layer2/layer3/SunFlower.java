package id.b2camp.syahrul.day6.accessmodifiers.layer1.layer2.layer3;

public class SunFlower {
    private String name;
    private String origin;
    private String color;


    public SunFlower(String name, String origin, String color) {
        this.name = name;
        this.origin = origin;
        this.color = color;


    }
    public static void main(String[] args) {
        SunFlower sunFlower = new SunFlower("Matahari", "Amerika utara", "Kuning"+".");
        System.out.println("Warna " + sunFlower.color + " pada bunga " + sunFlower.name + " ternyata sangat cantik,dan bunga matahari juga berasal dari " + sunFlower.origin+".");
    }

}
