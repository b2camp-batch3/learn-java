package id.b2camp.syahrul.day6.abstractclasses;

public class Pisces extends Zodiak{
    @Override
    public String getNamabulan() {
        return "19 February";
    }

    @Override
    public void setNamabulan(String namabulan) {
        super.setNamabulan(namabulan);
    }

    @Override
    public String getNamazodiak() {
        return "Pisces";
    }

    @Override
    public void setNamazodiak(String namazodiak) {
        super.setNamazodiak(namazodiak);
    }
}
