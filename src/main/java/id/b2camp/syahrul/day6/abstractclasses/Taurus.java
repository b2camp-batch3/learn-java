package id.b2camp.syahrul.day6.abstractclasses;

public class Taurus extends Zodiak {
    @Override
    public String getNamabulan() {
        return "20 April";
    }

    @Override
    public void setNamabulan(String namabulan) {
        super.setNamabulan(namabulan);
    }

    @Override
    public String getNamazodiak() {
        return "Taurus";
    }

    @Override
    public void setNamazodiak(String namazodiak) {
        super.setNamazodiak(namazodiak);
    }
}
