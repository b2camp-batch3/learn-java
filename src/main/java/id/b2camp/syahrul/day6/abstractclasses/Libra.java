package id.b2camp.syahrul.day6.abstractclasses;

public class Libra extends Zodiak{
    @Override
    public String getNamabulan() {
        return "23 Oktober";
    }

    @Override
    public void setNamabulan(String namabulan) {
        super.setNamabulan(namabulan);
    }

    @Override
    public String getNamazodiak() {
        return "Libra";
    }

    @Override
    public void setNamazodiak(String namazodiak) {
        super.setNamazodiak(namazodiak);
    }
}
