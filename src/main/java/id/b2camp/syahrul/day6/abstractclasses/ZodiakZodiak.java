package id.b2camp.syahrul.day6.abstractclasses;

public class ZodiakZodiak {
    public static void main(String[] args) {
        Taurus taurus = new Taurus();
        System.out.println("Saya lahir di tanggal " + taurus.getNamabulan() + " berarti saya termasuk zodiak " + taurus.getNamazodiak());
        System.out.println("---");


        Pisces pisces = new Pisces();
        System.out.println("Tanggal lahir saya adalah " + pisces.getNamabulan() + " dan saya adalah zodiak " + pisces.getNamazodiak());
        System.out.println("---");


        Libra libra = new Libra();
        System.out.println("Ulang tahun saya adalah tanggal " + libra.getNamabulan() + " dan zodiak saya adalah " + libra.getNamazodiak() );


    }
}
