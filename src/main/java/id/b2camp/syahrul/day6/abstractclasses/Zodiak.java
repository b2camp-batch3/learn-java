package id.b2camp.syahrul.day6.abstractclasses;

public abstract class Zodiak {
    String namabulan;
    String namazodiak;

    public String getNamabulan() {
        return namabulan;
    }

    public void setNamabulan(String namabulan) {
        this.namabulan = namabulan;
    }

    public String getNamazodiak() {
        return namazodiak;
    }

    public void setNamazodiak(String namazodiak) {
        this.namazodiak = namazodiak;
    }
}










