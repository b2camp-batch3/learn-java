package id.b2camp.syahrul.day6.interfaces;

import javax.xml.namespace.QName;

public class Vegetable {
    public static void main(String[] args) {
        TypesOfVegetable typesOfVegetable1 = new TypesOfVegetable("Tomat", "merah", "C");
        System.out.println("Ini adalah sayuran " + typesOfVegetable1.nama + " dengan warna yang cantik yaitu warna " + typesOfVegetable1.warna + " dan sayuran ini juga mengandung vitamin " + typesOfVegetable1.vitamin+".");
        System.out.println("---");
        TypesOfVegetable typesOfVegetable2 = new TypesOfVegetable("Wortel", "oren", "A");
        System.out.println("Ini adalah sayuran " + typesOfVegetable2.nama + "dengan warna " + typesOfVegetable2.warna + " sayuran ini juga mengandung vitamin " + typesOfVegetable2.vitamin+".");
        System.out.println("---");
        TypesOfVegetable typesOfVegetable3 = new TypesOfVegetable("Kangkung", "hijau", "A");
        System.out.println("Saya sedang makan sayuran " + typesOfVegetable3.nama + " ternyata warna kangkung adalah " + typesOfVegetable3.warna + " kangkung juga bervitamin yaitu vitamin " + typesOfVegetable3.vitamin+".");

    }
}
