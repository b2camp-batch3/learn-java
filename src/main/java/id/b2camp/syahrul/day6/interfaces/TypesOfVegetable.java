package id.b2camp.syahrul.day6.interfaces;

public class TypesOfVegetable implements VegetableNames, VegetableColor, Vitamin {
    String nama;
    String warna;
    String vitamin;

    public TypesOfVegetable(String nama, String warna, String vitamin) {
        this.nama = nama;
        this.warna = warna;
        this.vitamin = vitamin;
    }

    @Override
    public String getname() {
        return getname();

    }

    @Override
    public String getcolor() {
        return getcolor();
    }

    @Override
    public String getvitamin() {
        return getvitamin();


    }
}
