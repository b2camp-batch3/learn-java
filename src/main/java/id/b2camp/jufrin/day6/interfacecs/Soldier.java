package id.b2camp.jufrin.day6.interfacecs;

public class Soldier implements UnitType, UnitStrength, UnitWeapon {

    @Override
    public String name() {
        return "Soldier";
    }

    @Override
    public String getUnitType() {
        return "Infantry";
    }

    @Override
    public String getWeaponType() {
        return "Assault Rifle";
    }

    @Override
    public int getStrength() {
        return 100;
    }

    @Override
    public String getArmor() {
        return "Light";
    }
}
