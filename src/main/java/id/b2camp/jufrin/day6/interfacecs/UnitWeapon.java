package id.b2camp.jufrin.day6.interfacecs;

public interface UnitWeapon {
    String getWeaponType();
}
