package id.b2camp.jufrin.day6.interfacecs;

public interface UnitStrength {
    int getStrength();

    String getArmor();
}
