package id.b2camp.jufrin.day6.interfacecs;

public class Tank implements UnitType, UnitStrength, UnitWeapon {

    @Override
    public String name() {
        return "Tank";
    }

    @Override
    public String getUnitType() {
        return "Armored Vehicle";
    }

    @Override
    public String getWeaponType() {
        return "Cannon";
    }

    @Override
    public int getStrength() {
        return 300;
    }

    @Override
    public String getArmor() {
        return "Heavy";
    }
}
