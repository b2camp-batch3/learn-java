package id.b2camp.jufrin.day6.interfacecs;

public interface UnitType {
    String name();
    String getUnitType();

    String getWeaponType();

    int getStrength();

    String getArmor();
}

