package id.b2camp.jufrin.day6.interfacecs;

public class FighterJet implements UnitType, UnitStrength, UnitWeapon {

    @Override
    public String name() {
        return "Fighter Jet";
    }

    @Override
    public String getUnitType() {
        return "Aircraft";
    }

    @Override
    public String getWeaponType() {
        return "Missiles";
    }

    @Override
    public int getStrength() {
        return 250;
    }

    @Override
    public String getArmor() {
        return "Medium";
    }
}
