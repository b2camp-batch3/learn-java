package id.b2camp.jufrin.day6.interfacecs;

public class WarGameJufrin {
    public static void main(String[] args) {
        System.out.println("=== Welcome to Wargame Simulation ===");
        System.out.println();

        Tank tank = new Tank();
        displayUnitInfo(tank);

        Soldier soldier = new Soldier();
        displayUnitInfo(soldier);

        FighterJet fighterJet = new FighterJet();
        displayUnitInfo(fighterJet);
    }

    public static void displayUnitInfo(UnitType unit) {
        System.out.println("Unit: " + unit.name());
        System.out.println("Type: " + unit.getUnitType());
        System.out.println("Weapon: " + unit.getWeaponType());
        System.out.println("Strength: " + unit.getStrength());
        System.out.println("Armor: " + unit.getArmor());
        System.out.println("=====================================");
    }
}
