package id.b2camp.jufrin.day6.abstrakclass;

public class TankJufrin extends MilitaryUnitJufrin {

    public TankJufrin(int strength, int firepower) {
        super(strength, firepower);
    }

    @Override
    public void unitInfo() {
        System.out.println("Tank Unit (Jufrin)");
        System.out.println("Strength: " + strength);
        System.out.println("Firepower: " + firepower);
        System.out.println("Supercool Tank Camouflage Activated!");
        System.out.println("===============================================");
        System.out.println("=============BOOOOOOOOOOOOOOOOMMMMM============");
        System.out.println("===============================================");
    }
}
