package id.b2camp.jufrin.day6.abstrakclass;

public class InfantryJufrin extends MilitaryUnitJufrin {

    public InfantryJufrin(int strength, int firepower) {
        super(strength, firepower);
    }

    @Override
    public void unitInfo() {
        System.out.println("Infantry Unit (Jufrin)");
        System.out.println("Strength: " + strength);
        System.out.println("Firepower: " + firepower);
        System.out.println("Supercool Camo Uniform Equipped!");
        System.out.println("===============================================");
        System.out.println("=============BOOOOOOOOOOOOOOOOMMMMM============");
        System.out.println("===============================================");
    }
}
