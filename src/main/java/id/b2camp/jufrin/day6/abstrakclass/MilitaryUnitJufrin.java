package id.b2camp.jufrin.day6.abstrakclass;

public abstract class MilitaryUnitJufrin {
    protected int strength;
    protected int firepower;

    public MilitaryUnitJufrin(int strength, int firepower) {
        this.strength = strength;
        this.firepower = firepower;
    }

    public abstract void unitInfo();

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getFirepower() {
        return firepower;
    }

    public void setFirepower(int firepower) {
        this.firepower = firepower;
    }
}
