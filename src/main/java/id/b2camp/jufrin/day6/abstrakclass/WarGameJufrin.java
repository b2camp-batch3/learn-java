package id.b2camp.jufrin.day6.abstrakclass;

public class WarGameJufrin {
    public static void main(String[] args) {
        MilitaryUnitJufrin tank = new TankJufrin(150, 75);
        tank.unitInfo();

        MilitaryUnitJufrin infantry = new InfantryJufrin(80, 50);
        infantry.unitInfo();

        MilitaryUnitJufrin aircraft = new AircraftJufrin(200, 120);
        aircraft.unitInfo();
    }
}
