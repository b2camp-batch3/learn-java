package id.b2camp.jufrin.day6.accesmodifier.pack1.pack2;

public class RPGLocation {

    protected String name;
    protected String atmosphere;

    public RPGLocation(String name, String atmosphere) {
        this.name = name;
        this.atmosphere = atmosphere;
    }
}
