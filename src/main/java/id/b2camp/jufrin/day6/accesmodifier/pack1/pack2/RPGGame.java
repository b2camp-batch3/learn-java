package id.b2camp.jufrin.day6.accesmodifier.pack1.pack2;
import id.b2camp.jufrin.day6.accesmodifier.RPGCharacter;


public class RPGGame {
    public static void main(String[] args) {
        RPGCharacter hero = new RPGCharacter("-BLACKWOOD-", "MAGE");
        System.out.println("Welcome to the world of RPG!");
        System.out.println("Character name: " + hero.getName());
        System.out.println("Character class: " + hero.getCharacterClass());

        System.out.println("==================================================");

        RPGLocation village = new RPGLocation("Starting Village", "Peaceful");
        System.out.println("You've arrived at " + village.name);
        System.out.println("Current atmosphere: " + village.atmosphere);
    }
}