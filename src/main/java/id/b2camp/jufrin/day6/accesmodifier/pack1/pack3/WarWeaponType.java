package id.b2camp.jufrin.day6.accesmodifier.pack1.pack3;

public class WarWeaponType {
    public String typeName;
    public String description;

    public WarWeaponType(String typeName, String description) {
        this.typeName = typeName;
        this.description = description;
    }
}

