package id.b2camp.jufrin.day6.accesmodifier.pack1.pack3;

public class WarGameDemo {
    public static void main(String[] args) {
        WarWeaponType assaultRifle = new WarWeaponType("Assault Rifle", "Versatile firearm for close and medium range combat.");
        WarWeaponType sniperRifle = new WarWeaponType("Sniper Rifle", "Precision long-range firearm for taking out distant targets.");

        System.out.println("Welcome to the War Game Weapon Showcase!");
        System.out.println("Weapon Type: " + assaultRifle.typeName);
        System.out.println("Description: " + assaultRifle.description);

        System.out.println("==============================================");

        System.out.println("Weapon Type: " + sniperRifle.typeName);
        System.out.println("Description: " + sniperRifle.description);
    }
}

