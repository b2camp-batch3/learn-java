package id.b2camp.jufrin.day6.accesmodifier;

public class RPGCharacter {

    private String name;
    private String characterClass;

    public RPGCharacter(String name, String characterClass) {
        this.name = name;
        this.characterClass = characterClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(String characterClass) {
        this.characterClass = characterClass;
    }
}
