package id.b2camp.jufrin.day9;

@VeryImportant
public class Soldier implements Runnable, Combatant {

    @ImportantActivity
    private String name;
    private String division;
    private int health;

    public Soldier(String name, String division, int health) {
        this.name = name;
        this.division = division;
        this.health = health;
    }

    @ReadyForBattle(times = 3)
    public void prepareForBattle() {
        System.out.println("Preparing for battle...");
    }

    public void engageEnemy() {
        System.out.println("Engaging enemy forces...");
    }

    @Override
    public void run() {
        System.out.println("Soldier is running to the battlefield...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}