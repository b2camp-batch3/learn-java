package id.b2camp.jufrin.day9;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

public class WarSimulationApp {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
        Soldier soldier = new Soldier("John Doe", "Infantry", 100);

        Field[] fields = soldier.getClass().getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }
        System.out.println("===============================================================");
        System.out.println("----- Pause ---");
        System.out.println("");

        Method[] methods = soldier.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        System.out.println("==================================================================");
        System.out.println("----Pause ----");
        System.out.println("");

        if (soldier.getClass().isAnnotationPresent(VeryImportant.class)) {
            System.out.println("Soldier is of vital importance");
        }
        for (Field field : soldier.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(ImportantActivity.class)) {
                field.setAccessible(true);
                Object fieldValue = field.get(soldier);
                System.out.println(fieldValue + " contributes to mission success");
            }
        }
        System.out.println("=========================================================================");
        System.out.println("------Pause-----");
        System.out.println("========================================================================");
        for (Method method : soldier.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(ReadyForBattle.class)) {
                ReadyForBattle battle = method.getAnnotation(ReadyForBattle.class);
                for (int i = 0; i < battle.times() - 1; i++) {
                    method.invoke(soldier);
                }
            }
        }

        System.out.println("==========================================================================");
        System.out.println("----Final Pause----");
        System.out.println("==============================================================================");
        BattleEvent event = new BattleEvent("Battle of the Frontlines", LocalDateTime.now());
        System.out.println("Upcoming battle event: " + event.getName());
        System.out.println("Scheduled at: " + event.getTime());
    }
}
