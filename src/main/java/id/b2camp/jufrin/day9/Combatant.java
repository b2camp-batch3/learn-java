package id.b2camp.jufrin.day9;

public interface Combatant {

    void prepareForBattle();

    void engageEnemy();
}