CREATE SCHEMA jufrin_storegaming;

-- Create MsCustomer
CREATE TABLE jufrin_storegaming.MsCustomer (
    CustomerId CHAR(5) PRIMARY KEY,
    CustomerName VARCHAR(50),
    CustomerGender VARCHAR(10),
    CustomerPhone VARCHAR(13),
    CustomerAddress VARCHAR(100),
    CONSTRAINT cekIDCust CHECK (CustomerId ~ '^GC[0-9][0-9][0-9]$')
);

-- Create MsStaff
CREATE TABLE jufrin_storegaming.MsStaff (
    StaffId CHAR(5) PRIMARY KEY,
    StaffName VARCHAR(50),
    StaffGender VARCHAR(10),
    StaffPhone VARCHAR(13),
    StaffAddress VARCHAR(100),
    StaffSalary NUMERIC(11, 2),
    StaffPosition VARCHAR(20),
    CONSTRAINT cekIDStaff CHECK (StaffId ~ '^GS[0-9][0-9][0-9]$')
);

-- Create MsGameGenre
CREATE TABLE jufrin_storegaming.MsGameGenre (
    GameGenreId CHAR(5) PRIMARY KEY,
    GameGenreName VARCHAR(50),
    CONSTRAINT cekIDGameGenre CHECK (GameGenreId ~ '^GG[0-9][0-9][0-9]$')
);


-- Create MsGamez
CREATE TABLE jufrin_storegaming.MsGame (
    GameId CHAR(5) PRIMARY KEY,
    GameGenreId CHAR(5) REFERENCES jufrin_storegaming.MsGameGenre ON UPDATE CASCADE ON DELETE CASCADE,
    GameTitle VARCHAR(50),
    GamePrice NUMERIC(11, 2),
    GameStock NUMERIC(11, 2),
    CONSTRAINT cekIDGame CHECK (GameId ~ '^GM[0-9][0-9][0-9]$')
);

-- Create HeaderPurchaseTransaction
CREATE TABLE jufrin_storegaming.HeaderPurchaseTransaction (
    TransactionId CHAR(5) PRIMARY KEY,
    CustomerId CHAR(5) REFERENCES jufrin_storegaming.MsCustomer ON UPDATE CASCADE ON DELETE CASCADE,
    StaffId CHAR(5) REFERENCES jufrin_storegaming.MsStaff ON UPDATE CASCADE ON DELETE CASCADE,
    TransactionDate DATE,
    PaymentType VARCHAR(20),
    CONSTRAINT cekIDTrans CHECK (TransactionId ~ '^GP[0-9][0-9][0-9]$')
);

-- Create DetailPurchaseTransaction
CREATE TABLE jufrin_storegaming.DetailPurchaseTransaction (
    TransactionId CHAR(5) REFERENCES jufrin_storegaming.HeaderPurchaseTransaction ON UPDATE CASCADE ON DELETE CASCADE,
    GameId CHAR(5) REFERENCES jufrin_storegaming.MsGame ON UPDATE CASCADE ON DELETE CASCADE,
    PurchaseQuantity NUMERIC(11, 2),
    PRIMARY KEY (TransactionId, GameId)
);

-- Insert Data
INSERT INTO jufrin_storegaming.MsCustomer VALUES
('GC001', 'Kevin Axellino Triantio', 'Male', '081267381930', 'Jelambar Street no 17'),
('GC002', 'Kerin Augustin', 'Female', '081372116372', 'Ancol Barat Street no 190'),
('GC003', 'Fernando Lioexander', 'Male', '087824153627', 'Palmerah Street no 20'),
('GC004', 'Naufal Hafiz', 'Male', '081127173829', 'Duri Kepa Street no 9'),
('GC005', 'Arya Thomas', 'Male', '081811992617', 'Anggrek Street no 12');

INSERT INTO jufrin_storegaming.MsStaff VALUES
('GS001', 'Venky Eduardo', 'Male', '081927183617', 'Budi Raya Street no 100', 15000000, 'Manager'),
('GS002', 'Poppy Hwangsa Iswara', 'Female', '085714263367', 'Budi Raya Street no 11', 10000000, 'Supervisor'),
('GS003', 'Louis Rudy Valen', 'Male', '089915276278', 'Serpong Raya Street no 109', 6000000, 'Cashier'),
('GS004', 'Andika Leonardo', 'Male', '081189227888', 'BSD Raya Street no 1', 6500000, 'Cashier'),
('GS005', 'Nathaniel Putera', 'Male', '087715551782', 'Ciledug Street no 233', 7300000, 'Cashier'),
('GS006', 'Olive Kusuma', 'Female', '083287345383', 'Serpong Raya Street no 117', 4700000, 'Cashier');

INSERT INTO jufrin_storegaming.msstaff
(staffid, staffname, staffgender, staffphone, staffaddress, staffsalary, staffposition)
VALUES('GS007', 'Musafir Ahmad', 'Male', '081213334678', 'Durian Street 09',3000000, 'OB');


INSERT INTO jufrin_storegaming.MsGameGenre VALUES
('GG001', 'Action'),
('GG002', 'Adventure'),
('GG003', 'Role-Playing'),
('GG004', 'Simulation'),
('GG005', 'Strategy');

INSERT INTO jufrin_storegaming.MsGame VALUES
('GM001', 'GG001', 'Call of Duty', 600000, 30),
('GM002', 'GG001', 'Battlefield', 550000, 25),
('GM003', 'GG001', 'Counter-Strike', 400000, 40),
('GM004', 'GG001', 'Halo', 650000, 20),
('GM005', 'GG001', 'Rainbow Six Siege', 550000, 22),
('GM006', 'GG001', 'Fortnite', 0, 100),
('GM007', 'GG002', 'The Legend of Zelda', 900000, 15),
('GM008', 'GG002', 'Uncharted', 750000, 18),
('GM009', 'GG002', 'Red Dead Redemption', 1100000, 12),
('GM010', 'GG002', 'Assassin Creed', 800000, 20),
('GM011', 'GG003', 'Final Fantasy', 1200000, 10),
('GM012', 'GG003', 'The Elder Scrolls', 950000, 15),
('GM013', 'GG003', 'World of Warcraft', 1300000, 8),
('GM014', 'GG003', 'Dragon Age', 750000, 14),
('GM015', 'GG003', 'Dark Souls', 900000, 12),
('GM016', 'GG004', 'The Sims', 350000, 30),
('GM017', 'GG004', 'SimCity', 300000, 35),
('GM018', 'GG004', 'RollerCoaster Tycoon', 250000, 40),
('GM019', 'GG004', 'Stardew Valley', 200000, 45),
('GM020', 'GG005', 'Civilization', 700000, 17),
('GM021', 'GG005', 'Total War', 850000, 14),
('GM022', 'GG005', 'StarCraft', 600000, 18),
('GM023', 'GG005', 'Age of Empires', 500000, 20),
('GM024', 'GG005', 'Sid Meier Civilization', 750000, 15);

INSERT INTO jufrin_storegaming.HeaderPurchaseTransaction VALUES
('GP001', 'GC001', 'GS004', '2023-09-01', 'Credit'),
('GP002', 'GC002', 'GS005', '2023-09-02', 'Credit'),
('GP003', 'GC003', 'GS003', '2023-09-03', 'Cash'),
('GP004', 'GC004', 'GS005', '2023-09-04', 'Debit'),
('GP005', 'GC005', 'GS003', '2023-09-05', 'Debit'),
('GP006', 'GC001', 'GS005', '2023-09-06', 'Credit'),
('GP007', 'GC002', 'GS001', '2023-09-07', 'Cash'),
('GP008', 'GC003', 'GS002', '2023-09-08', 'Credit'),
('GP009', 'GC005', 'GS004', '2023-09-09', 'Debit');

INSERT INTO jufrin_storegaming.DetailPurchaseTransaction VALUES
('GP001', 'GM001', 2),
('GP001', 'GM005', 3),
('GP002', 'GM010', 2),
('GP002', 'GM015', 1),
('GP003', 'GM009', 1),
('GP004', 'GM001', 1),
('GP004', 'GM006', 3),
('GP004', 'GM015', 3),
('GP004', 'GM016', 1),
('GP005', 'GM016', 2),
('GP006', 'GM006', 4),
('GP006', 'GM015', 6),
('GP007', 'GM002', 2),
('GP007', 'GM005', 2),
('GP008', 'GM002', 3),
('GP008', 'GM006', 1),
('GP009', 'GM005', 1),
('GP009', 'GM006', 2);

UPDATE jufrin_storegaming.msstaff
SET staffname='Jufrin Abdul Hamid', staffgender='Male', staffphone='021333444666', staffaddress='jl.cempaka street', staffsalary=15000000, staffposition='Manager'
WHERE staffid='GS001';



SELECT staffid, staffname, staffgender, staffphone, staffaddress, staffsalary, staffposition
FROM jufrin_storegaming.msstaff;


SELECT transactionid, gameid, purchasequantity
FROM jufrin_storegaming.detailpurchasetransaction;

SELECT gameid, gamegenreid, gametitle, gameprice, gamestock
FROM jufrin_storegaming.msgame;

SELECT customerid, customername, customergender, customerphone, customeraddress
FROM jufrin_storegaming.mscustomer;


-- Contoh INNER JOIN antara MsCustomer dan HeaderPurchaseTransaction
SELECT
    Customer.CustomerName, Transaction.TransactionDate
FROM
    jufrin_storegaming.MsCustomer AS Customer
INNER JOIN
    jufrin_storegaming.HeaderPurchaseTransaction AS Transaction
ON
    Customer.CustomerId = Transaction.CustomerId;


   -- Contoh LEFT JOIN antara MsCustomer dan HeaderPurchaseTransaction
SELECT
    C.CustomerName, H.TransactionDate
FROM
    jufrin_storegaming.MsCustomer AS C
LEFT JOIN
    jufrin_storegaming.HeaderPurchaseTransaction AS H
ON
    C.CustomerId = H.CustomerId;

   -- Contoh RIGHT JOIN antara MsCustomer dan HeaderPurchaseTransaction
SELECT
    Customer.CustomerName, Transaction.TransactionDate
FROM
    jufrin_storegaming.MsCustomer AS Customer
RIGHT JOIN
    jufrin_storegaming.HeaderPurchaseTransaction AS Transaction
ON
    Customer.CustomerId = Transaction.CustomerId;

   -- Contoh penggunaan GROUP BY untuk menghitung total pembelian per pelanggan
SELECT
    Customer.CustomerName, SUM(Game.GamePrice * Detail.PurchaseQuantity) AS TotalPurchase
FROM
    jufrin_storegaming.MsCustomer AS Customer
INNER JOIN
    jufrin_storegaming.HeaderPurchaseTransaction AS Transaction
ON
    Customer.CustomerId = Transaction.CustomerId
INNER JOIN
    jufrin_storegaming.DetailPurchaseTransaction AS Detail
ON
    Transaction.TransactionId = Detail.TransactionId
INNER JOIN
    jufrin_storegaming.MsGame AS Game
ON
    Detail.GameId = Game.GameId
GROUP BY
    Customer.CustomerName;
--  GRUB by SINTAX

SELECT CustomerName
FROM jufrin_storegaming.MsCustomer
ORDER BY CustomerName ASC;

--full AUTER JOIN
-- Query dengan FULL OUTER JOIN
SELECT *
FROM jufrin_storegaming.MsCustomer AS Customer
FULL OUTER JOIN jufrin_storegaming.HeaderPurchaseTransaction AS Transaction
ON Customer.CustomerId = Transaction.CustomerId;

-- Query dengan CROSS JOIN
SELECT Customer.CustomerName, Genre.GameGenreName
FROM jufrin_storegaming.MsCustomer AS Customer
CROSS JOIN jufrin_storegaming.MsGameGenre AS Genre;







