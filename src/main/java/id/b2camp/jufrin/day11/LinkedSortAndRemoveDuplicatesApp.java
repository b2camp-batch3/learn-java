package id.b2camp.jufrin.day11;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

public class LinkedSortAndRemoveDuplicatesApp {
    public static void main(String[] args) {
        LinkedList<String> colectionName = new LinkedList<>();
        colectionName.add("JUFRIN");
        colectionName.add("GITA");
        colectionName.add("JERRY");
        colectionName.add("SENJAYA");
        colectionName.add("FAKHRI");
        colectionName.add("JERRY");
        colectionName.add("MUZZ");
        colectionName.add("SYAHRUL");
        colectionName.add("JUFRIN");
        System.out.println("==========================================================");
        System.out.println("Collection My number = "+colectionName);
        System.out.println("==========================================================");
        Collections.sort(colectionName);
        System.out.println("Collection sorted MY number = "+ colectionName);
        System.out.println("===========================================================");
        HashSet<String> unsetName = new HashSet<>(colectionName);
        colectionName.clear();
        unsetName.addAll(colectionName);
        System.out.println("Remove duplicated My collecttion Number = "+unsetName);
        System.out.println("==============================================================");
        LinkedList<String> sortedMynumber = new LinkedList<>(unsetName);
        Collections.sort(sortedMynumber);
        System.out.println("Sorted My numbeer 2 = "+ sortedMynumber);
        System.out.println("==============================================================");



    }
}
