package id.b2camp.jufrin.day11;

import java.util.Arrays;

public class SortArrayAndRemoveDuplicates {
    public static void main(String[] args) {
        int[] myCollection = {5,2,8,2,9,5,9,10,10,1,1};

        System.out.println("My Original Array: " + Arrays.toString(myCollection));

        // Sorting arry
        Arrays.sort(myCollection);

        System.out.println("My Sorted Array: " + Arrays.toString(myCollection));

        // Remove arry
        int[] myUniqueNumbers = removeDuplicates(myCollection);

        System.out.println("Array without Duplicates: " + Arrays.toString(myUniqueNumbers));

        // Eksekusi looop
        System.out.println("Looping through the unique elements:");
        for (int varNumbers : myUniqueNumbers) {
            System.out.println(varNumbers);
        }
    }

    public static int[] removeDuplicates(int[] arr) {
        int n = arr.length;
        int[] temp = new int[n];
        int j = 0;
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] != arr[i + 1]) {
                temp[j++] = arr[i];
            }
        }

        temp[j++] = arr[n - 1];

        int[] myArray = new int[j];
        System.arraycopy(temp, 0, myArray, 0, j);

        return myArray;
    }
}