package id.b2camp.jufrin.day11;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;

public class LinkedListSortAndRemoveDuplicatesHashMap {
    public static void main(String[] args) {

        LinkedList<Integer> myCollection = new LinkedList<>();
        myCollection.add(5);
        myCollection.add(2);
        myCollection.add(8);
        myCollection.add(2);
        myCollection.add(9);
        myCollection.add(5);

        System.out.println("Original LinkedList: " + myCollection);

        HashMap<Integer, Integer> numbersMap = new HashMap<>();
        for (Integer numbercollection : myCollection) {
            numbersMap.put(numbercollection,numbercollection);
        }

        ArrayList<Integer> uniqueNumbers = new ArrayList<>(numbersMap.keySet());
        Collections.sort(uniqueNumbers);
        System.out.println("Sorted LinkedList without Duplicates: " + uniqueNumbers);
    }
}
