package id.b2camp.jufrin.day11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class ArrayListSortAndRemoveDuplicatesApp {
    public static void main(String[] args) {
        ArrayList<Integer> collectionNumber= new ArrayList<>();
        collectionNumber.add(11);
        collectionNumber.add(12);
        collectionNumber.add(10);
        collectionNumber.add(60);
        collectionNumber.add(20);
        collectionNumber.add(11);
        collectionNumber.add(5);
        collectionNumber.add(1);
        System.out.println("=====================================================================");
        System.out.println("Collection My Number = " + collectionNumber);
        System.out.println("=====================================================================");
        System.out.println("Collection sort My number");
        Collections.sort(collectionNumber);
        System.out.println("sorted My Number = "+collectionNumber);
        System.out.println("======================================================================");
        HashSet<Integer> unsetNumbers = new HashSet<>(collectionNumber);
        collectionNumber.clear();
        unsetNumbers.addAll(collectionNumber);
        System.out.println("Remove duplicated My collecttion Number = "+unsetNumbers);
        System.out.println("=======================================================================");
        ArrayList<Integer> sortedUnsetNumber = new ArrayList<>(unsetNumbers);
        Collections.sort(sortedUnsetNumber);
        System.out.println("Unset Sorted MY Number 2 = "+ sortedUnsetNumber);
        System.out.println("=======================================================================");

    }
}
