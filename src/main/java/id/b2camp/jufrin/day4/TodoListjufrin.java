package id.b2camp.jufrin.day4;

import java.util.ArrayList;
import java.util.Scanner;

public class TodoListjufrin {
    public static Scanner input = new Scanner(System.in);
    public static ArrayList<String> tasks = new ArrayList<>();

    public static void main(String[] args) {
        viewShow();
    }

    public static void viewShow() {
        while (true) {

            System.out.println("=======ISIAN TODO-LIST======");
            show();
            System.out.println("============================");
            System.out.println();

            System.out.println("Pilihan menu : ");
            System.out.println("1. ADD To-do List");
            System.out.println("2. DELATE To-do List");
            System.out.println("X. EXIT To-List");

            String choice = input("Pilih opsi: ");

            if (choice.equals("1")) {
                viewAdd();
            } else if (choice.equalsIgnoreCase("x")) {
                break;
            } else if (choice.equals("2")) {
                viewRemove();
            } else {
                System.out.println("Pilihan tidak sesuai");
            }
        }
    }

    public static void show() {
        for (int i = 0; i < tasks.size(); i++) {
            String task = tasks.get(i);
            int no = i + 1;
            System.out.println(no + ". " + task);
        }
    }

    public static void viewAdd() {
        System.out.println("Menambahkan tugas");

        String task = input("Masukkan tugas (x) jika batal");

        if (!task.equals("x")) {
            add(task);
        }
    }

    public static boolean remove(int number) {
        if (number < 1 || number > tasks.size()) {
            return false;
        }

        tasks.remove(number - 1);
        return true;
    }

    public static void viewRemove() {
        System.out.println("Menghapus tugas");

        String numberStr = input("Masukkan nomor tugas (x) jika batal\n");
        if (!numberStr.equals("x")) {
            int number = Integer.parseInt(numberStr);
            boolean success = remove(number);
            if (!success) {
                System.out.println("Gagal menghapus tugas nomor " + number);
            }
        }
    }

    public static void add(String task) {
        tasks.add(task);
    }

    public static String input(String info) {
        System.out.println(info);
        return input.nextLine();
    }
}

