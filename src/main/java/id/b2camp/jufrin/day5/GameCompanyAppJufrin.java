package id.b2camp.jufrin.day5;

class Employee {
    String name;

    Employee() {

    }

    Employee(String name) { this.name = name;
    }
}

class GameDesigner extends Employee {
    String specialization;

    GameDesigner(String name, String specialization) {
        super(name);
        this.specialization = specialization;
    }
}

class GameArtist extends Employee {

    GameArtist(String name) {
        super(name);
    }
}

class GameDeveloper extends Employee {
    String programmingLanguage;

    GameDeveloper(String name, String programmingLanguage) {
        super(name);
        this.programmingLanguage = programmingLanguage;
    }
}

class GameTester extends Employee {

    GameTester(String name) {
        super(name);
    }
}

public class GameCompanyAppJufrin {
    public static void main(String[] args) {
        GameDesigner designer = new GameDesigner("Eka", "Game Mechanics");
        System.out.println(designer.name + " - Specialization: " + designer.specialization);

        GameArtist artist = new GameArtist("Budi");
        System.out.println(artist.name);

        System.out.println("=============================================");


        displaySpecialization(designer);
        displayProgrammingLanguage(new GameDeveloper("Jufrin", "Java"));
        displaySpecialization(new GameTester("Gina"));

        System.out.println("========================================================");

        Employee employee = new GameDesigner("Arga", "Game Techical");
        greetEmployee(employee);

        GameDesigner newDesigner = new GameDesigner("Ira", "Level Design");
        System.out.println(newDesigner.name + " - Specialization: " + newDesigner.specialization);

        Employee employeeDesigner = (Employee) newDesigner;
        System.out.println(employeeDesigner.name);

        System.out.println("==================================================");

        Employee[] employees = new Employee[4];
        employees[0] = new GameDesigner("Rina", "Narrative Design");
        employees[1] = new GameArtist("Eko");
        employees[2] = new GameDeveloper("Andre", "C++");
        employees[3] = new GameTester("Rita");

        for (Employee emp : employees) {
            greetEmployee(emp);
        }
    }

    static void greetEmployee(Employee employee) {
        System.out.println("Hello! Welcome to our game company. You are interacting with " + employee.name);
    }

    static void displaySpecialization(GameDesigner designer) {
        System.out.println(designer.name + "'s specialization in game design: " + designer.specialization);
    }

    static void displayProgrammingLanguage(GameDeveloper developer) {
        System.out.println(developer.name + " is proficient in programming language: " + developer.programmingLanguage);
    }

    static void displaySpecialization(GameTester tester) {
        System.out.println(tester.name + "'s specialization in game testing");
    }
}
