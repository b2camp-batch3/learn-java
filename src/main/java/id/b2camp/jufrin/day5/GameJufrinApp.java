package id.b2camp.jufrin.day5;

class Character {
    String name;
    int health;

    Character(String name, int health) {
        this.name = name;
        this.health = health;
    }

    void attack(Character target) {
        System.out.println(name + " attacks " + target.name + "!");

        while (target.health > 0) {
            double damage = Math.random() * 30;
            target.health -= Math.round(damage);
            System.out.println(name + " deals " + Math.round(damage) + " damage to " + target.name + ".");
            System.out.println(target.name + " has " + target.health + " health remaining.");
        }

        if (target instanceof Player) {
            System.out.println(target.name + " defeated! " + name + " is victorious!");
        } else {
            System.out.println(name + " defeated! " + target.name + " wins this battle!");
        }
    }

    void defend() {
        System.out.println(name + " braces for impact!");
    }
}

class Player extends Character {
    int experience;

    Player(String name, int health) {
        super(name, health);
        this.experience = 0;
    }

    void levelUp() {
        experience += 100;
        System.out.println(name + " gains experience and levels up!");
    }
}

class Enemy extends Character {
    String enemyType;

    Enemy(String name, int health, String enemyType) {
        super(name, health);
        this.enemyType = enemyType;
    }

    void taunt() {
        System.out.println(name + " taunts the player: 'You cannot defeat me!'");
    }
}

public class GameJufrinApp {
    public static void main(String[] args) {
        Character player = new Player("Hero", 100);
        Character enemy = new Enemy("Dragon", 200, "Fire-breathing Dragon");

        player.attack(enemy);
        enemy.attack(player);

        if (player instanceof Player) {
            Player playerCharacter = (Player) player;
            playerCharacter.levelUp();
        }

        if (enemy instanceof Enemy) {
            Enemy enemyCharacter = (Enemy) enemy;
            enemyCharacter.taunt();
        }

        player.defend();
        enemy.defend();
    }
}
