package id.b2camp.jufrin.day10.optionalClass;

import java.util.Optional;

public class RPGCharacterApp {
    public static void main(String[] args) {
        RPGCharacter jufrin = new RPGCharacter("Jufrin", "Ranger", "Bow", new Quest("Defeat the Orcs", "Mirkwood"));
        RPGCharacter gita = new RPGCharacter("Gita", "Wizard", null, new Quest("Retrieve the Ring", "Mount Doom"));
        RPGCharacter dsenjaya = new RPGCharacter("D.senjaya", "Archer", "Elven Bow", null);

        displayCharacterInfo(jufrin);
        displayCharacterInfo(gita);
        displayCharacterInfo(dsenjaya);
    }

    private static void displayCharacterInfo(RPGCharacter character) {
        System.out.println("Character: " + character.getName());
        System.out.println("Class: " + character.getCharacterClass());

        Optional<String> weaponOptional = character.getWeapon();
        weaponOptional.ifPresent(weapon -> System.out.println("Weapon: " + weapon));
        Optional<Quest> questOptional = character.getQuest();
        questOptional.ifPresent(quest -> System.out.println("Quest: " + quest.getDescription() + " (" + quest.getLocation() + ")"));


        System.out.println("=======================================================");
    }
}

