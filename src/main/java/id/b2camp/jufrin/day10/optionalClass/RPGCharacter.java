package id.b2camp.jufrin.day10.optionalClass;

import java.util.Optional;

class RPGCharacter {
    private String name;
    private String characterClass;
    private Optional<String> weapon;
    private Optional<Quest> quest;

    public RPGCharacter(String name, String characterClass, String weapon, Quest quest) {
        this.name = name;
        this.characterClass = characterClass;
        this.weapon = Optional.ofNullable(weapon);
        this.quest = Optional.ofNullable(quest);
    }

    public String getName() {
        return name;
    }

    public String getCharacterClass() {
        return characterClass;
    }

    public Optional<String> getWeapon() {
        return weapon;
    }

    public Optional<Quest> getQuest() {
        return quest;
    }
}
