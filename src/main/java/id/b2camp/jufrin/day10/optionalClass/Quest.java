package id.b2camp.jufrin.day10.optionalClass;

class Quest {
    private String description;
    private String location;

    public Quest(String description, String location) {
        this.description = description;
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }
}

