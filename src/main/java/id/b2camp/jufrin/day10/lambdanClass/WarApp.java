package id.b2camp.jufrin.day10.lambdanClass;
public class WarApp {
    public static void main(String[] args) {
        WarSimulation simpleSimulation = (attacker, defender) ->
            System.out.println(attacker + " is attacking " + defender);

        WarSimulation assaultSimulation = (attacker, defender) ->
            System.out.println(attacker + " launches an assault against " + defender);

        WarSimulation clashSimulation = (attacker, defender) ->
            System.out.println(attacker + " and " + defender + " are clashing in battle");

        simulateWar(simpleSimulation, "Red Army", "Blue Army");
        simulateWar(assaultSimulation, "Green Army", "Yellow Army");
        simulateWar(clashSimulation, "Orange Army", "Purple Army");
    }

    private static void simulateWar(WarSimulation warSimulation, String attacker, String defender) {
        warSimulation.simulate(attacker, defender);
    }
}




