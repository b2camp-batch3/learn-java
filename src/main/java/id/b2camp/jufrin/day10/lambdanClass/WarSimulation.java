package id.b2camp.jufrin.day10.lambdanClass;
interface WarSimulation {
    void simulate(String attacker, String defender);
}