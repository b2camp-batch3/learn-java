package id.b2camp.jufrin.day10.genericClass;

public class BitcoinPayment extends AbstractPaymentApp {
    public BitcoinPayment(Double amount) {
        super(amount);
    }

    public String toString() {
        return "BitcoinPayment()";
    }

    public void processPayment() {
        System.out.println("Processing Bitcoin payment: " + getAmount());
    }
}

