package id.b2camp.jufrin.day10.genericClass;

import java.util.ArrayList;
import java.util.List;

public class TransactionOrder {
    private Double amount;

    public void print() {
        System.out.println(this.amount);
    }

    public Double getAmount() {
        return this.amount;
    }

    public TransactionOrder(Double amount) {
        this.amount = amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}