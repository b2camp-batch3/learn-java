package id.b2camp.jufrin.day10.genericClass;

import id.b2camp.fakhri.day10.generic.Transaction;

public class SendCryptoApp {
    public SendCryptoApp() {
    }

    public static void main(String[] args) {
        PrintTransaction printTransaction = new PrintTransaction(3);
        printTransaction.print();
        TransactionOrder transactionOrder = new TransactionOrder(4.0);
        transactionOrder.print();
        System.out.println("==========================================================");
        System.out.println("Pause here");
        System.out.println("==============================================================");
        CryptoTransaction<Integer> integerCryptoTransaction = new CryptoTransaction(20000);
        CryptoTransaction<Double> doubleCryptoTransaction = new CryptoTransaction(40000.0);
        CryptoTransaction<String> stringCryptoTransaction = new CryptoTransaction("Send Cryptocurrency ");
        integerCryptoTransaction.print();
        doubleCryptoTransaction.print();
        stringCryptoTransaction.print();
        System.out.println("======================================================");
        System.out.println("---Pause---");
        System.out.println("=======================================================");
        BitcoinPayment bitcoinPayment = new BitcoinPayment(0.0025);
        TransactionJufrin<PaymentApp> bitcoinPaymentTransaction = new TransactionJufrin(bitcoinPayment);
        bitcoinPaymentTransaction.order();
        bitcoinPaymentTransaction.getPaymentType();
        ///setpPaymet
        //bitcoinPaymentTransaction.setPaymentType();
        EthereumPayment ethereumPayment = new EthereumPayment(1.5);
        TransactionJufrin<PaymentApp> ethereumPaymentTransaction = new TransactionJufrin(ethereumPayment);
        ethereumPaymentTransaction.order();
        ethereumPaymentTransaction.getPaymentType();

    }
}