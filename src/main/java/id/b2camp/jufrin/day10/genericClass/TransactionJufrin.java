package id.b2camp.jufrin.day10.genericClass;

public class TransactionJufrin<P extends PaymentMethod> {
    private P paymentType;

    public void order() {
        System.out.println("Processing payment: " + this.paymentType);
    }

    public TransactionJufrin(P paymentType) {
        this.paymentType = paymentType;
    }

    public P getPaymentType() {
        return this.paymentType;
    }

    public void setPaymentType(P paymentType) {
        this.paymentType = paymentType;
    }
}
