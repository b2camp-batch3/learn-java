package id.b2camp.jufrin.day10.genericClass;

public class PrintTransaction {
    private Integer amount;

    public void print() {
        System.out.println(this.amount);
    }

    public Integer getAmount() {
        return this.amount;
    }

    public PrintTransaction(Integer amount) {
        this.amount = amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}