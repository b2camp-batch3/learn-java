package id.b2camp.jufrin.day10.genericClass;

public class CryptoTransaction <T> {
    private T amount;

    public void print() {
        System.out.println(this.amount);
    }

    public T getAmount() {
        return this.amount;
    }

    public void setAmount(T amount) {
        this.amount = amount;
    }

    public CryptoTransaction(T amount) {
        this.amount = amount;
    }
}
