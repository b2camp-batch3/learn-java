package id.b2camp.jufrin.day10.genericClass;
/////testing Class main//
public class TestingJufrin {
    public static void main(String[] args) {
        PaymentMethod paymentMethod = (PaymentMethod)  new  PaymentMethod() {
            @Override
            public void processPayment() {

            }
        };
        paymentMethod.processPayment();

        paymentMethod = (PaymentMethod) new BitcoinPayment(0.01);
        paymentMethod.processPayment();

        paymentMethod = (PaymentMethod) new EthereumPayment(2.5);
        paymentMethod.processPayment();
    }
}
