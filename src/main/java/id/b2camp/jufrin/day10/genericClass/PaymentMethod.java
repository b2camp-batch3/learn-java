package id.b2camp.jufrin.day10.genericClass;

public interface PaymentMethod {
    void processPayment();
}