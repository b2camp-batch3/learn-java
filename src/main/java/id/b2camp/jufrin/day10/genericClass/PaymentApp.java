package id.b2camp.jufrin.day10.genericClass;

public class PaymentApp implements PaymentMethod {
    private Double amount;

    public Double getAmount() {
        return this.amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public PaymentApp(Double amount) {
        this.amount = amount;
    }

    @Override
    public void processPayment() {

    }
}