package id.b2camp.jufrin.day10.genericClass;


public class EthereumPayment extends AbstractPaymentApp {
    public EthereumPayment(Double amount) {
        super(amount);
    }

    public String toString() {
        return "EthereumPayment()";
    }

    public void processPayment() {
        System.out.println("Processing Ethereum payment: " + getAmount());
    }
}
