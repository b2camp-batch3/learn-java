package id.b2camp.jufrin.day10.genericClass;

abstract class AbstractPaymentApp implements PaymentMethod {
    private Double amount;

    public Double getAmount() {
        return this.amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public AbstractPaymentApp(Double amount) {
        this.amount = amount;
    }
}