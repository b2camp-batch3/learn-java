package id.b2camp.jufrin.day7.statickeyword;

public class Weapons {
    static class WeaponModel {
        private String modelName;
        private String ammoType;

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public String getAmmoType() {
            return ammoType;
        }

        public void setAmmoType(String ammoType) {
            this.ammoType = ammoType;
        }
    }

    static WeaponModel assaultRifle = new WeaponModel();
    static WeaponModel sniperRifle = new WeaponModel();
    static WeaponModel shotgun = new WeaponModel();
    static WeaponModel machineGun = new WeaponModel();
    static WeaponModel rocketLauncher = new WeaponModel();
    static WeaponModel pistol = new WeaponModel();
    static WeaponModel submachineGun = new WeaponModel();
    static WeaponModel flamethrower = new WeaponModel();
    static WeaponModel crossbow = new WeaponModel();
    static WeaponModel grenadeLauncher = new WeaponModel();

    static {
        assaultRifle.setModelName("Assault Rifle");
        assaultRifle.setAmmoType("5.56mm");

        sniperRifle.setModelName("Sniper Rifle");
        sniperRifle.setAmmoType("7.62mm");

        shotgun.setModelName("Shotgun");
        shotgun.setAmmoType("12-gauge");

        machineGun.setModelName("Machine Gun");
        machineGun.setAmmoType("7.62mm");

        rocketLauncher.setModelName("Rocket Launcher");
        rocketLauncher.setAmmoType("Rockets");

        pistol.setModelName("Pistol");
        pistol.setAmmoType("9mm");

        submachineGun.setModelName("Submachine Gun");
        submachineGun.setAmmoType("9mm");

        flamethrower.setModelName("Flamethrower");
        flamethrower.setAmmoType("Napalm");

        crossbow.setModelName("Crossbow");
        crossbow.setAmmoType("Bolts");

        grenadeLauncher.setModelName("Grenade Launcher");
        grenadeLauncher.setAmmoType("Grenades");
    }
}