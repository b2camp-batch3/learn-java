package id.b2camp.jufrin.day7.statickeyword;
public class Soldier {
    private String soldierName;

    public String getSoldierName() {
        return soldierName;
    }

    public void setSoldierName(String soldierName) {
        this.soldierName = soldierName;
    }

    static class SoldierDivision {
        private String division;

        public String getDivision() {
            return division;
        }

        public void setDivision(String division) {
            this.division = division;
        }
    }
}