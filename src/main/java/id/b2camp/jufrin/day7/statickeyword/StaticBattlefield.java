package id.b2camp.jufrin.day7.statickeyword;

public class StaticBattlefield {
    public static void main(String[] args) {
        Battlefield.battleInfo("Urban Battlefield");
        System.out.println("======================================================================================================");

        Weapons.WeaponModel weaponAssaultRifle = Weapons.assaultRifle;
        System.out.println("Armed with " + weaponAssaultRifle.getModelName() + " (" + weaponAssaultRifle.getAmmoType() + ")");
        printWeaponDetails(weaponAssaultRifle);

        Weapons.WeaponModel weaponSniperRifle = Weapons.sniperRifle;
        System.out.println("Snipers equipped with " + weaponSniperRifle.getModelName() + " (" + weaponSniperRifle.getAmmoType() + ")");
        printWeaponDetails(weaponSniperRifle);

        Weapons.WeaponModel shotgun = Weapons.shotgun;
        System.out.println("Close-quarters combatants wield " + shotgun.getModelName() + " (" + shotgun.getAmmoType() + ")");
        printWeaponDetails(shotgun);

        Weapons.WeaponModel machineGun = Weapons.machineGun;
        System.out.println("Support troops carry " + machineGun.getModelName() + " (" + machineGun.getAmmoType() + ")");
        printWeaponDetails(machineGun);

        Weapons.WeaponModel rocketLauncher = Weapons.rocketLauncher;
        System.out.println("Demolition experts use " + rocketLauncher.getModelName() + " (" + rocketLauncher.getAmmoType() + ")");
        printWeaponDetails(rocketLauncher);

        Weapons.WeaponModel pistol = Weapons.pistol;
        System.out.println("Sidearms include " + pistol.getModelName() + " (" + pistol.getAmmoType() + ")");
        printWeaponDetails(pistol);

        Weapons.WeaponModel submachineGun = Weapons.submachineGun;
        System.out.println("Fast-action operators wield " + submachineGun.getModelName() + " (" + submachineGun.getAmmoType() + ")");
        printWeaponDetails(submachineGun);

        Weapons.WeaponModel flamethrower = Weapons.flamethrower;
        System.out.println("Flamethrower units deploy " + flamethrower.getModelName() + " (" + flamethrower.getAmmoType() + ")");
        printWeaponDetails(flamethrower);

        Weapons.WeaponModel crossbow = Weapons.crossbow;
        System.out.println("Stealthy agents rely on " + crossbow.getModelName() + " (" + crossbow.getAmmoType() + ")");
        printWeaponDetails(crossbow);

        Weapons.WeaponModel grenadeLauncher = Weapons.grenadeLauncher;
        System.out.println("Explosive experts carry " + grenadeLauncher.getModelName() + " (" + grenadeLauncher.getAmmoType() + ")");
        printWeaponDetails(grenadeLauncher);

        System.out.println("===================================================================================================================");

        Soldier soldier = new Soldier();
        soldier.setSoldierName("Ethan");

        Soldier.SoldierDivision soldierDivision = new Soldier.SoldierDivision();
        soldierDivision.setDivision("Jufrin Team");

        System.out.println(soldier.getSoldierName() + " is part of " + soldierDivision.getDivision());
    }

    static void printWeaponDetails(Weapons.WeaponModel weapon) {
        System.out.println("Weapon Details:");
        System.out.println("Model Name: " + weapon.getModelName());
        System.out.println("Ammo Type: " + weapon.getAmmoType());
        System.out.println();
    }
}