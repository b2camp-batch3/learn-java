package id.b2camp.jufrin.day7.finalkeyword;

public class WarSimulationAppJufrin {
    public static void main(String[] args) {
        SniperJufrin sniper = new SniperJufrin();
        sniper.setName("Jul");
        sniper.setHealth(80);
        sniper.setAttackPower(95);
        sniper.setRifleType("Long-Range Precision Rifle");

        System.out.println(sniper.getName() + " emerges from the shadows, a deadly " + sniper.getRifleType() + " in hand.");
        System.out.println("With a piercing gaze, " + sniper.getName() + " readies to deliver precise death.");

        System.out.println("==============================================================================================");

        InfantryJufrin infantry = new InfantryJufrin();
        infantry.setName("Jerry");
        infantry.setHealth(100);
        infantry.setAttackPower(70);

        System.out.println(infantry.getName() + " stands tall on the battlefield, ready for the clash.");
        System.out.println("A formidable " + infantry.getName() + " equipped with unmatched bravery and a strong attack.");

        System.out.println("==================================================================================================");

        MedicJufrin medic = new MedicJufrin();
        medic.setName("Gita");
        medic.setHealth(90);
        medic.setMedicalKit("Advanced Healing Kit");

        System.out.println(medic.getName() + " rushes through the chaos, an " + medic.getMedicalKit() + " in hand.");
        System.out.println(medic.getName() + " is the beacon of hope, mending wounds and saving lives amidst the chaos.");
    }
}