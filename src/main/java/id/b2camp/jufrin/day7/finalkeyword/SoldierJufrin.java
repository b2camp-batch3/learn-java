package id.b2camp.jufrin.day7.finalkeyword;

public class SoldierJufrin {

    private String name;
    private int health;
    private int attackPower;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }
}

class InfantryJufrin extends SoldierJufrin {

    final String role(String task) {
        return task;
    }
}

final class SniperJufrin extends InfantryJufrin {

    private String rifleType;

    public String getRifleType() {
        return rifleType;
    }

    public void setRifleType(String rifleType) {
        this.rifleType = rifleType;
    }
}

final class MedicJufrin extends InfantryJufrin {

    private String medicalKit;

    public String getMedicalKit() {
        return medicalKit;
    }

    public void setMedicalKit(String medicalKit) {
        this.medicalKit = medicalKit;
    }
}