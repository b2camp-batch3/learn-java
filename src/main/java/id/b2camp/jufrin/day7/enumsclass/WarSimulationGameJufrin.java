package id.b2camp.jufrin.day7.enumsclass;

public class WarSimulationGameJufrin {
    public static void main(String[] args) {
        BattlefieldAction attack = BattlefieldAction.ATTACK;
        BattlefieldAction defend = BattlefieldAction.DEFEND;
        BattlefieldAction retreat = BattlefieldAction.RETREAT;

        BattleProcessor battle1 = new BattleProcessor("GARUDA Squad", "Bravo Squad", attack, "Machine Gun");
        System.out.println("Action: " + battle1.getAction());
        System.out.println(battle1.getAttacker() + " is launching an attack against " + battle1.getDefender());
        System.out.println("Engaging with " + battle1.getWeapon());

        System.out.println("=======================================================================================================");

        BattleProcessor battle2 = new BattleProcessor("GAJAH Squad", "Delta Squad", defend, "Bunker");
        System.out.println("Action: " + battle2.getAction());
        System.out.println(battle2.getDefender() + " is defending against " + battle2.getAttacker());
        System.out.println("Defending with " + battle2.getWeapon());

        System.out.println("========================================================================================================");

        BattleProcessor battle3 = new BattleProcessor("HARIMAU Squad", "Foxtrot Squad", retreat, "Smoke Grenades");
        System.out.println("Action: " + battle3.getAction());
        System.out.println(battle3.getAttacker() + " is retreating from " + battle3.getDefender());
        System.out.println("Using " + battle3.getWeapon() + " to cover the retreat.");
    }
}
