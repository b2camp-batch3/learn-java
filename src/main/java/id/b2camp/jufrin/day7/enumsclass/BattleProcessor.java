package id.b2camp.jufrin.day7.enumsclass;

class BattleProcessor {
    private String attacker;
    private String defender;
    private BattlefieldAction action;
    private String weapon;

    public BattleProcessor(String attacker, String defender, BattlefieldAction action, String weapon) {
        this.attacker = attacker;
        this.defender = defender;
        this.action = action;
        this.weapon = weapon;
    }

    public String getAttacker() {
        return attacker;
    }

    public String getDefender() {
        return defender;
    }

    public BattlefieldAction getAction() {
        return action;
    }

    public String getWeapon() {
        return weapon;
    }
}
