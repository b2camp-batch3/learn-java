package id.b2camp.jufrin.day7.enumsclass;

enum BattlefieldAction {
    ATTACK, DEFEND, RETREAT
}
