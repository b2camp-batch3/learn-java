package id.b2camp.jufrin.day7.anonymouseclass;

public class WarSimulationGame {

    public static void main(String[] args) {
        soldierAttributes player = new soldierAttributes() {
            @Override
            public int getHealth() {
                return 100;
            }
            @Override
            public int getAttackDamage() {
                return 20;
            }
            @Override
            public String getRank() {
                return "Captain";
            }
            @Override
            public String getSpecialty() {
                return "Sniper";
            }
            @Override
            public String getNationality() {
                return "INDONESIA";
            }
            @Override
            public String getDivision() {
                return "Alpha Division";
            }
            @Override
            public String getUniformColor() {
                return "Green";
            }
            @Override
            public String getWeapon() {
                return "M24 Sniper Rifle";
            }
        };

        System.out.println("========================================");
        System.out.println("Player Attributes:");
        System.out.println("Health: " + player.getHealth());
        System.out.println("Attack Damage: " + player.getAttackDamage());
        System.out.println("Rank: " + player.getRank());
        System.out.println("Specialty: " + player.getSpecialty());
        System.out.println("Nationality: " + player.getNationality());
        System.out.println("Division: " + player.getDivision());
        System.out.println("Uniform Color: " + player.getUniformColor());
        System.out.println("Weapon: " + player.getWeapon());
        System.out.println("========================================");
    }
}

