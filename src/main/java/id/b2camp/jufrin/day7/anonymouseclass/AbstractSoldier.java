package id.b2camp.jufrin.day7.anonymouseclass;
public abstract class AbstractSoldier implements soldierAttributes {
    @Override
    public abstract int getHealth();

    @Override
    public abstract int getAttackDamage();

    @Override
    public abstract String getRank();

    @Override
    public abstract String getSpecialty();
}
