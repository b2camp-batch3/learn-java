package id.b2camp.jufrin.day7.anonymouseclass;

 public interface soldierAttributes {
    int getHealth();
    int getAttackDamage();
    String getRank();
    String getSpecialty();
    String getNationality();
    String getDivision();
    String getUniformColor();
    String getWeapon();
}
