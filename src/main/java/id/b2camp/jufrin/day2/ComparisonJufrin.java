package id.b2camp.jufrin.day2;

public class ComparisonJufrin {
    public static void main(String[] args) {
        int a = 10;
        int b = 5;
        boolean isEqual = a == b;
        System.out.println("Apakah a sama dengan b? " + isEqual);
        boolean isNotEqual = a != b;
        System.out.println("Apakah a tidak sama dengan b? " + isNotEqual);
        boolean isLessThan = a < b;
        System.out.println("Apakah a lebih kecil dari b? " + isLessThan);
        boolean isGreaterThan = a > b;
        System.out.println("Apakah a lebih besar dari b? " + isGreaterThan);
        boolean isLessThanOrEqual = a <= b;
        System.out.println("Apakah a lebih kecil dari atau sama dengan b? " + isLessThanOrEqual);
        boolean isGreaterThanOrEqual = a >= b;
        System.out.println("Apakah a lebih besar dari atau sama dengan b? " + isGreaterThanOrEqual);

        boolean andri = false;
        boolean tiva =  true;
        boolean tika = false;
        boolean jufrin = false;

        System.out.println(andri&&tiva||tika&&jufrin);
        System.out.println(andri||tiva&&tiva && !(jufrin));

    }
}
