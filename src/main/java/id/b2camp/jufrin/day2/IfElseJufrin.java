package id.b2camp.jufrin.day2;

public class IfElseJufrin {
    public static void main(String[] args) {
        int pilihan = 3;
        System.out.println("Selamat datang di Restoran Sederhana!");
        System.out.println("Menu Makanan:");
        System.out.println("1. Nasi Goreng");
        System.out.println("2. Mie Goreng");
        System.out.println("3. Ayam Goreng");
        System.out.println("4. Bebek Goreng");
        System.out.println("5. Gado-gado");

        if (pilihan == 1) {
            System.out.println("Anda memesan Nasi Goreng.");
        } else if (pilihan == 2) {
            System.out.println("Anda memesan Mie Goreng.");
        } else if (pilihan == 3) {
            System.out.println("Anda memesan Ayam Goreng.");
        } else if (pilihan == 4) {
            System.out.println("Anda memesan Bebek Goreng.");
        } else if (pilihan == 5) {
            System.out.println("Anda memesan Gado-gado.");
        } else {
            System.out.println("Pilihan tidak Benar. Silakan pilih angka 1-5.");

            }
        }
    }
