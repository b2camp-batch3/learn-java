package id.b2camp.jufrin.day2;

public class SwitchCaseJufrin {
    public static void main(String[] args) {
        int pilihan = 3;

        System.out.println("Menu Makanan:");
        System.out.println("1. Ayam Goreng");
        System.out.println("2. Nasi Goreng");
        System.out.println("3. Mie Goreng");
        System.out.println("4. Sate");
        System.out.println("5. Soto");

        switch (pilihan) {
            case 1:
                System.out.println("Anda memilih Ayam Goreng.");
                System.out.println("Harga: Rp 20,000");
                break;
            case 2:
                System.out.println("Anda memilih Nasi Goreng.");
                System.out.println("Harga: Rp 15,000");
                break;
            case 3:
                System.out.println("Anda memilih Mie Goreng.");
                System.out.println("Harga: Rp 12,000");
                break;
            case 4:
                System.out.println("Anda memilih Sate.");
                System.out.println("Harga: Rp 25,000");
                break;
            case 5:
                System.out.println("Anda memilih Soto.");
                System.out.println("Harga: Rp 18,000");
                break;
            default:
                System.out.println("Pilihan Benar. Silakan pilih angka 1-5.");
        }
    }
}
