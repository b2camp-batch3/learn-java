package id.b2camp.jufrin.day2;

public class ArrayAndForLoopJufrin {
    public static void main(String[] args) {
        // Inisialisasi array
        int[] array = {1, 2, 3, 4, 5};

        for (int i = 0; i < array.length; i++) {
            System.out.println("HALAMAN = "+array[i]);

            String[] buah = {"Apel", "Pisang", "Jeruk", "Anggur", "Mangga"};

            System.out.println("Daftar buah:");
            for (int j = 0; j < buah.length; j++) {
                System.out.println(buah[j]);
            }

            int panjangArray = buah.length;
            System.out.println("\nPanjang array: " + panjangArray);
            System.out.println("\nMengakses elemen individual:");
            System.out.println("Buah pertama: " + buah[0]);
            System.out.println("Buah kedua: " + buah[1]);
            System.out.println("Buah terakhir: " + buah[panjangArray - 1]);

            buah[1] = "Pir";
            System.out.println("\nArray setelah dimodifikasi:");
            for (String buahItem : buah) {
                System.out.println(buahItem);

            }
        }
    }
}
