package id.b2camp.jufrin.day2;

public class ArimathicJufrin {
    public static void main(String[] args) {
        int a,b,c;
        double f,g,h,i;

        a = 20;
        b = 10;
        c = a+b;
        System.out.println("hasil ="+c);
        f = 2.0;
        g = 4.7;
        h = 9.0;
        i = f/g-h;
        System.out.println("hasil ="+i);

        int angka1 = 10;
        int angka2 = 5;
        int tambah = angka1 + angka2;
        System.out.println("penambahan: " + tambah);


        int kurang = angka1 - angka2;
        System.out.println("pengurangan: " + kurang);

        int perkalian = angka1 * angka2;
        System.out.println("perkalian: " +perkalian);

        double pembagian = (double) angka1 / angka2;
        System.out.println("Pembagian: " + pembagian);

        int hasilbagi = angka1 % angka2;
        System.out.println("hasil bagi: " + hasilbagi);

        int x = 5;
        x++; // Increment x by 1
        System.out.println("Increment: " + x);

        int y = 10;
        y--; // Decrement y by 1
        System.out.println("Decrement: " + y);

        // Math functions
        double o = 2.5;
        double z = 3.7;

        double maxVal = Math.max(a, b);
        System.out.println("Max value: " + maxVal);

        double minVal = Math.min(a, b);
        System.out.println("Min value: " + minVal);

        double sqrtA = Math.sqrt(a);
        System.out.println("Square root of a: " + sqrtA);

        double powB = Math.pow(b, 2);
        System.out.println("b^2: " + powB);

        double absoluteB = Math.abs(b);
        System.out.println("Absolute value of b: " + absoluteB);


    }
}
