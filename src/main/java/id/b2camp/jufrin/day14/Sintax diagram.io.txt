Table User as "User" {
  ID INT [primary key]
  Nama VARCHAR
  Email VARCHAR [unique]
  Password VARCHAR
  TanggalDaftar DATE
  Alamat VARCHAR
   LocationID INT [ref: > Location.ID]

}

Table Product as "Product" {
  ID INT [primary key]
  Nama VARCHAR
  Harga DECIMAL(10, 2)
  Status VARCHAR
  Kategori INT [ref: > Category.ID]
  UserID INT [ref: > User.ID]
  TanggalDibuat DATE
  Deskripsi TEXT

}

Table Category as "Category" {
  ID INT [primary key]
  Nama VARCHAR
  Deskripsi TEXT

}

Table Transaction as "Transaction" {
  ID INT [primary key]
  PembeliID INT [ref: > User.ID]
  PenjualID INT [ref: > User.ID]
  TanggalTransaksi DATE
  Jumlah DECIMAL(10, 2)
}

Table Image as "Image" {
  ID INT [primary key]
  ProductID INT [ref: > Product.ID]
  ImageURL VARCHAR
  Deskripsi TEXT

}

Table ReviewRating as "Review/Rating" {
  ID INT [primary key]
  ProductID INT [ref: > Product.ID]
  UserID INT [ref: > User.ID]
  Rating INT
  Komentar TEXT
  TanggalReview DATE

}

Table Location as "Location" {
  ID INT [primary key]
  Kota VARCHAR
  Provinsi VARCHAR
  Negara VARCHAR
  KodePos VARCHAR

}
