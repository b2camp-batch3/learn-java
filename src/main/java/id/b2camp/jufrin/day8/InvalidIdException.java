package id.b2camp.jufrin.day8;

public class InvalidIdException extends Throwable {
    public InvalidIdException(String message) {
        super(message);
    }
}
