package id.b2camp.jufrin.day8;

public class InvalidBirthdateException extends Throwable {
    public InvalidBirthdateException(String message) {
        super(message);
    }
}
