package id.b2camp.jufrin.day8;

public abstract class AbstractWarrior implements Combatant {
    private String id;
    private String rank;
    private String division;

    public AbstractWarrior(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }
    public abstract void performSpecialAbility();
}
