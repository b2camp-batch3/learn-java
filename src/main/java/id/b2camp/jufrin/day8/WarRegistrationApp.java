package id.b2camp.jufrin.day8;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class WarRegistrationApp {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws InvalidIdException, InvalidPasswordException, InvalidBirthdateException {
        System.out.println("Selamat datang di Pusat Pendaftaran Prajurit.\""+"Silakan isi formulir pendaftaran:");
        String id = getInput("ID: ");
        String password = getInput("Password: ");
        String birthdate = getInput("Birthdate (yyyy-MM-dd): ");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthdateParsed = LocalDate.parse(birthdate, formatter);

        try {
            RegistrationValidator.validateId(id);
            RegistrationValidator.validatePassword(password);
            RegistrationValidator.validateBirthdate(birthdateParsed);
        } catch (InvalidIdException e) {
            e.printStackTrace();
            throw new InvalidIdException(e.getMessage());
        } catch (InvalidPasswordException e) {
            e.printStackTrace();
            throw new InvalidPasswordException(e.getMessage());
        } catch (InvalidBirthdateException e) {
            e.printStackTrace();
            throw new InvalidBirthdateException(e.getMessage());
        } finally {
            System.out.println("\"Mohon maaf, Anda perlu mengisi ulang formulir pendaftaran.");
        }

       // RegistrationValidator.validateId(id);
        //RegistrationValidator.validateBirthdate(birthdateParsed);
       // RegistrationValidator.validatePassword("");
        //RegistrationValidator.validRuntimeinName("jufrin");
        //RegistrationValidator.validRuntimeinAddres("Jakarta");

        WarriorForm newWarrior = new WarriorForm(id, password, birthdateParsed);
        newWarrior.setRank("Private");
        newWarrior.setDivision("Infantry");

        System.out.println("Pendaftaran berhasil untuk prajurit dengan ID: " + newWarrior.getId());
        System.out.println("Rank: " + newWarrior.getRank());
        System.out.println("Division: " + newWarrior.getDivision());

        performCombatAction(newWarrior);
    }

    static String getInput(String message) {
        System.out.println(message);
        return scan.nextLine();
    }

    static void performCombatAction(Combatant combatant) {
        combatant.attack();
        combatant.defend();
    }
}
