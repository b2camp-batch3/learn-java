package id.b2camp.jufrin.day8;

import java.time.LocalDate;

public class WarriorForm extends AbstractWarrior {
    private String password;
    private LocalDate birthdate;

    public WarriorForm(String id, String password, LocalDate birthdate) {
        super(id);
        this.password = password;
        this.birthdate = birthdate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public void attack() {
        System.out.println("Warrior " + getId() + " Siap untuk Bertempurrr");
    }

    @Override
    public void defend() {
        System.out.println("Warrior " + getId() + "siap untuk bertahan");
    }

    @Override
    public void performSpecialAbility() {
        System.out.println("Warrior " + getId() + " SELALU BERJUANGGG GOOOO");
    }
}
