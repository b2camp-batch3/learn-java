package id.b2camp.jufrin.day8;

import java.time.LocalDate;
import java.util.Objects;

public class RegistrationValidator {
    public static void validateId(String id) throws InvalidIdException {
        if (id.isBlank()) {
            throw new InvalidIdException("ID harus diisi.");
        } else if (id.isEmpty()) {
            throw new InvalidIdException("ID tidak boleh kosong.");
        }
    }

    public static void validatePassword(String password) throws InvalidPasswordException {
        if (password.isBlank()) {
            throw new InvalidPasswordException("Kata sandi harus mengandung huruf dan angka.");
        } else if (password.isEmpty()) {
            throw new InvalidPasswordException("Kata sandi tidak boleh kosong.");
        }
    }

    public static void validateBirthdate(LocalDate birthdate) throws InvalidBirthdateException {
        if (Objects.isNull(birthdate)) {
            throw new InvalidBirthdateException("Tanggal lahir harus diisi.");
        } else if (birthdate.isAfter(LocalDate.now())) {
            throw new InvalidBirthdateException("Tanggal lahir tidak boleh setelah hari ini.");
        }
    }

    public static void validRuntimeinAddres(String addres) throws RuntimeException {
        if (addres.isBlank()) {
            throw new RuntimeException("Wajib mengisi alamat anda");
        } else if (addres.isEmpty()) {
            throw new RuntimeException("alamat tidak wajib kosong");
        }
    }
    public static void validRuntimeinName(String name) throws RuntimeException {
        if (name.isBlank()) {
            throw new RuntimeException("Wajib mengisi nama anda");
        } else if (name.isEmpty()) {
            throw new RuntimeException("nama tidak wajib kosong");
        }
    }
}
