package id.b2camp.jufrin.day8;

import java.time.LocalDate;

public interface Combatant {
    void attack();
    void defend();
}
