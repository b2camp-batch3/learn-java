--/add COLUMN

CREATE TABLE jufrin_table (
    id serial PRIMARY KEY,
    nama character varying(100) NOT NULL,
    alamat character varying(100) NOT NULL,
    nomor_hp character varying(15) NOT NULL,
    tanggal_lahir date NOT NULL,
    status_kawin boolean NOT NULL,
    jenis_kelamin character varying(10) NOT NULL
);


ALTER TABLE jufrin_table
ADD COLUMN id serial PRIMARY KEY,
ADD COLUMN nama character varying(100) NOT NULL,
ADD COLUMN alamat character varying(100) NOT NULL,
ADD COLUMN nomor_hp character varying(15) NOT NULL,
ADD COLUMN tanggal_lahir date NOT NULL,
ADD COLUMN status_kawin boolean NOT NULL,
ADD COLUMN jenis_kelamin character varying(10) NOT NULL;
;

--SELECT fitur

SELECT id, nama, alamat, nomor_hp, tanggal_lahir, status_kawin, jenis_kelamin
FROM public.jufrin_table;

SELECT id, nama, alamat, nomor_hp, tanggal_lahir, status_kawin, jenis_kelamin
FROM public.jufrin_table;


INSERT INTO public.jufrin_table
(nama, alamat, nomor_hp, tanggal_lahir, status_kawin, jenis_kelamin)
VALUES('yuni', 'bandung', '08775646333', '1999-10-24', false, 'famlae');


--delete colum where


--set COLUM

ALTER TABLE jufrin_table
    ALTER COLUMN nama SET DATA TYPE character varying(100),
    ALTER COLUMN tanggal_lahir SET DATA TYPE date,
    ALTER COLUMN status_kawin SET DATA TYPE boolean,
    ALTER COLUMN jenis_kelamin SET DATA TYPE character varying(10);

   ALTER TABLE jufrin_table
    ALTER COLUMN nama SET DATA TYPE character varying(100) NOT NULL,
    ALTER COLUMN tanggal_lahir SET DATA TYPE date NOT NULL,
    ALTER COLUMN status_kawin SET DATA TYPE boolean NOT NULL,
    ALTER COLUMN jenis_kelamin SET DATA TYPE character varying(10) NOT NULL;

--   delete COLUMN

   ALTER TABLE jufrin_table
DROP COLUMN id,
DROP COLUMN nama,
DROP COLUMN alamat,
DROP COLUMN nomor_hp,
DROP COLUMN tanggal_lahir,
DROP COLUMN status_kawin,
DROP COLUMN jenis_kelamin;

DROP TABLE jufrin_table;

--insert column

INSERT INTO public.jufrin_table
(nama, alamat, nomor_hp, tanggal_lahir, status_kawin, jenis_kelamin)
VALUES ('jojo', 'bandung', '0987644', '1997-12-14', false, 'laki-laki');

INSERT INTO public.jufrin_table
(nama, alamat, nomor_hp, tanggal_lahir, status_kawin, jenis_kelamin)
VALUES('budi', 'bandung', '08775646444', '1998-11-20', false, 'male');

INSERT INTO public.jufrin_table
(nama, alamat, nomor_hp, tanggal_lahir, status_kawin, jenis_kelamin)
VALUES('d.senjaya', 'bandung', '08775646333', '1990-09-24', TRUE, 'male');

--update COLUM

UPDATE public.jufrin_table
SET nama = 'yuni',
    alamat = 'bandung',
    nomor_hp = '08775646333',
    tanggal_lahir = '1999-10-24',
    status_kawin = false,
    jenis_kelamin = 'female'
WHERE id = 2;

DELETE FROM public.jufrin_table
WHERE id = 3;



