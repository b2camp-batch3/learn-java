package id.b2camp.jufrin.day3;

public class ArrayAndForLoop2DJufrin {
    public static void main(String[] args) {
        int nim;
        double[][] dafNilai;
        double nilaiRata;

        dafNilai = new double[5][2];
        dafNilai [1][0] = 80;
        dafNilai [1][1] = 60;
        dafNilai [2][0] = 70;
        dafNilai [2][1] = 90;
        dafNilai [3][0] = 100;
        dafNilai [3][1] = 80;
        dafNilai [4][0] = 70;
        dafNilai [4][1] = 90;
        for (nim = 1; nim <5; nim++){
            nilaiRata = dafNilai[nim][0] + dafNilai[nim][1] / 2;
            System.out.println("Nilai rata MahaSiswa ke -"+nim+" = "+nilaiRata);
}


    }
}
