package id.b2camp.jufrin.day3;

class Method{
    private String name;
    private int age;

    // Constructor
    public Method(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void sayHello() {
        System.out.println("Hello, my name is " + name + " and I am " + age + " years old.");
    }

    // Getter and setter methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

public class MethodJufrin {
    public static void main(String[] args) {
        Method jufrin = new Method("Jufrin", 23);
        Method tiva = new Method("Tiva", 13);


        jufrin.sayHello();
        tiva.sayHello();

        //getter and setter
        System.out.println("================");
        System.out.println(jufrin.getName());
        System.out.println(tiva.getAge());
        System.out.println("==================");
        jufrin.setAge(23);
        jufrin.setName("Hamid");

        jufrin.sayHello();
        tiva.sayHello();

    }
}
