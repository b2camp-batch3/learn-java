package id.b2camp.jufrin.day3;

import java.util.Scanner;

public class ScannerJufrin {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String name ;
        int age;
        double high ;


        System.out.print("my name is = ");
        name = sc.nextLine();
        System.out.print("yours old is = ");
        age = sc.nextInt();
        System.out.print("yours high is = ");
        high = sc.nextDouble();
        System.out.println("====================");
        System.out.println("my name is = "+name);
        System.out.println("my years old = "+age+"old");
        System.out.println("my high is = "+ high + "Cm");
    }
}
