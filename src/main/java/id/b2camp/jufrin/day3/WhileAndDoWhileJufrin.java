package id.b2camp.jufrin.day3;

public class WhileAndDoWhileJufrin {
    public static void main(String[] args) {
        int loop = 1;

        // While loop example
        while (loop <= 5) {
            System.out.println("looping: " + loop);
            loop++;
        }
        int looping = 1;

        // Do-while loop example
        do {
            System.out.println("===================");
            System.out.println("Loooping: " + looping);
            looping++;
        } while (looping <= 5);
    }
}
