package id.b2camp.jufrin.day12.stream;

enum WarriorType {
    INFANTRY,
    CAVALRY
}