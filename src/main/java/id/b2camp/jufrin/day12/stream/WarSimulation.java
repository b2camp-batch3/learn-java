package id.b2camp.jufrin.day12.stream;

import java.util.List;
import java.util.stream.Collectors;

public class WarSimulation {
    public static void main(String[] args) {
        List<Warrior> warriors = getWarriors();

        List<Warrior> infantryList = warriors.stream()
            .filter(warrior -> warrior.getType().equals(WarriorType.INFANTRY))
            .collect(Collectors.toList());

        List<Warrior> cavalryList = warriors.stream()
            .filter(warrior -> warrior.getType().equals(WarriorType.CAVALRY))
            .collect(Collectors.toList());

        simulateBattle(infantryList, cavalryList);
    }

    static List<Warrior> getWarriors() {
        return List.of(
            new Warrior("halamadrid", WarriorType.INFANTRY, 100, "Rifle"),
            new Warrior("halabarca", WarriorType.INFANTRY, 120, "Rifle"),
            new Warrior("helopanda", WarriorType.CAVALRY, 80, "Sword"),
            new Warrior("Kinderjoyy", WarriorType.CAVALRY, 90, "Sword"));
    }

    static void simulateBattle(List<Warrior> infantryList, List<Warrior> cavalryList) {
        System.out.println("Pertempuran dimulai!");

        while (!infantryList.isEmpty() && !cavalryList.isEmpty()) {
            Warrior infantrySoldier = infantryList.get(0);
            Warrior cavalrySoldier = cavalryList.get(0);

            infantrySoldier.attack(cavalrySoldier);
            cavalrySoldier.attack(infantrySoldier);


            infantryList.removeIf(warrior -> warrior.getStrength() <= 0);
            cavalryList.removeIf(warrior -> warrior.getStrength() <= 0);


            displayBattleInfo(infantryList, cavalryList);
        }

        determineWinner(infantryList, cavalryList);
    }

    static void displayBattleInfo(List<Warrior> infantryList, List<Warrior> cavalryList) {
        System.out.println("Infantry: " + infantryList.size() + " warriors remaining");
        System.out.println("Cavalry: " + cavalryList.size() + " warriors remaining");
        System.out.println();
    }

    static void determineWinner(List<Warrior> infantryList, List<Warrior> cavalryList) {

        if (infantryList.isEmpty() && !cavalryList.isEmpty()) {
            System.out.println("Pertempuran selesai! Cavalry menang!");
        } else if (!infantryList.isEmpty() && cavalryList.isEmpty()) {
            System.out.println("Pertempuran selesai! Infantry menang!");
        } else {
            System.out.println("Pertempuran selesai! Kedua kelompok imbang.");
        }
    }
}