package id.b2camp.jufrin.day12.stream;

class Warrior {
    private String name;
    private WarriorType type;
    private int strength;
    private String weapon;

    public Warrior(String name, WarriorType type, int strength, String weapon) {
        this.name = name;
        this.type = type;
        this.strength = strength;
        this.weapon = weapon;
    }

    public String getName() {
        return name;
    }

    public WarriorType getType() {
        return type;
    }

    public int getStrength() {
        return strength;
    }

    public String getWeapon() {
        return weapon;
    }

    public void attack(Warrior enemy) {

        int damage = 10;
        enemy.strength -= damage;
    }
}