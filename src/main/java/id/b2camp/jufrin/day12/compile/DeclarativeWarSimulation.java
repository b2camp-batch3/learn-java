package id.b2camp.jufrin.day12.compile;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class DeclarativeWarSimulation {
    public static void main(String[] args) {
        List<Warrior> warriors = getWarriors();

        while (warriors.size() > 1) {

            Random acak = new Random();
            int index1 = acak.nextInt(warriors.size());
            int index2 = acak.nextInt(warriors.size());

            if (index1 != index2) {
                Warrior warrior1 = warriors.get(index1);
                Warrior warrior2 = warriors.get(index2);


                battle(warrior1, warrior2);


                warriors = warriors.stream()
                    .filter(warrior -> !warrior.isDead())
                    .collect(Collectors.toList());
            }
        }

        if (warriors.isEmpty()) {
            System.out.println("Pertempuran selesai! Tidak ada pemenang.");
        } else {
            System.out.println("Pertempuran selesai! " + warriors.get(0).getName() + " adalah pemenangnya!");
        }
    }

    static List<Warrior> getWarriors() {
        return List.of(
            new Warrior("Arga", 100),
            new Warrior("Muzz", 120),
            new Warrior("Fankri", 80),
            new Warrior("Fanri", 90)
        );
    }

    static void battle(Warrior warrior1, Warrior warrior2) {

        Random random = new Random();
        int damage1 = random.nextInt(20);
        int damage2 = random.nextInt(20);

        warrior1.receiveDamage(damage2);
        warrior2.receiveDamage(damage1);
    }
}
