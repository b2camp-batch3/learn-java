package id.b2camp.jufrin.day12.compile;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ImperativeStyle{
    public static void main(String[] args) {
        List<Warrior> warriors = getWarriors();

        while (true) {
            List<Warrior> aliveWarriors = new ArrayList<>();
            for (Warrior warrior : warriors) {
                if (warrior.getStrength() > 0) {
                    aliveWarriors.add(warrior);
                }
            }

            if (aliveWarriors.size() < 2) {
                break;
            }

            Random random = new Random();
            int index1 = random.nextInt(aliveWarriors.size());
            int index2 = random.nextInt(aliveWarriors.size());

            if (index1 != index2) {
                Warrior warrior1 = aliveWarriors.get(index1);
                Warrior warrior2 = aliveWarriors.get(index2);

                battle(warrior1, warrior2);
            }
        }

        List<Warrior> aliveWarriors = new ArrayList<>();
        for (Warrior warrior : warriors) {
            if (warrior.getStrength() > 0) {
                aliveWarriors.add(warrior);
            }
        }

        if (aliveWarriors.isEmpty()) {
            System.out.println("Pertempuran selesai! Tidak ada pemenang.");
        } else {
            System.out.println("Pertempuran selesai! " + aliveWarriors.get(0).getName() + " adalah pemenangnya!");
        }
    }
    static List<Warrior> getWarriors() {
        return List.of(
            new Warrior("Jerry", 100),
            new Warrior("DAVID", 120),
            new Warrior("RINI", 80),
            new Warrior("GITA", 90)
        );
    }

    static void battle(Warrior warrior1, Warrior warrior2) {

        Random random = new Random();
        int damageOne = random.nextInt(20);
        int damageTwo = random.nextInt(20);

        warrior1.receiveDamage(damageTwo);
        warrior2.receiveDamage(damageOne);
    }
}
