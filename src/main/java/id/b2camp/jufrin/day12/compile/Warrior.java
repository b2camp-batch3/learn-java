package id.b2camp.jufrin.day12.compile;
class Warrior {
    private String name;
    private int strength;

    public Warrior(String name, int strength) {
        this.name = name;
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public int getStrength() {
        return strength;
    }

    public void receiveDamage(int damage) {
        strength -= damage;
    }

    public boolean isDead() {
        return strength <= 0;
    }
}
