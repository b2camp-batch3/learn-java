package id.b2camp.jufrin.day12.compile;

import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FunctionStyle {
    public static void main(String[] args) {
        List<Warrior> hero = getWarriors();

        Predicate<Warrior> isAlive = warrior -> warrior.getStrength() > 0;

        while (hero.stream().anyMatch(isAlive)) {
            List<Warrior> aliveWarriors = hero.stream()
                .filter(isAlive)
                .collect(Collectors.toList());

            if (aliveWarriors.size() < 2) {
                break;
            }

            Random random = new Random();
            int index1 = random.nextInt(aliveWarriors.size());
            int index2 = random.nextInt(aliveWarriors.size());

            if (index1 != index2) {
                Warrior warrior1 = aliveWarriors.get(index1);
                Warrior warrior2 = aliveWarriors.get(index2);

                battle(warrior1, warrior2);
            }
        }

        List<Warrior> aliveWarriors = hero.stream()
            .filter(isAlive)
            .collect(Collectors.toList());

        if (aliveWarriors.isEmpty()) {
            System.out.println("Pertempuran selesai! Tidak ada pemenang.");
        } else {
            System.out.println("Pertempuran selesai! " + aliveWarriors.get(0).getName() + " adalah pemenangnya!");
        }
    }

    static List<Warrior> getWarriors() {
        return List.of(
            new Warrior("Shahrul", 100),
            new Warrior("Andi", 120),
            new Warrior("Joko", 80),
            new Warrior("Jufrin", 90)
        );
    }

    static void battle(Warrior warrior1, Warrior warrior2) {
        Random random = new Random();
        int damage1 = random.nextInt(20);
        int damage2 = random.nextInt(20);

        warrior1.receiveDamage(damage2);
        warrior2.receiveDamage(damage1);
    }
}