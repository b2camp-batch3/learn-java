package id.b2camp.andis.day1;

import java.math.BigDecimal;
import java.time.LocalDate;

public class HelloAndis {

    public static void main(String[] args) {

        //primitif
        int angka = 2;
        boolean benar = true;
        char huruf = 'b';
        byte a= 100;
        short b= 999;
        float f1= 234.5f;

        System.out.println(angka);
        System.out.println(benar);
        System.out.println(huruf);
        System.out.println(a);
        System.out.println(b);
        System.out.println(f1);

        //bukan primitif
        LocalDate tomorrow = LocalDate.now();
        Boolean bohong = false;
        BigDecimal zero = BigDecimal.ZERO;
        Integer iniInteger = 15;
        Double sebelas = Double.valueOf(11);

        System.out.println(tomorrow);
        System.out.println(bohong);
        System.out.println(zero);
        System.out.println(iniInteger);
        System.out.println(sebelas);

    }
}
