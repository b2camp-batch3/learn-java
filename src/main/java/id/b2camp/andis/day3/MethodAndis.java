package id.b2camp.andis.day3;

import javax.print.DocFlavor;

public class MethodAndis {
    public static void main(String[] args) {

        String nameSaya = "Andis Ramadi Putra";
        System.out.println("This is my name" + nameSaya);
        System.out.println("This is my name " + nameSaya);
        System.out.println("---");
        System.out.println("Uppercase: " + nameSaya.toUpperCase());
        System.out.println("Lowercase: " + nameSaya.toLowerCase());
        System.out.println("First char : " + nameSaya.charAt(0));
        System.out.println("Last char : " + nameSaya.charAt(17));
        System.out.println("Substring : " + nameSaya.substring(12,18));
}
}