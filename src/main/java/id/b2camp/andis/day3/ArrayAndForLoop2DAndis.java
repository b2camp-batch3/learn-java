package id.b2camp.andis.day3;

public class ArrayAndForLoop2DAndis {
    public static void main(String[] args) {

        String[][] string2d = new String[][]{{"andis ", "ramadi ", "putra "}, {"this ", "is funny"}};
        for (String[] string1d : string2d) {
            for (String s : string1d) {
                System.out.print(s);
            }
            System.out.println();
            System.out.println("------");
        }
        int[][] Card = {{20, 15, 7},
                         {30, 10, 8},
                         {13, 47, 7},
        };
        int[][] Card2 = new int[3][3];
        Card2[0][0] = 20;
        Card2[0][1] = 15;
        Card2[0][2] = 7;
        Card2[1][0] = 30;
        Card2[1][1] = 10;
        Card2[1][2] = 8;
        Card2[2][0] = 13;
        Card2[2][1] = 47;
        Card2[2][1] = 7;

        System.out.println(Card[0][0]);
        System.out.println("-------");
        for (int i= 0; i < 3; i++) {
            System.out.println(Card[i][i]);
        }


    }
}
