package id.b2camp.andis.day3;

public class WhileAndDoWhile {
    public static void main(String[] args) {
        // do while akan berputar jika itu true
        //while hanya berjalan sekali

        //do while
        int a = 0;
        System.out.println("dijalankan program");
        while (true) {         //<< jika ingin berhenti ubah true menjadi false
            System.out.println("while loop ke-" + a);
            a++;

            //while
            boolean kondisi = true;
            System.out.println("dijalankan program");
            while (kondisi) {
                System.out.println("while loop ke-" + a);

                if (a == 10) {
                    kondisi = false;
                }
                a++;
                System.out.println("program selesai");

            }

        }

    }
}

