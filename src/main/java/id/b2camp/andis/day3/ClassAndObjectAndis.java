package id.b2camp.andis.day3;

public class ClassAndObjectAndis {

    }
    class Laptop {

        String pemilik;
        String merk;
        double ukuranLayar;

        String hidupkanLaptop() {
            return "Hidupkan Laptop";
        }
        String matikanLaptop() {
            return "Matikan Laptop";
        }
            public static void main (String args[]){
            Laptop laptopAndis = new Laptop();
            laptopAndis.pemilik ="Andis";
            laptopAndis.merk ="Asus";
            laptopAndis.ukuranLayar =15.6;

            System.out.println(laptopAndis.pemilik);
            System.out.println(laptopAndis.merk);
            System.out.println(laptopAndis.ukuranLayar);

            System.out.println(laptopAndis.hidupkanLaptop());
            System.out.println(laptopAndis.matikanLaptop());

            System.out.println("-------");

                //object terpisah
            Laptop laptopJaka = new Laptop();
            Laptop laptopAndi = new Laptop();
            Laptop laptopDina = new Laptop();

            laptopAndis.pemilik ="Andis";
            laptopAndi.pemilik ="Andi";
            laptopDina.pemilik ="Dina";

            System.out.println("Pemilik Laptop: "+laptopAndis.pemilik);
            System.out.println("Pemilik Laptop: "+laptopAndi.pemilik);
            System.out.println("Pemilik Laptop: "+laptopDina.pemilik);

            }
        }