package id.b2camp.andis.day3;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Scanner;
public class ScannerAndis {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("---BIODATA---");
        System.out.println("Nama anda: ");
        String nama = input.next();
        System.out.println("Umur anda: ");
        int umur = input.nextInt();
        System.out.println("Alamat anda: ");
        String alamat = input.next();

        System.out.println("----------------");

        System.out.println("Nama = " + nama);
        System.out.println("Umur = " + umur);
        System.out.println("Alamat = " + alamat);



    }
}
