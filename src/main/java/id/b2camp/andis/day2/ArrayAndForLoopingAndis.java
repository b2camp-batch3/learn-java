package id.b2camp.andis.day2;

import java.util.Arrays;

public class ArrayAndForLoopingAndis {
    public static void main(String[] args) {

        String[] roko = {"Magnum", "Signature", "Garpit", "Dunhill"};
        System.out.println (roko [0]);

        System.out.println("--------");

        for (int i = 0; i < roko.length; i++) {
            System.out.println(roko[i]);

            String[] names = {"Magnum", "Signature", "Garpit", "Dunhill"};
            System.out.println(Arrays.toString(names));
            System.out.println(names.length);
            System.out.println(names[3]);
        }
        // mengganti element array
        roko[2] = "Mild";
        System.out.println(roko[2]);

        System.out.println("--------");

        double[] myList = {10, 11, 12, 13};
        for (int i = 0; i < myList.length; i++) {
            System.out.println(myList[i] + " ");
        }

    }
    }