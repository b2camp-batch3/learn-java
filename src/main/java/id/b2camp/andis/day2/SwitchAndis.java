package id.b2camp.andis.day2;

import java.util.Scanner;

public class SwitchAndis {

    public static void main(String[] args) {
        Scanner masuk = new Scanner(System.in);
        int pil;
        System.out.println("Menu Makanan");
        System.out.println("1. Bakso\n2. Mie Ayam");
        System.out.println("Pilih : ");
        pil = masuk.nextInt();
        switch (pil){
            case 1:
                System.out.println("Bakso (7k/porsi");
                break;
            case 2:
                System.out.println("Mie Ayam (8k/porsi");
                break;
            default:
                System.out.println("Pilihan Salah");
                break;
        }







    }


}
