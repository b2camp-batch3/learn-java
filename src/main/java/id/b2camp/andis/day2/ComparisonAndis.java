package id.b2camp.andis.day2;

public class ComparisonAndis {

    public static void main(String[] args) {

        //symbol
        // <
        // <==
        // >==
        // >
        // ==
        // !=
        // ||
        // &&

        //number
        int korekApi = 30;
        int hargaRoko = 20;
        System.out.println(korekApi < hargaRoko);
        System.out.println(korekApi <= hargaRoko);
        System.out.println(korekApi >= hargaRoko);
        System.out.println(korekApi > hargaRoko);
        System.out.println(korekApi == hargaRoko);
        System.out.println(korekApi != hargaRoko || korekApi == hargaRoko);

        System.out.println("------");

        //boolean
        boolean caster= true;
        boolean melee= false;
        boolean range= true;
        System.out.println(caster && melee);
        System.out.println(caster && range);
        System.out.println(melee || range && caster && range );
        System.out.println((range && melee) || (caster || range));
        System.out.println((caster && melee) && (range || melee));








    }
}
