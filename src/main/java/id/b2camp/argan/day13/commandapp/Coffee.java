package id.b2camp.argan.day13.commandapp;

public class Coffee {

    private String name;
    private double harga;


    public Coffee(String name, double harga) {
        this.name = name;
        this.harga = harga;
    }

    public Coffee() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }


    public String toString() {
        return "Coffee{" +
                "name='" + name + '\'' +
                ", harga=" + harga +
                '}';
    }
}
