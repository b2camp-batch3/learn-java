package id.b2camp.argan.day13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FunctionalProgram {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("argan", "fahri", "jerry")
                .stream()
                .filter(s -> !s.equals("argan"))
                .collect(Collectors.toList());
        System.out.println(names);

    }

    static class Mobil {
        private String name;
    }
}
