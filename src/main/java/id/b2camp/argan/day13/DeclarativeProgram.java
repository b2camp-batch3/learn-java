package id.b2camp.argan.day13;

import java.util.Arrays;
import java.util.stream.Stream;

public class DeclarativeProgram {

    public static void main(String[] args) {
        Stream<Integer> numbers = Arrays.stream(new Integer[] {1, 2, 3, 4});
        int sum = numbers.reduce(1, Integer::sum);
        System.out.println(sum); // outputs 10
    }
}
