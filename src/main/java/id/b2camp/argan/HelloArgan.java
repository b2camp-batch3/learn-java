package id.b2camp.argan;

import java.math.BigDecimal;
import java.time.LocalDate;

public class HelloArgan {
    public static void main(String[] args) {

        // tipe data primitif

        int satu = 1;
        float duaNol = 2.0f;
        double duaSatu = 2.1;
        boolean benar = true;
        char isA = 'a';
        byte iniByte = 100;
        short iniShort = 120;
        long iniLong = 1000L;

        System.out.println(satu);
        System.out.println(duaNol);
        System.out.println(duaSatu);
        System.out.println(benar);
        System.out.println(isA);
        System.out.println(iniByte);
        System.out.println(iniShort);
        System.out.println(iniLong);

        // tipe data bukan primitif

        LocalDate now = LocalDate.now();
        Boolean salah = false;
        BigDecimal zero = BigDecimal.ZERO;
        Integer iniInteger = 10;
        Long duaBelasDua = Long.valueOf(12);
        Double sebelas = Double.valueOf(11);


        System.out.println(now);
        System.out.println(salah);
        System.out.println(zero);
        System.out.println(iniInteger);
        System.out.println(duaBelasDua);
        System.out.println(sebelas);



    }
}
