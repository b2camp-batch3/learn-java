package id.b2camp.gita.day11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListGitaApp {
    public static void main(String[] args) {

        List<String> fruitList = new ArrayList<>();
        fruitList.add("Strawberry");
        fruitList.add("Anggur");
        fruitList.add("Apel");
        fruitList.add("Sirsak");
        fruitList.add("Semangka");
        fruitList.add("Melon");

        System.out.println(fruitList);
        System.out.println(fruitList.size());
        fruitList.remove("Sirsak");
        fruitList.remove("Melon");
        System.out.println(Arrays.toString(fruitList.toArray()));
        System.out.println(fruitList.get(3));
        System.out.println(fruitList.get(0));

        System.out.println("---");

        List<String> names = new ArrayList<>();
        names.add("Siti");
        names.add("Maryam");
        names.add("Mara");
        names.add("Ainun");

        System.out.println(names);
        System.out.println(names.get(1));
        System.out.println(names.size());
    }
}
