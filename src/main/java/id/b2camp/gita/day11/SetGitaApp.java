package id.b2camp.gita.day11;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetGitaApp {
    public static void main(String[] args) {

        Set<String> names = new HashSet<>(List.of("Lala", "Lala", "Rara", "Lani", "Ara", "Lala", "Rara"));
        System.out.println(names);
        System.out.println(names.size());

        System.out.println("---");

        Set<Integer> numbers = new HashSet<>(List.of(1, 1, 3, 5, 5, 5, 5, 7, 9, 11, 11, 13));
        System.out.println(numbers);
        System.out.println(numbers.size());
    }
}
