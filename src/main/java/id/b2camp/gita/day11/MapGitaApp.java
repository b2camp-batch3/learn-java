package id.b2camp.gita.day11;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapGitaApp {
    public static void main(String[] args) {

        Map<Integer, String> seaAnimals = new HashMap<>();
        seaAnimals.put(1, "Hiu");
        seaAnimals.put(2, "Paus");
        seaAnimals.put(3, "Gurita");
        seaAnimals.put(4, "Cumi - cumi");
        seaAnimals.put(5, "Kepiting");
        seaAnimals.put(6, "Lobster");
        seaAnimals.put(7, "Kerang");
        seaAnimals.put(8, "Bintang laut");
        seaAnimals.put(9, "Penyu");
        seaAnimals.put(10, "Plankton");

        System.out.println(seaAnimals);
        seaAnimals.remove(1);
        System.out.println(seaAnimals);
        System.out.println(seaAnimals.get(8));

        System.out.println("---");

        HashMap<String, List<String>> stuff = new HashMap<>();
        ArrayList<String> elektronik = new ArrayList<>(List.of("Smartphone", "Laptop", "Televisi", "Kulkas"));
        stuff.put("Elektronik", elektronik);

        System.out.println(stuff);
        System.out.println(stuff.get("Elektronik"));

        System.out.println("--");

        stuff.put("Non elektronik", List.of("Meja", "Buku", "Pulpen", "Baju"));
        System.out.println(stuff);
        System.out.println(stuff.get("Non elektronik"));
    }
}
