package id.b2camp.gita.day11;

import java.util.LinkedList;
import java.util.List;

public class LinkedListGitaApp {
    public static void main(String[] args) {

        LinkedList<Integer> numbers = new LinkedList<>();
        int list = 0;
        for (int i = 0; i < 1000; i++) {
            numbers.add(list++);
        }
        System.out.println(numbers);
        System.out.println(numbers.get(20));

        System.out.println("---");

        List<String> animals = new LinkedList<>();
        animals.add("Kucing");
        animals.add("Ayam");
        animals.add("Singa");
        animals.add("Harimau");
        animals.add("Tikus");
        animals.add("Ikan");
        animals.add("Bebek");
        animals.add("Burung");

        LinkedList<String> hewan = new LinkedList<>();
        hewan.addAll(animals);
        System.out.println(hewan);
        hewan.remove("Ikan");
        System.out.println(hewan);
        System.out.println(hewan.size());
        System.out.println(hewan.get(3));
    }
}
