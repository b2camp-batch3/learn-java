package id.b2camp.gita.day1;

import java.time.LocalDate;

public class HelloGita {

    public static void main(String[] args) {

        boolean benar = true;
        float flo = 2.7f;
        int age = 18;
        double koma = 0.5;
        char a = 'A';

        String[] nama = {"Lala", "Leo", "Lucy"};
        Integer angka = 1;
        Boolean salah = false;
        LocalDate now = LocalDate.now();
        Long isLong = 59L;

        System.out.println(benar);
        System.out.println(flo);
        System.out.println(age);
        System.out.println(koma);
        System.out.println(a);

        System.out.println("---");

        System.out.println(nama[2]);
        System.out.println(angka);
        System.out.println(salah);
        System.out.println(now);
        System.out.println(isLong);
    }
}