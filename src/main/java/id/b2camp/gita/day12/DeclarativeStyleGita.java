package id.b2camp.gita.day12;

import java.util.List;

public class DeclarativeStyleGita {
    public static void main(String[] args) {

        List<String> names = List.of("Angga", "Anggi", "Rara", "Rere");

        if (names.contains("Ani")) {
            System.out.println("Nama Ani terdaftar");
        } else {
            System.out.println("Nama Ani tidak terdaftar");
        }

        long count = names.stream().count();
        System.out.println(count);
    }
}