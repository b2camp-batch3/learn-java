package id.b2camp.gita.day12;

import java.util.List;

public class ImperativeStyleGita {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -2, -5, -7);

        System.out.println("Bilangan Genap: ");
        for (int number : numbers) {
            if (number % 2 == 0) {
                System.out.print(number + ", ");
            }
        }

        System.out.println("\n");

        System.out.println("Bilangan Ganjil: ");
        for (int number : numbers) {
            if (number % 2 != 0) {
                System.out.print(number + ", ");
            }
        }

        System.out.println("\n");

        System.out.println("Bilangan positif : ");
        for (int number : numbers) {
            if (number > 0) {
                System.out.print(number + ", ");
            }
        }

        System.out.println("\n");

        System.out.println("Bilangan Negatif: ");
        for (int number : numbers) {
            if (number < 0) {
                System.out.print(number + ", ");
            }
        }
    }
}