package id.b2camp.gita.day12.stream;

import id.b2camp.gita.day12.stream.Animals;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class StreamApp {
    public static void main(String[] args) {

        List<Animals> hewan = animals();

        List<Animals> mamalia = hewan.stream()
                .filter(animals -> animals.getAnimalClass().equalsIgnoreCase("Mamalia"))
                .collect(Collectors.toList());
        mamalia.forEach(System.out::println);

        BigDecimal sumMamalia = hewan.stream()
                .filter(animals -> animals.getAnimalClass().equalsIgnoreCase("Mamalia"))
                .map(Animals::getPopulation)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println("Populasi hewan berjenis mamalia adalah = " + sumMamalia);

        System.out.println("---");

        List<Animals> mamaliaHerbivora = hewan.stream()
                .filter(animals -> animals.getFoodType().equalsIgnoreCase("Herbivora"))
                .filter(animals -> animals.getAnimalClass().equalsIgnoreCase("Mamalia"))
                .collect(Collectors.toList());
        mamaliaHerbivora.forEach(System.out::println);

        System.out.println("---");

        List<Animals> legs = hewan.stream()
                .filter(animals -> animals.getAnimalLegs() == 2)
                .collect(Collectors.toList());
        legs.forEach(System.out::println);

        System.out.println("---");

        List<Animals> amphibi = hewan.stream()
                .filter(animals -> animals.getAnimalClass().equalsIgnoreCase("Amphibi"))
                .collect(Collectors.toList());
        amphibi.forEach(System.out::println);
    }
    static List<Animals> animals() {
        return List.of(
                new Animals("Singa","Mamalia", "Karnivora", 4, BigDecimal.valueOf(250_000_000)),
                new Animals("Ayam","Aves", "Omnivora", 2, BigDecimal.valueOf(500_000_000)),
                new Animals("Kambing","Mamalia", "Herbivora", 4, BigDecimal.valueOf(400_000_000)),
                new Animals("Katak","Amphibi", "Karnivora", 4, BigDecimal.valueOf(600_000_000)),
                new Animals("Buaya","Reptil", "Karnivora", 4, BigDecimal.valueOf(100_000_000)),
                new Animals("Elang","Aves", "Karnivora", 2, BigDecimal.valueOf(70_000_000)),
                new Animals("Salamander","Amphibi", "Herbivora", 4, BigDecimal.valueOf(2_000_000)),
                new Animals("Merpati","Aves", "Herbivora", 2, BigDecimal.valueOf(300_000_000)),
                new Animals("Sapi","Mamalia", "Herbivora", 4, BigDecimal.valueOf(450_000_000))
        );
    }
}