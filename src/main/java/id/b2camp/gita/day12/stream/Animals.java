package id.b2camp.gita.day12.stream;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Animals {
    private String animalName;
    private String animalClass;
    private String foodType;
    private int animalLegs;
    private BigDecimal population;
}