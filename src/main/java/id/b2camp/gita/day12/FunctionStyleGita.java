package id.b2camp.gita.day12;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FunctionStyleGita {
    public static void main(String[] args) {

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        int sum = numbers.stream()
                .reduce(0, (x, y) -> x + y);

        System.out.println("Jumlah seluruh angka : " + sum);

        List<Integer> squaredNumbers = numbers.stream()
                .map(number -> number * number)
                .collect(Collectors.toList());

        System.out.println("Angka kuadrat : " + squaredNumbers);

        numbersInfo();

    }
    static void numbersInfo(){
        System.out.println("List terdiri dari 10 bilangan ");
    }
}