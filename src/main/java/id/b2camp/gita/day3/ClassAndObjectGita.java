package id.b2camp.gita.day3;

public class ClassAndObjectGita {
    public static void main(String[] args) {

        ClassAndObjectGita.Sekolah murid = new ClassAndObjectGita.Sekolah("Leo", 433, "11 IPA 1", "SMAN 3 Depok");
        System.out.println(murid.infoSiswa());

        ClassAndObjectGita.Sekolah murid1 = new ClassAndObjectGita.Sekolah("Lala", 234,"12 IPS 3", "SMAN 1 Jakarta");
        System.out.println(murid1.infoSiswa());

        ClassAndObjectGita.Sekolah murid2 = new ClassAndObjectGita.Sekolah("Lili", 345,"10 IPA 2", "SMAN 13 Tangerang");
        System.out.println(murid2.infoSiswa());

        ClassAndObjectGita.Sekolah murid3 = new ClassAndObjectGita.Sekolah("Lela", 892,"11 TKJ 3", "SMKN 1 Tegal");
        System.out.println(murid3.infoSiswa());

    }

    public static class Sekolah {

        String namaSiswa;
        int nomorIdSiswa;
        String kelas;
        String namaSekolah;


        Sekolah(String namaSiswa, int noIdSiswa, String kelas, String namaSekolah) {
            this.namaSiswa = namaSiswa;
            this.nomorIdSiswa = noIdSiswa;
            this.kelas = kelas;
            this.namaSekolah = namaSekolah;

        }

        String infoSiswa() {
            return "Nama Siswa Adalah " + namaSiswa + ". Nomor ID Yaitu " + nomorIdSiswa + ". Dari kelas " + kelas +". Asal sekolah " + namaSekolah +".";
        }
    }

}
