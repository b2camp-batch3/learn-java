package id.b2camp.gita.day3;

import java.util.Arrays;

public class ArrayAndForLoop2DGita {
    public static void main(String[] args) {

        String[][]nama = {
                {"Lala","Lili","Lulu"},
                {"Lele","Lolo","Lela"},
                {"Leli","Lelu","Lula"}
        };

        for (int c = 0; c < nama.length; c++) {
            for (int d = 0; d < nama.length; d++)
                System.out.print(nama[c][d] + " ");
        }

        System.out.print("\n");
        System.out.println("---");

        for (int c = nama.length - 1; c >= 0; c--) {
            for (int d = nama.length - 1; d >= 0; d--)
                System.out.print(nama[c][d] + " ");
        }

        System.out.print("\n");
        System.out.println("---");

        String[][] huruf = new String[][] { {"A","B","C"},
                                            {"D","E","F"},
                                            {"G","H","I"}};
        for (String[] a : huruf) {
            for (String b : a) {
                System.out.print(b + ", ");
            }
        }
    }
}
