package id.b2camp.gita.day3;

import java.security.SecureRandom;
import java.util.Locale;

public class MethodGita {
    public static void main(String[] args) {

        String upper = message("Hello Everybody!");
        System.out.println(upper);

        String lower = message1("Hallo Semuanya!");
        System.out.println(lower);

        System.out.println("---");

        String nama = name("Nana");
        System.out.println(nama);

        int umur = age(18);
        System.out.println("Umur saya " + umur + " tahun");


    }
        static String message(String a) {
        return  a.toUpperCase();
    }
        static String message1(String b) {
        return b.toLowerCase();
        }

        static String name(String c) {
        return "Nama saya adalah " + c;
        }

        static int age(int d) {
        return d;
        }
}

