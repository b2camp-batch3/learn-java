package id.b2camp.gita.day3;

import java.util.Arrays;

public class WhileAndDoWhileGita {
    public static void main(String[] args) {

        // While

        String[] c = new String[]{"A", "B", "C", "D"};

        while (true){
            System.out.println(Arrays.toString(c));
            break;
        }
        System.out.println("---");

        int angka = 1;
        while (angka <= 5) {
            System.out.println(angka);
            angka++;
        }

        System.out.println("---");
        int angka1 = 5;
        while (angka1 >= 1) {
            System.out.println(angka1);
            angka1--;
        }
        System.out.println("---");

        // Do While

        int a = 6;

        do {
            System.out.println(a);
            a--;
        } while (a >= 2);

        System.out.println("---");

        do {
            System.out.println(a);
            a++;
        } while (a <= 5);
    }
}
