package id.b2camp.gita.day3;

import java.util.Scanner;

public class ScannerGita {
    public static void main(String[] args) {

        Scanner i = new Scanner(System.in);

        int alasAtas, alasBawah, tinggi, sisiKanan, sisiKiri;

        System.out.println("--- Menghitung luas dan keliling trapesium ---");
        System.out.println();

        System.out.print("Alas Atas = ");
        alasAtas = i.nextInt();
        System.out.print("Alas Bawah = ");
        alasBawah = i.nextInt();
        System.out.print("Tinggi = ");
        tinggi = i.nextInt();
        System.out.print("Sisi Kanan = ");
        sisiKanan = i.nextInt();
        System.out.print("Sisi Kiri = ");
        sisiKiri = i.nextInt();

        System.out.println("--------------------------------");

        double luas = 0.5 * (alasAtas + alasBawah) * tinggi;
        System.out.println("Luas trapesium adalah = " + luas);
        double keliling = alasAtas + alasBawah + sisiKanan + sisiKiri;
        System.out.println("Keliling trapesium adalah = " + keliling);

        System.out.println("--------------------------------");


    }
}
