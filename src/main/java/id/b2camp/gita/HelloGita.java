package id.b2camp.gita;

public class HelloGita {
    public static void main(String[] args) {

        // Tipe Data Primitive
        int age = 30;
        short number = 29734;
        char value = 'A';
        double decimal = 22.51;
        boolean benar = true;

        // Tipe Data Non Primitive
        Integer id = 155;
        Float flo = 8.12f;
        Byte nomor = 64;
        Boolean salah = false;
        String[] nama = {"Arsya", "Rara", "Ani", "Leo"};

        System.out.println("=========== PRIMITIVE DATA TYPE ==========");
        System.out.println(age);
        System.out.println(number);
        System.out.println(value);
        System.out.println(decimal);
        System.out.println(benar);

        System.out.println("========= NON PRIMITIVE DATA TYPE ========");
        System.out.println(id);
        System.out.println(flo);
        System.out.println(nomor);
        System.out.println(salah);
        System.out.println(nama [3]);
        System.out.println("==========================================");
    }
}
