package id.b2camp.gita.day5;

public class Hospital {
    public static void main(String[] args) {

        MedicalStaff medicalStaff = new Doctor("dr. Sara","kardiolog","Kardiologi");
        introduceDoctor(medicalStaff);

        MedicalStaff medicalStaff1 = new Nurse("Nunu", "anestesi", 2);
        introduceNurse(medicalStaff1);

        System.out.println("---");

        Doctor doktor = new Doctor("dr. Lani","bedah umum","medis pusat");
        diagnose(doktor);

        Nurse nurse = new Nurse("Lulu","pediatrik",9);
        assist(nurse);

        System.out.println("---");

        introduceDoctor(new MedicalStaff("dr. Leo","penyakit dalam"));
        diagnose(new Doctor("dr. Ana","kulit","dermatologist"));
        assist(new Nurse("Siska", "kulit",6));
    }
    static void introduceDoctor(MedicalStaff medicalStaff) {
        System.out.println("Nama saya " + medicalStaff.name + ", saya seorang dokter spesialis " + medicalStaff.specialization + ".");
    }
    static void introduceNurse(MedicalStaff medicalStaff) {
        System.out.println("Nama saya " + medicalStaff.name + ", saya seorang perawat spesialis " + medicalStaff.specialization + ".");
    }
    static  void diagnose(Doctor doctor) {
        System.out.println(doctor.name + " sedang mendiagnosis pasien di departemen " + doctor.department + ".");
    }
    static void assist(Nurse nurse) {
        System.out.println("Suster " + nurse.name + " sedang membantu dokter dengan pengalaman " + nurse.experience + " tahun.");
    }
}

class MedicalStaff {
    String name;
    String specialization;

    MedicalStaff(String name, String specialization) {
        this.name = name;
        this.specialization = specialization;
    }

}
class Doctor extends MedicalStaff {
    String department;

    Doctor(String name, String specialization, String department) {
        super(name, specialization);
        this.department = department;
    }

}
class Nurse extends MedicalStaff {
    int experience;

    Nurse(String name, String specialization, int experience) {
        super(name, specialization);
        this.experience = experience;
    }

}
