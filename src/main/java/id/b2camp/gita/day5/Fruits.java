package id.b2camp.gita.day5;

public class Fruits {
    public static void main(String[] args) {

        Fruit jeruk = new Orange("jeruk", "oren");
        jeruk.display();
        System.out.println("Saya sedang memakan buah " + jeruk.fruitName);

        System.out.println("---");

        Fruit mangga = new Mango("mangga", "hijau");
        mangga.display();
        System.out.println("Buah " + mangga.fruitName + " rasanya manis");

        System.out.println("---");

        Fruit alpukat = new Avocado("alpukat", "hijau");
        alpukat.display();
    }
}

class Fruit {
    String fruitName;
    String fruitColor;
    Fruit(String fruitName, String fruitColor) {
        this.fruitName = fruitName;
        this.fruitColor = fruitColor;
    }
    void display() {
        System.out.println("Nama buah ini adalah " + fruitName);
    }

}

class Orange extends Fruit {
    Orange (String fruitName,String fruitColor) {
        super(fruitName, fruitColor);
    }

    void display() {
        super.display();
        System.out.println( fruitName + " biasanya berwarna " + fruitColor);
    }


}

class Mango extends Fruit {
    Mango(String fruitName, String fruitColor) {
        super(fruitName, fruitColor);
    }

    void display() {
        super.display();
        System.out.println( fruitName + " biasanya berwarna " + fruitColor);
    }

}
class Avocado extends Fruit {
    Avocado(String fruitName, String fruitColor){
        super(fruitName, fruitColor);
    }


    void display() {
        super.display();
        System.out.println( fruitName + " biasanya berwarna " + fruitColor);
    }

}