package id.b2camp.gita.day6.interfaces;

public interface AnimalLegs {

    int getLegs();
}
