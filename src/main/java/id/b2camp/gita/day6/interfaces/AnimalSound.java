package id.b2camp.gita.day6.interfaces;

public interface AnimalSound {

    String getSound();
}
