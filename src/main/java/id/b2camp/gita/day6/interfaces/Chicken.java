package id.b2camp.gita.day6.interfaces;

public class Chicken implements AnimalType, AnimalSound, AnimalLegs {

    @Override
    public String name() {
        return "Ayam";
    }

    @Override
    public String getAnimalType() {
        return "unggas";
    }

    @Override
    public String getFoodType() {
        return "omnivora";
    }

    @Override
    public String getSound() {
        return "cluck cluck";
    }

    @Override
    public int getLegs() {
        return 2;
    }
}
