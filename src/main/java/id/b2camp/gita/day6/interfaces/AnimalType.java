package id.b2camp.gita.day6.interfaces;

public interface AnimalType {

    String name();
    String getFoodType();
    String getAnimalType();
}
