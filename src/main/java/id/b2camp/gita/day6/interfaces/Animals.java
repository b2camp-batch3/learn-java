package id.b2camp.gita.day6.interfaces;

public class Animals {
    public static void main(String[] args) {

        Lion lion = new Lion();
        System.out.println("Nama hewan adalah " + lion.name());
        System.out.println(lion.name() + " merupakan jenis " + lion.getAnimalType() + " dengan jenis makanan " + lion.getFoodType());
        System.out.println(lion.name() + " mengeluarkan suara " + lion.getSound());
        System.out.println(lion.name() + " merupakan hewan berkaki " + lion.getLegs());

        System.out.println("---");

        Frog frog = new Frog();
        System.out.println("Nama hewan adalah " + frog.name());
        System.out.println(frog.name() + " merupakan jenis " + frog.getAnimalType() + " dengan jenis makanan " + frog.getFoodType());
        System.out.println(frog.name() + " mengeluarkan suara " + frog.getSound());
        System.out.println(frog.name() + " merupakan hewan berkaki " + frog.getLegs());

        System.out.println("---");

        Chicken chicken = new Chicken();
        System.out.println("Nama hewan adalah " + chicken.name());
        System.out.println(chicken.name() + " merupakan jenis " + chicken.getAnimalType() + " dengan jenis makanan " + chicken.getFoodType());
        System.out.println(chicken.name() + " mengeluarkan suara " + chicken.getSound());
        System.out.println(chicken.name() + " merupakan hewan berkaki " + chicken.getLegs());
    }
}
