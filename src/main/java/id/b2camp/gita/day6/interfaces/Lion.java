package id.b2camp.gita.day6.interfaces;

public class Lion implements AnimalType,AnimalSound , AnimalLegs {

    @Override
    public String name() {
        return "Singa";
    }

    @Override
    public String getAnimalType() {
        return "mamalia";
    }

    @Override
    public String getFoodType() {
        return "karnivora";
    }

    @Override
    public String getSound() {
        return "roarrr";
    }

    @Override
    public int getLegs() {
        return 4;
    }
}

