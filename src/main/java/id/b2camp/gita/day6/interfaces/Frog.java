package id.b2camp.gita.day6.interfaces;

public class Frog implements AnimalType, AnimalSound, AnimalLegs {

    @Override
    public String name() {
        return "Katak";
    }

    @Override
    public String getAnimalType() {
        return "amfibi";
    }

    @Override
    public String getFoodType() {
        return "karnivora";
    }

    @Override
    public String getSound() {
        return "croak croak";
    }

    @Override
    public int getLegs() {
        return 4;
    }
}
