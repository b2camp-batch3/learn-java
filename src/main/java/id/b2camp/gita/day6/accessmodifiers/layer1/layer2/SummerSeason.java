package id.b2camp.gita.day6.accessmodifiers.layer1.layer2;

public class SummerSeason {
    public static void main(String[] args) {
        Summer summer = new Summer("Musim panas", 35, "mines curah hujan");
        System.out.println(summer.name + " di Australia mencapai suhu " + summer.temperature + " derajat celcius.");
        System.out.println("Dengan kondisi " + summer.condition + ".");
    }
}
