package id.b2camp.gita.day6.accessmodifiers.layer1;

public class Spring {

    private String name;
    private int temperature;
    private String condition;

    public Spring(String name, int temperature, String condition) {
        this.name = name;
        this.temperature = temperature;
        this.condition = condition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
