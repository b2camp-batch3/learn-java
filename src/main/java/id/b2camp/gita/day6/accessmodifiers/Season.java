package id.b2camp.gita.day6.accessmodifiers;

import id.b2camp.gita.day6.accessmodifiers.layer1.Spring;
import id.b2camp.gita.day6.accessmodifiers.layer1.layer2.Summer;
import id.b2camp.gita.day6.accessmodifiers.layer1.layer2.layer3.Autumn;

public class Season {
    public static void main(String[] args) {
        Spring spring = new Spring("Musim semi", 24,"salju mencair");
        System.out.println(spring.getName() + " di Jepang mencapai suhu " + spring.getTemperature() + " derajat celcius.");
        System.out.println("Dengan kondisi " + spring.getCondition() + ".");

        System.out.println("---");

        Autumn autumn = new Autumn("Musim gugur", 27, "daun berguguran");
        System.out.println(autumn.name + " di Canada mencapai suhu " + autumn.temperature + " derajat celcius.");
        System.out.println("Dengan kondisi " + autumn.condition + ".");
    }
}
