package id.b2camp.gita.day6.accessmodifiers.layer1.layer2.layer3;

public class Winter {
    public static void main(String[] args) {
        Winter winter = new Winter("Musim dingin", -2,"bersalju");
        System.out.println(winter.name + " di Siberia mencapai suhu " + winter.temperature + " derajat celcius.");
        System.out.println("Dengan kondisi " + winter.condition + ".");
    }
    private String name;
    private int temperature;
    private String condition;

    public Winter(String name, int temperature, String condition) {
        this.name = name;
        this.temperature = temperature;
        this.condition = condition;
    }
}
