package id.b2camp.gita.day6.accessmodifiers.layer1.layer2;

public class Summer {

    protected String name;
    protected int temperature;
    protected String condition;

    public Summer(String name, int temperature, String condition) {
        this.name = name;
        this.temperature = temperature;
        this.condition = condition;
    }
}
