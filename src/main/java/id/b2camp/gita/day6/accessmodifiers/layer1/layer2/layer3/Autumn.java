package id.b2camp.gita.day6.accessmodifiers.layer1.layer2.layer3;

public class Autumn {
    public String name;
    public int temperature;
    public String condition;

    public Autumn(String name, int temperature, String condition) {
        this.name = name;
        this.temperature = temperature;
        this.condition = condition;
    }
}
