package id.b2camp.gita.day6.abstractclasses;

public class ShapeView {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.setCountAround(5);
        rectangle.shapeName();
        System.out.println("Luas persegi adalah " + rectangle.getCalculateArea());
        System.out.println("Keliling persegi adalah " + rectangle.getCountAround());

        System.out.println("---");

        Triangle triangle = new Triangle(5,7);
        triangle.shapeName();
        System.out.println("Luas segitiga sama sisi adalah " + triangle.getCalculateArea());
        System.out.println("Keliling segitiga sama sisi adalah " + triangle.getCountAround());

        System.out.println("---");

        Circle circle = new Circle();
        circle.setCalculateArea(7);
        circle.shapeName();
        System.out.println("Luas lingkaran adalah " + circle.getCalculateArea());
        System.out.println("Keliling lingkaran adalah " + circle.getCountAround());
    }
}
