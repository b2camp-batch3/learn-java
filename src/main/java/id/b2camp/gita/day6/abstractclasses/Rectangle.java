package id.b2camp.gita.day6.abstractclasses;

public class Rectangle extends Shape {

    private double s;
    @Override
    public void shapeName() {
        System.out.println("Ini adalah persegi");
    }

    @Override
    public void setCalculateArea(double s) {
        this.s = s;
    }
    @Override
    public double getCalculateArea() {
        return s * s;
    }

    @Override
    public void setCountAround(double s) {
        this.s = s;
    }

    @Override
    public double getCountAround() {
        return s * 4;
    }
}
