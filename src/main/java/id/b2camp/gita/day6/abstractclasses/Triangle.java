package id.b2camp.gita.day6.abstractclasses;

public class Triangle extends Shape {

    private double a;
    private double t;

    public Triangle(double a, double t) {
        this.a = a;
        this.t = t;
    }
    @Override
    public void shapeName() {
        System.out.println("Ini adalah segitiga sama sisi");
    }

    @Override
    public double getCalculateArea() {
        return 0.5 * a * t;
    }

    @Override
    public double getCountAround() {
        return a * 3;
    }
}
