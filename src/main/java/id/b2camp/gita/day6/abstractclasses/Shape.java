package id.b2camp.gita.day6.abstractclasses;

public abstract class Shape {
    private double calculateArea;
    private double countAround;

    public abstract void shapeName();

    public double getCalculateArea() {
        return calculateArea;
    }

    public void setCalculateArea(double calculateArea) {
        this.calculateArea = calculateArea;
    }

    public double getCountAround() {
        return countAround;
    }

    public void setCountAround(double countAround) {
        this.countAround = countAround;
    }
}
