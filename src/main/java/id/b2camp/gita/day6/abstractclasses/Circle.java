package id.b2camp.gita.day6.abstractclasses;

public class Circle extends Shape {
    private double r;

    @Override
    public void shapeName() {
        System.out.println("Ini adalah lingkaran");
    }

    @Override
    public void setCalculateArea(double r) {
        this.r = r;
    }

    @Override
    public double getCalculateArea() {
        return 3.14 * r * r;
    }


    @Override
    public double getCountAround() {
        return 2 * 3.14 * r;
    }
}
