package id.b2camp.gita.day7.enums;

public enum Calculator {
    MULTIPLICATION, DIVISION,  ADDITION, SUBTRACTION;
}
