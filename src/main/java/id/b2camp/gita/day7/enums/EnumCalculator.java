package id.b2camp.gita.day7.enums;

public class EnumCalculator {
    public static void main(String[] args) {
        Calculator perkalian = Calculator.MULTIPLICATION;
        Calculator Pembagian = Calculator.DIVISION;
        Calculator penjumlahan = Calculator.ADDITION;
        Calculator pengurangan = Calculator.SUBTRACTION;

        ProcessCalculator proses = new ProcessCalculator(10, 5, perkalian);
        System.out.println("Anda memilih " + proses.getStatus());
        System.out.println("Maka hasilnya " + (proses.getNumbers1() * proses.getNumbers2()));

        System.out.println("---");

        ProcessCalculator proses1 = new ProcessCalculator(20, 5, Pembagian);
        System.out.println("Anda memilih " + proses1.getStatus());
        System.out.println("Maka hasilnya " + (proses1.getNumbers1() / proses1.getNumbers2()));

        System.out.println("---");

        ProcessCalculator proses2 = new ProcessCalculator(12, 8, penjumlahan);
        System.out.println("Anda memilih " + proses2.getStatus());
        System.out.println("Maka hasilnya " + (proses2.getNumbers1() + proses2.getNumbers2()));

        System.out.println("---");

        ProcessCalculator proses3 = new ProcessCalculator(49, 7, pengurangan);
        System.out.println("Anda memilih " + proses3.getStatus());
        System.out.println("Maka hasilnya " + (proses3.getNumbers1() - proses3.getNumbers2()));
    }
}
