package id.b2camp.gita.day7.enums;

public class ProcessCalculator {
    private int numbers1;
    private int numbers2;
    private Calculator status;

    public ProcessCalculator(int numbers1, int numbers2, Calculator status) {
        this.numbers1 = numbers1;
        this.numbers2 = numbers2;
        this.status = status;
    }

    public int getNumbers1() {
        return numbers1;
    }

    public void setNumbers1(int numbers1) {
        this.numbers1 = numbers1;
    }

    public int getNumbers2() {
        return numbers2;
    }

    public void setNumbers2(int numbers2) {
        this.numbers2 = numbers2;
    }

    public Calculator getStatus() {
        return status;
    }

    public void setStatus(Calculator status) {
        this.status = status;
    }

}