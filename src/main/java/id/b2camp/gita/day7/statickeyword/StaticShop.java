package id.b2camp.gita.day7.statickeyword;

public class StaticShop {
    public static void main(String[] args) {

        Shop.shopName("CHANEL Clothes");
        System.out.println("---");

        String brand = Products.BRAND;
        boolean original = Products.IS_ORIGINAL;
        String category = Products.CATEGORY;
        System.out.println(brand);
        System.out.println(original);
        System.out.println(category);

        System.out.println("---");

        Employee employee = new Employee();
        employee.setEmployeeName("Siti");

        Employee.EmployeeSection employeeSection = new Employee.EmployeeSection();
        employeeSection.setSection("kasir");

        System.out.println(employee.getEmployeeName() + " sedang melayani pembeli di bagian " + employeeSection.getSection());
    }
}
