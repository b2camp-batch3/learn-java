package id.b2camp.gita.day7.finalkeyword;

public class Cutlery {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class Fork extends Cutlery {

    final String function(String fungsi) {
            return fungsi;
        }
}
final class Spoon extends Fork {

}
final class Knife extends Fork {

}

