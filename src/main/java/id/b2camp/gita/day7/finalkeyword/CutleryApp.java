package id.b2camp.gita.day7.finalkeyword;

public class CutleryApp {
    public static void main(String[] args) {
        Spoon spoon = new Spoon();
        spoon.setName("Sendok");
        System.out.println(spoon.getName() + " berfungsi untuk " + spoon.function("mengambil makanan"));

        Fork fork = new Fork();
        fork.setName("Garpu");
        System.out.println(fork.getName() + " berfungsi untuk " + fork.function("mengambil dan menancapkan makanan"));

        Knife knife = new Knife();
        knife.setName("Pisau");
        System.out.println(knife.getName() + " berfungsi untuk " + knife.function("memotong makanan"));
    }
}
