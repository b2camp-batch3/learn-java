package id.b2camp.gita.day7.anonymousclass;

public class AnonymousPerson {
    public static void main(String[] args) {

        Person person =new Person() {
            @Override
            public String name() {
                return "Nana";
            }
        };
        System.out.println("Nama saya adalah " + person.name());

        PersonAttribute personAttribute = new PersonAttribute() {
            @Override
            public int age() {
                return 18;
            }

            @Override
            public void address() {
                System.out.println("Alamat saya di Jl. Melati no. 00 RT 01/ RW 01");
            }

            @Override
            public String gender() {
                return "perempuan";
            }
        };
        System.out.println("Usia saya " + personAttribute.age() + " tahun");
        personAttribute.address();
        System.out.println("Jenis kelamin saya adalah " + personAttribute.gender());
    }
}
