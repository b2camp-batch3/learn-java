package id.b2camp.gita.day7.anonymousclass;

public abstract class Person {
    public abstract String name();
}
