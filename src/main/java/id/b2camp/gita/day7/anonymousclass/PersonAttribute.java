package id.b2camp.gita.day7.anonymousclass;

public interface PersonAttribute {
    int age();
    void address();
    String gender();
}
