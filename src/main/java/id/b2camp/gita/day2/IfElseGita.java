package id.b2camp.gita.day2;

public class IfElseGita {
    public static void main(String[] args) {


        var nilaiSiswa = 90;
        var nilaiKehadiran = 75;

        if (nilaiSiswa >= 75 && nilaiKehadiran >= 70) {
            System.out.println("Selamat Anda lulus!");
        }else {
            System.out.println("Silahkan ulangi tahun depan");
        }

        System.out.println("---");

        if (nilaiSiswa >= 90) {
            System.out.println("Nilai Anda A");
        } else if (nilaiSiswa >= 80) {
            System.out.println("Nilai Anda B");
        } else if (nilaiSiswa >= 70) {
            System.out.println("Nilai Anda C");
        } else {
            System.out.println("Nilai Anda D");
        }

    }
}

