package id.b2camp.gita.day2;

public class ComparisonGita {
    public static void main(String[] args) {
        int value1 = 10;
        int value2 = 7;

        System.out.println(value1 > value2);
        System.out.println(value1 < value2);
        System.out.println(value1 >= value2);
        System.out.println(value1 <= value2);
        System.out.println(value1 == value2);
        System.out.println(value1 != value2);
        System.out.println(value1 > value2 && value1 == value2);
        System.out.println(value1 != value2 || value1 <= value2);

        System.out.println("---");

        boolean active = true;
        boolean notActive = false;
        boolean isValid = true;

        System.out.println(active || notActive);
        System.out.println(active && notActive);
        System.out.println(isValid && notActive);
        System.out.println(active && notActive || isValid);
        System.out.println(isValid || notActive && active);

        System.out.println("---");

        System.out.println(true && false);
        System.out.println(true && false || false);
        System.out.println(true || false && true || false);
        System.out.println(true && false || false && true);


    }

}
