package id.b2camp.gita.day2;

import java.util.Arrays;

public class ArrayAndForLoopGita {
    public static void main(String[] args) {

        String[] nama = {"Leo", "Lala", "Lani", "Lulu"};
        System.out.println(nama[1]);
        System.out.println(nama[2]);
        System.out.println(Arrays.toString(nama));
        for (String name : nama)
            if (name.equalsIgnoreCase("Leo")) {
                System.out.println(name);
            }

         int[] angka = new int[7];
         angka[3] = 9;
         angka[0] = 3;
         angka[5] = 2;
         angka[6] = 1;

        System.out.println("---");

         System.out.println(Arrays.toString(angka));
         System.out.println(angka.length);

         System.out.println("---");

         int[] nomor = {6, 2, 3, 1, 7, 9, 5, 8, 4};

        for (int u : nomor)
            if (u % 2 == 1) {
                System.out.println(u + " adalah ganjil");
            }
        System.out.println("---");

         for (int u : nomor)
             if (u % 2 == 0) {
                 System.out.println(u + " adalah genap");
             }

         System.out.println("---");

         for (int n = 0; n < angka.length; n++) {
             System.out.println(angka[n]);
         }

         System.out.println("---");

         for (int l = angka.length - 1; l >= 0; l--) {
             System.out.println(angka[l]);
         }

         System.out.println("---");

         for (int b = nama.length - 1; b >= 0; b--) {
             System.out.println(nama[b]);
        }

        System.out.println("---");
        for (int c = 0; c < nama.length; c++) {
            System.out.println(nama[c]);
        }
    }
}
