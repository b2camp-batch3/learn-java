package id.b2camp.gita.day2;

public class SwitchGita {
    public static void main(String[] args) {

        int angka1 = 20;
        int angka2 = 5;
        String operasi = "Dikali";
        Integer result;

        switch (operasi) {
            case "Dikali" : result = angka1 * angka2;
                System.out.println("Hasilnya = " + result); break;
            case "Dibagi" : result = angka1 / angka2;
                System.out.println("Hasilnya = " + result); break;
            case "Ditambah" : result = angka1 + angka2;
                System.out.println("Hasilnya = " + result); break;
            case "Dikurang" : result = angka1 - angka2;
                System.out.println("Hasilnya = " + result); break;
        }

    }
}
