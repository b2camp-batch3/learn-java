package id.b2camp.gita.day2;

public class ArithmeticGita {
    public static void main(String[] args) {
        int numbers1 = 10;
        int numbers2 = 5;

        System.out.println(numbers2 - numbers1);
        System.out.println(numbers1 + numbers2);
        System.out.println(numbers2 * numbers1);
        System.out.println(numbers1 / numbers2);

        System.out.println("...");

        System.out.println(numbers1 + 6);
        System.out.println(349 * 2457);
        System.out.println(2938 / 356);
        System.out.println(1000 - 643);
        System.out.println((500 - 475) * 3 + 60 / 7);
        System.out.println(35 / (10 - 5) * 7 + 25);

        System.out.println("...");

        System.out.println(Math.max(4, 9));
        System.out.println(Math.min(7, 9));
        System.out.println(Math.floor(3.2));
        System.out.println(Math.pow(3.0, 8));
        System.out.println(Math.log(1));

    }
}
