CREATE TABLE gita_table (
    ID INT PRIMARY key NOT NULL,
    Name VARCHAR(50) NOT NULL,
    Date_of_birth DATE NOT NULL,
    Gender VARCHAR(10) NOT NULL,
    Address VARCHAR(40) NOT NULL,
    Email VARCHAR(25) NOT NULL,
    Phone_number VARCHAR(15) NOT NULL,
    City VARCHAR(20) NOT NULL,
    Country VARCHAR(12) NOT NULL
);

INSERT INTO gita_table (ID, Name, Date_of_birth, Gender, Address, Email, Phone_number, City, Country)
VALUES (1, 'Hana', '2005-02-20', 'Female', 'Jl. Mawar no.10', 'hana@gmail.com','0888887621', 'Tangerang', 'Indonesia'),
       (2, 'Dion', '2000-05-12', 'Male', 'Jl. Melati no.12', 'dionn@gmail.com','0832158743', 'Depok', 'Indonesia'),
       (3, 'Zack', '2002-03-05', 'Male', 'Jl. Rose no.1', 'zach@gmail.com','0874325643', 'Sydney', 'Australia'),
       (4, 'Rose', '1999-12-30', 'Female', 'Jl. Jasmine no.10', 'rosee@gmail.com','0865878523', 'London', 'England'),
       (5, 'Harry', '1998-07-09', 'Male', 'Jl. Dandelions no.5', 'harry@gmail.com','0813795634', 'Bern', 'Swiss');

SELECT * FROM gita_table WHERE Gender LIKE 'f%';

SELECT * FROM gita_table ORDER BY Date_of_birth ASC;

SELECT * FROM gita_table ORDER BY Date_of_birth DESC;

SELECT * FROM gita_table WHERE NOT Country = 'Indonesia';

SELECT * FROM gita_table ORDER BY ID ASC;

SELECT * FROM gita_table ORDER BY ID DESC;

SELECT * FROM gita_table WHERE Name LIKE '%a%';

SELECT * FROM gita_table WHERE Country = 'Indonesia' OR City = 'Bern';

SELECT * FROM gita_table WHERE Gender = 'Male' AND Name LIKE '%a%';

UPDATE gita_table SET City = 'Bogor' WHERE Name = 'Dion';

DELETE FROM gita_table WHERE ID = 1;

SELECT * FROM gita_table;

drop table gita_table;