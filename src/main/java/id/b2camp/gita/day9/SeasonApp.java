package id.b2camp.gita.day9;

import javax.print.attribute.standard.Fidelity;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SeasonApp {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
        Season musim = new Season("musim hujan","curah hujan tinggi");
        Field[] field1 = musim.getClass().getDeclaredFields();
        for (Field field : field1) {
            System.out.println(field);
        }
        Method[] methods = musim.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        System.out.println("---");

        if (musim.getClass().isAnnotationPresent(Weather.class)) {
            System.out.println("Cuacanya sangat mendung");
        }
        for (Field field : musim.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(SeasonInfo.class)) {
                field.setAccessible(true);
                Object object = field.get(musim);
                System.out.println("Saat ini sedang " + object);
            }
        }

        System.out.println("---");

        for (Method method : musim.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(PossibleDisaster.class)) {
                PossibleDisaster possibleDisaster = method.getAnnotation(PossibleDisaster.class);
                for (int i = 0; i < possibleDisaster.disaster(); i++){
                    method.invoke(musim);
                }
            }
        }

        System.out.println("---");

        SeasonWithLombok season = new SeasonWithLombok("Oktober - Maret","iklim tropis");
        System.out.println("Musim hujan biasanya terjadi pada bulan " + season.getSeasonCalendar());
        System.out.println("Musim hujan hanya terjadi di negara dengan " + season.getPlace());
    }
}
