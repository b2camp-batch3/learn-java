package id.b2camp.gita.day9;

@Weather
public class Season {

    @SeasonInfo
    private String seasonName;
    private String description;

    public Season(String seasonName, String description) {
        this.seasonName = seasonName;
        this.description = description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @PossibleDisaster(disaster = 2)
    public void flood() {
        System.out.println("Bencana yang kemungkinan terjadi adalah banjir");
    }
    public void landslide() {
        System.out.println("Bencana yang kemungkinan terjadi adalah tanah longsor");
    }

}

