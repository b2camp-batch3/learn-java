package id.b2camp.gita.day9;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SeasonWithLombok {
    private String seasonCalendar;
    private String place;
}
