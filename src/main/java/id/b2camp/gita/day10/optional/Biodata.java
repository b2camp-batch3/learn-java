package id.b2camp.gita.day10.optional;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class Biodata {
    private String name;
    private String gender;
    private String status;
    private String address;

}
