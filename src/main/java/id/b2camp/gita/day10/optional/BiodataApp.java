package id.b2camp.gita.day10.optional;

import java.util.Optional;

public class BiodataApp {
    public static void main(String[] args) {

        System.out.println("- BIODATA -");

        Biodata biodata =new Biodata("Siti", "Perempuan",null, "Jl. Mawar no.1 RT 01/ RW 01");

        String nama = Optional.ofNullable(biodata.getName()).
                map(String::toUpperCase).
                orElse("Nama tidak diketahui");
        System.out.println("Nama :  " + nama);


        String jenisKelamin = Optional.ofNullable(biodata.getGender()).
                orElse("Jenis kelamin tidak diketahui");
        System.out.println("Jenis kelamin : " + jenisKelamin);

        String status = Optional.ofNullable(biodata.getStatus()).
                orElse("Status tidak diketahui");
        System.out.println("Status : " + status);

        String alamat = Optional.ofNullable(biodata.getAddress()).
                orElse("Alamat tidak diketahui");
        System.out.println("Alamat : " + alamat);
    }
}
