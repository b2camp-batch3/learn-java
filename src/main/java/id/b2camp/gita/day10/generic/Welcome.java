package id.b2camp.gita.day10.generic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Welcome <T extends BookStore> {

    private T bookStore;
    public void welcome() {
        System.out.println("Selamat datang di " + bookStore);
    }
}
