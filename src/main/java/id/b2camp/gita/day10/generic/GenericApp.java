package id.b2camp.gita.day10.generic;

public class GenericApp {
    public static void main(String[] args) {

        GramediaStore name = new GramediaStore("Gramedia");
        Welcome<GramediaStore> storeWelcome = new Welcome<>(name);
        storeWelcome.welcome();
        System.out.println();

        Book<String> bookName = new Book<>("Nebula");
        bookName.print();

        Book<Double> bookPrice = new Book<>(95000.00);
        bookPrice.print();

        Book<String> author = new Book<>("Tere Liye");
        author.print();

        System.out.println("---");

        Book<String> bookName1 = new Book<>("Laut bercerita");
        bookName1.print();

        Book<Double> bookPrice1 = new Book<>(99000.00);
        bookPrice1.print();

        Book<String> author1 = new Book<>("Leila S. Chudori");
        author1.print();
    }
}
