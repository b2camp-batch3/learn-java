package id.b2camp.gita.day10.generic;

import lombok.*;

@Getter
@Setter
@ToString
public class GramediaStore extends BookStore {
    public GramediaStore(String storeName) {
        super(storeName);
    }
}
