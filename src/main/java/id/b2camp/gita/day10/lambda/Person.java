package id.b2camp.gita.day10.lambda;

public interface Person {

    void person(String name, int age);

}
