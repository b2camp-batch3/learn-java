package id.b2camp.gita.day10.lambda;

public class PersonApp {
    public static void main(String[] args) {
        printPerson(new Person() {
            @Override
            public void person(String name, int age) {
                System.out.println("Nama saya " + name);
                System.out.println("Umur saya " + age + " tahun");
            }
        });

        printPerson((n, a) -> {
            System.out.println("Saat ini umur " + n + " adalah " + a + " tahun");
        });

        person(PersonApp::info);
    }
    private static void printPerson(Person person){
        person.person("Leo",18);
    }
    private static void person(PersonInfo personInfo) {
        personInfo.info();
    }
    private static void info() {
        System.out.println("Hobi saya adalah bernyayi");
    }
}
