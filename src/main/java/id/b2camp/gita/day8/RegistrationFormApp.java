package id.b2camp.gita.day8;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class RegistrationFormApp {
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws InvalidName, InvalidDateOfBirth, InvalidSchoolName {
        System.out.println("Formulir Pendaftaran Peserta OSN");

        String nama = scanner("Nama : ");
        String tanggalLahir = scanner("Tanggal lahir : ");
        String namaSekolah = scanner("Nama sekolah : ");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateOfBirth = LocalDate.parse(tanggalLahir, formatter);

        try {
            Validation.validateName(nama);
            Validation.validateDateOfBirth(dateOfBirth);
            Validation.validateSchoolName(namaSekolah);
        } catch (InvalidName e) {
            throw new InvalidName(e.getMessage());
        } catch (InvalidDateOfBirth e) {
            throw new InvalidDateOfBirth(e.getMessage());
        } catch (InvalidSchoolName e) {
            throw new InvalidSchoolName(e.getMessage());
        } finally {
            System.out.println("Akan selalu dieksekusi");
        }

//        Validation.validateNameRuntime(nama);
//        Validation.validateDateRuntime(dateOfBirth);
//        Validation.validateSchoolNameRuntime(namaSekolah);

        System.out.println("---");

    RegistrationForm registrationForm = new RegistrationForm(nama, dateOfBirth ,namaSekolah);
        System.out.println("Peserta dengan nama " + registrationForm.getName() + " dari sekolah " + registrationForm.getSchoolName() + " berhasil mendaftar.");
    }
    static String scanner(String message) {
        System.out.println(message);
        return input.nextLine();
    }
}
