package id.b2camp.gita.day8;

public class InvalidSchoolName extends Throwable {
    public InvalidSchoolName(String message) {
        super(message);
    }
}
