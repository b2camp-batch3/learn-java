package id.b2camp.gita.day8;

import java.time.LocalDate;

public class RegistrationForm {
    private String name;
    private LocalDate dateOfBirth;
    private String SchoolName;

    public RegistrationForm(String name, LocalDate dateOfBirth, String schoolName) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        SchoolName = schoolName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }
}
