package id.b2camp.gita.day8;

public class InvalidName extends Throwable {
   public InvalidName(String message) {
       super(message);
   }
}
