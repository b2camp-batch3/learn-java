package id.b2camp.gita.day8;

import id.b2camp.fakhri.day8.InvalidDateException;

import java.time.LocalDate;
import java.util.Objects;

public class Validation {

    public static void validateName(String name) throws InvalidName {
        if (name.isEmpty()) {
            throw new InvalidName("Nama tidak boleh kosong");
        }
    }

    public static void validateDateOfBirth(LocalDate dateOfBirth) throws InvalidDateOfBirth {
        if (Objects.isNull(dateOfBirth)) {
            throw new InvalidDateOfBirth("Tanggal lahir tidak boleh kosong");
        } else if (dateOfBirth.isAfter(LocalDate.now())) {
            throw new InvalidDateOfBirth("Tanggal lahir tidak boleh melebihi hari ini");
        }
    }

    public static void validateSchoolName(String schoolName) throws InvalidSchoolName {
        if (schoolName.isEmpty()) {
            throw new InvalidSchoolName("Nama Sekolah tidak boleh kosong");
        }
    }

    public static void validateNameRuntime(String name) throws RuntimeException {
        if (name.isEmpty()) {
            throw new RuntimeException("Nama tidak boleh kosong");
        }
    }

    public static void validateDateRuntime(LocalDate dateOfBirth) throws RuntimeException {
        if (Objects.isNull(dateOfBirth)) {
            throw new RuntimeException("Tanggal lahir tidak boleh kosong");
        } else if (dateOfBirth.isAfter(LocalDate.now())) {
            throw new RuntimeException("Tanggal lahir tidak boleh melebihi hari ini");
        }
    }
    public static void validateSchoolNameRuntime(String schoolName) throws RuntimeException {
        if (schoolName.isEmpty()) {
            throw new RuntimeException("Nama Sekolah tidak boleh kosong");
        }
    }
}
