package id.b2camp.gita.day8;

public class InvalidDateOfBirth extends Throwable {
    public InvalidDateOfBirth(String message) {
        super(message);
    }
}
