package id.b2camp.gita.day4;

import java.util.Scanner;
class TodoListGita {
    public static void main(String[] args) {

        viewShow();
    }
    public static Scanner input = new Scanner(System.in);
    public static String[] kode = new String[10];

    public static void show(){
        for (var i = 0; i < kode.length;i++) {
            var todo = kode[i];
            var no = i + 1;

            if (todo != null){
                System.out.println(no + " " + todo);
            }
        }
    }
    public static void add(String todo){
        var isFull = true;
        for (var i = 0; i < kode.length; i++){
            if (kode[i] == null){
                isFull = false;
                break;
            }
        }
        if (isFull){
            var temp = kode;
            kode = new String[kode.length * 2];

            for (var i = 0; i < temp.length; i++){
                kode[i] = temp[i];
            }
        }
        for (var i = 0; i < kode.length; i++){
            if (kode[i] == null){
                kode[i] = todo;
                break;
            }
        }
    }
    public static boolean remove(Integer number){
        if ((number - 1) >= kode.length){
            return false;
        }else if (kode[number - 1] == null){
            return false;
        }else {
            for (var i = (number -1 ); i < kode.length; i++){
                if (i == (kode.length) - 1){
                    kode[i] = null;
                }else {
                    kode[i] = kode[i + 1];
                }
            }
        }
        return true;
    }
    public static String input(String info){
        System.out.println(info);
        String data = input.nextLine();
        return data;
    }
    public static void viewShow(){
        while (true) {
            show();

            System.out.println("Pilihan menu : ");
            System.out.println("1. Tambah");
            System.out.println("2. Hapus");
            System.out.println("x. Keluar");

            var input = input("Pilih opsi: ");

            if (input.equals("1")){
                viewAdd();
            }else if (input.equals("2")){
                viewRemove();
            }else if (input.equals("x")){
                break;
            }else {
                System.out.println("Pilihan tidak sesuai");
            }
        }
    }
    public static void viewAdd(){
        System.out.println("Menambahkan todo");

        var todo = input("Masukkan kode (x) jika batal");

        if (todo.equals("x")){

        }else {
            add(todo);
        }
    }
    public static void viewRemove(){
        System.out.println("Menghapus todo");

        var number = input("Masukkan kode (x) jika batal");

        if (number.equals("x")) {

        }else {
            boolean sucsess = remove(Integer.valueOf(number));
            if (!sucsess){
                System.out.println("Gagal menghapus todo nomor ke : " + number);
            }
        }

    }

}