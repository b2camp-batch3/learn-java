CREATE TABLE members (
    member_id INT primary key not null,
    member_code VARCHAR(10) not null,
    member_name VARCHAR(50) not null,
    member_phone VARCHAR(15) not null,
    member_address VARCHAR(100) not null
);

INSERT INTO members (member_id, member_code, member_name, member_phone, member_address)
VALUES (10, '225', 'Nana', '0842857345', 'Jl. Tulip no.1'),
       (11, '643', 'Rere', '0823547736', 'Jl. Mawar no.7'),
       (12, '556', 'Leo', '0846482471', 'Jl. Melati no.12'),
       (13, '123', 'Lila', '084285477', 'Jl. Raflesia no.3'),
       (14, '289', 'Zara', '0816648246', 'Jl. Cempaka no.5'),
       (15, '720', 'Rio', '0847850381', 'Jl. Kembnag no.9');

CREATE TABLE books (
    book_id INT primary key not null,
    book_title VARCHAR(50) not null,
    book_author VARCHAR(50) not null,
    book_publisher VARCHAR(50) not null,
    publication_year CHAR(4) not null,
    stock_quantity INT not null
);

INSERT INTO books (book_id, book_title, book_author, book_publisher, publication_year, stock_quantity)
VALUES (101, 'Nebula', 'Tere Liye', 'Gramedia Pustaka', '2020', 50),
       (201, 'Laut bercerita', 'Leila S. Chudori', 'Kepustakaan Gramedia', '2017', 100),
       (301, 'Filosofi Teras', 'Henry Manampiring', 'Kompas Media', '2018', 25),
       (401, 'Esok Lebih Baik', 'Dr. Abdullah Al-maghluts', 'Penerbit Alvabet', '2022', 120),
       (501, 'Insecurity', 'Alvi Syahrin', 'Alvi Ardi Publishing', '2021', 200),
       (601, 'Atomic Habits', 'James Clear', 'Gramedia Pustaka', '2023', 175);

CREATE TABLE staff (
    staff_id INT primary key not null,
    staff_name VARCHAR(50) not null,
    staff_position VARCHAR(50) not null,
    staff_phone VARCHAR(50) not null,
    staff_address VARCHAR(100) not null
);

INSERT INTO staff (staff_id, staff_name, staff_position, staff_phone, staff_address)
VALUES (20,   'Rio', 'Kepala Perpus', '0842784759', 'Jl. Dandelions no.1'),
	   (21,   'Angga', 'Administrasi Perpus',  '0876534225', 'Jl. Rose no.2'),
	   (22,   'Laras', 'Layanan Perpus',  '0865312575', 'Jl. Anggrek no.9'),
	   (23,   'Anggi', 'Wakil Kepala Perpus', '0874545578', 'Jl. Merak no.2'),
	   (24,   'Tera', 'Administrasi Perpus',  '0879532483', 'Jl. Singa no.15'),
	   (25,   'Aga', 'Layanan Perpus',  '08932426787', 'Jl. Cendrawasih no.4');

CREATE TABLE returns (
    return_id INT primary key not null,
    return_date DATE not null,
    fine_amount VARCHAR(12) not null,
    book_id INT,
    member_id INT,
    staff_id INT,
    FOREIGN KEY (book_id) REFERENCES books(book_id),
    FOREIGN KEY (member_id) REFERENCES members(member_id),
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id)
);

INSERT INTO returns (return_id, return_date, fine_amount, book_id, member_id, staff_id)
VALUES (30, '2023-02-20','Rp. 0', 101, 10, 20),
       (31, '2022-06-25','Rp. 10000', 201, 11, 21),
       (32, '2023-01-30','Rp. 5000', 301, 12, 22),
       (33, '2022-09-03','Rp. 0', 401, 13, 23),
       (34, '2023-02-01','Rp. 0', 501, 14, 24),
       (35, '2023-05-27','Rp. 15000', 601, 15, 25);

CREATE TABLE borrowers (
    borrower_id INT primary key not null,
    borrow_date DATE not null,
    return_date DATE not null,
    book_id INT,
    member_id INT,
    staff_id INT,
    FOREIGN KEY (book_id) REFERENCES books(book_id),
    FOREIGN KEY (member_id) REFERENCES members(member_id),
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id)
);

INSERT INTO borrowers (borrower_id, borrow_date, return_date, book_id, member_id, staff_id)
VALUES (40, '2023-01-20', '2023-02-20', 101, 10, 20),
       (41, '2022-04-15', '2022-06-25', 201, 11, 21),
       (42, '2022-12-01', '2023-01-30', 301, 12, 22),
       (43, '2023-04-20', '2023-05-20', 401, 13, 23),
       (44, '2022-02-10', '2022-03-29', 501, 14, 24),
       (45, '2023-03-30', '2023-04-12', 601, 15, 25);

CREATE TABLE shelves (
    shelf_id INT primary key not null,
    shelf_name VARCHAR(50) not null,
    book_id INT,
    FOREIGN KEY (book_id) REFERENCES books(book_id)
);

INSERT INTO shelves (shelf_id, shelf_name, book_id)
VALUES (50, 'Fiksi Ilmiah', 101),
	   (51,  'Kehidupan', 201),
	   (52,  'Non Fiksi', 301),
	   (53, 'Non Fiksi', 401),
	   (54,  'Non Fiksi', 501),
	   (55,  'Non Fiksi', 601);


select s.shelf_name, b.book_title, b.book_author, s.shelf_id
from gita_library.shelves s
inner join gita_library.books b on b.book_id  = s.book_id;

select b.borrow_date , m.member_name
from gita_library.borrowers b
full join gita_library.members m on m.member_id = b.member_id;

select r.return_date, b.book_title, b.stock_quantity
from gita_library."returns" r
left join gita_library.books b on b.book_id = r.book_id;

select b.borrow_date, b.member_id, b2.book_title
from gita_library.borrowers b
right join gita_library.books b2 on b2.book_id = b.book_id;

select r.return_date , r.staff_id , s.staff_name , s.staff_position
from gita_library."returns" r
left join gita_library.staff s on s.staff_id = r.staff_id
where r.staff_id = s.staff_id
group by r.return_date, r.staff_id , s.staff_name , s.staff_position;

select count(*)
from gita_library.members m
where m.member_id = 12;

select sum(stock_quantity)
from books b;

select member_id from members m
where member_id = 10
union
select book_id from books b
where book_id = 101;

select count(book_id), book_title
from books b
group by book_title
having count(book_id) < 2;