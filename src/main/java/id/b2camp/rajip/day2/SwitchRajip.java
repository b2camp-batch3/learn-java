package id.b2camp.rajip.day2;

public class SwitchRajip {
    public static void main(String[] args) {

        int code = 400;

        switch (code) {
            case 201 :
                System.out.println("Created");
                break;
            case 400 :
                System.out.println("Bad Request");
                break;
            case 401 :
                System.out.println("Unauthorized");
                break;
            case 404 :
                System.out.println(" The requested resource was not found");
                break;
            case 500 :
                System.out.println("There was an error on the server and the request could not be completed");
                break;
            default:
                System.out.println("Everything OK");

        }
    }
}
