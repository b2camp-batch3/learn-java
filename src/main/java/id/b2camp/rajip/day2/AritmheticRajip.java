package id.b2camp.rajip.day2;

public class AritmheticRajip {
    public static void main(String[] args) {

//        Keliling dan Area
//        Persegi
        int side = 7;

        int kelilingPersegi = 4 * side;
        double areaPersegi = Math.pow(side,2);

//        Peresgi panjang
        int panjang = 10;
        int lebar = 6;

        int kelilingPersegiPanjang = 2*(panjang+lebar);
        int areaPersegiPanjang = panjang*lebar;

//        jajargenjang
        int tinggi = 3;
        int alas = 10;
        int sisi = 5;

        double kelilingJajargenjang = (2*sisi)+(2*alas);
        double areaJajargenjang = alas * tinggi;

//        triangle
        int height = 10;
        int base = 6;

        double areaTriangle = 0.5*height*base;

//        circle
        int r = 6;

        double kelilingLingkaran = 2*r*Math.PI;
        double areaLingkaran = Math.PI*Math.pow(r,2);


        System.out.println("Luas dan Keliling Bangun Datar\n");

        System.out.println("Persegi dengan sisi = "+side +
                "\nkeliling = "+kelilingPersegi+
                "\nluas = "+areaPersegi);

        System.out.printf("\nPersegi dengan panjang %d dan lebar %d\n" +
                "keliling = %d\n" +
                "luas = %d\n", panjang, lebar, kelilingPersegiPanjang, areaPersegiPanjang);

        System.out.printf("\nJajargenjang dengan sisi %d, tinggi %d dan sisi %d\n", sisi, tinggi, alas);
        System.out.println("keliling = "+ kelilingJajargenjang +
                "\nluas = "+ areaJajargenjang);

        System.out.printf("\nSegitiga dengan tinggi %d dan alas %d\n", height, base);
        System.out.println("luas = "+ areaTriangle);

        System.out.println("\nLingkaran dengan radius " + r +
                "\nkeliling = "+kelilingLingkaran+
                "\nluas = "+ areaLingkaran);
    }
}
