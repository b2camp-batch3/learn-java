package id.b2camp.rajip.day2;

public class IfElseRajip {
    public static void main(String[] args) {
        int year = 2001;

        String kabisat = year%4 == 0 ? year + " adalah tahun kabisat" : year + " bukan tahun kabisat";
        System.out.println(kabisat);

        int score = 90;

        if (score > 85){
            System.out.println("your grade is A");
        } else if (score > 75) {
            System.out.println("your grade is B");
        } else if (score > 60) {
            System.out.println("your grade is C");
        } else if (score > 30) {
            System.out.println("your grade is D");
        } else System.out.println("your grade is E");

//    genap
        System.out.println("\n Bilangan genap 1 -100");
        for (int i = 1; i <= 100 ; i++) {
            if (i%2==0){
                System.out.println(i);
            }
        }
    }
}
