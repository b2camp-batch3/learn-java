package id.b2camp.rajip.day2;

import java.util.Arrays;

public class ArrayAndForLoopRajip {
    public static void main(String[] args) {

//        Array []
        int [] arrayInt = {1,2,3};
        String[] animals = new String[4];
        animals[0]= "cat";
        animals[1]= "dog";
        animals[3]= "mouse";

        System.out.println(Arrays.toString(arrayInt));
        System.out.println(Arrays.toString(animals));

//        ForLoop
        for (int i = 0; i <animals.length ; i++) {
            System.out.println(animals[i]);
        }


//        tabel perkalian
        int number = 6;

        for (int i = 0; i <= 10; i++) {
            System.out.println(number+" * "+i+" = "+number*i);
        }
    }
}
