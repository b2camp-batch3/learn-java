package id.b2camp.rajip.day2;

public class ComparisonRajip {
    public static void main(String[] args) {
//        comparison boolean
        boolean a = true;
        boolean b = false;

        System.out.println("comparison boolean");
        System.out.println(a || b);
        System.out.println(a && b);
        System.out.println(a || a && b);
        System.out.println(a == b);
        System.out.println(a != b);

//        comparison number
        double x = 2.567;
        double y = 2.6;

        System.out.println("\ncomparison number");
        System.out.println(x>y);
        System.out.println(x>=y);
        System.out.println(x<y);
        System.out.println(x<=y);
        System.out.println(x==y);
        System.out.println(x!=y);

//        comparison string
        String firstName = "Rajip";
        String lastName = "Pangestu";

        System.out.println("\ncomparison string");
        System.out.println(firstName.equalsIgnoreCase("rajip")); /* membandingakan 2 string lebih baik menggunakan equals() karena khusus membandingkan string*/
        System.out.println(firstName == "Rajip"); /* == membandingkan referensi object*/
    }
}
