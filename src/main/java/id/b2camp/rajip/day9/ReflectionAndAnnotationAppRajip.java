package id.b2camp.rajip.day9;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionAndAnnotationAppRajip {
    /*
    Reflection kemampuannya dalam pemerograman Java adalah
    * untuk mirroring atau
    * memeriksa, mengakses dan memanifulasi informasi class, object, method dan properti saat diakses
     */

    public static void main (String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Square square = new Square ("square", 4);
        Field[] declaredFields = square.getClass ().getDeclaredFields ();
        Method[] declaredMethods = square.getClass ().getDeclaredMethods ();
        System.out.println (Arrays.toString (declaredFields));

        for (Field field : declaredFields){
            System.out.println ("Field yang ada di class square : "+ field);
        }
        for (Method method : declaredMethods){
            System.out.println ("Method yang ada di class square : " + method);
        }

        System.out.println ("----");

        CircleRajip instance = new CircleRajip ();

        // Proses anotasi CalculateCircle
        if (instance.getClass ().isAnnotationPresent (CalculateCircle.class)){
            System.out.println ("Class annotation whit CalculateCircle");
        }

        // Proses anotasi FieldCircle
        Field[] fields = instance.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(FieldCircle.class)) {
                System.out.println("Field " + field.getName() + " is annotated with FieldCircle");
            }
        }

        // Proses anotasi ValueOfCircle
        Method[] methods = instance.getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(ValueOfCircle.class)) {
                System.out.println("Method " + method.getName() + " is annotated with ValueOfCircle");
            }
        }

        // Panggil method areaCircle dan aroundCircle
        Method areaMethod = instance.getClass().getMethod("areaCircle", double.class);
        Method aroundMethod = instance.getClass().getMethod("aroundCircle", double.class);

        double r = 5.0;
        double area = (double) areaMethod.invoke(instance, r);
        double around = (double) aroundMethod.invoke(instance, r);

        System.out.println("Area of circle with r = " + r + ": " + area);
        System.out.println("Circumference of circle with r = " + r + ": " + around);

        // Annotation Lombok
        System.out.println ("-----");

        Triangle triangle = new Triangle ();
        triangle.setName ("Triangle A");
        triangle.setBase (2.6);
        triangle.setHeight (4.9);
        triangle.area ();

    }
}
