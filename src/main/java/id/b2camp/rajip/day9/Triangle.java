package id.b2camp.rajip.day9;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Triangle {
    private String name;
    private  double base;
    private  double height;


    public void area () {
        System.out.println ("area of " + name + " is " + base * height);
    }
}
