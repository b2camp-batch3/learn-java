package id.b2camp.rajip.day9;

public class Square {
    private String name;
    private int side;

    public Square (String name, int side) {
        this.name = name;
        this.side = side;
    }

    public void squareName(){
        System.out.println ("square : " + name);
    }

    private double area(){
        return side*side;
    }
}
