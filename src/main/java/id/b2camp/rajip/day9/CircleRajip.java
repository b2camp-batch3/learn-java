package id.b2camp.rajip.day9;

@CalculateCircle
public class CircleRajip {
    @FieldCircle
    private String name;
    private double r;

    @ValueOfCircle
    public double areaCircle(double r) {
        return Math.pow (r, 2) * Math.PI;
    }

    public double aroundCircle(double r) {
        return 2 * Math.PI * r;
    }
}
