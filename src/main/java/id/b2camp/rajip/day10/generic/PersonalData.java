package id.b2camp.rajip.day10.generic;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class PersonalData<T extends Person> {
    private T value;

    public String getValue(String name) {
        return name;
    }

    public int getValue(Integer age) {
        return age;
    }

    public boolean getValue(Boolean isMarried) {
        return isMarried;
    }
}
