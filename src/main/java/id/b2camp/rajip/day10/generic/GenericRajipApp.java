package id.b2camp.rajip.day10.generic;

public class GenericRajipApp {
    /*
    Java Generic = fitur yang memungkinkan dalam pembuatan class, method, interface yang dapat diolah dengan tipe data
    yang belum ditentukan.
    Ini digunakan untuk membuat kode yang lebih umum dan dapat digunakan kembali tanpa mengulangi kode yang serupa.

    Catatan : type data harus non primitive
     */
    public static void main(String[] args) {
        //Object disini bisa menampung object dari tipe apapun
        CustomValue<Object> objectStr = new CustomValue<>();
        objectStr.setValue("a");
        objectStr.setValue(2);

        //Sedangkan jika diisi langsung dengan tipe data yang diinginkan atau diinisiasi langsung.
        CustomValue<Integer> intCustom = new CustomValue<>(2);
        CustomValue<String> srtCustom = new CustomValue<>("Hello");
        CustomValue<Boolean> boolCustom = new CustomValue<>(true);

        System.out.println("\n-------\n");
        System.out.println(intCustom);
        System.out.println(srtCustom);
        System.out.println(boolCustom);

        /*
        Tapi agar fungsinya lebih optimal custom ini bisa kita tentukan objectnya sendiri
        dalam bungkusannya <>
         */
        Person person = new Person("Angga", 20, false);
        PersonalData<Person> data = new PersonalData<>(person);

        System.out.println("\n-------\n");
        System.out.println(data.getValue(person.getName()));
        System.out.println(data.getValue(person.getAge()));
        System.out.println(data.getValue(person.getIsMarried()));
    }
}
