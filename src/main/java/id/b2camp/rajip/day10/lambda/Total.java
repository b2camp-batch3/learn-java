package id.b2camp.rajip.day10.lambda;

public interface Total {
    double calculateProfit(double capital, int quantity, double sellingPrice);
}
