package id.b2camp.rajip.day10.lambda;

/*
Lambda memungkinkan kita menulis lebih ringkas dan ekpresif dalam penggunaan
object Anonymous class
- Lambda juga dipakai terutama dalam penggunaan Collections
- Lambda juga digunakan dalam mengirim parameter ke suatu method Collections Framework
    - forEach
    - map
    - filter

- Bentuk umum Lambda
parameter -> expression
 */
public class LambdaRajipApp {
    public static void main(String[] args) {
        Total profit1 = new Total() {
            @Override
            public double calculateProfit(double capital, int quantity, double sellingPrice) {
                return (sellingPrice * quantity) - (capital * quantity);
            }
        };
        System.out.println(profit1.calculateProfit(30000, 10, 32000));

        // With Lambda
        Total profit2 = (capital, quantity, sellingPrice) ->
                (sellingPrice * quantity) - (capital * quantity);

        System.out.println(profit2.calculateProfit(20000,10,25000));
    }
}
