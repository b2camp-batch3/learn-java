package id.b2camp.rajip.day10.optional;

import java.util.Optional;

public class OptionalRajipApp {
    /*
    Java Optional hadir untuk mengatasi situasi dimana nilai mungkin kosong atau null
     */

    public static void main(String[] args) {
        Songs songInfo = new Songs(null, null, null);
        Optional<Songs> result = Optional.of(songInfo);

        String title = result.map(Songs::getTitle)
                .orElse("Song tidak memiliki judul");

        String artist = result.map(Songs::getArtis)
                .orElse("Song tidak memiliki artis");

        int year = result.map(Songs::getYear)
                .orElse(0);

        System.out.println(title);
        System.out.println(artist);
        System.out.println(year);
    }
}
