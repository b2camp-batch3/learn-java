package id.b2camp.rajip.day10.optional;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Songs {
    private String title;
    private String artis;
    private Integer year;

}
