package id.b2camp.rajip.day6.abstractclasses;


public class PhysicalProduct extends Product{
    protected double weight;
    @Override
    public void displayInfo () {
        System.out.println("Physical Product - Name: " + name +
                ", Price: Rp" + price +
                ", Weight: " + weight + " gram");
    }
}
