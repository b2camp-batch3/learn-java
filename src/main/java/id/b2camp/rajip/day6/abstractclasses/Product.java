package id.b2camp.rajip.day6.abstractclasses;

/*
abstract class adalah contrak umum yang mana saat di wariskan
otomatis method nya akan di overriding kesetiap child nya.
 */
abstract class Product {
    protected String name;
    protected double price;

    public abstract void displayInfo();
}
