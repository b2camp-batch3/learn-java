package id.b2camp.rajip.day6.abstractclasses;

public class ProductApp {
    public static void main (String[] args) {
        PhysicalProduct physicalProduct = new PhysicalProduct ();

        physicalProduct.name = "Book";
        physicalProduct.price = 20000;
        physicalProduct.weight = 500;
        physicalProduct.displayInfo ();
    }
}
