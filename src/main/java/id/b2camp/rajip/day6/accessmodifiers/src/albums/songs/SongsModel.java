package id.b2camp.rajip.day6.accessmodifiers.src.albums.songs;

public class SongsModel {
    protected String id;
    protected String  title;
    protected   int year;
    protected int duration;

    protected void model () {
        System.out.println ("id : " + id);
        System.out.println ("title : " + title);
        System.out.println ("year : " + year);
        System.out.println ("duration : " + duration );
    }
}
