package id.b2camp.rajip.day6.accessmodifiers.src.albums.songs;

class Genre extends SongsModel {
    protected String genre;

    protected void model () {
        System.out.println ("id : " + id);
        System.out.println ("title : " + title);
        System.out.println ("year : " + year);
        System.out.println ("duration : " + duration );
        System.out.println ("genre : " + genre);
    }
}