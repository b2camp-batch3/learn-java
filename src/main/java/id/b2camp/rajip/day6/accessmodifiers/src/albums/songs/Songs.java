package id.b2camp.rajip.day6.accessmodifiers.src.albums.songs;

public class Songs {

/*   ini adalah contoh penerapan access modifiers protected
    yang mana hanya bisa diakses di class, package dan subclass nya
 */

    public static void main (String[] args) {
        SongsModel songsModel = new SongsModel ();
        Performer performer = new Performer ();


        songsModel.id = "123";
        songsModel.title = "Sebelah Mata";
        songsModel.year = 2008;

        performer.id = "123";
        performer.title = "Yellow";
        performer.year = 2008;
        performer.genre = "Pop";
        performer.performer = "Coldplay";

        songsModel.model ();
        System.out.println ("-------");
        performer.model ();
    }
}
