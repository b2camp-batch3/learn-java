package id.b2camp.rajip.day6.accessmodifiers.src.albums.songs;

class Performer extends Genre {

    protected String performer;

    protected void model () {
        System.out.println ("id : " + id);
        System.out.println ("title : " + title);
        System.out.println ("year : " + year);
        System.out.println ("duration : " + duration );
        System.out.println ("genre : " + genre);
        System.out.println ("performer : " + performer);
    }
}