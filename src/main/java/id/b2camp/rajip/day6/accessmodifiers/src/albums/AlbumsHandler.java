package id.b2camp.rajip.day6.accessmodifiers.src.albums;

/*
private => hanya diakses di Class
cara pemanggilan  field yang disetting dengan akses private bisa dipanggil dengan bantuan
getter dan setter.
 */
public class AlbumsHandler {
    private String id;
    private String name;
    private int year;

    public AlbumsHandler () {
    }

    public void albums() {
        albumsPrivate ();
    }

    private void albumsPrivate () {
        System.out.println ("Albums");
    }

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public int getYear () {
        return year;
    }

    public void setYear (int year) {
        this.year = year;
    }
}
