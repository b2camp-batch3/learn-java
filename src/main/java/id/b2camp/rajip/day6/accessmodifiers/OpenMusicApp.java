package id.b2camp.rajip.day6.accessmodifiers;


import id.b2camp.rajip.day6.accessmodifiers.src.OpenMusic;
import id.b2camp.rajip.day6.accessmodifiers.src.albums.AlbumsHandler;

/*
Access modifier adalah kemampuan membuat class, field, method dan constructor dapat diakses dari mana.

Ada beberapa access modifiers diantaranya
* public => Bisa diakses di semua (Class, Package, Subclass, Word)
* protect => Bisa diakses (Class, Package, Subclass)
* default (no modifiers) => Class & Package
* private => hanya diakses di Class
 */
public class OpenMusicApp {
    public static void main (String[] args) {
        OpenMusic openMusic = new OpenMusic ();
        AlbumsHandler albumsHandler = new AlbumsHandler ();

        openMusic.routesOpenMusic = "localhost:5000";

        albumsHandler.setId ("1123");
        albumsHandler.setName ("Bintang di surga");
        albumsHandler.setYear (2004);

        System.out.println ("berjalan di server https://"+  openMusic.routesOpenMusic);
        System.out.println ("output :");
        albumsHandler.albums ();
        System.out.println (albumsHandler.getId ());
        System.out.println (albumsHandler.getName ());
        System.out.println (albumsHandler.getYear ());
    }
}
