package id.b2camp.rajip.day6.interfaces;

public class Product implements ProductNames, Payment{
    private int code;
    private int quantity;
    private double price;

    public int getCode () {
        return code;
    }

    public void setCode (int code) {
        this.code = code;
    }

    public int getQuantity () {
        return quantity;
    }

    public void setQuantity (int quantity) {
        this.quantity = quantity;
    }

    public double getPrice () {
        return price;
    }

    public void setPrice (double price) {
        this.price = price;
    }

    @Override
    public double calculatePayment (int quantity, double price) {
        return quantity * price;
    }

    @Override
    public String productName (int code) {
        switch (code) {
            case 11:
                return "Book";
            case 12:
                return "Shirt";
            case 13:
                return "Clothes";
            default:
                return "";
        }
    }
}
