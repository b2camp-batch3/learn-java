package id.b2camp.rajip.day6.interfaces;

/*
interface adalah kontak khusus yang hanya bisa diterampkan dengan 'implement'
jadi method nya bisa dimiliki || diterapkan class mana saja baik child atau pun parents nya.
 */
public class ProductApp {
    public static void main (String[] args) {
        Product product = new Product ();
        String name = product.productName (12);
        double resultPrice = product.calculatePayment (2, 56500);
        System.out.println (name + " ," + resultPrice);

        System.out.println ("-----");

        product.setCode (12);
        product.setQuantity (4);
        product.setPrice (14700);

        System.out.println (product.productName (product.getCode ()));
        System.out.println (product.calculatePayment (product.getQuantity (), product.getPrice ()));
    }
}
