package id.b2camp.rajip.day6.interfaces;

public interface Payment {
    double calculatePayment(int quantity, double price);
}
