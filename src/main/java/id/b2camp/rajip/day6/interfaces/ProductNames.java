package id.b2camp.rajip.day6.interfaces;

public interface ProductNames {
    String productName (int code);
}
