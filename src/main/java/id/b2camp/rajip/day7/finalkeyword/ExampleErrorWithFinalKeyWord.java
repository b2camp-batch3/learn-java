package id.b2camp.rajip.day7.finalkeyword;

public class ExampleErrorWithFinalKeyWord extends TaxCalculation{
    void changeFinalField(){
//        tax = 2;
        System.out.println (tax);

//  Nilai tax yang sudah final tidak bisa diubah
    }
}

class ExampleErrorExtendsClassFinal  extends PaymentCalculation {
//    final double calculateDiscount() {
//        return 2;
//    }
}
