package id.b2camp.rajip.day7.finalkeyword;

public class PaymentCalculation extends TaxCalculation {

    private double price;
    private  int quantity;

    public double getPrice () {
        return price;
    }

    public void setPrice (double price) {
        this.price = price;
    }

    public int getQuantity () {
        return quantity;
    }

    public void setQuantity (int quantity) {
        this.quantity = quantity;
    }

    double total(){
        return price*quantity;
    }

//    ini adalah method final jadi method ini tidak bisa diubah.
    final double calculateDiscount(){
        double total = (price * quantity)+(tax*(price * quantity));

        if (total > 1_000_000) {
            return total*0.95;
        } else {
            return total;
        }
    }
}
