package id.b2camp.rajip.day7.finalkeyword;

/*
keyword final adalah niali akhir bisa dipakai di field, method ataupun class.
Jika sudah final artinya nilainya tidak bisa diubah, dioverriding atau diwariskan.
 */
public class PaymentCalculationApp {
    public static void main (String[] args) {
        PaymentCalculation payment = new PaymentCalculation ();
        payment.setPrice (650000);
        payment.setQuantity (6);

        double v = payment.calculateDiscount ();

        System.out.println (payment.getPrice () + " | @" + payment.getQuantity ());
        System.out.println ("Total " + payment.total ());
        System.out.println ("Tax 10% = " + payment.tax * payment.total ());


        System.out.println (v);

    }
}
