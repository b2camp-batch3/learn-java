package id.b2camp.rajip.day7.statickeyword;

public class CurrencyConverter {
    protected static double exchangeRate = 15321;


    protected static double converterToIdr(double usdAmount){
        return usdAmount * exchangeRate;
    }

    protected static double converterToUsd(double idrAmount){
        return idrAmount / exchangeRate;
    }
}
