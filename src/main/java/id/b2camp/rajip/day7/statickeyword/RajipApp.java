package id.b2camp.rajip.day7.statickeyword;


/*
    Dengan menggunakan static key word kita bisa membuat field, method atau class method yang bisa diakses langsung
    tanpa melalui objectnya
 */
public class RajipApp {
    public static void main (String[] args) {

//        pemanggilan field static
        double subtotal = 150000;
        double total = subtotal + (subtotal*Fees.TAX_RATE) + Fees.SHIPPING_COST;

//        pemanggilan method static
        double amountInUsd = 10;
        double amountInIrd = 2_000_000;
        double usdToIrd = CurrencyConverter.converterToIdr (amountInUsd);
        double irdToUsd = CurrencyConverter.converterToUsd (amountInIrd);

        System.out.println ("Fee Shopping " + total);
        System.out.println ("-----");
        System.out.println ("USD = " + amountInUsd + " = " + "IDR = " + usdToIrd);
        System.out.println ("IDR = " + amountInIrd + " = " + "USD = " + irdToUsd);

    }
}
