package id.b2camp.rajip.day7.enums;

/*
Class enum berisikan nilai terbatas atau sudah ditentukan sebelumnya
 */
public enum Currency {
    DOLLAR("United States Dollar", "$"),
    EURO("Euro", "€"),
    YEN("Japanese Yen", "¥"),
    RUPIAH("Indonesian Rupiah", "Rp");

    private final String name;
    private final String symbol;

    Currency(String name, String symbol) {
        this.name = name;
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }
}
