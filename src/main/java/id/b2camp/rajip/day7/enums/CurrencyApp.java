package id.b2camp.rajip.day7.enums;

public class CurrencyApp {
    public static void main (String[] args) {
        Currency rupiah = Currency.RUPIAH;
        Currency dollar = Currency.DOLLAR;
        Currency yen = Currency.YEN;
        Currency euro = Currency.EURO;
        System.out.println("Currency " + rupiah.getName() + " has symbol " + rupiah.getSymbol());
        System.out.println("Currency " + dollar.getName() + " has symbol " + dollar.getSymbol());
        System.out.println("Currency " + yen.getName() + " has symbol " + yen.getSymbol());
        System.out.println("Currency " + euro.getName() + " has symbol " + euro.getSymbol());
    }
}
