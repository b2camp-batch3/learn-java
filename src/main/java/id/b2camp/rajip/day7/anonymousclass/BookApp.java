package id.b2camp.rajip.day7.anonymousclass;

public class BookApp {
    /*
    anonymous class dipakai untuk pemanggilan method langsung.
    biasanya digunakan sekali pakai.
     */

    public static void main (String[] args) {

        /* ini cara instantiate nya lansung dengan constructor method nya */
        Book book = new Book () {
            @Override
            public void bookName () {
                System.out.println ("Rich Dad, Poor Dad");
            }
        };

        PublisherBook publisherBook = new PublisherBook () {
            @Override
            public void publishedBook (String date) {
            }
        };

//        /* instantiate bisa menggunakan lambda */
//        PublisherBook publisherBook = date -> System.out.println (date);

        book.bookName ();
        publisherBook.publishedBook ("20/01/2000");
    }
}
