package id.b2camp.rajip.day11;

import java.util.ArrayList;
import java.util.List;

public class ArrayLIstRajipApp {
    public static void main(String[] args) {
        List<String> fruits = new ArrayList<>(List.of("Apple", "Banana", "Lemon"));
        fruits.add("Orange");
        System.out.println(fruits);
        fruits.add(1, "Pir");
        fruits.remove(2);
        System.out.println(fruits);
        System.out.println(fruits.get(0));
    }
}