package id.b2camp.rajip.day11;

import java.util.LinkedList;
import java.util.List;

public class LinkedListRajipApp {
    public static void main(String[] args) {
        List<Double> doubleLinkedList = new LinkedList<>(List.of(2.0, 444.0, 12.0));
        System.out.println(doubleLinkedList);
        doubleLinkedList.add(0, 19.8);
        System.out.println(doubleLinkedList);
        System.out.println(doubleLinkedList.get(3));
    }
}