package id.b2camp.rajip.day11;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapRajipApp {
    public static void main(String[] args) {
        Map<String, String> users = new HashMap<>();
        users.put("a11", "mamat");
        users.put("a12", "mamat");
        users.put("a13", "rahmat");
        System.out.println(users);

        users.replace("a12", "mamat", "ahmad");
        System.out.println(users);
        System.out.println(users.get("a13"));

        Map<String, String> users2 = new TreeMap<>();
        users2.put("a11", "mamat");
        users2.put("a14", "mamat");
        users2.put("a10", "rahmat");
        System.out.println(users2);
    }
}