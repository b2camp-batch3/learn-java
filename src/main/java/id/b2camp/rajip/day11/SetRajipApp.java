package id.b2camp.rajip.day11;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SetRajipApp {
    public static void main(String[] args) {
        Set<String> albums = new HashSet<>(List.of("Jatuh bangun", "jatuh bangun", "Kupu-kupu malam", "Kupu-kupu malam", "Yellow", "YELLOW"));
        System.out.println(albums);
        albums.add("Yellow");
        System.out.println(albums);

        // Tree urutan ascending
        Set<String> albumsTree = new TreeSet<>(List.of("a", "A", "C", "B", "b"));
        System.out.println(albumsTree);

        TreeSet<Integer> numbersTree = new TreeSet<> (List.of(2, 3, 15, 106, 70, 9, 2, 1, 8, 10));
        System.out.println(numbersTree);
        System.out.println(numbersTree.ceiling(10));
        System.out.println(numbersTree.higher(10));
        System.out.println(numbersTree.floor(11));
        System.out.println(numbersTree.headSet(10));
    }
}