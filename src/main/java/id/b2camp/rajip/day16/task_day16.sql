CREATE TABLE public.rajip_table (
	id int4 NOT NULL,
	first_name varchar NOT NULL,
	last_name varchar NOT NULL,
	email varchar NOT NULL,
	city varchar NULL,
	CONSTRAINT rajip_table_pk PRIMARY KEY (id)
);

INSERT INTO rajip_table  (id, first_name, last_name, email, city)
VALUES
(1, 'Rajip', 'Pangestu', 'rajip@gmail.com', 'Bandung' ),
(2, 'Agus', 'Rangga', 'mamah@gmail.com', 'Bandung' ),
(3, 'Rahma', 'Rahmi', 'rahma@gmail.com', 'Jakarta' ),
(4, 'Dudu', 'Dudi', 'dudu.dudi@gmail.com', 'Medan' ),
(5, 'Ina', 'Rahmi', 'Rara@gmail.com', 'Semarang' );

UPDATE rajip_table
SET first_name = 'Rifal', last_name ='Rifaldi', email ='rifalgmail.com'
where id = 1;


INSERT INTO rajip_table  (id, first_name, last_name, email, city)
VALUES
(10, 'Indra', 'Lesmana', 'indra.lemana@gmail.com', 'Bekasi' );

delete from rajip_table
where id =10;

// Menampilkan semua isi rajip_table
select * from rajip_table rt

// menampilkan hanya coloum tertentu
select id, first_name , city  from rajip_table rt

// sorting city asc (orderby)
select id, first_name , city  from rajip_table rt
order by city asc

// top 4 by id use limit
select id, first_name , city  from rajip_table rt
order by id asc
limit 4
