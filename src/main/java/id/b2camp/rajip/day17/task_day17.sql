CREATE TABLE rajip.customers (
    id INT PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255)
);

-- Insert data for Customers
INSERT INTO rajip.customers (id, name, email)
VALUES
    (1, 'Andi', 'andi@gmail.com'),
    (2, 'Anisa', 'anisa@yahoo.com'),
    (3, 'Boby', 'boby@gmail.com'),
    (4, 'Cucu', 'cucu@gmail.com'),
   	(5, 'Rangga', 'rangga@gmail.com'),
   	(6, 'Budi', 'budi12@gmail.com');

CREATE TABLE rajip.products (
    id INT PRIMARY KEY,
    product_name VARCHAR(255),
    price numeric
);

-- Insert data for Products
INSERT INTO Products (id, product_name, price)
VALUES
    (201, 'Laptop HP', 8000000),
    (202, 'Smartphone Samsung', 5000000),
    (203, 'Monitor Dell', 2000000),
    (204, 'Keyboard Logitech', 500000),
    (205, 'Laptop Asus', 8500000),
    (206, 'Smartphone Iphone', 7500000);

CREATE TABLE rajip.orders (
    id INT PRIMARY KEY,
    order_date DATE,
    customer_id INT,
    FOREIGN KEY (customer_id) REFERENCES rajip.customers(id)
);

-- Insert data for Orders
INSERT INTO rajip.orders (id, order_date, customer_id)
VALUES
    (101, '2023-09-14', 1),
    (102, '2023-09-15', 2),
    (103, '2023-09-15', 3),
    (104, '2023-09-16', 4);


CREATE TABLE rajip.order_details (
    id INT PRIMARY KEY,
    order_id INT,
    product_id INT,
    quantity INT,
    FOREIGN KEY (order_id) REFERENCES rajip.orders(id),
    FOREIGN KEY (product_id) REFERENCES rajip.products(id)
);

-- Insert data for OrderDetails
INSERT INTO rajip.order_details (id, order_id, product_id, quantity)
VALUES
    (301, 101, 201, 2),
    (302, 101, 203, 1),
    (303, 102, 202, 3),
    (304, 102, 204, 2),
    (305, 103, 201, 1),
    (306, 103, 204, 1),
    (307, 104, 202, 1),
    (308, 104, 203, 3);


select * from rajip.customers c
order by c.name desc

select * from rajip.orders o

select * from rajip.products p

select * from rajip.order_details od

--referensi join
--https://codepolitan.com/blog/tujuh-teknik-join-di-sql-596c537f0deb3

select c.name,
o.order_date
from rajip.customers c
full join rajip.orders o on c.id = o.customer_id

select c.name,
o.order_date
from rajip.customers c
join rajip.orders o on c.id = o.customer_id

select c.name,
o.order_date
from rajip.customers c
left join rajip.orders o on c.id = o.customer_id

select c.name,
o.order_date
from rajip.customers c
right join rajip.orders o on c.id = o.customer_id

select p.product_name  from rajip.products p
where price > 5000000;

select c.name,
p.product_name ,
p.price,
od.quantity ,
sum(p.price * od.quantity) as total_spent
from rajip.customers c
left join rajip.orders o on c.id = o.customer_id
left join rajip.order_details od on o.id = od.order_id
left join rajip.products p on od.product_id = p.id
group by c.name,
p.product_name,
od.quantity,
p.price
having sum(p.price * od.quantity) > 1000000;

--select custumers ketika membeli prouduct
select c.name,
c.email ,
o.order_date ,
p.product_name ,
p.price
from rajip.customers c
join rajip.orders o on c.id = o.customer_id
join rajip.order_details od on o.id = od.order_id
join rajip.products p on od.product_id = p.id
where p.product_name = 'Laptop HP';


--Select 3 top Nama Pelanggan yang Memiliki Harga Pesanan Tertinggi
select c.name as customer_name, max(p.price * od.quantity) as highest_order_price
from rajip.customers c
join rajip.orders o on c.id = o.customer_id
left join rajip.order_details od on o.id = od.order_id
left join rajip.products p on od.product_id = p.id
group by c.name
order by highest_order_price desc
limit 3;

select rajip.customers.name, count(rajip.orders.id) as order_count
from rajip.customers
inner join rajip.orders on rajip.customers.id = rajip.orders.customer_id
group by rajip.customers.name
having count(rajip.orders.id) >= 1
order by order_count desc
limit 3;






