package id.b2camp.rajip.day12;

public class ImperativeStyleRajip {
    /*
    Imperative style dalam pemerograman java
    - berfokus pada cara program berjalan dengan memberikan urutan perintah
    yang dieksekusi langkah demi langkah
    - contohnya : penggunaan loop -> for, while, if juga IO
     */
    public static void main (String[] args) {
        System.out.println("Square of numbers :");
        for (int i = 1; i <= 10; i++) {
            int kuadrat = i * i;
            System.out.println("Square of " + i + " is " + kuadrat);
        }

        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int sumOfOddNumbers = 0;
        for (int number : numbers) {
            if (number % 2 != 0) {
                sumOfOddNumbers += number;
            }
        }
        System.out.println ("----");
        System.out.println("total odd number: " + sumOfOddNumbers);
    }
}
