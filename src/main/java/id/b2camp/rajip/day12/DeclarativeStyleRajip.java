package id.b2camp.rajip.day12;

import java.util.List;

public class DeclarativeStyleRajip {
    /*
    Declarative style di java
    contohnya penggunaan annotation Lombok
    - berfokus pada hasil
    - tidak khawatir akan langkah-langkahnya
    - contoh :  --annotation Lombok
                -- method size length
     */
    public static void main (String[] args) {

        List<Integer> numbers = List.of(1,2,3,4,5,6,7,8,9,10);
        double countEvenNumbers = numbers.stream()
                .filter(number -> number % 2 == 0)
                .count();

        System.out.println("countEvenNumbers " + countEvenNumbers);
    }
}
