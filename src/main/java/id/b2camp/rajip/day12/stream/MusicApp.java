package id.b2camp.rajip.day12.stream;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MusicApp {
    public static void main (String[] args) {
        InMemory inMemory = new InMemory ();
        List<Albums> albums = inMemory.albumsList ();
        List<Songs> songs = inMemory.songsList ();

        List<String> albumsNames = albums.stream ()
                .map (Albums::getName)
                .collect(Collectors.toList());
        System.out.println ("\nall Albums name : " + albumsNames);

        List<String> songsTitle =songs.stream ()
                .map (Songs::getTitle)
                .collect(Collectors.toList());
        System.out.println ("\nall songs title : " + songsTitle);

        // get year albums more 2000
        System.out.println ("\n Album more 2000");
        List<Albums> albumsAfter2000 = albums.stream ()
                .filter (a-> a.getYear () > 2000)
                .collect(Collectors.toList());
        albumsAfter2000.forEach (System.out::println);

        System.out.println ("\n---------\n");

        // Membuat map album with songs
        Map<String, List<Songs>> albumSongsMap = songs.stream()
                .collect(Collectors.groupingBy(Songs::getAlbumId));

        albums.forEach(album -> {
            System.out.println("Album: " + album.getName() + " (" + album.getYear() + ")");
            List<Songs> songsInAlbum = albumSongsMap.get(album.getId());
            if (songsInAlbum != null) {
                songsInAlbum.forEach(
                        song -> System.out.println(" - " + song.getTitle() + " (" + song.getDuration() + "s)"));
            }
            System.out.println();
        });

        // sorted songs most played
        List<Songs> songsMostPlayed =songs.stream ()
                .sorted ((x,y)->Integer.compare (y.getPlayed (), x.getPlayed ())) // sorted descending
                .collect(Collectors.toList());
        songsMostPlayed.forEach (System.out::println);

        /* sorted songs, title and artis ascending
        urutannya harus diperhatikan jika ingin title lalu artis
         */
        List<Songs> songsSortedByTitle = songs.stream()
                .sorted(Comparator.comparing (Songs::getArtis)) //artis
                .sorted(Comparator.comparing (Songs::getTitle))  // title
                .collect(Collectors.toList());
        System.out.println ("\n----\n");
        songsSortedByTitle.forEach(System.out::println);

        //Top 3
        List<Songs> topPlayedSongs = songs.stream()
                .sorted((song1, song2) -> Integer.compare(song2.getPlayed(), song1.getPlayed()))
                .limit(3)
                .collect(Collectors.toList());
        System.out.println ("\n------\n");

        System.out.println("Top 3 Played Songs :");
        for (int i = 0; i < topPlayedSongs.size(); i++) {
            System.out.println((i + 1) + ". " + topPlayedSongs.get(i).getTitle());
        }
    }
}
