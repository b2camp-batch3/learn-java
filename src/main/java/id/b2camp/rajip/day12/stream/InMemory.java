package id.b2camp.rajip.day12.stream;

import java.util.List;

public class InMemory {
    List<Albums> albumsList() {
        return List.of (
                new Albums ("album-1","Sinestesia", 2015),
                new Albums ("album-2","Meteora", 2003),
                new Albums ("album-3","Viva la Vida", 2008),
                new Albums ("album-4","Dizzy Up the Girl", 2001)
        );
    }

    List<Songs> songsList() {
        return List.of (
                new Songs("song-1", "Putih", "Efek Rumah Kaca", Genre.POP, 2012, 640, "album-1", 235648),
                new Songs("song-2", "In the End", "Linkin Park", Genre.ALTERNATIVE_ROCK, 2003, 216, "album-2", 789432),
                new Songs("song-3", "Viva la Vida", "Coldplay", Genre.POP_ROCK, 2008, 287, "album-3", 123456),
                new Songs("song-4", "Iris", "Goo Goo Dolls", Genre.ALTERNATIVE_ROCK, 1998, 289, "album-4", 987654),
                new Songs("song-5", "Kuning", "Efek Rumah Kaca", Genre.POP, 2015, 590, "album-1", 564738),
                new Songs("song-6", "Somewhere I Belong", "Linkin Park", Genre.ALTERNATIVE_ROCK, 2003, 223, "album-2", 876543),
                new Songs("song-7", "Clocks", "Coldplay", Genre.POP_ROCK, 2002, 309, "album-3", 349857),
                new Songs("song-8", "Slide", "Goo Goo Dolls", Genre.ALTERNATIVE_ROCK, 1998, 224, "album-4", 245631),
                new Songs("song-9", "Lagu Kesepian", "Efek Rumah Kaca", Genre.POP, 2007, 222, "album-1", 762489),
                new Songs("song-10", "Numb", "Linkin Park", Genre.ALTERNATIVE_ROCK, 2003, 186, "album-2", 432187),
                new Songs("song-11", "Iris", "Peterpan", Genre.ALTERNATIVE_ROCK, 2008, 289, "album-12", 97654),
                new Songs("song-12", "Iris", "Coldplay", Genre.ALTERNATIVE_ROCK, 2008, 360, "album-13", 107654)

        );
    }
}
