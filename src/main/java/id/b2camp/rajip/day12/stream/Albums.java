package id.b2camp.rajip.day12.stream;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Albums {
    private String id;
    private String name;
    private int year;

}
