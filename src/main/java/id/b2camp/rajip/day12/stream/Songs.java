package id.b2camp.rajip.day12.stream;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Songs {
    private String id;
    private String title;
    private String artis;
    private Genre genre;
    private int year;
    private int duration;
    private String albumId;
    private int played;
}
