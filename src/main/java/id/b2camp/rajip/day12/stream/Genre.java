package id.b2camp.rajip.day12.stream;

public enum Genre {
    POP_ROCK,
    POP,
    ALTERNATIVE_ROCK,
}
