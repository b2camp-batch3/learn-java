package id.b2camp.rajip.day12;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FunctionalStyleRajip {
    /*
    Functional style
    berfokus pada fungsi
    Contohnya :
    - lambda
    - interface functional
    - stream
     */

    public static void main (String[] args) {

        // contoh funtional interface
        // instance Predicate menggunakan ekspresi lambda
        Predicate<Integer> isEven = number -> number % 2 == 0;
        System.out.println(isEven.test(4));
        System.out.println(isEven.test(7));

        List<Integer> numbers = List.of(1,2,3,4,5,6,7,8,9,10);
        int sum = numbers.stream()
                .reduce(0, Integer::sum);
        System.out.println ("----");
        System.out.println (sum);

        List<String> names = Arrays.asList("Ali","Cucu", "Bobby","Bob", "Rahmat", "Ani", "Agung");
        List<String> filteredNames = names.stream()
                .filter(name -> name.length() > 3)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println ("----");
        filteredNames.forEach (System.out::println);

    }
}
