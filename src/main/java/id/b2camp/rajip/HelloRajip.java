package id.b2camp.rajip;

import java.math.BigDecimal;
import java.time.LocalDate;


public class HelloRajip {

    /* The main difference between primitive and non-primitive data types are:
- Primitive types are predefined (already defined) in Java. Non-primitive types are created by the programmer and is not defined by Java (except for String).
- Non-primitive types can be used to call methods to perform certain operations, while primitive types cannot.
- A primitive type has always a value, while non-primitive types can be null.
- A primitive type starts with a lowercase letter, while non-primitive types starts with an uppercase letter.
- Examples of non-primitive types are Strings, Arrays, Classes, Interface, etc. You will learn more about these in a later chapter.
*/
    public static void main(String[] args) {
//        TODO make 5 type data primitive and non primitive

//        Primitive
       byte a = 1;
       short b = -22;
       int c = 20392032;
       long d = -1239120381;
       float e = 2.333f;
       boolean f = true;
       char g = 'a';

//        Non Primitive
        String name = "Rajip";
        Integer age = Integer.valueOf(26);
        Character myChar = 'B';
        Boolean isMarried = false;
        LocalDate now = LocalDate.now();
        BigDecimal myBigDecimal = BigDecimal.valueOf(10_900_800);

        System.out.println("Primitive type");
        System.out.println("byte: \t"+a);
        System.out.println("short: \t"+b);
        System.out.println("int: \t"+c);
        System.out.println("long: \t"+d);
        System.out.println("float: \t"+e);
        System.out.println("boolean: \t"+f);
        System.out.println("char: \t"+g);

        System.out.println("\nNon-Primitive type\n");
        System.out.println("my name is " + name + "\n" + "my age is "+ age);
        System.out.println(isMarried);
        System.out.println(now);
        System.out.println(myBigDecimal);
        System.out.println(myChar);
    }
}
