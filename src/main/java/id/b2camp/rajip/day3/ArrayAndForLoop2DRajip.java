package id.b2camp.rajip.day3;

import java.util.Arrays;

public class ArrayAndForLoop2DRajip {
    public static void main (String[] args) {
        String[][] biodata = new String[2][3];
        biodata[0][0] = "Rajip";
        biodata[0][1] = "Pangestu";
        biodata[0][2] = "Uhuyyy";
        for (String[] row : biodata)
            System.out.println (Arrays.toString (row));

        System.out.println ("------");

        int[][] int2d = new int[3][3];
        int2d[0][0] = 1;
        int2d[1][1] = 1;
        int2d[2][2] = 1;
        for (int[] row : int2d)
            System.out.println (Arrays.toString (row));
    }
}
