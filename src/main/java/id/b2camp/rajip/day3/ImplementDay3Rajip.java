package id.b2camp.rajip.day3;

import java.util.Scanner;

public class ImplementDay3Rajip {
    public static void main (String[] args) {

        Scanner input = new Scanner (System.in);
        System.out.println ("Name :");
        String name = input.next ();
        System.out.println ("Code motor :");
        int code = input.nextInt ();
        System.out.println ("DownPayment :");
        int downPayment = input.nextInt ();
        System.out.println ("How long (Month) :");
        int month = input.nextInt ();

        double credit = motorCredit (motorPrice (code), downPayment,month);

//        ini pemanggilan kelas Motor
        Motor motor = new Motor (sayHello (name),
                code,
                motorType (code),
                motorPrice (code),
                downPayment,
                month);

        if (credit >= 0 && credit <= motor.motorPrice){
            System.out.println (motor.name);
            System.out.println ("code : "+ motor.motorCode);
            System.out.println ("motor type : " + motor.motorType);
            System.out.println ("motor price : "+ motor.motorPrice);
            System.out.println ("your downPayment : " + motor.downPayment);
            System.out.println ("in : " + motor.month +" month");
            System.out.println ("------");
            System.out.println (name + ", your credit motor is IDR " + credit +" for "+ month +" month");
        } else {
            System.out.println ("invalid progress credit motor");
        }

    }

//    class & object
    static class Motor {
        String name;
        int motorCode;
        String motorType;
        int motorPrice;
        int downPayment;
        int month;

    public Motor (String name, int motorCode, String motorType, int motorPrice, int downPayment, int month) {
        this.name = name;
        this.motorCode = motorCode;
        this.motorType = motorType;
        this.motorPrice = motorPrice;
        this.downPayment = downPayment;
        this.month = month;
    }
}

//    method
    static String sayHello (String name){
        return "Hello, " +name;
    }
    static String motorType (int code){
        switch (code) {
            case 111 :
                return "Honda Beat";
            case 112 :
                return "Honda Vario";
            case 113 :
                return "Honda PCX";
            case 114 :
                return "Honda ADV";
        }
        return null;
    }

    static int motorPrice (int code){
        switch (code) {
            case 111 :
                return 18_900_000;
            case 112 :
                return 29_513_000;
            case 113 :
                return 36_085_000;
            case 114 :
                return 39_400_000;
        }
        return 0;
    }

    static double motorCredit (int priceMotor, int downPayment, int month) {
        int debt = priceMotor - downPayment;
        double rate = 0.15/12;
        double totalRate = debt * rate;

        return (debt+totalRate)/month;
    }
}
