package id.b2camp.rajip.day3;

public class WhileAndDoWhileRajip {
    public static void main (String[] args) {
        int i = 0;
        while (i < 5) {
            System.out.println(i);
            i++;
        }

        char myChar = 'A';
        while (myChar<'Z'){
            System.out.println (myChar);
            myChar++;
        }

        System.out.println ("----");
        int x = 4;
        do {
            System.out.println (x);
            x++;
        } while (x < 10);

    }
}
