package id.b2camp.rajip.day3;

public class MethodRajip {
    public static void main (String[] args) {
        System.out.println (sayHello ("Rajip"));
        System.out.println (total (200000,12));
        totalVoid ();
    }

    static String sayHello (String name){
        return "Hello " + name;
    }

    static  double total (double price, int quantity){
        return price*quantity;
    }
    static void totalVoid(){
        double price = 20000;
        int quantity = 4;
        System.out.println ("total is " +price*quantity);
    }
//    void tidak mengembalikan nilai yang dimuat tapi hanya melakukan tindakan.
//    sedangkan String, int, double dsb mengambalikan nilai ("return").
}
