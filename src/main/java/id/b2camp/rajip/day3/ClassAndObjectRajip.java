package id.b2camp.rajip.day3;

public class ClassAndObjectRajip {
    public static void main (String[] args) {
        Biodata biodataRajip = new Biodata ("iXctisa001", "Rajip", "Gatsu", 26, true);
        String gender = biodataRajip.isMale ? "Male" : "Female";
        System.out.println ("id: "+biodataRajip.id);
        System.out.println ("name: "+biodataRajip.name);
        System.out.println ("address: "+biodataRajip.address);
        System.out.println ("age: "+biodataRajip.age);
        System.out.println ("gender: "+gender);
    }

    static class Biodata{
        String id;
        String name;
        String address;
        int age;
        boolean isMale;

        public Biodata (String id, String name, String address, int age, boolean isMale) {
            this.id = id;
            this.name = name;
            this.address = address;
            this.age = age;
            this.isMale = isMale;
        }
    }
}
