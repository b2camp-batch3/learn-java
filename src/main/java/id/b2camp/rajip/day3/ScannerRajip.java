package id.b2camp.rajip.day3;

import java.util.Scanner;

public class ScannerRajip {
    public static void main (String[] args) {
        Scanner input = new Scanner (System.in);
        System.out.println ("Hello welcome to The Ayam Bank\n" +
                "Please complete form below");
        System.out.println ("Name :");
        String name = input.next ();
        System.out.println ("age :");
        int age = input.nextInt ();
        System.out.println ("your credit :");
        double dNumber = input.nextDouble ();
        System.out.println ("the data entered is correct");
        boolean bool = input.nextBoolean ();

        System.out.println (name);
        System.out.println (age);
        System.out.println (dNumber);
        System.out.println (bool);
    }
}
