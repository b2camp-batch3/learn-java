package id.b2camp.rajip.day8;

import id.b2camp.rajip.day8.validator.InvalidAuthorException;
import id.b2camp.rajip.day8.validator.InvalidTitleException;
import id.b2camp.rajip.day8.validator.InvalidYearException;
import id.b2camp.rajip.day8.validator.ValidationBookUtils;

import java.util.Scanner;

public class AddBookApp {
    /*
    Error di java representasikan dengan exception
    Jika ingin membuat class exception maka harus membuat class yang extends ke Throwable
     */
    public static void main (String[] args) throws InvalidYearException, InvalidTitleException, InvalidAuthorException {

        Books books = new Books ();
        ValidationBookUtils validationBookUtils = new ValidationBookUtils ();

        Scanner input = new Scanner (System.in);
        System.out.println ("Input title :");
        String title = input.nextLine ();
        System.out.println ("Input author :");
        String author = input.nextLine ();
        System.out.println ("Input year :");
        int year = input.nextInt ();


    /*
    - Saat akan memanggil sebuah exception maka kita wajib menggunakan try catch
    - Jika kita tidak menggunakan try catch otomatis program akan berhenti atau error saat dijalankan
     */

        try {
            validationBookUtils.validateTitle (title);
            validationBookUtils.validateAuthor (author);
            validationBookUtils.validateYear (year);
        } catch (InvalidTitleException | InvalidAuthorException e) {
            throw new InvalidTitleException (e.getMessage ());
        } catch (InvalidYearException e){
            throw new InvalidYearException ("Tahun gagal diinput " + e.getMessage ());
        } finally {
            System.out.println ("Hasil yang akan dieksekusi walaupun ada error");
        }

        System.out.println ("Data books " + books.getTitle () + ", telah sesuai" );


//        contoh pemanggilan runtimeException

//        validationBookUtils.validateWhitRuntimeException (title, author, year);
//        System.out.println ("Ini tidak akan dieksekusi jika ada exception"); /* Berbeda dengan exception */
    }
}
