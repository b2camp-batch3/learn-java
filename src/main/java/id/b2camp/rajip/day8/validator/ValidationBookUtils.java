package id.b2camp.rajip.day8.validator;


public class ValidationBookUtils {
    /*
    Setiap hasil yang berkemungkinan Exception(error),
    harus dikembalikan dengan "throw new"
     */
    public void validateTitle(String title) throws InvalidTitleException{
        if (title.isEmpty() || title.isBlank()) {
            throw new InvalidTitleException ("Title cannot be blank");
        } else if (title.equalsIgnoreCase("null")) {
            throw new InvalidTitleException ("Title cannot be null");
        }
    }

    public void validateAuthor(String author) throws InvalidAuthorException{
        if (author.isEmpty() || author.isBlank()) {
            throw new InvalidAuthorException ("Author cannot be blank");
        } else if (author.equalsIgnoreCase("null")) {
            throw new InvalidAuthorException ("Author cannot be null");
        }
    }

    public void validateYear(int year) throws InvalidYearException {
        if(year < 1900 || year > 2100){
            throw new InvalidYearException ("Invalid year, Year must > 1900");
        }
    }

    /*RuntimeException
    * tidak wajib untuk di throw*/
    public void validateWhitRuntimeException(String title, String author, int year) /*throws RuntimeException*/{
    if (title.isEmpty() || title.isBlank() && author.isEmpty () || author.isBlank ()) {
        throw new RuntimeException ("Title / author cannot be blank");
    } else if (title.equalsIgnoreCase("null")) {
        throw new RuntimeException ("Title / author cannot be null");
    } else if (year < 1900 || year > 2100) {
        throw  new RuntimeException ("Invalid year");
    }
    }
}
