package id.b2camp.rajip.day8.validator;

public class InvalidTitleException extends Throwable{
    public InvalidTitleException (String message) {
        super(message); // message adalah field dari Throwable
    }
}
