package id.b2camp.rajip.day8.validator;

public class InvalidAuthorException extends Throwable{
    public InvalidAuthorException (String message){
        super(message);
    }
}
