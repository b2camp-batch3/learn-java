package id.b2camp.rajip.day8.validator;

public class InvalidYearException extends Throwable{
    public InvalidYearException (String message){
        super(message);
    }
}
