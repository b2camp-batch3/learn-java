package id.b2camp.rajip.day4;

import java.util.Scanner;

public class SimpleCoffeeShopAppRajip {

    static Scanner input = new Scanner(System.in);
    static String[] orderList = new String[10];

    public static void main(String[] args) {
        String name;
        int code;
        int quantity;
        double totalPayment = 0;
        int orderCount = 0;

        System.out.println("Enter your name");
        name = input.next();

        menu();

        while (true) {
            cart();

            System.out.println("Enter your code order (0 to finish)");
            code = input.nextInt();
            if (code == 0) {
                break;
            } else if (code > 0 && code< 10) {
                System.out.println ("invalid code");
            } else if (code > 15) {
                System.out.println ("invalid code");
            } else {
                System.out.println("Enter your quantity");
                quantity = input.nextInt();

                int price = price(code);
                String productName = productName(code);
                double total = totalPayment(price, quantity);

                String order = productName + " / Price: " + price + " / Quantity: " + quantity + " / Total: " + total;
                orderList[orderCount] = order;
                totalPayment += total;
                orderCount++;
                System.out.println("Added to order: " + order + "\n");
            }
        }

        System.out.println("Hello " + name + ", here's your order:");
        System.out.println("Product / Price / Quantity / Total");
        for (int i = 0; i < orderCount; i++) {
            if (orderList[i] != null) {
                System.out.println(orderList[i]);
            }
        }
        System.out.println("Total Payment: " + totalPayment);
    }

    static void cart() {
        for (int i = 0; i < orderList.length; i++) {
            String menu = orderList[i];
            int number = 1 + i;

            if (menu != null) {
                System.out.println(number + ". " + menu);
            }
        }
    }

    static void menu() {
        System.out.println("=====Menu=====");
        System.out.println("Code\t Product Name\t Price");
        System.out.println(11 + "\t\t Long Black \t 15K\n" +
                12 + "\t\t Vietnam Drip \t 20K\n" +
                13 + "\t\t Cappuccino \t 28K\n" +
                14 + "\t\t Latte \t\t\t 25K\n" +
                15 + "\t\t Mineral Water \t 5K\n");
    }

    static int price(int code) {
        switch (code) {
            case 11:
                return 15000;
            case 12:
                return 20000;
            case 13:
                return 28000;
            case 14:
                return 25000;
            case 15:
                return 5000;
            default:
                return 0;
        }
    }

    static String productName(int code) {
        switch (code) {
            case 11:
                return "Long Black";
            case 12:
                return "Vietnam Drip";
            case 13:
                return "Cappuccino";
            case 14:
                return "Latte";
            case 15:
                return "Mineral Water";
            default:
                return "";
        }
    }

    static double totalPayment(int price, int quantity) {
        return price * quantity;
    }
}
