package id.b2camp.rajip.day5;

public class NotesApp {
    public static void main (String[] args) {
        Note note = new Note ();
        TextNote textNote = new TextNote ();
        ImageNote imageNote = new ImageNote ();

        note.title = "Parents";
        note.label = "This is Parent class";

        textNote.title = "Text note";
        textNote.label = "Text is child in Note";
        textNote.text = "This is about inheritance";

        imageNote.title = "wallpaper";
        imageNote.label = "this wallpaper desktop PC";
        imageNote.url = "123.image.com";

        note.displayNote ();
        System.out.println ("-----");
//        ini adalah method dari kelas parents yang diakses child
        textNote.displayNote ();
        System.out.println (textNote.text);
        System.out.println ("-----");
//        ini adalah pemanggilan method yang sudah di-overriding
        imageNote.displayNote ();
    }
}

class Note{
    String title;
    String label;

    void displayNote (){
        System.out.println ("Title : " + title +
                "\n" +"Content : " + label);
    }
}


/* ini adalah inheritance Note,
biasanya bisa dilihat dengan class xxx "extends" Parents.
attribute dan method yang dimiliki kelas parents otomatis diwariskan ke child nya.
 */

class TextNote extends Note {
    String text;

}

class ImageNote extends Note {
    String url;


/*  ini adalah method overriding
mendeklarasikan ulang method yang ada di class parents
 */
    void displayNote(){
        System.out.println ("title : " + title);
        System.out.println ("label : "+ label);
        System.out.println ("url : " + url);
    }
}