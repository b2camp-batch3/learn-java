package id.b2camp.rajip.day5;

public class SalaryCalculationApp {
    public static void main (String[] args) {

/*  polymorphism (banyak bentuk)
    Kemampuan sebuah object berubah bentuk ke bentuk lain
 */
        Employee employee = new Employee ("Adi");
        Employee salariedEmployee = new SalariedEmployee ("Budi", 40000000);
        Employee hourlyEmployee = new HourlyEmployee ("Rajip", 20000,10);
        Employee overTime = new OverTime ("Ujang", 15000, 5);

        System.out.println ("example Polymorphism\n");

        System.out.println ("name : " + employee.name + "\nsalary : " + employee.calculateSalary ());
        System.out.println ("\n name : " + salariedEmployee.name + "\nsalary : " + salariedEmployee.calculateSalary ());
        System.out.println ("\n name : " + hourlyEmployee.name + "\nhours : " + hourlyEmployee.calculateSalary ());
        System.out.println ("\n name : " + overTime.name + "\novertime : " + overTime.calculateSalary ());


//        ini adalah variable hiding
        Employee employeeUser = employee;

//        gunakan scope agar lebih eksplisit
        Employee hourlyEmployeeUser = (Employee) hourlyEmployee;

        employeeUser.name = "Indra";
        hourlyEmployeeUser.name = "Udin";



        System.out.println ("\n ini menggunakan variable hiding");
        System.out.println (employeeUser.name);
        System.out.println (hourlyEmployeeUser.name);


//        Polymorphism tidak dapat mengakses langsung field atau method yang ada di class child nya

    /*    Employee cek = new HourlyEmployee ();
        double hourlyRateCek = ((HourlyEmployee) cek).hourlyRate;
        double workedCek = ((HourlyEmployee) cek).hourlyRate;
        double calculateCek = ((HourlyEmployee) cek).calculateSalary (); // ini field cek jadi output 0

        hourlyRateCek = 200000;
        workedCek = 4;
        System.out.println ("\n----------\n");

        System.out.println (workedCek);
        System.out.println (hourlyRateCek);
        System.out.println (calculateCek);

        OverTime overTimeUser = (OverTime) new Employee ();
        String a = overTimeUser.name = "Aji";
        Double b = overTimeUser.hourlyRate = 50000;
        Double c = overTimeUser.hoursWorked = 4;
        Double total = overTimeUser.calculateSalary ();

        System.out.println (total);

     */

    }
}

class Employee {
    String name;

    double calculateSalary () {
        return 0;
    }

    Employee (String name) {
        this.name = name;
    }
/* ini adalah constructor overloading
   membuat parameter sesuai yang dibutuhkan dan lebih flexible
 */
    Employee (){

    }
}

class SalariedEmployee extends  Employee {
    double monthlySalary;

//    constructor overloading
    SalariedEmployee (String name, double monthlySalary) {
        super (name);
        this.monthlySalary = monthlySalary;
    }
    SalariedEmployee (double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }
    SalariedEmployee (String name) {
        super (name);
    }
    SalariedEmployee(){

    }
}

class HourlyEmployee extends Employee {
    double hourlyRate;
    double hoursWorked;

    public HourlyEmployee (String name, double hourlyRate, double hoursWorked) {
        super (name);
        this.hourlyRate = hourlyRate;
        this.hoursWorked = hoursWorked;
    }

    public HourlyEmployee (double hourlyRate, double hoursWorked) {
        this.hourlyRate = hourlyRate;
        this.hoursWorked = hoursWorked;
    }
    HourlyEmployee (String name) {
        super (name);
    }

    HourlyEmployee(){

    }

//    method overriding
    double calculateSalary () {
        return hourlyRate * hoursWorked;
    }
}

class OverTime extends  HourlyEmployee {

    OverTime (String name, double hourlyRate, double hoursWorked) {
        super (name, hourlyRate, hoursWorked);
    }

    OverTime (double hourlyRate, double hoursWorked) {
        super (hourlyRate, hoursWorked);
    }

    OverTime (String name) {
        super (name);
    }

    //    method overriding
    double calculateSalary () {
        return hourlyRate * hoursWorked * 2;
    }
}