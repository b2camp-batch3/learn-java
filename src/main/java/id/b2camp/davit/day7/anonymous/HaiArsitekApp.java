package id.b2camp.davit.day7.anonymous;

public class HaiArsitekApp {

    public static void main(String[] args) {

        HaiArsitek english = new HaiArsitek() {
            @Override
            public void sayHello() {
                System.out.println("Hai ");

            }

            @Override
            public void sayHello(String name) {
                System.out.println("Hai " + name);

            }
        };
        english.sayHello();
        english.sayHello("Ujang Codet");
    }
}
