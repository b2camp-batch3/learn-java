package id.b2camp.davit.day7.anonymous;


/**
 * Anonymous Class
 * class tanpa nama
 * kemampuan mendeklarasikan class, sekaligus meng-instanisiasi objectnya secara langsung
 * anonymous class sebenarnya termasuk inner class, dimana outer class nya adalah tempat dimana kita membuat anonymous class tersebut
 * anonymous class sangat cocok ketika kita berhadapan dengan kasus membuat implementasi interface atau abstract class sederhana, tanpa harus membuat implementasi class nya.
 */

public interface HaiArsitek {

    void sayHello();

    void sayHello(String name);
}
