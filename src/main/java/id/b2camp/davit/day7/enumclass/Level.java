package id.b2camp.davit.day7.enumclass;

public enum Level {
    STANDAR ("Standard Level"),
    PREMIUM ("Premium Level"),
    VIP ("VIP Level");

    private String description;

    Level (String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }
}
