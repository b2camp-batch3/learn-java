package id.b2camp.davit.day7.enumclass;

public class CustomerApp {
    public static void main(String[] args) {


        Customer customer = new Customer();
        customer.setName("Hatano Yui");
        customer.setLevel(Level.VIP);

        System.out.println(customer.getName());
        System.out.println(customer.getLevel());
        System.out.println(customer.getLevel().getDescription());
    }
}
