package id.b2camp.davit.day7.statickeyword;

/**
 * kode : Static Method
 */

public class Math {
    public static int sum (int... values){
        int total = 0;
        for (var value : values){
            total += value;
        }
        return total;
    }
}
