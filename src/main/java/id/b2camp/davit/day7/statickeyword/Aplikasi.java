package id.b2camp.davit.day7.statickeyword;

/**
 * kode : Static Block
 */

public class Aplikasi {
    public static final int processor;

    static {
        System.out.println("mengakses class aplikasi");
        processor = Runtime.getRuntime().availableProcessors();
    }
}
