package id.b2camp.davit.day7.statickeyword;

public class StaticCalling {

    public static void main(String[] args) {
        System.out.println(Constant.application);
        System.out.println(Constant.version);

        System.out.println(Math.sum(1,1,1,1,1,1,1,1,1,1,1,1,1));

        Country.City city = new Country.City();
        city.setName("Cimahi");

        System.out.println(city.getName());

        System.out.println(Aplikasi.processor);
    }
}
