package id.b2camp.davit.day7.finalkeyword;

/**
 * Final Class
 * jika digunakan di variabel, maka variabel tersebut tidak bisa berubah lagi datanya
 * final pun bisa digunakan di class, dimana jika kita menggunakan kata kunci final sebelum class, maka kita mendandakan bahwa class tersebut tidak bisa diwariskan lagi
 * secara otomatis semua class child nya akan error
 */

/**
 * Final Method
 * kata kunci final juga bisa digunakan di method
 * jika sebuah method kita tambahkan kata kunci final, maka artinya method tersebut tidak bisa di override lagi di class child nya
 */

public class SosialMedia {
    String name;
}
class Instagram extends SosialMedia{

    final void login (String username, String password){
        // isi method
    }

}

class FakeInstagram extends SosialMedia{

//    ERROR
//    void login (String username, String password){
        // isi method
}
