package id.b2camp.davit.day10.genericapp;


/**
 * Generic class adalah class atau interface yang memiliki parameter type
 * generic parameter type
 * a. E -- Element (biasa digunakan di collection dan struktur data)
 * b. K -- Key
 * c. N -- Number
 * d. T -- Type
 * e. V -- Value
 * S.U.V -- 2nd, 3rd, 4th types
 */

public class MyData <T> {

    private T data;

    public MyData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
