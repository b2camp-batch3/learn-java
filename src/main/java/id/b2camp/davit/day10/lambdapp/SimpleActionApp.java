package id.b2camp.davit.day10.lambdapp;

import id.b2camp.davit.day10.lambda.DjarumAction;

public class SimpleActionApp {

    public static void main(String[] args) {

        DjarumAction djarumAction = new DjarumAction() {
            @Override
            public String action(String name) {
                return "Budi Ruhiat";
            }
        };

        System.out.println(djarumAction.action("Budi Ruhiat"));

        // Versi Lambda ( Lebih Sederhana )

//        DjarumAction djarumAction1 = (String name) -> {
//            return "Budi Ruhiat";
//        };
//        System.out.println(djarumAction1.action("Budi Ruhiat"));


        // Lambda dengan parameter

//        DjarumAction djarumAction2 = (String value) -> {
//            return "Hai" + value;
//        };
//        DjarumAction djarumAction3 = (name) -> {
//            return "Hello" + name;
//        };

        // Lambda Tanpa Blok

        DjarumAction djarumAction4 = (String value) -> "Logitech" + value;

        DjarumAction djarumAction5 = (value) -> "M22" + value;

        DjarumAction djarumAction6 = value -> "Stringray" + value;
    }
}
