package id.b2camp.davit.day10.generic;

import id.b2camp.davit.day10.genericapp.Pair;

public class MyPair {

    public static void main(String[] args) {

        Pair<String, Integer> pair = new Pair<String, Integer>("Abah", 63);
        System.out.println(pair.getFirst());
        System.out.println(pair.getSecond());
    }
}
