package id.b2camp.davit.day10.generic;

import id.b2camp.davit.day10.genericapp.MyData;

public class MyGeneric {

    public static void main(String[] args) {

        MyData<String> stringMyData = new MyData<String>("Abah");
        MyData<Integer> intergerMyData = new MyData<Integer>(15);

        String stringValue = stringMyData.getData();
        Integer integerMyData = intergerMyData.getData();

        System.out.println(stringValue);
        System.out.println(integerMyData);
    }
}
