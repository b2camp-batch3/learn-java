package id.b2camp.davit.day10.optional;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class SkyNet {

    public static void main(String[] args) {
        taroNet("Onime No Kyo");
        String name = null;

        taroNet(null);

    }

    public static void taroNet (String name){

//        Optional.ofNullable(name)
//                .map(String::toUpperCase) // replace method reference
//                .ifPresent(value -> System.out.println("Hello " + value));



        Optional.ofNullable(name)
                .map(String::toUpperCase) // replace method reference
                .ifPresentOrElse(
                        value -> System.out.println("Hello " + value),
                () -> System.out.println("Hai ")
                );

        String upperName = Optional.ofNullable(name)
                .map(String::toUpperCase)
                .orElse("Mikasa");
        System.out.println("Hello " + upperName);



//
//
//
//        Optional<String> optionalName = Optional.ofNullable(name);


        // Anonymous Class tanpa menggunakan lambda
//        Optional<String> optionalNameUpper = optionalName.map(new Function<String, String>() {
//            @Override
//            public String apply(String name){
//                return name.toUpperCase();
//            }
//        });

//        Optional<String> optionalNameUpper1 = optionalName.map(value -> value.toUpperCase()); // optional masih manual

//        optionalNameUpper1.ifPresent(new Consumer<String>() {
//            @Override
//            public void accept(String value) {
//                System.out.println("Hello" + value);
//            }
//        });

//        optionalNameUpper1.ifPresent(value -> System.out.println("Hello " + value)); // optional masih manual

//        if check null
//        if (name != null){
//            String upperName = name.toUpperCase();
//            System.out.println("Hello " + upperName);
//        }
    }
}
