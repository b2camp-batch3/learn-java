package id.b2camp.davit.day10.lambda;

@FunctionalInterface
public interface DjarumAction {

    String action(String name);
}
