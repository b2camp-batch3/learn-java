package id.b2camp.davit.day5.inher.typeb;

class Parent {
    String name;
    void doIt(){
        System.out.println("Do it in parent");
    }
}
class Child extends Parent{
    String name;
    void doIt(){
        System.out.println("Do it in Child");
        System.out.println("Parent name is " + super.name);
    }
}
