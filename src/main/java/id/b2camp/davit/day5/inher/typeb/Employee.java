package id.b2camp.davit.day5.inher.typeb;

class Employee {

    String name;

    Employee (String name){
        this.name = name;
    }

    void sayHello(String name){
        System.out.println("Hi " + name + ", My Name is Employee " + this.name);
    }
}
