package id.b2camp.davit.day5.inher.typeb;

class Direktur extends Manager{

    Direktur(String name){
        super(name);
    }
    void sayHello(String name){
        System.out.println("Hi " + name + ", My Name is Direktur " + this.name);
    }

}
