package id.b2camp.davit.day5.inher;



public class ShinobiStrike {
    public static void main(String[] args) {
        Hero hero1 = new Hero("Uchiha Madara", 115, 645);
        HeroStrength hero2 = new HeroStrength("Senju Hashirama", 120, 650);

        hero1.display();
        hero2.display();

        hero1.attack(hero2);
        hero2.attack(hero1);

        hero1.display();
        hero2.display();
    }
}
