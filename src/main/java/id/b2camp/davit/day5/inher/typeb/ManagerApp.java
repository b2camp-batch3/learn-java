package id.b2camp.davit.day5.inher.typeb;

public class ManagerApp {
    public static void main(String[] args) {

        var manager = new Manager("Dedi Gunaedi");
        manager.sayHello("Dodi Irawan");

        var direktur = new Direktur("Iman Firmansyah");
        direktur.sayHello("Dodi Irawan");

    }
}
