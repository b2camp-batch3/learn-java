package id.b2camp.davit.day5.inher.typeb;

public class PolymorphApp {
    public static void main(String[] args) {

        Employee employee = new Employee("Huda");
        employee.sayHello("Adryan");

        employee = new Manager("Sentot");
        employee.sayHello("Adryan");

        employee = new Direktur("David");
        employee.sayHello("Adryan");

        sayHello(new Employee("Huda"));
        sayHello(new Manager("Sentot"));
        sayHello(new Direktur("David"));
    }

    static void sayHello (Employee employee){
        if (employee instanceof Direktur){
            Direktur direktur = (Direktur) employee;
            System.out.println("Hello Direktur " + direktur.name);
        } else if (employee instanceof Manager) {
            Manager manager = (Manager) employee;
            System.out.println("Hello Manager " + manager.name);
        } else {
            System.out.println("Hello " + employee.name);
        }

    }

}
