package id.b2camp.davit.day5.polyapp.polymorph;

    public class HeroStrength extends Hero{
        String type = "Strength";

        HeroStrength(String nama){
            super(nama);
        }

        @Override
        void display(){
            super.display();
            System.out.println("Type \t: " + this.type);
        }
    }
