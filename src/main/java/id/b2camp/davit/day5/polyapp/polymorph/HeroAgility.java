package id.b2camp.davit.day5.polyapp.polymorph;

public class HeroAgility extends Hero{
    String type = "Agility";

    HeroAgility(String nama){
        super(nama);
    }

    @Override
    void display(){
        super.display();
        System.out.println("Type \t: " + this.type);
    }

    void showoff(){
        System.out.println("Aing hero Agility!!");
    }
}
