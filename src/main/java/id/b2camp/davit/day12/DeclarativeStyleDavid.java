package id.b2camp.davit.day12;

import java.util.List;

public class DeclarativeStyleDavid {

    public static void main(String[] args) {

        List<String> kawasakiMotors = List.of("ZX 25", "ZX 3", "ZX 4", "ZX 636", "ZX 10R", "ZX 10RR", "H2");

        if (kawasakiMotors.contains("ZX 636")) {
            System.out.println(true + ", Koleksi Motor Kawasaki : " + kawasakiMotors);
        }

    }
}
