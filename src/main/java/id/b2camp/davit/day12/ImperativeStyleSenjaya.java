package id.b2camp.davit.day12;

import java.util.List;

public class ImperativeStyleSenjaya {

    public static void main(String[] args) {

        List<Integer> numbers = List.of(0, 1, 40, 77, 80, 5, 62, 33, 84, 9, -17, -23, -89);

        System.out.println("Bilangan Genap: ");
        for (int number : numbers) {
            if (number % 2 == 0) {
                System.out.print(number + ", ");
            }
        }

        System.out.println("\n");

        System.out.println("Bilangan Ganjil: ");
        for (int number : numbers) {
            if (number % 2 != 0) {
                System.out.print(number + ", ");
            }
        }

        System.out.println("\n");

        System.out.println("Bilangan positif : ");
        for (int number : numbers) {
            if (number > 0) {
                System.out.print(number + ", ");
            }
        }

        System.out.println("\n");

        System.out.println("Bilangan Negatif: ");
        for (int number : numbers) {
            if (number < 0) {
                System.out.print(number + ", ");
            }
        }



    }
}