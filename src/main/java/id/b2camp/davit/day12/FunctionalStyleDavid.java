package id.b2camp.davit.day12;


import java.util.List;
import java.util.stream.Collectors;

public class FunctionalStyleDavid {

    public static void main(String[] args) {

        List<String> names = List.of("Abraham", "Lilianna", "Jessica", "Kasumi", "Hayabusa", "Ujang");
        if (names.contains("Hayabusa")) {
            System.out.println("Hayabusa ditemukan");
        } else {
            System.out.println("Hayabusa tidak ditemukan");
        }
        System.out.println("-----------------------------------------------------------------------");
        List<String> listname = List.of("Kevin", "David", "Illithya", "Luciana", "Michael", "Asep", "Mia");
        List<String> names1 = listname.stream()
                .filter(name -> name.length() < 6)
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        names1.forEach(System.out::println);
        System.out.println("-----------------------------------------------------------------------");

        List<String> lastname = List.of("Adrian", "Ivanka", "Iwan", "Haning", "Fadia", "Bianca");
        List<String> names2 = lastname.stream()
                .filter(name -> name.length() != 5)
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        names2.forEach(System.out::println);


    }

}

