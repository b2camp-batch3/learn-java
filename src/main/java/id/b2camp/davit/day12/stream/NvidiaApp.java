package id.b2camp.davit.day12.stream;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class NvidiaApp {

    public static void main(String[] args) {

        List<GPU> graphicCard = getGraphic();

        List<GPU> seriRTX = new ArrayList<>();
        for (GPU graphicCard1 : graphicCard){
            if (graphicCard1.getSeries().equals(GeForce.RTX)){
                seriRTX.add(graphicCard1);
            }
        }
        seriRTX.forEach(System.out::println);

        System.out.println(" ---------- ");
        List<GPU> seriGTX = graphicCard.stream()
                .filter(graphicCard2 -> graphicCard2.getSeries().equals(GeForce.GTX))
                .collect(Collectors.toList());
        seriGTX.forEach(System.out::println);

        System.out.println(" ---------- ");
        List<GPU> lowToHigh = graphicCard.stream()
                .sorted(Comparator.comparing(GPU::getRenderConfig).reversed().thenComparing(GPU::getRenderConfig)
                        .reversed()).collect(Collectors.toList());
        lowToHigh.forEach(System.out::println);

        System.out.println(" ---------- ");
        boolean lessThan = graphicCard.stream()
                .allMatch(gpu -> gpu.getRenderConfig() < 2560);
        System.out.println(lessThan);

        System.out.println(" ---------- ");
        List<GPU> powerFull = graphicCard.stream()
                .filter(gpu -> gpu.getRenderConfig() > 2304)
                .collect(Collectors.toList());
        powerFull.forEach(System.out::println);

        System.out.println(" ---------- ");
        BigDecimal amountShading = graphicCard.stream()
                .filter(gpu -> gpu.getRenderConfig() > 1408)
                .map(GPU::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(amountShading);

        System.out.println(" ---------- ");
        List<GPU> brandVGA = graphicCard.stream()
                .filter(gpu -> gpu.getBrand().equals("Nvidia Asus"))
                .collect(Collectors.toList());
        brandVGA.forEach(System.out::println);

        System.out.println(" \n ");
        Map<GeForce, List<GPU>> typeGeforce = graphicCard.stream()
                .collect(Collectors.groupingBy(GPU::getSeries));

        typeGeforce.forEach((((geForce, gpus) -> {
            System.out.println(geForce);
            gpus.forEach(System.out::println);
            System.out.println();
        }
                )));

    }
    static List<GPU> getGraphic(){
        return List.of(
                new GPU("Nvidia Gigabyte", "AD102", 40, 4090, GeForce.RTX, "Ada Lovelace", "Ray Tracing", 16384,24, 2022, BigDecimal.valueOf(35000000)),
                new GPU("Nvidia MSI", "GP104", 10, 1080, GeForce.GTX, "Pascal", "Non Ray Tracing", 2560, 8, 2017, BigDecimal.valueOf(13800000)),
                new GPU("Nvidia Zotac", "TU104", 20, 2080, GeForce.RTX, "Turing", "Ray Tracing", 2944, 8, 2018, BigDecimal.valueOf(25000000)),
                new GPU("Nvidia Asus", "GA102", 30, 3090, GeForce.RTX, "Amphere", "Ray Tracing", 10496, 24, 2020,BigDecimal.valueOf(30000000)),
                new GPU("Nvidia Galax", "TU116", 16, 1660, GeForce.GTX, "Turing", "Non Ray Tracing", 1408, 6, 2019,BigDecimal.valueOf(7000000)),
                new GPU("Nvidia EVGA", "GM204", 900, 980, GeForce.GTX, "Maxwell", "Non Ray Tracing", 2048, 4, 2014,BigDecimal.valueOf(12000000)),
                new GPU("Nvidia Palit", "GK110", 700, 780, GeForce.GTX, "Kepler", "Non Ray Tracing", 2304, 3, 2013,BigDecimal.valueOf(10000000))
        );
    }
}
