package id.b2camp.davit.day12.stream;

import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class GPU {

    private String brand;
    private String processor;
    private int generation;
    private int numberSeries;
    private GeForce series;
    private String architecture;
    private String nameSeries;
    private int renderConfig;
    private int memory;
    private int year;
    private BigDecimal price;

    }
