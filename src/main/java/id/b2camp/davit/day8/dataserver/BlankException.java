package id.b2camp.davit.day8.dataserver;

/**
 * blank exception
 */

public class BlankException extends RuntimeException{

        public BlankException(String message){
            super(message);
        }
}
