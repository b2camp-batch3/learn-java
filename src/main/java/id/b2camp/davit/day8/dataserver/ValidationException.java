package id.b2camp.davit.day8.dataserver;

/**
 * Jenis Exception ada 3
 * 1. Checked Exception
 * 2. Runtime Exception
 * 3. Error
 */

public class ValidationException extends Throwable{

    public ValidationException(String message){
        super(message);
    }
}
