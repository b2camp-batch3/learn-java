package id.b2camp.davit.day8.dataserver;

import id.b2camp.davit.day8.logins.LoginRequest;

public class ValidationUtil {
    public static void validate(LoginRequest loginRequest) throws ValidationException, NullPointerException {
        if (loginRequest.getUsername() == null) {
            throw new NullPointerException("Username is Null");
        } else if (loginRequest.getUsername().isBlank()) {
            throw new ValidationException("Username is blank");
        }

        if (loginRequest.getPassword() == null) {
            throw new NullPointerException("Password is null");
        } else if (loginRequest.getPassword().isBlank()) {
            throw new ValidationException("Password is blank");
        }
    }

    public static void validateRuntime(LoginRequest loginRequest) {
        if (loginRequest.getUsername() == null) {
            throw new NullPointerException("Username is Null");
        } else if (loginRequest.getUsername().isBlank()) {
            throw new BlankException("Username is blank");
        }

        if (loginRequest.getPassword() == null) {
            throw new NullPointerException("Password is null");
        } else if (loginRequest.getPassword().isBlank()) {
            throw new BlankException("Password is blank");
        }
    }
}
