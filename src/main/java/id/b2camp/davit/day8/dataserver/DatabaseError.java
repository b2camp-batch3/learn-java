package id.b2camp.davit.day8.dataserver;


/**
 * database error
 */

public class DatabaseError extends Error{

    public DatabaseError(String message){
        super(message);
    }
}
