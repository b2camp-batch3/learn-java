package id.b2camp.davit.day8.dataserver;

import id.b2camp.davit.day8.logins.LoginRequest;

public class OricoApp {
    public static void main(String[] args) {

        LoginRequest loginRequest = new LoginRequest("abahcodet", "jamesbond");

        try {
            ValidationUtil.validate(loginRequest);
            System.out.println("Data Valid");
        }catch (ValidationException exception){
            System.out.println("Data Invalid : " + exception.getMessage());
        }catch (NullPointerException exception){
            System.out.println("Data Null : " + exception.getMessage());

//            type 2 & Block Finnaly (selalu meng eksekusi data valid maupun yang tidak valid)
//        }  catch (ValidationException | NullPointerException | IllegalArgumentException){
//            System.out.println("" +);
//        }finally {
//            System.out.println("");
        }

        LoginRequest loginRequest2 = new LoginRequest("abahcodet", "abahcodet");
        ValidationUtil.validateRuntime(loginRequest2);
        System.out.println("Success");
    }
}
