package id.b2camp.davit.day8.logins;

public class MituBabyApp {

    public static void main(String[] args) {

        LoginRequest loginRequest = new LoginRequest("abahcodet", "jamesbond");

        System.out.println(loginRequest.getUsername());
        System.out.println(loginRequest.getPassword());

        System.out.println(new LoginRequest().getUsername());
        System.out.println(new LoginRequest("abahcodet").getUsername());
        System.out.println(new LoginRequest("abahcodet", "jamesbond").getUsername());
    }
}
