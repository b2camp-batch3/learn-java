package id.b2camp.davit.day6.data;

/**
 * Encapsulation
 * encapsulation artinya memastikan data sensitif sebuah object tersembunyi dari akses luar
 * bertujuan agar bisa menjaga data sebuah object tetap baik dan valid
 * untuk mencapai ini, biasanya kita akan membuat semua field menggunakan access modifier private, sehingga tidak bisa diakses atau diubah diri luar
 * agar bisa diubah, kita akan menyediakan method untuk mengubah dan mendapatkan field tersebut.
 */

/**
 * Getter and Setter
 * proses encapsulation sudah dibuat standarisasinya, bisa menggunakan getter dan setter method
 * getter adalah function yang dibuat untuk mengambil data field
 * setter adalah function untuk mengubah data field
 */

/**
 * Getter dan Setter Method
 * tipe data : boolean, primitif, object
 * getter method : boolean (isXxx()), primitif (getXxx()), object (getXxx()).
 * setter method : boolean (setXxx(boolean value)), primitif (setXxx(primitifvalue)), object (setXxx(objectvalue)).
 */

public class Category {

    private String id;

    private boolean expensive;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id != null){
            this.id = id;
        }

    }

    public boolean isExpensive() {
        return expensive;
    }

    public void setExpensive(boolean expensive) {
        this.expensive = expensive;
    }
}
