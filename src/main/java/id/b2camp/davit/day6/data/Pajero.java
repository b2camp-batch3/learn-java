package id.b2camp.davit.day6.data;

public class Pajero implements Car, IsMaintenance {
    public void drive(){
        System.out.println("Pajero Drive");
    }
    public int getTire(){
        return 4;

    }

    public String getBrand() {
        return "Mitsubishi";
    }

    @Override
    public boolean isMaintenance() {
        return false;
    }
}
