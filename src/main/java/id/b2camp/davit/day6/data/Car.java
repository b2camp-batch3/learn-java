package id.b2camp.davit.day6.data;

/**
 * Interface
 * Abstract class bisa digunakan sebagai kontrak untuk class child nya
 * namun sebenranya yang lebih tepat untuk kontrak adalah interface
 * interface bukan sebagai user interface atau UI/UX
 * interface mirip dengan abstract class, yang membedakan adalah interface; semua method otomatis abstract, tidak memiliki block
 * di interface tidak boleh memiliki field, hanya constant ( field yang tidak bisa diubah )
 * untuk mewariskan interface, tidak menggunakan kata kunci extends, melainkan implements
 */

public interface Car extends HasBrand, IsMaintenance{

    void drive();

    int getTire();

    // Default Method

    default boolean isBig(){
        return false;
    }
}
