package id.b2camp.davit.day6.data;

public interface IsMaintenance {

    boolean isMaintenance();
}
