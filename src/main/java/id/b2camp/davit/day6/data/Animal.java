package id.b2camp.davit.day6.data;

public abstract class Animal {
    public String name;

    public abstract void run();
}
