package id.b2camp.davit.day6.aplikasi;

import id.b2camp.davit.day6.data.CityDavit;

public class LocationAppDavit {
    public static void main(String[] args) {

        var city = new CityDavit();
        city.name = "Cimahi";
        System.out.println(city.name);
    }
}
