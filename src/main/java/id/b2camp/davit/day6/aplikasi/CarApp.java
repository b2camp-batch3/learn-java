package id.b2camp.davit.day6.aplikasi;

import id.b2camp.davit.day6.data.Car;
import id.b2camp.davit.day6.data.Pajero;

public class CarApp {
    public static void main(String[] args) {
        Car car = new Pajero();
        System.out.println(car.getTire());
        car.drive();
    }
}
