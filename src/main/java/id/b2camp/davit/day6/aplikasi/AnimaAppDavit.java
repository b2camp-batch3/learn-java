package id.b2camp.davit.day6.aplikasi;

import id.b2camp.davit.day6.data.Animal;
import id.b2camp.davit.day6.data.CatDavit;

public class AnimaAppDavit {
    public static void main(String[] args) {

        Animal animal = new CatDavit();
        animal.name = "puss";
        animal.run();
    }
}
