package id.b2camp.davit.day9.annotations;

/** Annotation
 * menambahkan metadata ke kode program yang kita buat
 * tidak semua orang membutuhkan annotation , biasanya annotation digunakan saat membuat library/framework
 * annotation sendiri bisa diakses menggunakan reflection
 * annotation hanya bisa memiliki method dengan tipe data sederhana, dan bisa memiliki default value.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Fancy {

    String name ();

    String[] tags() default {};
}
