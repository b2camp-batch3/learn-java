package id.b2camp.davit.day9.reflection;

import id.b2camp.davit.day8.dataserver.BlankException;
import id.b2camp.davit.day9.annotations.NotBlank;

import java.lang.reflect.Field;

public class ValidationReflec {

    public static void validationReflection(Object object){

        Class aClass = object.getClass();
        Field[] fields = aClass.getDeclaredFields();

        for (var field : fields){
            field.setAccessible(true);

            if (field.getAnnotation(NotBlank.class) != null){

                try {
                    String value = (String) field.get(object);

                    if (value == null || value.isBlank()){
                        throw new BlankException("Field " + field.getName() + "Is Blank");
                    }
                }catch (IllegalAccessException exception) {
                    System.out.println("Cannot Access " + field.getName());
                }

            }
        }
    }
}
