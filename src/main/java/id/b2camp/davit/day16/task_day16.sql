CREATE TABLE senjaya_steam.user (
  "UserID" INT PRIMARY KEY,
  "Username" VARCHAR(50),
  "Email" VARCHAR(100),
  "Password" VARCHAR(100)
);

CREATE TABLE senjaya_steam.game (
  "GameID" INT PRIMARY KEY,
  "Title" VARCHAR(100) NOT NULL,
  "Description" TEXT,
  "Genre" VARCHAR(50),
  "ReleaseDate" DATE,
  "UserID" INT
);

CREATE TABLE senjaya_steam.reviews (
  "ReviewID" INT PRIMARY KEY,
  "GameID" INT,
  "UserID" INT,
  "Rating" INT,
  "Comment" TEXT
);

CREATE TABLE senjaya_steam.library (
  "LibraryID" INT PRIMARY KEY,
  "UserID" INT,
  "GameID" INT,
  "PurchaseDate" DATE
);

CREATE TABLE senjaya_steam.wishlist (
  "WishlistID" INT PRIMARY KEY,
  "UserID" INT,
  "GameID" INT
);

CREATE TABLE senjaya_steam.transaction (
  "TransactionID" INT PRIMARY KEY,
  "UserID" INT,
  "PurchaseDate" DATE,
  "Amount" DECIMAL(10, 2),
  "GameID" INT
);

CREATE TABLE senjaya_steam.friends (
  "FriendshipID" INT PRIMARY KEY,
  "UserID1" INT,
  "UserID2" INT,
  "Status" VARCHAR(20)
);

CREATE TABLE senjaya_steam.chat (
  "ChatID" INT PRIMARY KEY,
  "SenderID" INT,
  "ReceiverID" INT,
  "Message" TEXT,
  "Timestamp" TIMESTAMP
);

ALTER TABLE senjaya_steam.game ADD FOREIGN KEY ("UserID") REFERENCES senjaya_steam.user ("UserID");

ALTER TABLE senjaya_steam.reviews  ADD FOREIGN KEY ("GameID") REFERENCES senjaya_steam.game ("GameID");

ALTER TABLE senjaya_steam.reviews  ADD FOREIGN KEY ("UserID") REFERENCES senjaya_steam.user ("UserID");

ALTER TABLE senjaya_steam.library  ADD FOREIGN KEY ("UserID") REFERENCES senjaya_steam.user ("UserID");

ALTER TABLE senjaya_steam.library  ADD FOREIGN KEY ("GameID") REFERENCES senjaya_steam.game ("GameID");

ALTER TABLE senjaya_steam.wishlist ADD FOREIGN KEY ("UserID") REFERENCES senjaya_steam.user ("UserID");

ALTER TABLE senjaya_steam.wishlist  ADD FOREIGN KEY ("GameID") REFERENCES senjaya_steam.game ("GameID");

ALTER TABLE senjaya_steam.transaction  ADD FOREIGN KEY ("UserID") REFERENCES senjaya_steam.user ("UserID");

ALTER TABLE senjaya_steam.transaction  ADD FOREIGN KEY ("GameID") REFERENCES senjaya_steam.game ("GameID");

ALTER TABLE senjaya_steam.friends ADD FOREIGN KEY ("UserID1") REFERENCES senjaya_steam.user ("UserID");

ALTER TABLE senjaya_steam.friends ADD FOREIGN KEY ("UserID2") REFERENCES senjaya_steam.user ("UserID");

ALTER TABLE senjaya_steam.chat ADD FOREIGN KEY ("SenderID") REFERENCES senjaya_steam.user ("UserID");

ALTER TABLE senjaya_steam.chat ADD FOREIGN KEY ("ReceiverID") REFERENCES senjaya_steam.user ("UserID");

INSERT INTO senjaya_steam."user"
("UserID", "Username", "Email", "Password")
VALUES(1, 'abahcodet', 'abahcodet@gmail.com', 'aingboloho');

INSERT INTO senjaya_steam."user"
("UserID", "Username", "Email", "Password")
VALUES(3, 'UHD4K', 'logitech4577@gmail.com', 'arjunaburung');

INSERT INTO senjaya_steam."user"
("UserID", "Username", "Email", "Password")
VALUES(2, 'cikoneng', 'marloedan@gmail.com', 'boaedan');

INSERT INTO senjaya_steam."user"
("UserID", "Username", "Email", "Password")
VALUES(4, 'asepsaepudin7', 'ikerasep@gmail.com', 'armagedon');


SELECT "UserID", "Username", "Email", "Password"
FROM senjaya_steam."user";

UPDATE senjaya_steam."user"
SET "Username"='adam', "Email"='adamreyhan@gmail.com', "Password"='cumahanyaseorang'
WHERE "UserID"=2;

UPDATE senjaya_steam."user"
SET "Username"='ujangcodet92', "Email"='adamreyhan@gmail.com', "Password"='hanyadiaseorangyangkucinta'
WHERE "UserID"=2;

DELETE FROM senjaya_steam."user"
WHERE "UserID"=3;

INSERT INTO senjaya_steam.game
("GameID", "Title", "Description", "Genre", "ReleaseDate", "UserID")
VALUES(1, 'Red Dead Redemption 2', 'menceritakan tentang buronan geng van der linch', 'Action, Adventure, open world', '2019-10-10', 2);

INSERT INTO senjaya_steam.game
("GameID", "Title", "Description", "Genre", "ReleaseDate", "UserID")
VALUES(2, 'PUBG', 'Battle Royale', 'action open world', '2018-03-17', 2);

INSERT INTO senjaya_steam.game
("GameID", "Title", "Description", "Genre", "ReleaseDate", "UserID")
VALUES(3, 'Hogwart Legacy', 'menjadi penyihir terkuat di bumi', 'action role playing game and open world', '2023-02-15', 2);

INSERT INTO senjaya_steam.game
("GameID", "Title", "Description", "Genre", "ReleaseDate", "UserID")
VALUES(4, 'Ryse : Son Of Rome', 'menaklukan emperor romawi yang korup', 'Advanture and action', '2014-07-07', null);

UPDATE senjaya_steam.game
SET "Title"='Ryse : Son Of Rome', "Description"='menaklukan emperor romawi yang korup', "Genre"='Adventure and action', "ReleaseDate"='2014-07-07', "UserID"=4
WHERE "GameID"=4;

SELECT "GameID", "Title", "Description", "Genre", "ReleaseDate", "UserID"
FROM senjaya_steam.game;

INSERT INTO senjaya_steam."library"
("LibraryID", "UserID", "GameID", "PurchaseDate")
VALUES(0, 2, 1, '2020-07-10');

DELETE FROM senjaya_steam."library"
WHERE "LibraryID"=1;

SELECT "LibraryID", "UserID", "GameID", "PurchaseDate"
FROM senjaya_steam."library";

INSERT INTO senjaya_steam."library"
("LibraryID", "UserID", "GameID", "PurchaseDate")
VALUES(1, 2, 3, '2023-04-05');

SELECT "LibraryID", "UserID", "GameID", "PurchaseDate"
FROM senjaya_steam."library";

INSERT INTO senjaya_steam.wishlist
("WishlistID", "UserID", "GameID")
VALUES(4, 1, 2),
(1,2,3),
(2,1,1),
(3,2,2);

SELECT "WishlistID", "UserID", "GameID"
FROM senjaya_steam.wishlist;

INSERT INTO senjaya_steam."transaction"
("TransactionID", "UserID", "PurchaseDate", "Amount")
VALUES(1, 2, '2020-07-10', 1);

SELECT "TransactionID", "UserID", "PurchaseDate", "Amount"
FROM senjaya_steam."transaction";

INSERT INTO senjaya_steam."transaction"
("TransactionID", "UserID", "PurchaseDate", "Amount", "GameID")
VALUES(1, 2, '2023-10-14', 1, 2),
(2, 2, '2023-10-10', 1, 1),
(3, 2, '2023-10-14', 2, 2);

INSERT INTO senjaya_steam.reviews
("ReviewID", "GameID", "UserID", "Rating", "Comment")
VALUES(1, 1, 2, 9.6/10, 'awesome');

UPDATE senjaya_steam.reviews
SET "GameID"=1, "UserID"=2, "Rating"=9.6/10, "Comment"='game of the year'
WHERE "ReviewID"=1

SELECT "ReviewID", "GameID", "UserID", "Rating", "Comment"
FROM senjaya_steam.reviews;

INSERT INTO senjaya_steam.friends
("FriendshipID", "UserID1", "UserID2", "Status")
VALUES(0, 1, 2, 'LGBT');

SELECT "FriendshipID", "UserID1", "UserID2", "Status"
FROM senjaya_steam.friends;

INSERT INTO senjaya_steam.chat
("ChatID", "SenderID", "ReceiverID", "Message", "Timestamp")
VALUES(0, 1, 2, 'i luph u', '2023-08-08');

SELECT "ChatID", "SenderID", "ReceiverID", "Message", "Timestamp"
FROM senjaya_steam.chat;