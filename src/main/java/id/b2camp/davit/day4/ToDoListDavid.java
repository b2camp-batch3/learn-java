package id.b2camp.davit.day4;

public class ToDoListDavid {

    public static String[] model = new String[10];

    public static java.util.Scanner scanner = new java.util.Scanner(System.in);
    public static void main(String[] args) {
        viewShowToDoList();


    }

    /**
     * Menampilkan ToDo List
     */
    public static void showToDoList(){
        System.out.println("TODOLIST");
        for (var i = 0; i < model.length; i++){
            var todo = model[i];
            var no = i + 1;

            if (todo != null){
                System.out.println(no + ". " + todo);
            }
        }

    }
    public static void testShowToDoList(){
        model[0] = "Learn Java = B2Camp";
        model[1] = "Aplikasi Todolist";
        showToDoList();
    }

    /**
     * Menambah ke ToDo List
     */
    public static void addToDoList( String todo ){
        // cek apakah datanya penuh
        var isFull = true;
        for (var i = 0; i < model.length; i++){
            if (model[i] == null){
                // model masih ada yang kosong
                isFull = false;
                break;
            }
        }
        // jika penuh, resize ukuran array
        if (isFull){
            var temp = model;
            model = new String[model.length * 2];

            for (int i = 0; i < temp.length; i++) {
                model[i] = temp[i];
            }
        }


        // tambahkan ke posisi data array nya null
        for ( var i = 0; i < model.length; i++){
            if (model[i] == null){
                model [i] = todo;
                break;
            }
        }

    }
    public static void testAddToDoList(){
        for (int i = 0; i < 25; i++) {
            addToDoList(" Contoh ToDO ke." + i);
        }
        showToDoList();
    }

    /**
     * Menghapus ToDo dari List
     */
    public static boolean removeToDoList(Integer number){
        if ((number -1) >= model.length){
            return false;
        } else if (model[number-1] == null) {
            return false;
        }else {
            for (int i = (number - 1); i < model.length; i++) {
                if (i == (model.length - 1)){
                    model[i] = null;
                }else {
                    model[i] = model[i + 1];
                }

            }
            return true;
        }
    }
    public static void testRemoveToDoList(){
        addToDoList("one");
        addToDoList("two");
        addToDoList("three");
        addToDoList("four");
        addToDoList("five");

        var result = removeToDoList(20);
        System.out.println(result);
        result = removeToDoList(7);
        System.out.println(result);
        result = removeToDoList(2);
        System.out.println(result);
        showToDoList();


    }
    public static String input(String info){
        System.out.print(info + " : ");
        String data = scanner.nextLine();
        return data;
    }
    public static void testInput(){
        var name = input("Nama");
        System.out.println("Hai " + name);

        var band = input("Nama band");
        System.out.println("Drummer dari " + band);
    }

    /**
     * menampilkan view todo list
     */
    public static void viewShowToDoList(){
        while (true){
            showToDoList();
            System.out.println("MENU : ");
            System.out.println("1. Tambah");
            System.out.println("2. Hapus");
            System.out.println("X. Keluar");

            var input = input("Pilih");

            if (input.equals("1")){
                viewAddToDoList();
            } else if (input.equals("2")) {
                viewRemoveToDoList();
            } else if (input.equals("x")) {
                break;
            } else {
            System.out.println("Pilihan Tidak Dimengerti");
            }
        }


    }
    public static void testViewShowToDoList(){
        addToDoList("Nvidia");
        addToDoList("Amd Radeon");
        viewShowToDoList();
    }

    /**
     * menampilkan view menambah todo list
     */
    public static void viewAddToDoList(){
        System.out.println("Add Todolist");

        var todo = input("todo (x jika batal)");
        
        if (todo.equals("x") ){
            // cancel
        }else {
            addToDoList(todo);
        }

    }
    public static void testViewAddToDoList(){
        viewAddToDoList();

        showToDoList();
    }

    /**
     * menampilkan view menghapus todo list
     */
    public static void viewRemoveToDoList(){
        System.out.println("Menghapus Todolist");

        var number = input("Nomor Yang Dihapus (x jika batal)");

        if (number.equals("x")){
            // batal
        } else {
            boolean success = removeToDoList(Integer.valueOf(number));
            if (!success){
                System.out.println("Gagal Menghapus : " + number);
            }
        }

    }
    public static void testViewRemoveToDoList(){



        showToDoList();

        viewRemoveToDoList();

        showToDoList();

    }
}
