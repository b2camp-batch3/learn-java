package id.b2camp.davit.day11;

import java.util.LinkedList;
import java.util.List;

public class CardApp {
    public static void main(String[] args) {

        List<String> kartuGrafis = new LinkedList<>();

        kartuGrafis.add("GTX 1080");
        kartuGrafis.add("RTX 2080");
        kartuGrafis.add("GTX 1080 Ti");
        kartuGrafis.add("GTX Titan Pascal");
        kartuGrafis.add("RTX 3080");
        kartuGrafis.add("RTX 3090 Ti");

        System.out.println(kartuGrafis.size());

        kartuGrafis.addAll(kartuGrafis);

        System.out.println(kartuGrafis);

        System.out.println("\n");
        System.out.println(kartuGrafis.get(1));
        System.out.println(kartuGrafis.get(4));
        System.out.println(kartuGrafis.get(5));
        System.out.println("\n");

        List<Integer> angka = new LinkedList<>();

        int iniNomer = 0;
        for (int i = 0; i < 100; i++){
            angka.add(iniNomer++);
        }
        System.out.println(angka);
        System.out.println(angka.get(25));
        System.out.println(angka.get(78));
        angka.add(15);
    }
}
