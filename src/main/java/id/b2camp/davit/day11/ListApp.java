package id.b2camp.davit.day11;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ArrayList dan LinkedList
 * Arraylist adalah implementasi dari list menggunakan array
 * Default kapasitas array di arraylist adalah 10
 * ketika memasukkan data dan array sudah penuh, maka secara otomatis arraylist akan membuat array baru dengan kapasitas baru dengan ukuran kapasitas lama + data baru
 * LinkedList adalah implementasi list dengan struktur data Double linked list.
 * ARRAYLIST = add, get, set, remove
 * add = FAST jika kapasitas array masih cukup, SLOW jika sudah penuh
 * get = FAST karena tinggal gunakan index array
 * set = FAST karena tinggal gunakan index array
 * remove = SLOW karena harus menggeser data dibelakang yang dihapus
 * LINKEDLIST = add, get, set, remove
 * add = FAST karena hanya menambah node diakhir
 * get = SLOW karena harus dicek dari node awal sampai ketemu indexnya
 * set = SLOW cek node awal sampai ketemu
 * remove = FAST ubah prev dan next node sebelah yang dihapus
 */

public class ListApp {

    public static void main(String[] args) {

        List<String> bandNames = new ArrayList<>();
        bandNames.add("Lamb Of God");
        bandNames.add("Suicide Silence");
        bandNames.add("Bring Me The Horizon");
        bandNames.add("Dream Theater");
        bandNames.add("Chelsea Grin");
        bandNames.add("As I Lay Dying");
        bandNames.add("Slipknot");
        System.out.println(bandNames);
        System.out.println(bandNames.size());
        System.out.println("\n");

        bandNames.remove("Dream Theater");
        bandNames.add("Slaughter To Prevail");
        bandNames.add("Avanged Sevenfold");
        System.out.println(bandNames);
        System.out.println("\n");

        bandNames.remove("As I Lay Dying");
        bandNames.add("Dying Fetus");
        System.out.println(bandNames);
        System.out.println(Arrays.toString(bandNames.toArray()));
        System.out.println("-----------------");
        System.out.println(bandNames.get(4));
        System.out.println(bandNames.get(2));
    }
}
