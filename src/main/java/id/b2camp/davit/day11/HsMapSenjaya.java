package id.b2camp.davit.day11;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.List.of;

public class HsMapSenjaya {

    public static void main(String[] args) {


        Map<Integer, String> homeStyle = new HashMap<>();
        System.out.println("Design");


        homeStyle.put(1, "Kontemporer Style");
        homeStyle.put(2, "Modern Minimalist Style");
        homeStyle.put(3, "Modern Industrial Style");
        homeStyle.put(4, "Art Deco Style");
        homeStyle.put(5, "Scandinavian Style");
        homeStyle.put(6, "Rustic Style");
        homeStyle.put(7, "Futuristik Style");
        homeStyle.put(8, "Klasik Mediterania Style");
        homeStyle.put(9, "Modern Glass House Style");
        homeStyle.put(10, "Bohemian Full Color Style");


        System.out.println(homeStyle.size());
        System.out.println(homeStyle);
        System.out.println("\n");
        System.out.println(homeStyle.get(7));
        System.out.println(homeStyle.get(4));



        HashMap<String, List<String>> konsultanPerencana = new HashMap<>();
        ArrayList<String> architect = new ArrayList<>(of("Davit", "Prana", "Ario", "Fazlan"));
        konsultanPerencana.put("Arsitek", architect);
        System.out.println(konsultanPerencana);
        System.out.println(konsultanPerencana.get("Arsitek"));

        System.out.println("\n");
        ArrayList<String> designer = new ArrayList<>(List.of("Hedi", "Mamat", "Naufal", "Adi"));
        konsultanPerencana.put("Designer", designer);
        System.out.println(konsultanPerencana);
    }
}
