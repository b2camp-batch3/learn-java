package id.b2camp.davit.day2;

public class SwitchStatementDavid {

    public static void main(String[] args) {

        var nilai = "E";

        switch (nilai){
            case "A":
                System.out.println("Selamat, Anda Lulus Dengan Nilai Baik");
                break;
            case "B":
                    System.out.println("Nilai Anda Baik Sekali");
                    break;
            case "C":
                        System.out.println("Nilai Anda Cukup Baik");
                        break;
            case "D" :
                System.out.println("Anda Tidak Lulus");
                break;
            default:
                System.out.println("Sepertinya Anda Butuh Ke Klink Tong Fang");
        }
    }
}
