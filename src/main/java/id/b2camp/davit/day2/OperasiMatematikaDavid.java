package id.b2camp.davit.day2;

public class OperasiMatematikaDavid {
    public static void main(String[] args) {


        int a = 200;
        int b = 10;

        System.out.println(a+b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b);
        System.out.println(a%b);

//        Augmented Assignments

        int c = 100;

        c += 10;
        System.out.println(c);

        c -= 10;
        System.out.println(c);

        c *= 10;
        System.out.println(c);

        /*
        Unary Operator
        a. ++ = a=a+1
        b. -- = a=a-1
        c. - = Negative
        d. + = Positive
        e. ! = Boolean Kebalikan
         */

        int d = 200;
        d++;
        System.out.println(d);
        d--;
        System.out.println(d);
        System.out.println(!true);
    }
}
