package id.b2camp.davit.day2;

import java.util.Arrays;

public class Array {

    public static void main(String[] args) {

//        Tipe Data Array
//        a. Array adalah tipe data yang berisikan kumpulan data dengan tipe yang sama
//        b. jumlah data di array tidak bisa berubah setelah pertama kali dibuat

        String[] stringArray;
        stringArray = new String[3];

        stringArray[0]= "Kevin";
        stringArray[1]= "Hadinata";
        stringArray[2]= "Senjaya";

        System.out.println(stringArray[0]);
        System.out.println(stringArray[1]);
        System.out.println(stringArray[2]);

//        String[] stringArray2 = new String[3];

//        String[] namaNama = {
//                "Kevin", "Hadinata", "Senjaya"
//        };
//        namaNama[0]= null;


//        Code : Array Initializer


//       int[] arrayInt = new int[]{
//               100, 200, 300, 400, 500, 600, 700, 800, 900
//       };

       long[] arrayLOng ={
               16L, 24L, 32L, 64L, 72L, 87L, 95L
       };

       arrayLOng[0]= 0;

        System.out.println(arrayLOng.length);

        String[][] members ={
                {"Kevin", "Hadinata", "Senjaya"},
                {"Illithya", "Adiva", "Naura"},
                {"Asri", "Rusnina", "Dewi"},
        };
        System.out.println(members[0][0]);
        System.out.println(members[1][0]);
        System.out.println(members[2][0]);

        String[] anggotaKeluarga = {
                "Kevin Hadinata Senjaya",
                "Illithya Adiva Naura",
                "Asri Rusnina Dewi",
        };
        System.out.println(Arrays.toString(anggotaKeluarga));


    }
}
