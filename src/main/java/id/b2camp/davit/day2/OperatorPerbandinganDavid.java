package id.b2camp.davit.day2;

public class OperatorPerbandinganDavid {
    public static void main(String[] args) {


        /*
        Operator Perbandingan
        1. > = Lebih dari
        2. < = Kurang dari
        3. >= = Lebih dari sama dengan
        4. <= = Kurang dari sama dengan
        5. == = Sama dengan
        6. != = Tidak sama dengan
         */




        int a = 100;
        int b = 700;

        System.out.println(a>b);
        System.out.println(a<b);
        System.out.println(a>=b);
        System.out.println(a<=b);
        System.out.println(a==b);
        System.out.println(a!=b);



        /*
        Operasi Boolean
        a. && = dan
        b. || = Or/Atau
        c. ! = Kebalikan

        1. Operasi && / Dan

        a. true && true = true
        b. true && false = false
        c. false && true = false
        d. false && false = false


        2. Operasi || / Or

        a. true || true = true
        b. true || false = true
        c. false || true = true
        d. false || false = false



        3. Operasi ! / Kebalikan
        a. !true = !false
        b. !false = !true
         */

        var absen = 80;
        var nilaiAkhir = 75;

        boolean lulusAbsen = absen >= 75;
        boolean lulusNilai = nilaiAkhir >= 75;

        var lulus = lulusAbsen && lulusNilai;
        System.out.println(lulus);
    }
}
