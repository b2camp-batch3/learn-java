package id.b2camp.davit.day2;

public class ForLoopDavid {

    public static void main(String[] args) {


        /*
        For Loop
               a. For adalah salah satu kata kunci yang bisa digunakan untuk melakukan perulangan
                b. Blok kode yang terdapat didalam for akan selalu diulangi selama kondisi for terpenuhi
         */

       for (var counter = 1; counter <= 10; counter++){
           System.out.println("Angka " + counter);
       }



    }
}
