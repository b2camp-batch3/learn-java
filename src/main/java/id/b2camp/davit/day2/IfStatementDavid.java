package id.b2camp.davit.day2;

public class IfStatementDavid {

    public static void main(String[] args) {

//        If Statement and Else Statement


        int nilai = 80;
        int absen = 90;

//        var lulusNilai = nilai >= 75;
//        var lulusAbsen = absen >= 75;
//
//        var lulus = lulusNilai && lulusAbsen;

        if (nilai >= 75 && absen >= 75) {
            System.out.println("Congratulations !!!");
        } else {
            System.out.println("Try Again");
        }

        if (nilai >= 80 && absen >= 80) {
            System.out.println("A");
        } else if (nilai >= 70 && absen >= 70) {
            System.out.println("B");
        } else if (nilai >= 60 && absen >= 60) {
            System.out.println("C");
        } else if (nilai >= 50 && absen >= 50) {
            System.out.println("D");
        } else {
            System.out.println("E");
        }

//        Ternary Operator


        int nilai1 = 80;
        String ucapan = nilai1 >= 75 ? "Selamat Anda Lulus" : "Silahkan Dicoba Lagi";
        System.out.println(ucapan);
    }
}
