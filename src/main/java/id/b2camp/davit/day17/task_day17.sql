select * from senjaya_steam.game g
inner join senjaya_steam."user" u on u."UserID" = g."UserID"

select g."GameID" ,
g."Title" ,
g."Genre" ,
u."Username"
from senjaya_steam.game g
left join senjaya_steam."user" u on u."UserID" = g."UserID"

select g."GameID" ,
g."Title" ,
g."Genre" ,
u."Username"
from senjaya_steam.game g
right join senjaya_steam."user" u on u."UserID" = g."UserID"

select g."GameID" ,
g."Title" ,
g."Genre" ,
u."Username"
from senjaya_steam.game g
full join senjaya_steam."user" u on u."UserID" = g."UserID"

select *
from senjaya_steam.game g
join senjaya_steam.wishlist w on w."GameID"  = g."GameID"

select *from senjaya_steam."transaction" t

select g."Title" ,
g."Description" ,
count(t."GameID")
from senjaya_steam."transaction" t
join senjaya_steam.game g on g."GameID" = t."GameID"
join senjaya_steam."user" u on u."UserID" = t."UserID"
group by g."Title",
g."Description"
order by g."Title" desc

select g."Title" ,
g."Description" ,
count(t."GameID")
from senjaya_steam."transaction" t
join senjaya_steam.game g on g."GameID" = t."GameID"
join senjaya_steam."user" u on u."UserID" = t."UserID"
group by g."Title",
g."Description"
having count(t."GameID") > 1