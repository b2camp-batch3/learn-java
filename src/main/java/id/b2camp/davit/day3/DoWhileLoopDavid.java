package id.b2camp.davit.day3;

public class DoWhileLoopDavid {

    public static void main(String[] args) {

        var counter = 100;

        do {
            System.out.println("Angka " + counter);
            counter++;
        }while (counter <= 10);
    }
}
