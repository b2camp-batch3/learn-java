package id.b2camp.davit.day3;


class Mahasiswa{
    String nama;
    String NIM;
    String jurusan;
    double IPK;
    int umur;

}

public class ClassAndObjectDavid {

    public static void main(String[] args) {

        Mahasiswa mahasiswa1 = new Mahasiswa();
        mahasiswa1.nama = "Davit Senjaya";
        mahasiswa1.NIM = "212011143";
        mahasiswa1.jurusan = "Teknik Arsitektur";
        mahasiswa1.IPK = 3.46;
        mahasiswa1.umur = 24;

        System.out.println(mahasiswa1.nama);
        System.out.println(mahasiswa1.NIM);
        System.out.println(mahasiswa1.jurusan);
        System.out.println(mahasiswa1.IPK);
        System.out.println(mahasiswa1.umur);

    }
}
