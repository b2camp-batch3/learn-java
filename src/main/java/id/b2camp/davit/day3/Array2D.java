package id.b2camp.davit.day3;


public class Array2D {

    public static void main(String[] args) {

        System.out.println("Array Multidimensi");

        int[][] array2D = {{1, 2}, {3, 4}};
        printarray2D(array2D);

        int[][] arrayAngka = new int[5][4];

        printarray2D(arrayAngka);


//        for (int i = 0; i < arrayAngka.length; i++){
//            System.out.print("[");
//            for ( int j = 0; j < arrayAngka[i].length; j++){
//                System.out.print(arrayAngka[i] [j] + ",");
//            }
//            System.out.print("]\n");
//        }

//        for (int [] baris: arrayAngka) {
//            System.out.print("[");
//            for (int angka : baris){
//                System.out.print(angka + ",");
//            }
//            System.out.print("]\n");
//
//        }

        int [][] array2d_2 = {
                {1,2,3,4},
                {5,6,7,8},
                {9,10,11,12},
        };
        printarray2D(array2d_2);

        int [][] arrayRagged = {
                {5,6,7},
                {9,15,24},
                {78},
        };
        printarray2D(arrayRagged);





    }
    private static void printarray2D (int[][] dataArray){
        for (int [] baris: dataArray) {
            System.out.print("[");
            for (int angka : baris){
                System.out.print(angka + ",");
            }
            System.out.print("]\n");

        }

    }
}
