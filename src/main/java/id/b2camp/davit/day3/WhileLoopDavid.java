package id.b2camp.davit.day3;

public class WhileLoopDavid {
    public static void main(String[] args) {

        var counter = 1;

        while (counter <= 10){
            System.out.println("No." + counter);
            counter++;
        }
    }
}
