package id.b2camp.jerri.day11;

import java.util.*;

public class ListTruckWithSetJr {
    public static void main(String[] args) {
        Set<String> truckNames = new HashSet<>(List.of("Volvo","MAN","ISUZU","DAF","DAF"));
        System.out.println(truckNames);

        List<String> truckNamesA = new ArrayList<>(List.of("VOLVO","VOLVO","DAF","DAF","DAF"));
        System.out.println(truckNamesA);

        Set<String> unknownTruckNames = new HashSet<>(truckNamesA);
        System.out.println(unknownTruckNames);

        Collections.synchronizedSet(unknownTruckNames);

    }



}
