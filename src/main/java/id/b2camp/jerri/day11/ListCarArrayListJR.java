package id.b2camp.jerri.day11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class ListCarArrayListJR {
    public static void main(String[] args) {
        String[] carNames = new String[5];
        carNames[0] = "BMW";
        carNames[1] = "ISUZU";
        carNames[2] = "TOYOTA";
        System.out.println(Arrays.toString(carNames));

        System.out.println("----");
        List<String> carList = new ArrayList<>();
        carList.add("SUZUKI");
        carList.add("Mitshubishi");
        carList.add("FORD");
        System.out.println(Arrays.toString(carList.toArray()));
        carList.add("WULING");
        System.out.println(Arrays.toString(carList.toArray()));
        carList.remove("Mitshubishi");
        System.out.println(Arrays.toString(carList.toArray()));

        System.out.println("----inijeda----");
        List<String> carlistA = new ArrayList<>();
        carlistA.addAll(carList);
        System.out.println(Arrays.toString(carlistA.toArray()));
        carlistA.removeAll(carlistA);
        System.out.println(Arrays.toString(carlistA.toArray()));

        System.out.println("------jeda lagi ------");
        List<String> carListB = new ArrayList<>(List.of("Pajero","Hilux","Fortuner","CR-V"));
        System.out.println(Arrays.toString(carListB.toArray()));
        carListB.add("Strada Triton");
        System.out.println(Arrays.toString(carListB.toArray()));

        System.out.println("----inijeda----");
        System.out.println(carNames[1]);
        System.out.println(carListB.get(4));
        System.out.println("---jeda---");
        System.out.println(carNames.length);
        System.out.println(carListB.size());

        Collections.shuffle(carListB);

    }
}
