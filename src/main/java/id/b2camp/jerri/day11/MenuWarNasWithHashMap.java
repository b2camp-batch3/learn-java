package id.b2camp.jerri.day11;

import java.util.*;

public class MenuWarNasWithHashMap {
    public static void main(String[] args) {
        Map<String,Double> menuMakananList = new HashMap<>();
        menuMakananList.put("Rendang",16000.0);
        menuMakananList.put("Tetelan",15000.0);
        menuMakananList.put("Bakso Beranak",25000.0);
        System.out.println("Daftar menu "+ menuMakananList);
        System.out.println(menuMakananList.get("Rendang"));
        System.out.println(menuMakananList.get("Tetelan"));
        System.out.println("-----");
        System.out.println("Batas");
        System.out.println("-----");

        Map<String, List<String>> menuMinumanList = new HashMap<>();
        List<String> minumanList = new ArrayList<>(List.of("Copucino","Espresso Coffe","Americano"));
        List<String> tehList = new ArrayList<>(List.of("Lemon tea","Black tea","Chay tea"));
        menuMinumanList.put("Menu teh" , tehList);
        menuMinumanList.put("Menu minuman", minumanList);
        System.out.println("Berbagai menu minuman " + menuMinumanList);

        System.out.println("");
        System.out.println("batas");
        System.out.println("");
        System.out.println(menuMinumanList.get("Menu teh"));
        System.out.println(menuMinumanList.get("Menu minuman"));

        menuMinumanList.put("Menu teh", List.of("Teh Tarik","Teh Serbuk", "Teh Iketan"));
        System.out.println(menuMinumanList);
        menuMinumanList.replace("Menu teh", List.of("Thai tea"));
        System.out.println(menuMinumanList);
        menuMinumanList.put("Menu Milk",List.of("Milkshake","Vanilla Milkshake","Strowberry Milkshake"));
        System.out.println(menuMinumanList);
        System.out.println(menuMinumanList.size());
        System.out.println(menuMinumanList.entrySet());
        Collection<List<String>> value = menuMinumanList.values();
        System.out.println(value);
        Collections.synchronizedMap(menuMakananList);

    }
}
