package id.b2camp.jerri.day9;

@VeryImportant
public class RideMoto {

    @ImportantActivy

    private String motorcyle;
    private int maxSpeed;

    public RideMoto(String motorcyle, int maxSpeed) {
        this.motorcyle = motorcyle;
        this.maxSpeed = maxSpeed;
    }
    @WeRide(maxSpeed = 3)
    public void rideNow(){
        System.out.println("ride motorcyle now");
    }
    public void speedUp(){
        System.out.println("you can speed up your motorcycle");
    }


    public String getMotorcyle() {
        return motorcyle;
    }

    public void setMotorcyle(String motorcyle) {
        this.motorcyle = motorcyle;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
