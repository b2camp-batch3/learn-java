package id.b2camp.jerri.day9;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

public class RideAppJR {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
        RideMoto kecepatan = new RideMoto("Hayabusa", 300);
        Field[] iniField = kecepatan.getClass().getDeclaredFields();
        for (Field field : iniField) {
            System.out.println(field);
        }
        System.out.println("");
        System.out.println("----- jeda ---");
        System.out.println("");

        Method[] iniMethod = kecepatan.getClass().getDeclaredMethods();
        for (Method method : iniMethod) {
            System.out.println(method);
        }

        System.out.println("");
        System.out.println("----di Jeda ----");
        System.out.println("");

        if (kecepatan.getClass().isAnnotationPresent(VeryImportant.class)) {
            System.out.println("kecepatan adalah hal yang penting");
        }
        for (Field field : kecepatan.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(ImportantActivy.class)) {
                field.setAccessible(true);
                Object objectKecepatan = field.get(kecepatan);
                System.out.println(objectKecepatan + " kecepatan memberi kepuasan");
            }
        }
        System.out.println("");
        System.out.println("------jeda lagi -----");
        System.out.println("");
        for (Method method : kecepatan.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(WeRide.class)) {
                WeRide ride = method.getAnnotation(WeRide.class);
                for (int i = 0; i < ride.maxSpeed() - 1; i++) {
                    method.invoke(kecepatan);
                }
            }

        }

        System.out.println("");
        System.out.println("----jeda akhit----");
        System.out.println("---");
        RideLombokJR riding = new RideLombokJR("Mandalika", LocalDateTime.now());
        System.out.println("event Berkendara " + riding.getTempat());
        System.out.println("akan direalisasikan pada waktu " + riding.getWaktu());
    }
}
