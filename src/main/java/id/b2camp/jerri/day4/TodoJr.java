package id.b2camp.jerri.day4;

import java.util.Scanner;

public class TodoJr {
    static String[] model = new String[10];
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            tampilanTodo();
            System.out.println("MENU :");
            System.out.println("1. Tambah");
            System.out.println("2. Hapus");
            System.out.println("x. keluar");

            String masukan = masukan("pilih");
            if (masukan.equals("1")) {
                formTambahtodoList();
            } else if (masukan.equals("2")) {
                formHapus();
            } else if (masukan.equals("x")) {
                break;
            } else {
                System.out.println("invalid choose");
            }

        }
    }

    private static void formHapus() {
        System.out.println("Hapus");
        String masukan = masukan("Hapus berdasarkan angka atau ketik x untuk keluar");
        if (masukan.equals("x")){

        }else {
            boolean sukses  = hapusTodolist(Integer.valueOf(masukan));
            if (!sukses){
                System.out.println("Gagal menghapus list " + masukan);
            }
        }
    }

    private static boolean hapusTodolist(int nomor) {
        if ((nomor - 1) >= model.length){
            return false;
        }
        if (model[nomor - 1] == null){
            return false;
        }

        for (int i = (nomor - 1); i < model.length; i++) {
            if (i == (model.length - 1)){
                model[i] = null;
            }else {
                model[i] = model [i + 1];
            }

        }

        return true;
    }

    private static void formTambahtodoList() {
        System.out.println("Tambahkan list");
        String masukan = masukan("pilih berdasarkan angka atau ketik x untuk keluar ");

        if (masukan.equals("x")){

        }else {
            tambahtodo(masukan);
        }
    }

    private static void tambahtodo(String masukan) {
        boolean full = true;

        for (int i = 0; i < model.length; i++) {
            if (model[i] == null){
                full = false;
                break;
            }
        }
            if (full){
                String[] tempModel = model;
                model = new String[model.length * 2];

                for (int i = 0; i < tempModel.length; i++) {
                    model[i] = tempModel[i];
                }
            }
                for (int i = 0; i < model.length; i++) {
                    if(model[i] == null){
                        model[i] = masukan;
                        break;
                    }
        }
    }


    private static String masukan(String pesan) {
        System.out.println(pesan);
        return scan.nextLine();

    }


    private static void tampilanTodo() {
        for (int i = 0; i < model.length; i++) {
            String todo = model[i];
            int nomor = i + 1;

            if (todo != null){
                System.out.println(nomor + "." + todo);
            }
        }
    }
}
