package id.b2camp.jerri.day5;

public class BikeCatalog {
    public static void main(String[] args) {
        Bikes bikes = new Bikes("Kawasaki");
        System.out.println(bikes.Brand);
        System.out.println("----");

        BikesBasic sportBikes = new SportBikes("Ducati");
        System.out.println(sportBikes.Brand);

        System.out.println("----");
        BikesBasic adventureBikes = new AdventureBikes("KTM");

        System.out.println("---");
        csService(new Bikes("Kawasaki"));
        csService(new BikesBasic("BMW"));
        csService(new SportBikes("Ducati"));

        System.out.println("---");
        AdventureBikes KTM = new AdventureBikes("Duke");
        MaxCC(KTM);
        System.out.println("");

        System.out.println(">Variable Hiding<");
        Yamaha yamaha = new Yamaha("r25");
        System.out.println(yamaha.Brand);

        Bikes bikesYamaha = (Bikes) yamaha;
        System.out.println(bikesYamaha.Brand);
    }
    static void csService(Bikes bikes){
        System.out.println("Selamat datang di JeraysMotoshop,disini kamu bisa mengganti part motor " + bikes.Brand + " mu");

    }
    static void MaxCC(BikesBasic bikesBasic){
        if (bikesBasic instanceof SportBikes){
            SportBikes bikes =(SportBikes) bikesBasic;
            bikes.MaxCC = 1000;
            System.out.println("CC of our Sport Bikes " + bikes.MaxCC);
        } else if (bikesBasic instanceof AdventureBikes) {
            AdventureBikes adventureBikes = (AdventureBikes) bikesBasic;
            adventureBikes.MaxCC = 1200;
            System.out.println("CC of our Adventure Bikes " + adventureBikes.MaxCC);

        } else {
            System.out.println("lebih dari itu Adalah HyperBike");
        }

    }
}

class Bikes {
    String Brand;
    public Bikes(String brand) {
        Brand = brand;
    }
}

class BikesBasic extends Bikes{
    int MaxCC;
    public BikesBasic(String brand) {
        super(brand);
        this.MaxCC = MaxCC;
    }
}

class SportBikes extends BikesBasic{
    public SportBikes(String brand) {
        super(brand);
    }
}

class AdventureBikes extends BikesBasic{
    public AdventureBikes(String brand) {
        super(brand);
    }
}

class Yamaha extends Bikes{
    String Brand;
    public Yamaha(String brand) {
        super(brand);
    }
}