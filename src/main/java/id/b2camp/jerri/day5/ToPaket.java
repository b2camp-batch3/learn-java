package id.b2camp.jerri.day5;

import java.util.Scanner;

public class ToPaket {
    static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        String name = scan.nextLine();
        AdminKurir.namaKurir = name;
        System.out.println("Hello saya Admin dari toPaket,perkenalkan saya " + AdminKurir.namaKurir +" Ada bisa saya bantu" );
        KurirBox kurirBox = new KurirBox("Yanto","Palembang");
        System.out.println("Hello saya Kurir box dari to paket,perkenalan saya " + KurirBox.namaKurir +" tolong untuk detail alamatnya");
        System.out.println("lokasi kurir berada di " + kurirBox.location);
        System.out.println("----");
        KurirMotor kurirMotor = new KurirMotor("Yusuf","Lampung");
        System.out.println("lokasi kurir berada di " + kurirMotor.location);
        kurirMotor.penyapa("ayu");

        System.out.println("----");
        System.out.println(kurirMotor.maxPaketMotor());

    }
}

class AdminKurir{
    static String namaKurir;
    static String location;
    void penyapa(String tocustomer){
        System.out.println("Hello " +tocustomer+ " saya Admin dari toPaket,perkenalkan saya " + AdminKurir.namaKurir +" Ada bisa saya bantu" );
    }
    int maxPaket(){
        return 15;
    }
    AdminKurir(String namaKurir , String location){
        this.namaKurir = namaKurir;
        this.location = location;
    }
}

class KurirBox extends AdminKurir{

    KurirBox(String namaKurir,String location) {
        super(namaKurir,location);
    }

}
class KurirMotor extends AdminKurir{
    KurirMotor(String namaKurir,String location) {
        super(namaKurir,location);
    }

    public KurirMotor() {
        super(namaKurir,location);
    }

    void penyapa(String tocustomer){
        System.out.println("Hello " +tocustomer+ " saya Kurir motor dari toPaket,perkenalkan saya " + AdminKurir.namaKurir +" Sebentar saya ke alamat anda" );
    }
    int maxPaketMotor(){
        return super.maxPaket();
    }
}
