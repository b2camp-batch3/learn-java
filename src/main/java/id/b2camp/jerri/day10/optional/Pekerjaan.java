package id.b2camp.jerri.day10.optional;

public class Pekerjaan {
    private String namaPekerjaan;
    private String alamatTempatKerja;

    public void setNamaPekerjaan(String namaPekerjaan) {
        this.namaPekerjaan = namaPekerjaan;
    }

    public void setAlamatTempatKerja(String alamatTempatKerja) {
        this.alamatTempatKerja = alamatTempatKerja;
    }

    public String getNamaPekerjaan() {
        return this.namaPekerjaan;
    }

    public String getAlamatTempatKerja() {
        return this.alamatTempatKerja;
    }

    public Pekerjaan(String namaPekerjaan, String alamatTempatKerja) {
        this.namaPekerjaan = namaPekerjaan;
        this.alamatTempatKerja = alamatTempatKerja;
    }
}
