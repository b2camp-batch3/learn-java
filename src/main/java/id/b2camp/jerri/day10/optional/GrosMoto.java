package id.b2camp.jerri.day10.optional;

import java.util.Optional;

public class GrosMoto {
    public GrosMoto() {
    }

    public static void main(String[] args) {
        Peminat peminat = new Peminat("Jerays", "Bandung", (String)null);
        String namaPeminat = peminat.getName().toLowerCase();
        String alamatPeminat = peminat.getAlamat().toLowerCase();
        System.out.println("nama : " + namaPeminat + " alamat: " + alamatPeminat);
        if (peminat.getNoHp() != null) {
            System.out.println(peminat.getNoHp().toLowerCase());
        } else {
            System.out.println("no hp peminat tidak ada ");
        }

        System.out.println("");
        System.out.println("-----inijeda----");
        System.out.println("");
        Optional<String> noHp = Optional.ofNullable(peminat.getNoHp());
        if (noHp.isPresent()) {
            System.out.println(((String)noHp.get()).toLowerCase());
        } else {
            System.out.println("no hp peminat tidak ada ");
        }

        System.out.println("");
        System.out.println("-----batas----");
        System.out.println("");
        String noHpa = (String)Optional.ofNullable(peminat.getNoHp()).map(String::toLowerCase).orElse("no hp peminat tidak ada ");
        System.out.println(noHpa);
        System.out.println("----batas----");
        Peminat peminatC = null;
        String tidakMempunyaiPekerjaan = (String)Optional.ofNullable(peminatC).map((aa) -> {
            return aa.getPekerjaan();
        }).map((bb) -> {
            return bb.getNamaPekerjaan();
        }).orElse("tidak mempunyai pekerjaan");
        System.out.println(tidakMempunyaiPekerjaan);
    }
}
