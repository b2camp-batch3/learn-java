package id.b2camp.jerri.day10.optional;

public class Peminat {
    private String name;
    private String alamat;
    private String noHp;
    private Pekerjaan pekerjaan;

    public Peminat(String name, String alamat, String noHp, Pekerjaan pekerjaan) {
        this.name = name;
        this.alamat = alamat;
        this.noHp = noHp;
        this.pekerjaan = pekerjaan;
    }

    public Peminat(String name, String alamat, String noHp) {
        this.name = name;
        this.alamat = alamat;
        this.noHp = noHp;
    }

    public String getName() {
        return this.name;
    }

    public String getAlamat() {
        return this.alamat;
    }

    public String getNoHp() {
        return this.noHp;
    }

    public Pekerjaan getPekerjaan() {
        return this.pekerjaan;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public void setPekerjaan(Pekerjaan pekerjaan) {
        this.pekerjaan = pekerjaan;
    }
}
