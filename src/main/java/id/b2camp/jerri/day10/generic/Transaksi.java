package id.b2camp.jerri.day10.generic;

public class Transaksi <P extends PembayaranApp>{
    private P tipePembayaran;

    public void pesan() {
        System.out.println("pembayaran lagi di proses " + this.tipePembayaran);
    }

    public Transaksi(P tipePembayaran) {
        this.tipePembayaran = tipePembayaran;
    }

    public P getTipePembayaran() {
        return this.tipePembayaran;
    }

    public void setTipePembayaran(P tipePembayaran) {
        this.tipePembayaran = tipePembayaran;
    }
}
