package id.b2camp.jerri.day10.generic;

public class SendToApp {
    public SendToApp() {
    }

    public static void main(String[] args) {
        PrintOrder printOrder = new PrintOrder(3);
        printOrder.print();
        DoubleOrder doubleOrder = new DoubleOrder(4.0);
        doubleOrder.print();
        System.out.println("");
        System.out.println("ini jeda");
        System.out.println("");
        NumOrder<Integer> integerNumOrder = new NumOrder(20000);
        NumOrder<Double> doubleNumOrder = new NumOrder(40000.0);
        NumOrder<String> stringNumOrder = new NumOrder("Kirim Paket ");
        integerNumOrder.print();
        doubleNumOrder.print();
        stringNumOrder.print();
        System.out.println("");
        System.out.println("---jeda---");
        System.out.println("");
        PembayaranCash pembayaranCash = new PembayaranCash(25000.0);
        Transaksi<PembayaranApp> pembayaranAppTransaksi = new Transaksi(pembayaranCash);
        pembayaranAppTransaksi.pesan();
        PembayaranEmoney pembayaranEmoney = new PembayaranEmoney(35000.0);
        Transaksi<PembayaranEmoney> pembayaranEmoneyTransaksi = new Transaksi(pembayaranEmoney);
        pembayaranEmoneyTransaksi.pesan();
    }
}
