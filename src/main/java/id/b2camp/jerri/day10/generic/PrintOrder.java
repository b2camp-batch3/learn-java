package id.b2camp.jerri.day10.generic;

public class PrintOrder {
    private Integer price;

    public void print() {
        System.out.println(this.price);
    }

    public Integer getPrice() {
        return this.price;
    }

    public PrintOrder(Integer price) {
        this.price = price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
