package id.b2camp.jerri.day10.generic;

public class NumOrder<AB> {
    private AB price;

    public void print() {
        System.out.println(this.price);
    }

    public AB getPrice() {
        return this.price;
    }

    public void setPrice(AB price) {
        this.price = price;
    }

    public NumOrder(AB price) {
        this.price = price;
    }
}
