package id.b2camp.jerri.day10.generic;

public class PembayaranApp {
    private Double harga;

    public Double getHarga() {
        return this.harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public PembayaranApp(Double harga) {
        this.harga = harga;
    }
}
