package id.b2camp.jerri.day10.generic;

public class DoubleOrder {
    private Double price;

    public void print() {
        System.out.println(this.price);
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public DoubleOrder(Double price) {
        this.price = price;
    }
}
