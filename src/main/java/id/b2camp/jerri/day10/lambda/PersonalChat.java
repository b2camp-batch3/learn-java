package id.b2camp.jerri.day10.lambda;

public interface PersonalChat {
    void print(String var1, String var2);
}
