package id.b2camp.jerri.day10.lambda;

public class GochatApp {
    public GochatApp() {
    }

    public static void main(String[] args) {
        printChat(new PersonalChat() {
            public void print(String send, String pick) {
                System.out.println(send + " to " + pick);
            }
        });
        printChat((aa, bb) -> {
            System.out.println(aa + "tes Chat " + bb);
        });
        printChat((aa, bb) -> {
            System.out.println("tes");
        });
        printChatpar(GochatApp::print);
    }

    private static void print() {
        System.out.println("tes aja");
    }

    private static void printChat(PersonalChat personalChat) {
        personalChat.print("Hallo buddy: ", "Hay there");
    }

    private static void printChatpar(PersonalParaJr personalPara) {
        personalPara.print();
    }
}
