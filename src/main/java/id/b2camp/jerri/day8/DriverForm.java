package id.b2camp.jerri.day8;

import java.time.LocalDate;

public class DriverForm {
    private String id;
    private String password;
    private LocalDate tanggalLahir;

    public DriverForm(String id, String password, LocalDate tanggalLahir) {
        this.id = id;
        this.password = password;
        this.tanggalLahir = tanggalLahir;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(LocalDate tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
}