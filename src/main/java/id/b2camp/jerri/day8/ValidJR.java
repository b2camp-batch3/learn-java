package id.b2camp.jerri.day8;

import java.time.LocalDate;
import java.util.Objects;

public class ValidJR {
    public static void validID(String id) throws IdInvalidJR{
        if(id.isBlank()){
            throw new IdInvalidJR("isi Id anda");
        } else if (id.isEmpty()) {
            throw new IdInvalidJR("id tidak boleh di kosongkan");
        }
    }
    public static void validPass(String pass) throws PassInvalidJR{
        if (pass.isBlank()){
            throw new PassInvalidJR("Password harus diisi dengan huruf dan angka");
        } else if (pass.isEmpty()) {
            throw new PassInvalidJR("tidak boleh kosong");
        }
    }
    public static void validTanggalLahir(LocalDate tgll) throws TanggalLahirInJR{
        if (Objects.isNull(tgll)){
            throw new TanggalLahirInJR("isi tanggal lahir");
        } else if (tgll.isAfter(LocalDate.now())) {
            throw new TanggalLahirInJR("Tidak boleh lebih dari tanggal sekarang");
        }
    }
    public static void validIDOnRuntime(String id) throws RuntimeException{
        if(id.isBlank()){
            throw new RuntimeException("isi Id anda");
        } else if (id.isEmpty()) {
            throw new RuntimeException("id tidak boleh di kosongkan");
        }
    }
    public static void validPassOnRuntime(String pass) throws RuntimeException{
        if (pass.isBlank()){
            throw new RuntimeException ("Password harus diisi dengan huruf dan angka");
        } else if (pass.isEmpty()) {
            throw new RuntimeException("tidak boleh kosong");
        }
    }
    public static void validTanggalLahirOnRuntime(LocalDate tgll) throws RuntimeException{
        if (Objects.isNull(tgll)){
            throw new RuntimeException("isi tanggal lahir");
        } else if (tgll.isAfter(LocalDate.now())) {
            throw new RuntimeException("Tidak boleh lebih dari tanggal sekarang");
        }
    }


}
