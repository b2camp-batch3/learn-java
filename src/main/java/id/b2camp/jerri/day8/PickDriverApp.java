package id.b2camp.jerri.day8;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class PickDriverApp {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IdInvalidJR, PassInvalidJR, TanggalLahirInJR {
        System.out.println("Selamat datang para mitra yang baru mendaftar silahkan isi form pendaftaran");
        String id = masukan("Id : ");
        String password = masukan("password :");
        String tanggalLahir = masukan("Tanggal lahir");
        DateTimeFormatter formatter =DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate tanggalLahirskr = LocalDate.parse(tanggalLahir,formatter);
        try {
        ValidJR.validID(id);
        ValidJR.validPass(password);
        ValidJR.validTanggalLahir(tanggalLahirskr);
       } catch (IdInvalidJR e){
            e.printStackTrace();
            throw new IdInvalidJR(e.getMessage());
        } catch (PassInvalidJR e){
            e.printStackTrace();
            throw new PassInvalidJR(e.getMessage());
        } catch (TanggalLahirInJR e){
            e.printStackTrace();
        throw new TanggalLahirInJR(e.getMessage());
        } finally {
             System.out.println("mohon maaf anda harus mengulang form pendaftaran");
        }

   ValidJR.validIDOnRuntime(id);
//    ValidJR.validPassOnRuntime(password);
//    ValidJR.validTanggalLahirOnRuntime(tanggalLahirskr);

        DriverForm newDriver = new DriverForm(id, password, tanggalLahirskr);
        System.out.println("berhasil mendaftar " + newDriver.getId() );
    }
    static String masukan(String message) {
        System.out.println(message);
        return scan.nextLine();
    }
}
