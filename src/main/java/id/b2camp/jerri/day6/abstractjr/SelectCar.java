package id.b2camp.jerri.day6.abstractjr;

import java.math.BigDecimal;

public abstract class SelectCar {
   private String carName;
   private int priceCar;

   public abstract void workshoplocate();

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public int getPriceCar() {
        return priceCar;
    }

    public void setPriceCar(int priceCar) {
        this.priceCar = priceCar;
    }
}
