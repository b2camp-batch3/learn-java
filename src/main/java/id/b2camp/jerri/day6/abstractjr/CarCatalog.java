package id.b2camp.jerri.day6.abstractjr;

public class CarCatalog {
    public static void main(String[] args) {
        CarBrand carBrand = new CarBrand();
        carBrand.setCarName("Pajero");
        carBrand.setPriceCar(500_000_000);
         System.out.println("This car name is " + carBrand.getCarName() + " and this car price is Rp" + carBrand.getPriceCar());
        carBrand.workshoplocate();
    }
}
