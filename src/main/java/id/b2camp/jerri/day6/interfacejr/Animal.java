package id.b2camp.jerri.day6.interfacejr;

interface Binatang {
    public void animalSound(); // interface method
    public void sleep(); // interface method
}

class Kambing implements Binatang {
    public void animalSound() {
        System.out.println("Kambing : mbekkkkkk");
    }
    public void sleep() {
        System.out.println("Snore....Snore");
    }
}

public class Animal {
    public static void main(String[] args) {
        Kambing myKambing = new Kambing();
        myKambing.animalSound();
        myKambing.sleep();
    }
}