package id.b2camp.jerri.day6.accesmodifierjr.tab1;

public class Mobil {
    private String brand;
    private String size;
    private double harga;
    private int jumlah ;

    public Mobil(String brand,String size,int jumlah ,double harga) {
        this.brand = brand;
        this.size = size;
        this.jumlah = jumlah;
        this.harga = harga;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
}
