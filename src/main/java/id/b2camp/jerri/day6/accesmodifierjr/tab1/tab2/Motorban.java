package id.b2camp.jerri.day6.accesmodifierjr.tab1.tab2;

public class Motorban {
    public static void main(String[] args) {
        Motor corsa = new Motor("Corsa R99","120/80",3,460_000);
        int total = (int)(corsa.jumlah * corsa.harga);
        System.out.println(corsa.brand + " dengan size " + corsa.size + " dan jumlah " + corsa.jumlah + " unit totalnya adalah Rp" +total);
    }
}
