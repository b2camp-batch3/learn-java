package id.b2camp.jerri.day6.accesmodifierjr;

import id.b2camp.jerri.day6.accesmodifierjr.tab1.Mobil;
import id.b2camp.jerri.day6.accesmodifierjr.tab1.tab2.Motor;

public class BanappJR {
    public static void main(String[] args) {

        Mobil maxXis = new Mobil("Maxxis","220/60",4,500_000);
        int total = (int) (maxXis.getJumlah() * maxXis.getHarga());
        maxXis.setBrand("Maxxis Diamond");
        System.out.println(maxXis.getBrand() +" dengan size " + maxXis.getSize() + " dengan jumlah " + maxXis.getJumlah() + " unit adalah Rp" +total);

    }
}
