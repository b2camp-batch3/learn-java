package id.b2camp.jerri.day6.accesmodifierjr.tab1.tab2.tab3;

public class Truk {
    public static void main(String[] args) {
        Truk ban1 = new Truk("Bridgestonne","300/80",6,1_500_000);
        int total = (int) (ban1.jumlah * ban1.harga);
        System.out.println(ban1.brand + " dengan size " +  ban1.size + "dan jumlah " + ban1.jumlah + " memiliki Rp" +total);
    }
    private String brand;
    private String size;
    private double harga;
    private int jumlah;

    private Truk(String brand,String size,int jumlah ,double harga) {
        this.brand = brand;
        this.size = size;
        this.jumlah = jumlah;
        this.harga = harga;
    }
}
