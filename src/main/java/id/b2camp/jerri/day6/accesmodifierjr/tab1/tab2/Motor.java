package id.b2camp.jerri.day6.accesmodifierjr.tab1.tab2;

public class Motor {
    protected String brand;
    protected String size;
    protected double harga;
    protected int jumlah;

    protected Motor(String brand,String size,int jumlah, double harga) {
        this.brand = brand;
        this.size = size;
        this.jumlah = jumlah;
        this.harga = harga;
    }
}
