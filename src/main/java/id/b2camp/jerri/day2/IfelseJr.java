package id.b2camp.jerri.day2;

public class IfelseJr {
    public static void  main(String[]args){
        if (30 > 25){
            System.out.println("30 lebih besar dari 25");
        }
        int a = 12;
        int b = 30;
        if (a < b){
            System.out.println("b lebih kecil dari a ");
        }
        System.out.println("----");
        int waktu = 12;
        if (waktu < 7){
            System.out.println("Selamat siang");
        }else{
            System.out.println("Selamat pagi");
        }
        System.out.println("---");
        int time = 14;
        if (time < 10) {
            System.out.println("Good morning.");
        } else if (time < 18) {
            System.out.println("Good day.");
        }  else {
            System.out.println("Good evening.");
        }
        System.out.println("---");
        int jam = 22;
        String result;
        result = (jam < 17) ? "good day" : "good evening";
        System.out.println(result);
    }
}
