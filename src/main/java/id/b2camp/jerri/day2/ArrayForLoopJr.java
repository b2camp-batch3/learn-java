package id.b2camp.jerri.day2;

import java.util.Arrays;

public class ArrayForLoopJr {
    public static void main(String[]args){
        int[] nomor = {20,30,40,50,60};
        String[] BrandMotor = {"KTM","HONDA","YAMAHA","KAWASAKI","BAJAJ"};
        System.out.println(Arrays.toString(BrandMotor));
        System.out.println(Arrays.toString(nomor));
        System.out.println("---");
        for (int c= 0; c < BrandMotor.length; c++){
            System.out.println(BrandMotor[c]);
        }
        System.out.println("---");
        for (int i= 0; i < nomor.length; i++){
            System.out.println(nomor[i]);
        }
        System.out.println("---");
        for (int j = nomor.length - 1; j >= 0; j--){
            System.out.println(nomor[j]);
        }

    }
}
