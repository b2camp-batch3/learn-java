package id.b2camp.jerri.day2;

public class SwitchJr {
    public static void main (String[]args){
        int Motor = 5;
        switch (Motor){
            case 1:
                System.out.println("Ktm Duke 250");
                break;
            case 2:
                System.out.println("TVS apache 310");
                break;
            case 3:
                System.out.println("BMW G310");
                break;
            case 4:
                System.out.println("Yamaha r25");
                break;
            case 5:
                System.out.println("Kawasaki Ninja 250");
                break;
            case 6:
                System.out.println("Bajaj Dominar 400");
                break;
            default:
                System.out.println("Coming soon...");
        }
    }
}
