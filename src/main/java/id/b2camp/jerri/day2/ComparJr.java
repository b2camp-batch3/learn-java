package id.b2camp.jerri.day2;

public class ComparJr {
    public static void main (String[]args){
        int ccDuke = 390;
        int ccApache = 200;
        System.out.println(ccDuke < ccApache);
        System.out.println(ccDuke > ccApache);
        System.out.println(ccDuke <= ccApache);
        System.out.println(ccDuke >= ccApache );
        System.out.println(ccDuke == ccApache);
        System.out.println(ccDuke != ccApache);
        System.out.println("---");

        boolean funRide = true;
        boolean funRace = false;
        boolean funTour = true;
        System.out.println(funRace && funRide);
        System.out.println(funRide && funTour);
        System.out.println(funRide || funTour);
        System.out.println(funRace || funRide && funTour);

        System.out.println("---");
        System.out.println("Jerry".equalsIgnoreCase("jerry"));
        System.out.println("Pulsar".equals("pulsar"));

    }
}
