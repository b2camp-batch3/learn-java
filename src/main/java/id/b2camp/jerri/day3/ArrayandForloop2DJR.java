package id.b2camp.jerri.day3;

public class ArrayandForloop2DJR {
    public static void main(String[] args) {
        String[][] adv2d = new String[][]{{"Lets "," begin "," with"},{"Motorcycle",},{"fun ", "to" ," ride"}};
        for (String[] strings1 : adv2d) {
            for (String v : strings1) {
                System.out.print(v);
            }
            System.out.println();
        }
        System.out.println("-----jeda-----");

        int[][] nomor1 = new int[2][5];
        for (int[] papan : nomor1) {
            for (int nomors : papan) {
                System.out.print(nomors);
            }
            System.out.println();
            
        }

    }
}
