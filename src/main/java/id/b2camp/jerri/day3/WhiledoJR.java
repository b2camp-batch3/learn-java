package id.b2camp.jerri.day3;

public class WhiledoJR {
    public static void main(String[] args) {
        int j = 0;

        do {
            System.out.println(j);
            ++j;
        } while(j <= 50);
    }
}
