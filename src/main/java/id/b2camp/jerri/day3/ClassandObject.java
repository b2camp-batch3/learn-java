package id.b2camp.jerri.day3;

public class ClassandObject {
    public static void main(String[] args) {
        Bike bike1 = new Bike("KTM","Duke",250);
        Bike bike2 = new Bike("KTM","RC",200);
        System.out.println("ini adalah motormu " + bike1.product +  " " + bike1.cc );
        System.out.println("ini adalah motormu " + bike2.product +  " " + bike2.cc);

    }
    static class Bike {
        String brand;
        String product;
        int cc;

        Bike(String brand,String product,int cc){
            this.brand = brand;
            this.product = product;
            this.cc = cc;
        }
    }
}
