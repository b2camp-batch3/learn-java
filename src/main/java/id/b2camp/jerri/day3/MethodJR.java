package id.b2camp.jerri.day3;

public class MethodJR {
    public static void main(String[] args) {
        String nama  = "Jerays";
        System.out.println(nama.toUpperCase());
        System.out.println("----");

        char[] huruf1 = {'j','k','l','m'};
        char[] huruf2 = {'k','m','n','n'};
        System.out.println("<ini pake int>");
        System.out.println("yang ini punya jumlah " + konchar(huruf1));
        System.out.println("yang ini punya jumlah " + konchar(huruf2));
        System.out.println("<ini pake String>");
        System.out.println(pesanChar(huruf1));
        System.out.println(pesanChar(huruf2));
        System.out.println("<method overloading>");
        System.out.println(pesanChar(huruf1, "yang ini punya jumlah "));
        System.out.println(pesanChar(huruf2,"yang ini punya jumlah "));
        System.out.println("----");
        System.out.println("jumlah character yang di cari " +searcHuruf(huruf2,'n'));
        System.out.println("jumlah character yang di cari " +searcHuruf(huruf1,'k'));

    }
    static int konchar(char[] hurufparam) {
        return hurufparam.length;
    }
    static String pesanChar(char[] hurufparam){
        return "yang ini punya jumlah " + hurufparam.length;
    }
    static String pesanChar(char[] hurufparam , String desc){
        return desc + hurufparam.length;
    }
    static int searcHuruf(char[] hurufparam, char searchChar) {
        int kon = 0;
        for (char huruf : hurufparam) {
            if (huruf == searchChar) {
                kon++;
            }

        }
        return kon;
    }
}
