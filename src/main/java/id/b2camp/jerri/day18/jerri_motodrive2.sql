CREATE TABLE jerri_motodriver.driver (
	id_driver int4 NOT NULL,
	on_off bool NOT NULL,
	nama_driver varchar(50) NOT NULL,
	kendaraan_driver varchar(50) NOT NULL,
	plat_nomor varchar(12) NOT NULL,
	lokasi_penjemputan varchar(300) NOT NULL,
	lokasi_tujuan varchar(300) NOT NULL,
	harga_orderan float8 NOT NULL,
	"riwayat orderan" varchar NOT NULL,
	CONSTRAINT driver_app PRIMARY KEY (id_driver)
);

INSERT INTO jerri_motodriver.driver (id_driver, on_off, nama_driver, kendaraan_driver, plat_nomor, lokasi_penjemputan, lokasi_tujuan, harga_orderan, "riwayat orderan")
VALUES(1, true, 'Aceng', 'Yamaha Mio Z', 'D 5010 AJ', 'Hotel Holiday inn,Pasteur', 'UNISBA', 10000.0, '19 Agustus 2023');
INSERT INTO jerri_motodriver.driver (id_driver, on_off, nama_driver, kendaraan_driver, plat_nomor, lokasi_penjemputan, lokasi_tujuan, harga_orderan, "riwayat orderan")
VALUES(2, false, 'Redho', 'Honda Beat Pop', 'D 8989 AAD', 'Universitas Maranata', 'Gasibu', 13000.0, '19 Agustus 2023');
INSERT INTO jerri_motodriver.driver (id_driver, on_off, nama_driver, kendaraan_driver, plat_nomor, lokasi_penjemputan, lokasi_tujuan, harga_orderan, "riwayat orderan")
VALUES(3, true, 'Agustian', 'Suzuki Address 125', 'D 1750 ABD', 'Lintas Travel Pasteur', 'Sariwangi Regency', 14000.0, '20 Agustus 2023');
INSERT INTO jerri_motodriver.driver (id_driver, on_off, nama_driver, kendaraan_driver, plat_nomor, lokasi_penjemputan, lokasi_tujuan, harga_orderan, "riwayat orderan")
VALUES(4, true, 'Ardian', 'Suzuki Nex 2 ', 'D 1880 AJJ', 'Cibaligo Permai', 'Dago Wisata,Surapati', 25000.0, '20 Agustus 2023');
INSERT INTO jerri_motodriver.driver (id_driver, on_off, nama_driver, kendaraan_driver, plat_nomor, lokasi_penjemputan, lokasi_tujuan, harga_orderan, "riwayat orderan")
VALUES(5, false, 'Kurdi', 'Suzuki Skywave 125', 'D 3990 ZU', 'Jl.Jalaprang,Pahlawan', 'Cipatik,Batujajar', 50000.0, '21 Agustius 2023');


CREATE TABLE jerri_motodriver.customer (
	id_driver int4 NOT NULL,
	customer_name varchar(50) NOT NULL,
	nama_driver varchar(50) NOT NULL,
	plat_nomor varchar(12) NOT NULL,
	lokasi_penjemputan varchar(300) NOT NULL,
	lokasi_tujuan varchar(300) NOT NULL,
	harga_orderan float8 NOT NULL,
	ulasan varchar(300) NULL,
	rating varchar(5) NULL,
	CONSTRAINT customer_pk PRIMARY KEY (id_driver)
);
INSERT INTO jerri_motodriver.customer (id_driver, customer_name, nama_driver, plat_nomor, lokasi_penjemputan, lokasi_tujuan, harga_orderan, ulasan, rating)
VALUES(1, 'Arsela', 'Aceng', 'D 5010 AJ', 'Hotel Holiday inn,Pasteur', 'UNISBA', 10000.0, NULL, NULL);
INSERT INTO jerri_motodriver.customer (id_driver, customer_name, nama_driver, plat_nomor, lokasi_penjemputan, lokasi_tujuan, harga_orderan, ulasan, rating)
VALUES(2, 'justin', 'Redho', 'D 8989 AAD', 'Universitas Maranata', 'Gasibu', 13000.0, 'Driver Ramah ', '5');


CREATE TABLE jerri_motodriver.trip_location (
	id_driver int4 NOT NULL,
	nama_driver varchar(50) NOT NULL,
	customer_name varchar(50) NOT NULL,
	lokasi_penjemputan varchar(300) NOT NULL,
	lokasi_tujuan varchar(300) NOT NULL,
	harga_orderan float8 NOT NULL,
	CONSTRAINT trip_location_pk PRIMARY KEY (id_driver)
);

CREATE TABLE jerri_motodriver.riwayat_orderan (
	id_driver int4 NOT NULL,
	customer_name varchar(50) NOT NULL,
	tanggal_orderan date NOT NULL,
	lokasi_tujuan varchar(300) NOT NULL,
	harga_orderan float8 NOT NULL,
	jumlah_orderan int4 NOT NULL,
	ulasan varchar(300) NULL,
	rating varchar(5) NULL,
	CONSTRAINT riwayat_orderan_pk PRIMARY KEY (id_driver)
);

start transaction ;
insert into jerri_motodriver.driver (id_driver,nama_driver) values(12,'jerays');
insert into jerri_motodriver.driver (id_driver,nama_driver) values(20,'jungkis');
insert into jerri_motodriver.driver (id_driver,nama_driver) values(25,'Ludo');
commit;

start transaction;
insert into jerri_motodriver.driver (id_driver,nama_driver) values(25,'Ludo');
rollback;

start transaction;
UPDATE jerri_motodriver.driver SET id_driver=11 WHERE nama_driver='lana';
UPDATE jerri_motodriver.driver SET harga_orderan =25000 WHERE id_driver  = 25;
commit;

select * from jerri_motodriver.driver d
select driver.id_driver,driver.nama_driver,harga_orderan  from jerri_motodriver.driver