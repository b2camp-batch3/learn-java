package id.b2camp.jerri.day7.anonymclassjr;

public class SekolahApp {

    public static void main(String[] args) {

        Sekolah sekolah = new Sekolah() {

            @Override
            public void sekolahNegeri() {
                System.out.println("sekolah negeri");
            }

            @Override
            public void sekolahSwasta() {
                System.out.println("sekolah swasta");
            }
        };
        sekolah.sekolahNegeri();
        sekolah.sekolahSwasta();

        System.out.println("----");
        Siswa siswa = new Siswa() {

            @Override
            public void belajar() {
                System.out.println("siswa lagi belajar di sekolah");
            }
        };
        siswa.belajar();

    }
}
