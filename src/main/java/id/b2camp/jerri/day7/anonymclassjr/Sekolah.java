package id.b2camp.jerri.day7.anonymclassjr;

public interface Sekolah {

    void sekolahNegeri();
    void sekolahSwasta();
}
