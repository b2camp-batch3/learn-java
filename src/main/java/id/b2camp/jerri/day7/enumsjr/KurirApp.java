package id.b2camp.jerri.day7.enumsjr;

public class KurirApp {
    public KurirApp() {
    }

    public static void main(String[] args) {
        Pengiriman otw = Pengiriman.DALAM_PERJALANAN;
        Pengiriman sampai = Pengiriman.TERKIRIM;
        Pengiriman diKembalikan = Pengiriman.RETUR;
        System.out.println(otw);
        System.out.println(sampai);
        System.out.println(diKembalikan);

        System.out.println("--Batas--");

        String otwString = otw.name();
        String sampaiString = sampai.name();
        String dikembalikanString = diKembalikan.name();
        System.out.println(otwString);
        System.out.println(sampaiString);
        System.out.println(dikembalikanString);

        System.out.println("--INI BATAS--");
        Pengiriman pengirimanOtw = Pengiriman.valueOf(otwString);
        Pengiriman pengirimanSampai = Pengiriman.valueOf(sampaiString);
        Pengiriman pengirimanDikembalikan = Pengiriman.valueOf(dikembalikanString);
        System.out.println(pengirimanOtw);
        System.out.println(pengirimanSampai);
        System.out.println(pengirimanDikembalikan);
        System.out.println("---Batas LAGI---");

        Pembayaran bayar = new Pembayaran(20_000,Pengiriman.DALAM_PERJALANAN);
        System.out.println("Pengiriman sudah dibayarkan " + bayar.getBayar() + " dan status pengiriman " + bayar.getStatus());
        System.out.println("---jeda---");
        System.out.println("Tracking " + bayar.getStatus().getCode() + " " + bayar.getStatus().getDeskripsi());
    }
}
