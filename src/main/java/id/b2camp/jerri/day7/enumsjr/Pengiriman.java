package id.b2camp.jerri.day7.enumsjr;

public enum Pengiriman {

    TERKIRIM(01,"paket anda sudah sampai"),
    DALAM_PERJALANAN(02,"paket anda sedang dalam perjalanan")
    , RETUR(03,"paket dikembalikan ke penjual");

    final int code;
    final String deskripsi;

    Pengiriman(int code, String deskripsi) {
        this.code = code;
        this.deskripsi = deskripsi;
    }

    public int getCode() {
        return code;
    }

    public String getDeskripsi() {
        return deskripsi;
    }
}
