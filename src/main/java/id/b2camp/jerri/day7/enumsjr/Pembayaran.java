package id.b2camp.jerri.day7.enumsjr;

public class Pembayaran {
    private  double bayar;
    private Pengiriman status;

    public Pembayaran(double bayar, Pengiriman status) {
        this.bayar = bayar;
        this.status = status;
    }

    public double getBayar() {
        return bayar;
    }

    public void setBayar(double bayar) {
        this.bayar = bayar;
    }

    public Pengiriman getStatus() {
        return status;
    }

    public void setStatus(Pengiriman status) {
        this.status = status;
    }
}
