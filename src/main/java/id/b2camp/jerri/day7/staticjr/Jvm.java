package id.b2camp.jerri.day7.staticjr;

public class Jvm {

    String bahasa;

    public Jvm(String bahasa) {
        this.bahasa = bahasa;
    }

    public String getBahasa() {
        return bahasa;
    }

    public void setBahasa(String bahasa) {
        this.bahasa = bahasa;
    }

    static class  java{
        String versi;

        public String getVersi() {
            return versi;
        }

        public void setVersi(String versi) {
            this.versi = versi;
        }
    }
}
