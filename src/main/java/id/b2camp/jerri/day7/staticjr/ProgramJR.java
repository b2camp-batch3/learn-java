package id.b2camp.jerri.day7.staticjr;

public class ProgramJR {
    public static void main(String[] args) {

        String js = ProgramFieldJr.CODE_LANGUAGE;
        boolean isDynamic = ProgramFieldJr.IS_DYNAMIC;
        int version = ProgramFieldJr.VERSION;
        System.out.println(js);
        System.out.println(isDynamic);
        System.out.println(version);

        System.out.println("----batas-----");
        ProgramMetJR.multi(10);
        ProgramMetJR.plus(40000,600);
        ProgramMetJR.min(10000,5000);

        System.out.println("---batas Lagi nih ---");
        Jvm.java  javaProg = new Jvm.java();
        javaProg.setVersi("14");
        System.out.println(javaProg.getVersi());;

    }
}
