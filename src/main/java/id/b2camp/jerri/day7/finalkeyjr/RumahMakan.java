package id.b2camp.jerri.day7.finalkeyjr;

public class RumahMakan {
    private String menu;

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }
}

class FoodMenu extends  RumahMakan{
    final void favoritFood(String foodies){
        System.out.println(foodies);
    }
}

final class Tumisan extends  FoodMenu{

}
