package id.b2camp.jerri.day12.stream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamMotorJr {
    public static void main(String[] args) {
        List<MotorcycleCatalog> motors = getMoto();

        List<MotorcycleCatalog> nakedMotoList = new ArrayList<>();
        for (MotorcycleCatalog motorList  : motors) {
            if (motorList.getMotorType().equals(Type.NAKED)){
                nakedMotoList.add(motorList);
            }
        }
        nakedMotoList.forEach(System.out::println);

        System.out.println("");
        System.out.println("----jeda----");
        System.out.println("");

        List<MotorcycleCatalog> adventureMotoList = motors.stream()
                .filter(motorList -> motorList.getMotorType().equals(Type.ADVENTURE))
                .collect(Collectors.toList());
        adventureMotoList.forEach(System.out::println);


        System.out.println("");
        System.out.println("---jeda---");
        System.out.println("");

        List<MotorcycleCatalog> smallToBigBike = motors.stream()
                .sorted(Comparator.comparing(MotorcycleCatalog::getMotorCC).reversed().thenComparing(MotorcycleCatalog::getMotorType ))
                .collect(Collectors.toList());
        smallToBigBike.forEach(System.out::println);

        System.out.println("");
        System.out.println("----ini Jeda----");
        System.out.println("");

        boolean middleToBig = motors.stream()
                .allMatch(motorcycleCatalog -> motorcycleCatalog.getMotorCC() > 100);
        System.out.println(middleToBig);

        System.out.println("");
        System.out.println("---Jeda---");
        System.out.println("");

        List<MotorcycleCatalog>bigBike = motors.stream()
                .filter(motorcycleCatalog -> motorcycleCatalog.getMotorCC()>500)
                .collect(Collectors.toList());
        bigBike.forEach(System.out::println);

        System.out.println("----");
        System.out.println("jeda");
        System.out.println("----");

        BigDecimal priceOfAllMoto = motors.stream()
                .filter(motorcycleCatalog -> motorcycleCatalog.getMotorCC()>250)
                .map(MotorcycleCatalog::getHargaMotor)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(priceOfAllMoto);

        Map<Type, List<MotorcycleCatalog>> byType = motors.stream()
                .collect(Collectors.groupingBy(MotorcycleCatalog::getMotorType));
        byType.forEach((type, motorcycleCatalogs) ->{
            System.out.println(type);
            motorcycleCatalogs.forEach(System.out::println);
            System.out.println();
        });

        List<Map.Entry<Type, List<MotorcycleCatalog>>> kolek = byType.entrySet()
                .stream()
                .filter(aa -> aa.getKey().equals(Type.ADVENTURE))
                .collect(Collectors.toList());
        kolek.forEach(System.out::println);

        System.out.println("");
        System.out.println("----jeda----");
        System.out.println("");

        motors.stream()
                .filter(motorcycleCatalog -> motorcycleCatalog.getMotorType().equals(Type.SPORT))
                .min(Comparator.comparing(MotorcycleCatalog::getMotorCC))
                .ifPresent(System.out::println);




    }
    static List<MotorcycleCatalog> getMoto(){
        return List.of(
                new MotorcycleCatalog("KTM Duke",390,Type.NAKED, BigDecimal.valueOf(120_000_000)),
                new MotorcycleCatalog("Ducati Panigale",900,Type.SPORT,BigDecimal.valueOf(395_000_000)),
                new MotorcycleCatalog("BMW GS 1200",1200,Type.ADVENTURE,BigDecimal.valueOf(500_000_000)),
                new MotorcycleCatalog("Yamaha Xmax",250,Type.MATIC,BigDecimal.valueOf(66_000_000)),
                new MotorcycleCatalog("Honda Beat ESAF",110,Type.MATIC,BigDecimal.valueOf(19_000_000)),
                new MotorcycleCatalog("KTM Adventure",390,Type.ADVENTURE,BigDecimal.valueOf(130_000_000)),
                new MotorcycleCatalog("Yamaha Mt25",250,Type.NAKED,BigDecimal.valueOf(60_000_000)),
                new MotorcycleCatalog("BMW s1000rr",1000,Type.SPORT,BigDecimal.valueOf(495_000_000)),
                new MotorcycleCatalog("Yamaha R1",1000,Type.SPORT,BigDecimal.valueOf(499_000_000)),
                new MotorcycleCatalog("Honda CBR 250RR",250,Type.SPORT,BigDecimal.valueOf(80_000_000)),
                new MotorcycleCatalog("Triumph Tiger",800,Type.ADVENTURE,BigDecimal.valueOf(395_000_000))
        );
    }
}
