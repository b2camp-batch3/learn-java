package id.b2camp.jerri.day12.stream;

import java.math.BigDecimal;

public class MotorcycleCatalog {
    private String motorName;
    private int motorCC;
    private Type motorType;
    private BigDecimal hargaMotor;

    public String getMotorName() {
        return this.motorName;
    }

    public int getMotorCC() {
        return this.motorCC;
    }

    public Type getMotorType() {
        return this.motorType;
    }

    public BigDecimal getHargaMotor() {
        return this.hargaMotor;
    }

    public void setMotorName(String motorName) {
        this.motorName = motorName;
    }

    public void setMotorCC(int motorCC) {
        this.motorCC = motorCC;
    }

    public void setMotorType(Type motorType) {
        this.motorType = motorType;
    }

    public void setHargaMotor(BigDecimal hargaMotor) {
        this.hargaMotor = hargaMotor;
    }

    public MotorcycleCatalog(String motorName, int motorCC, Type motorType, BigDecimal hargaMotor) {
        this.motorName = motorName;
        this.motorCC = motorCC;
        this.motorType = motorType;
        this.hargaMotor = hargaMotor;
    }

    public String toString() {
        String var10000 = this.getMotorName();
        return "MotorcycleCatalog(motorName=" + var10000 + ", motorCC=" + this.getMotorCC() + ", motorType=" + this.getMotorType() + ", hargaMotor=" + this.getHargaMotor() + ")";
    }
}
