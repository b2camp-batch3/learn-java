package id.b2camp.jerri.day12.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SeblakFunction {
    public SeblakFunction() {
    }

    public static void main(String[] args) {
        List<String> seblaks = (List) Arrays.asList("baso", "emplang", "makaroni", "ceker").stream().filter((s) -> {
            return !s.equals("makaroni");
        }).collect(Collectors.toList());
        System.out.println(seblaks);
    }
}
