package id.b2camp.jerri.day12.stream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BasoImperativ {
    public BasoImperativ() {
    }

    public static void main(String[] args) {
        List<String> basoList = List.of("Urat", "Cincang", "Mercon", "Telor", "Beranak");
        boolean ada = false;
        Iterator var3 = basoList.iterator();

        String baso;
        while(var3.hasNext()) {
            baso = (String)var3.next();
            if (baso.equals("Urat")) {
                ada = true;
                break;
            }
        }

        if (ada) {
            System.out.println("Urat ada");
        } else {
            System.out.println("Urat tidak ada");
        }

        var3 = basoList.iterator();

        while(var3.hasNext()) {
            baso = (String)var3.next();
            if (baso.length() == 4) {
                System.out.println(baso.toUpperCase() + ", ");
            }
        }

        List<String> results = new ArrayList();
        Iterator var9 = results.iterator();

        while(var9.hasNext()) {
            String result = (String)var9.next();

            try {
                results.add(transform(result));
            } catch (IOException var7) {
                System.out.println(var7.getMessage());
            }
        }

        System.out.println(results);
    }

    static String transform(String name) throws IOException {
        if (Math.random() > 0.7) {
            throw new IOException("reason");
        } else {
            return name.toLowerCase();
        }
    }
}
