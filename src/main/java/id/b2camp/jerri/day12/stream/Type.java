package id.b2camp.jerri.day12.stream;

public enum Type {
    ADVENTURE,
    SPORT,
    NAKED,
    MATIC;

    private Type() {
    }
}
