CREATE TABLE public.jerri_motortable (
	id int4 NOT NULL,
	nama_motor varchar(20) NOT NULL,
	"type" varchar(30) NOT NULL,
	harga float8 NOT NULL,
	alamat_dealer varchar(300) NOT NULL,
	nama_pemesan varchar(50) NOT NULL,
	tanggal_pemesanan timestamptz NOT NULL,
	CONSTRAINT jerri_motortable_pk PRIMARY KEY (id)
);

insert into jerri_motortable (id,nama_motor,type,harga,alamat_dealer,nama_pemesan,tanggal_pemesanan)
values(1,"KTM DUKE 390","Naked",120.000.000,"Pondok Indah,Jakarta Selatan","Ryandi",2023-09-09 00:00:00.000 +0700);
insert into jerri_motortable (id,nama_motor,type,harga,alamat_dealer,nama_pemesan,tanggal_pemesanan)
values(2,"HUNTER MAVERIK 500","Adventure",130.000.000,"Kute,Bali","Leon",2023-09-08 00:00:00.000 +0700);
insert into jerri_motortable (id,nama_motor,type,harga,alamat_dealer,nama_pemesan,tanggal_pemesanan)
values(3,"KTM RC 200","Sport",35.000.000,"Pondok Indah,Jakarta Selatan","Eris",2023-08-09 00:00:00.000 +0700);
insert into jerri_motortable (id,nama_motor,type,harga,alamat_dealer,nama_pemesan,tanggal_pemesanan)
values(4,"BMW 310 S","Naked",110.000.000,"Senayan,Jakarta","Ronald",2023-08-09 00:00:00.000 +0700);

delete into jerri_motortable (id,nama_motor,type,harga,alamat_dealer,nama_pemesan,tanggal_pemesanan)
                             values(3,"KTM RC 200","Sport",35.000.000,"Pondok Indah,Jakarta Selatan","Eris",2023-08-09 00:00:00.000 +0700);

select *  from jerri_motortable where nama_motor = 'KTM';

select * from jerri_motortable where id = 1;

select *  from jerri_motortable where nama_motor like 'NTR';

select *  from jerri_motortable where nama_motor like 'RIK';

select *  from jerri_motortable where harga =  130.000.000;

select *  from jerri_motortable where harga > 35.000.000;

select *  from jerri_motortable where harga > 110.000.000;

select * from jerri_motortable order by harga desc;

select * from jerri_motortable order by harga asc;

delete from jerri_motortable where id = 2;





