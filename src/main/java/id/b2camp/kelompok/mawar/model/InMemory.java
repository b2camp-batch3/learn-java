package id.b2camp.kelompok.mawar.model;

import java.util.List;

public class InMemory {
    public List<Product> getProducts(){
        return List.of(

            new Product ("a1","Indomie",2750,100),
            new Product ("a2","Susu Ultra",6750,35),
            new Product ("a3","Tolak Angin",3750,50),
            new Product ("b1","Gudang Garam",27500,10),
            new Product ("b2","Aqua",3500,80),
            new Product ("b3","Sabun",5000,15),
            new Product ("c1","Roti",2000,10),
            new Product ("c2","Biskuit",1000,20)
        );
    }
}
