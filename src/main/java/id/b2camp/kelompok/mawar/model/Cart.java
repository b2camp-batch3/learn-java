package id.b2camp.kelompok.mawar.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Cart {
    private String id;
    private String productName;
    private Double price;
    private Integer quantity;
    private Double sumOfProduct;

    public Double sumOfProducts () {
        return price*quantity;
    }
}
