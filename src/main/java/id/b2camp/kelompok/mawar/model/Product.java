package id.b2camp.kelompok.mawar.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Product {
    private String id;
    private String productName;
    private double price;
    private int stock;
}
