package id.b2camp.kelompok.mawar;

import id.b2camp.kelompok.mawar.model.Cart;
import id.b2camp.kelompok.mawar.model.InMemory;
import id.b2camp.kelompok.mawar.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WarkopAbahApp {
    public static void main (String[] args) {
        Scanner input = new Scanner (System.in);
        InMemory memory = new InMemory ();
        List<Product> products = new ArrayList<> (memory.getProducts ());
        List<Cart> carts = new ArrayList<> ();

        while (true) {
            try {

                System.out.println ("Menu App :\n" +
                        "adminAdd / adminRemove / adminUpdate / adminView" +
                        "/ cart / EXIT: ");
                String layoutMenu = input.nextLine ();

                if (layoutMenu.equalsIgnoreCase ("exit")) {
                    break; // Keluar
                } else if (layoutMenu.equalsIgnoreCase ("adminAdd")) {
                    System.out.println ("Halo Admin. Masukkan informasi produk: ");
                    System.out.println ("Masukan Id : ");
                    String id = input.nextLine ();

                    boolean idExists = false;
                    for (Product existingProduct : products) {
                        if (existingProduct.getId().equals(id)) {
                            idExists = true;
                            break;
                        }
                    }

                    if (idExists) {
                        System.out.println("Produk dengan ID tersebut sudah ada dalam daftar.");
                    } else {
                    System.out.println ("Masukan Nama Barang : ");
                    String name = input.nextLine ();
                    System.out.println ("Masukan Harga : ");
                    double price = input.nextDouble ();
                    System.out.println ("Masukan Stok : ");
                    int stok = input.nextInt ();

                    Product product = new Product (id, name, price, stok);
                    products.add (product);

                    System.out.println ("Produk berhasil ditambahkan.");
                    }

                } else if (layoutMenu.equalsIgnoreCase ("adminRemove")) {
                    System.out.println ("Halo Admin. Masukkan ID produk yang ingin dihapus: ");
                    String targetId = input.nextLine ();

                    for (int i = 0; i < products.size (); i++) {
                        if (products.get (i).getId ().equals (targetId)) {
                            products.remove (i);
                            System.out.println ("Produk dengan ID " + targetId + " telah dihapus.");
                        }
                    }

                } else if (layoutMenu.equalsIgnoreCase ("adminUpdate")) {
                    System.out.println ("Halo Admin. Masukkan ID produk yang ingin diupdate: ");
                    String targetId = input.nextLine ();

                    System.out.println ("Nama produk baru yang akan diupdate: ");
                    String newName = input.nextLine ();
                    System.out.println ("Harga baru yang akan diupdate : ");
                    double newPrice = Double.parseDouble (input.nextLine ());
                    System.out.println ("Stok yang akan diupdate: ");
                    int newStock = Integer.parseInt (input.nextLine ());

                    Product selectedProduct = products.stream ()
                            .filter (product -> product.getId ().equalsIgnoreCase (targetId))
                            .findAny ()
                            .orElse (null);
                    if (selectedProduct != null) {
                        selectedProduct.setProductName (newName);
                        selectedProduct.setPrice (newPrice);
                        selectedProduct.setStock (newStock);
                    } else {
                        System.out.println ("Product id tidak ditemukan. Silahkan kembali ke menu utama");
                    }


                } else if (layoutMenu.equalsIgnoreCase ("adminView")) {
                    System.out.println ("Daftar Semua Produk:");

                    for (Product product : products) {
                        System.out.println ("Id : " + product.getId () +
                                " Barang : " + product.getProductName () +
                                " Harga : " + product.getPrice () +
                                " Stok : " + product.getStock ());

                    }


                } else if (layoutMenu.equalsIgnoreCase ("cart")) {
                    while (true) {
                        carts.forEach (System.out::println);

                        System.out.println ("Halo customer\n" +
                                "add / remove / update /buy / exit ");

                        String customerAction = input.nextLine ();

                        if (customerAction.equalsIgnoreCase ("exit")) {
                            break;
                        } else if (customerAction.equalsIgnoreCase ("add")) {
                            System.out.println("Masukkan ID produk yang ingin ditambahkan ke keranjang: ");
                            String targetProductId = input.nextLine();

                            // Cek apakah produk dengan ID yang sama sudah ada dalam keranjang
                            boolean productAlreadyInCart = carts.stream()
                                    .anyMatch(cartItem -> cartItem.getId().equalsIgnoreCase(targetProductId));

                            if (productAlreadyInCart) {
                                System.out.println("Produk dengan ID " + targetProductId + " sudah ada dalam keranjang.");
                            } else {
                                // Implementasi stream findBy id
                                Product selectedProduct = products.stream()
                                        .filter(product -> product.getId().equalsIgnoreCase(targetProductId))
                                        .findAny()
                                        .orElse(null);

                                if (selectedProduct != null) {
                                    System.out.println("Masukkan jumlah produk yang ingin ditambahkan ke keranjang: ");
                                    int quantity = input.nextInt();
                                    input.nextLine(); // Membersihkan newline dari buffer input

                                    Cart cartItem = new Cart();
                                    cartItem.setId(selectedProduct.getId());
                                    cartItem.setProductName(selectedProduct.getProductName());
                                    cartItem.setPrice(selectedProduct.getPrice());
                                    cartItem.setQuantity(quantity);
                                    cartItem.setSumOfProduct(cartItem.sumOfProducts());

                                    carts.add(cartItem);
                                    System.out.println("Produk telah ditambahkan ke keranjang.");
                                } else {
                                    System.out.println("Produk dengan ID " + targetProductId + " tidak ditemukan.");
                                }
                            }


                        } else if (customerAction.equalsIgnoreCase ("remove")) {
                            System.out.println ("Masukan ID Produk yang akan dihapus: ");
                            String targetId = input.nextLine ();

                            for (int i = 0; i < carts.size (); i++) {
                                if (carts.get (i).getId ().equals (targetId)) {
                                    carts.remove (i);
                                    System.out.println ("Produk di keranjang dengan ID : " + targetId + " berhasil dihapus");
                                }

                            }
                        } else if (customerAction.equalsIgnoreCase ("update")) {
                            System.out.println ("Masukkan kuantitas ID yang akan di update : ");
                            String updatetargetId = input.nextLine ();

                            Cart selectedProduct = carts.stream ()
                                    .filter (cart -> cart.getId ().equalsIgnoreCase (updatetargetId))
                                    .findAny ()
                                    .orElse (null);

                            if (selectedProduct != null) {
                                System.out.println ("Masukkan jumlah produk yang baru : ");
                                int newQuantity = Integer.parseInt (input.nextLine ());

                                selectedProduct.setQuantity (newQuantity);
                                selectedProduct.setSumOfProduct (selectedProduct.sumOfProducts ());
                                System.out.println ("Jumlah produk dalam keranjang berhasil di perbarui");
                            } else {
                                System.out.println ("Produk dengan ID " + updatetargetId + " tidak di temukan dalam keranjang");
                            }


                        } else if (customerAction.equalsIgnoreCase ("buy")) {
                            System.out.println ("List Cart:");
                            double total = 0;
                            for (Cart cart : carts) {
                                System.out.println ("Cart Id: " + cart.getId () +
                                        ", Nama Produk: " + cart.getProductName () +
                                        ", Harga: " + cart.getPrice () +
                                        ", Kuantitas: " + cart.getQuantity () +
                                        ", Jumlah Produk: " + cart.getSumOfProduct ());
                                total += cart.getSumOfProduct ();
                            }
                            System.out.println ("Total Pembelian: " + total);

                            System.out.println ("Apakah Anda ingin melanjutkan pembelian? (yes/no): ");
                            String isBuy = input.nextLine ();
                            if (isBuy.equalsIgnoreCase ("yes")) {
                                boolean isStockOk = true;


                                for (Cart cart : carts) {
                                    Product selectedCartItem = products.stream ()
                                            .filter (product -> product.getId ().equalsIgnoreCase (cart.getId ()))
                                            .findFirst ()
                                            .orElse (null);

                                    if (selectedCartItem != null) {
                                        int requestedQuantity = cart.getQuantity ();
                                        int availableStock = selectedCartItem.getStock ();

                                        if (requestedQuantity > availableStock) {
                                            isStockOk = false; // Stok tidak mencukupi
                                            System.out.println ("Stok tidak cukup untuk produk "+selectedCartItem.getId ()+ ", "
                                                    + selectedCartItem.getProductName () + "." +
                                                    "Sisa Stock " + selectedCartItem.getStock ());
                                            break; // Berhenti jika ada stok yang tidak mencukupi
                                        }
                                    }
                                }

                                if (isStockOk) {
                                    // Eksekusi jika stock mencukupi
                                    for (Cart cart : carts) {
                                        Product selectedProduct = products.stream ()
                                                .filter (product -> product.getId ().equalsIgnoreCase (cart.getId ()))
                                                .findFirst ()
                                                .orElse (null);

                                        if (selectedProduct != null) {
                                            int requestedQuantity = cart.getQuantity ();
                                            int availableStock = selectedProduct.getStock ();
                                            // mengurangi stok
                                            selectedProduct.setStock (availableStock - requestedQuantity);
                                        }
                                    }

                                    // hapus keranjang karena berhasil dibeli
                                    carts.clear ();
                                    System.out.println ("Pembelian berhasil dilakukan.");
                                } else {
                                    System.out.println ("Pembelian dibatalkan.");
                                }
                            } else {
                                System.out.println ("Pembelian dibatalkan.");
                            }

                        } else {
                            System.out.println ("Invalid");
                        }
                        input.nextLine ();
                    }

                } else {
                    System.out.println ("INVALID");
                }
                input.nextLine ();

            } catch (Exception e) {
                System.out.println ("Maaf aksi yang anda masukan salah");
                input.nextLine ();
            }
        }
    }
}