package id.b2camp.kelompok.day13.kelompokZ;

import java.util.*;

class MenuMakananManager {
  //  private Set<String> existingMenuNames = new HashSet<>();
    private Map<String, Double> hargaAwal = new HashMap<>();
    private List<MenuMakananService> menuList = new ArrayList<>();
    public List<MenuMakananService> getMenuList() {
        return menuList;

    }

    public void setMenuList(List<MenuMakananService> menuList) {
        this.menuList = menuList;
    }
        //TODO SEDIKIT ADA PENAMBAHAN
        public void addMenu(String name, double price, int quantity) {
            MenuMakananService newMenu = new MenuMakananService(name, price, quantity);

            if (hargaAwal.containsKey(name)) {
                double initialPrice = hargaAwal.get(name);
                double updatedPrice = initialPrice * (newMenu.getQuantity() + quantity);
                newMenu.setPrice(updatedPrice);
            } else {
                hargaAwal.put(name, price);
            }

            menuList.add(newMenu);
        }
//// TODO ada sedikit   Perubahan udah di ubah
    public void menuUpdate(int index, String updateName, double updatePrice, int quantity, double basePrice) {
        MenuMakananService menuUpdate = menuList.get(index);
        double initialPrice = menuUpdate.getPrice() / menuUpdate.getQuantity();
        menuUpdate.setName(updateName);
        menuUpdate.setPrice(updatePrice);
        menuUpdate.setQuantity(quantity);
        if (initialPrice != updatePrice) {
            double totalPrice = MenuMakananManager.calculateTotalPrice(updatePrice, quantity);
            menuUpdate.setPrice(totalPrice);
            menuUpdate.setQuantity(quantity);
        }
    }

    // /perubahan pada menu manager
    public boolean menuDelete(int menuIndexDelete) {
        if (menuIndexDelete >= 0 && menuIndexDelete < menuList.size()) {
            MenuMakananService deleteMenu = menuList.get(menuIndexDelete);
            menuList.remove(menuIndexDelete);
            System.out.println("Menu \"" + deleteMenu.getName() + "\" sudah dihapus");
            System.out.println(" Maaf," + deleteMenu.getName().toUpperCase() + " sim kuring jangar");
            return false;
        } else {
            System.out.println("Menu dengan Index tersebut tidak ditemukan.");
            return false;
        }
    }
    static double calculateTotalPrice(double basePrice, int quantity) {
        double totalPrice = basePrice * quantity;
        return totalPrice;
    }
}
