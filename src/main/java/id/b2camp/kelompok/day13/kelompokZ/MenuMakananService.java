package id.b2camp.kelompok.day13.kelompokZ;
class MenuMakananService {
    private String name;
    private double price;
    private int quantity;

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {

        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity( int quantity) {
        return quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public MenuMakananService(String name, Double price , Integer quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }//
    public String getName() {
        return name;
    }
    public double getBasePrice() {
        return 0;
    }
    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {

        return name + " - " + price + "="+ quantity;
    }


}