package id.b2camp.kelompok.day13.kelompokZ;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
public class CommandAppMakanan {
    public static void main(String[] args) {
        MenuMakananManager menuManager = new MenuMakananManager();
        Scanner scanner = new Scanner(System.in);
//

        while (true) {
            System.out.println("+++++Menu Yang Tersedia+++++");
            System.out.println("1. Tambah Makanan");
            System.out.println("2. Hapus Makanan");
            System.out.println("3. Update Makanan");
            System.out.println("4. Tampilkan Menu Makanan");
            System.out.println("5. Keluar Dari App");
            System.out.println("+++++Version 1.1++++++++");
            System.out.print("Pilih Menu operasi: ");
//// TODO ADA PERUBAHAN KODE PADA SAAT PENAMBAHAN MAKANAN
            try {
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    while (true) {
                        System.out.print("Masukkan nama Makanan (atau 0 untuk membatalkan): ");
                        String input = scanner.nextLine();

                        if (input.equals("0")) {
                            System.out.println("Penambahan makanan dibatalkan.");
                            break;
                        }

                        String name = input;

                        System.out.print("Masukkan harga dasar Makanan: ");
                        double basePrice = scanner.nextDouble();
                        System.out.print("Masukkan jumlah Makanan: ");
                        int quantity = scanner.nextInt();
                        scanner.nextLine();

                        boolean found = false;
                        for (MenuMakananService menu : menuManager.getMenuList()) {
                            if (menu.getName().equalsIgnoreCase(name)) {
                                found = true;
                                System.out.println("Maaf, makanan dengan nama yang sama sudah ada. Silakan pilih nama yang lain atau masukkan 0 untuk membatalkan.");
                                break;
                            }
                        }
//
                        if (!found) {
                            try {
                                double totalPrice = MenuMakananManager.calculateTotalPrice(basePrice, quantity);
                                menuManager.addMenu(name, totalPrice, quantity);
                                System.out.println("Makanan berhasil ditambahkan.");
                                break;
                            } catch (Exception e) {
                                System.out.println("Terjadi kesalahan saat menambahkan makanan. Silakan coba lagi.");
                            }
                        }
                    }
                    break;

                // TODO Davit
                case 2:
                    // Menampilkan daftar menu
                    System.out.println("Daftar Menu:");
                    for (int i = 0; i < menuManager.getMenuList().size(); i++) {
                        MenuMakananService menu = menuManager.getMenuList().get(i);
                        System.out.println((i + 1) + ". " + menu.getName() +
                            " - Harga: " + menu.getPrice() +
                            ", Quantity: " + menu.getQuantity());
                    }
///TODO ADA SDIKIT PENAMBAH KODE PEMBANTALAN
                    System.out.print("Masukkan nomor Makanan yang ingin dihapus (atau masukkan 0 untuk membatalkan): ");
                    int menuIndexDelete = scanner.nextInt();
                    scanner.nextLine();
                    if (menuIndexDelete == 0) {
                        System.out.println("Penghapusan dibatalkan.");
                    } else if (menuIndexDelete >= 1 && menuIndexDelete <= menuManager.getMenuList().size()) {
                        menuManager.menuDelete(menuIndexDelete - 1);
                        System.out.println("Makanan berhasil dihapus.");
                    } else {
                        System.out.println("Makanan dengan nomor tersebut tidak ditemukan.");
                    }
                    break;

                case 3: {
                    // TODO: CASE BUAT TEH GITA (OKE DONE YAAA..)
                    System.out.println("Daftar Makanan:");
                    for (int i = 0; i < menuManager.getMenuList().size(); i++) {
                        MenuMakananService menu = menuManager.getMenuList().get(i);
                        System.out.println((i + 1) + ". " + menu.getName() +
                            " - Harga: " + menu.getPrice() +
                            ", Quantity: " + menu.getQuantity());
                    }
                    System.out.print("Masukkan nomor Makanan yang ingin diupdate (atau 0 untuk membatalkan): ");
                    int menuIndexUpdate = scanner.nextInt();
                    scanner.nextLine();

                    if (menuIndexUpdate == 0) {
                        System.out.println("Operasi diupdate dibatalkan.");
                    } else if (menuIndexUpdate >= 1 && menuIndexUpdate <= menuManager.getMenuList().size()) {
                        MenuMakananService menuToUpdate = menuManager.getMenuList().get(menuIndexUpdate - 1);
                        double basePrice = menuToUpdate.getBasePrice();

                        System.out.println("Makanan yang akan diupdate:");
                        System.out.println("Nama: " + menuToUpdate.getName());
                        System.out.println("Harga: " + menuToUpdate.getPrice());
                        System.out.println("Quantity: " + menuToUpdate.getQuantity());

                        System.out.print("Masukkan nama Makanan baru (kosongkan jika tidak ingin mengubah): ");
                        String newName = scanner.nextLine();

                        // TODO  ADA SEDIKIT PENAMBAHAN CODE UNTUK MENCEK DALAM MENULIST MAKAN ADA ATAU TIDAK ADA
                        /// TODO KALAU ADA MAKAN ENGGAK BISA DI UPDATE DENGAN NAMA YANG SAMA ==;
                        boolean nameExists = menuManager.getMenuList().stream()
                            .anyMatch(menu -> menu.getName().equalsIgnoreCase(newName));

                        if (nameExists) {
                            System.out.println("Maaf, nama makanan tersebut sudah ada dalam daftar. Silakan pilih nama yang berbeda.");
                        } else {
                            System.out.print("Masukkan harga Makanan baru (kosongkan jika tidak ingin mengubah): ");
                            String newPriceStr = scanner.nextLine();
                            System.out.print("Masukkan jumlah Makanan baru (kosongkan jika tidak ingin mengubah): ");
                            String newQuantityStr = scanner.nextLine();

                            if (newName.equals("0") || newPriceStr.equals("0") || newQuantityStr.equals("0")) {
                                System.out.println("Operasi diupdate dibatalkan.");
                            } else {
                                double newPrice = newPriceStr.isEmpty() ? menuToUpdate.getPrice() : Double.parseDouble(newPriceStr);
                                int newQuantity = newQuantityStr.isEmpty() ? menuToUpdate.getQuantity() : Integer.parseInt(newQuantityStr);
                                menuManager.menuUpdate(menuIndexUpdate - 1, newName.isEmpty() ? menuToUpdate.getName() : newName, newPrice, newQuantity, basePrice);
                                System.out.println("Menu berhasil diupdate!");
                            }
                        }
                    } else {
                        System.out.println("Menu dengan nomor tersebut tidak ditemukan.");
                    }
                    break;
                }


                case 4: {
                    // TODO: BUAT KANG JERRY (oke done yaaa..)
                    List<MenuMakananService> sortedMenuList = new ArrayList<>(menuManager.getMenuList());

                    // Sort the menu list based on initial price (harga awal)
                    sortedMenuList.sort(Comparator.comparingDouble(menu -> menu.getPrice() / menu.getQuantity()));

                    System.out.println("Daftar Menu (diurutkan berdasarkan Harga Dasar):");
                    for (int i = 0; i < sortedMenuList.size(); i++) {
                        MenuMakananService menu = sortedMenuList.get(i);
                        double initialPrice = (menu.getPrice() / menu.getQuantity());
                        System.out.println((i + 1) + ". " + menu.getName() +
                            " - Harga Dasar: " + initialPrice +
                            ", Harga: " + menu.getPrice() +
                            ", Quantity: " + menu.getQuantity());
                    }
                    break;

            }
                case 5 : {
                    System.out.println("Program Berhenti.");
                    System.exit(0);
                }
                default:
                    System.out.println("Pilihan tidak ada dalam menu");
            }
            } catch (Exception e) {
                System.out.println(" Maaf(: , Mohon masukkan angka!!!!");
                scanner.nextLine();
            }
        }
    }
}

