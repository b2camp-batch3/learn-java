# B2CampID Batch 3

Project ini sebagai codebase bootcamp materi java basic, oop, functional programming dan declarative programming.

## Panduan git branching dan commit message

* Our code is use trunk based development git branching strategy (using short-lived branch to update the master branch), 
  Please use master as source branch to your each of your short-lived branch

    ```
    <master>-------new branch---------------merged----> latest update
                         \                     /
                  <student/task>----commit----MR
    ```

* Examples of our short-lived branch name

    ```
    fakhri/task-day7
    argan/task-day8
    muslich/task-day9
    ```
  
* Please use the following conventional commit message

    ```
    <type>(optional-scope): <description> #optional-jira-ticket
  
    optional '!' after <type> or (scope) to draw attention on breaking changes
    ```

* Examples of commit message

    ```
    feat: improve performance with lazy load implementation for images #10012
    fix: prevent racing of requests
    fix(entity): fixed validation not blank for user #20024
    chore: update npm dependency to latest version #30036
    refactor(api)!: rename api group path for notification 
    docs: update docs for setup project guide
    style: remove unused import on all codebase
    perf(repository)!: remove n+1 query on get user
    ci: add gitlab ci configuration
    build(dependencies): update docker build to expose resources
    revert: let us never again speak of the noodle incident
    ```
  
* Conventional commit type
  * feat - a new feature is introduced with the changes 
  * fix – a bug fix has occurred 
  * chore – changes that do not relate to a fix or feature and don't modify src or test files (for example updating dependencies)
  * refactor – refactored code that neither fixes a bug nor adds a feature
  * docs – updates to documentation such as a the README or other markdown files
  * style – changes that do not affect the meaning of the code, likely related to code formatting such as white-space, missing semi-colons, and so on. 
  * test – including new or correcting previous tests
  * perf – performance improvements
  * ci – continuous integration related 
  * build – changes that affect the build system or external dependencies
  * revert – reverts a previous commit



